package com.ract.purelink.webservice;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Java class for HomeQuoteDetails complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HomeQuoteDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="channelCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="riskType" type="{http://www.ract.com.au/RactPureLink}RiskTypeCode"/>
 *         &lt;element name="ratingDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="buildingOnly" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="contentsOnly" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="buildingAndContents" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="insuredAddress" type="{http://www.ract.com.au/RactPureLink}Address"/>
 *         &lt;element name="occupancyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="yearConstructed" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="buildingLayoutCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="wallConstructionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="roofConstructionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="businessUsageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="securityAlarmTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="deadLocksWindowLocksCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="buildingWorksCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="goodOrderAndRepair" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="hardwiredSmokeAlarm" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="oldestResidentDob" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="mortgagee" type="{http://www.ract.com.au/RactPureLink}MortgageeList"/>
 *         &lt;element name="totalBuildingSumInsured" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="buildingIndexed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="personalEffectsRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="specifiedPersonalEffectsSumInsured" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="specifiedPersonalEffects" type="{http://www.ract.com.au/RactPureLink}SpecifiedItemList"/>
 *         &lt;element name="unspecifiedPersonalEffectsCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="totalContentsSumInsured" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="contentsIndexed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="specifiedContentsSumInsured" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="unspecifiedContentsSumInsured" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="specifiedContents" type="{http://www.ract.com.au/RactPureLink}SpecifiedItemList"/>
 *         &lt;element name="paymentFrequency" type="{http://www.ract.com.au/RactPureLink}PaymentFrequency"/>
 *         &lt;element name="subRiskType" type="{http://www.ract.com.au/RactPureLink}SubRiskTypeCode"/>
 *         &lt;element name="accidentalDamageBuilding" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="fusionBuilding" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="stormGatesAndFenceBuilding" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="domesticWorkersCompBuilding" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="accidentalDamageContents" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="fusionContents" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="domesticWorkersCompContents" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="fusionInvestorBuilding" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="fusionInvestorContents" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="stormGatesAndFenceInvestorBuilding" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="pipDiscount" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="staffIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="staffDiscountCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="discretionaryDiscountCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="silverSaverDiscount" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="campaignDiscountCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="buildingExcessCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="investorBuildingExcessCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contentsExcessCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="investorContentsExcessCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="baseAnnualPremium" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="gst" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="stampDuty" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="totalAnnualPremium" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="installmentAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="claimHistory" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="liabilityClaimHistory" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="claimRefused" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="insuranceRefused" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="criminalOffences" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="criminalOffenceDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HomeQuoteDetails", propOrder = { "channelCode", "riskType", "ratingDate", "buildingOnly", "contentsOnly", "buildingAndContents", "insuredAddress", "occupancyCode", "yearConstructed", "buildingLayoutCode", "wallConstructionCode", "roofConstructionCode", "businessUsageCode", "securityAlarmTypeCode", "deadLocksWindowLocksCode", "buildingWorksCode", "goodOrderAndRepair", "hardwiredSmokeAlarm", "oldestResidentDob", "mortgagee", "totalBuildingSumInsured", "buildingIndexed", "personalEffectsRequired", "specifiedPersonalEffectsSumInsured", "specifiedPersonalEffects", "unspecifiedPersonalEffectsCode", "totalContentsSumInsured", "contentsIndexed", "specifiedContentsSumInsured", "unspecifiedContentsSumInsured", "specifiedContents", "paymentFrequency", "subRiskType", "accidentalDamageBuilding", "fusionBuilding", "stormGatesAndFenceBuilding", "domesticWorkersCompBuilding", "accidentalDamageContents", "fusionContents", "domesticWorkersCompContents", "fusionInvestorBuilding", "fusionInvestorContents", "stormGatesAndFenceInvestorBuilding", "pipDiscount", "staffIndicator", "staffDiscountCode", "discretionaryDiscountCode", "silverSaverDiscount", "campaignDiscountCode", "buildingExcessCode", "investorBuildingExcessCode", "contentsExcessCode", "investorContentsExcessCode", "baseAnnualPremium", "gst", "stampDuty", "totalAnnualPremium", "installmentAmount", "claimHistory", "liabilityClaimHistory", "claimRefused", "insuranceRefused", "criminalOffences", "criminalOffenceDetails" })
public class HomeQuoteDetails {

    @XmlElement(required = true, nillable = true)
    protected String channelCode;
    @XmlElement(required = true)
    protected RiskTypeCode riskType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ratingDate;
    protected boolean buildingOnly;
    protected boolean contentsOnly;
    protected boolean buildingAndContents;
    @XmlElement(required = true, nillable = true)
    protected Address insuredAddress;
    @XmlElement(required = true, nillable = true)
    protected String occupancyCode;
    protected int yearConstructed;
    @XmlElement(required = true, nillable = true)
    protected String buildingLayoutCode;
    @XmlElement(required = true, nillable = true)
    protected String wallConstructionCode;
    @XmlElement(required = true, nillable = true)
    protected String roofConstructionCode;
    @XmlElement(required = true, nillable = true)
    protected String businessUsageCode;
    @XmlElement(required = true, nillable = true)
    protected String securityAlarmTypeCode;
    @XmlElement(required = true, nillable = true)
    protected String deadLocksWindowLocksCode;
    @XmlElement(required = true, nillable = true)
    protected String buildingWorksCode;
    protected boolean goodOrderAndRepair;
    protected boolean hardwiredSmokeAlarm;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar oldestResidentDob;
    @XmlElement(required = true, nillable = true)
    protected MortgageeList mortgagee;
    protected int totalBuildingSumInsured;
    protected boolean buildingIndexed;
    protected boolean personalEffectsRequired;
    protected int specifiedPersonalEffectsSumInsured;
    @XmlElement(required = true, nillable = true)
    protected SpecifiedItemList specifiedPersonalEffects;
    @XmlElement(required = true, nillable = true)
    protected String unspecifiedPersonalEffectsCode;
    protected int totalContentsSumInsured;
    protected boolean contentsIndexed;
    protected int specifiedContentsSumInsured;
    protected int unspecifiedContentsSumInsured;
    @XmlElement(required = true, nillable = true)
    protected SpecifiedItemList specifiedContents;
    @XmlElement(required = true)
    protected PaymentFrequency paymentFrequency;
    @XmlElement(required = true)
    protected SubRiskTypeCode subRiskType;
    protected boolean accidentalDamageBuilding;
    protected boolean fusionBuilding;
    protected boolean stormGatesAndFenceBuilding;
    protected boolean domesticWorkersCompBuilding;
    protected boolean accidentalDamageContents;
    protected boolean fusionContents;
    protected boolean domesticWorkersCompContents;
    protected boolean fusionInvestorBuilding;
    protected boolean fusionInvestorContents;
    protected boolean stormGatesAndFenceInvestorBuilding;
    protected boolean pipDiscount;
    protected boolean staffIndicator;
    @XmlElement(required = true, nillable = true)
    protected String staffDiscountCode;
    @XmlElement(required = true, nillable = true)
    protected String discretionaryDiscountCode;
    protected boolean silverSaverDiscount;
    @XmlElement(required = true, nillable = true)
    protected String campaignDiscountCode;
    @XmlElement(required = true, nillable = true)
    protected String buildingExcessCode;
    @XmlElement(required = true, nillable = true)
    protected String investorBuildingExcessCode;
    @XmlElement(required = true, nillable = true)
    protected String contentsExcessCode;
    @XmlElement(required = true, nillable = true)
    protected String investorContentsExcessCode;
    @XmlElement(required = true)
    protected BigDecimal baseAnnualPremium;
    @XmlElement(required = true)
    protected BigDecimal gst;
    @XmlElement(required = true)
    protected BigDecimal stampDuty;
    @XmlElement(required = true)
    protected BigDecimal totalAnnualPremium;
    @XmlElement(required = true)
    protected BigDecimal installmentAmount;
    protected int claimHistory;
    protected int liabilityClaimHistory;
    protected boolean claimRefused;
    protected boolean insuranceRefused;
    protected boolean criminalOffences;
    @XmlElementRef(name = "criminalOffenceDetails", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<String> criminalOffenceDetails;

    /**
     * Gets the value of the channelCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getChannelCode() {
        return channelCode;
    }

    /**
     * Sets the value of the channelCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setChannelCode(String value) {
        this.channelCode = value;
    }

    /**
     * Gets the value of the riskType property.
     * 
     * @return possible object is {@link RiskTypeCode }
     * 
     */
    public RiskTypeCode getRiskType() {
        return riskType;
    }

    /**
     * Sets the value of the riskType property.
     * 
     * @param value
     *            allowed object is {@link RiskTypeCode }
     * 
     */
    public void setRiskType(RiskTypeCode value) {
        this.riskType = value;
    }

    /**
     * Gets the value of the ratingDate property.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getRatingDate() {
        return ratingDate;
    }

    /**
     * Sets the value of the ratingDate property.
     * 
     * @param value
     *            allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setRatingDate(XMLGregorianCalendar value) {
        this.ratingDate = value;
    }

    /**
     * Gets the value of the buildingOnly property.
     * 
     */
    public boolean isBuildingOnly() {
        return buildingOnly;
    }

    /**
     * Sets the value of the buildingOnly property.
     * 
     */
    public void setBuildingOnly(boolean value) {
        this.buildingOnly = value;
    }

    /**
     * Gets the value of the contentsOnly property.
     * 
     */
    public boolean isContentsOnly() {
        return contentsOnly;
    }

    /**
     * Sets the value of the contentsOnly property.
     * 
     */
    public void setContentsOnly(boolean value) {
        this.contentsOnly = value;
    }

    /**
     * Gets the value of the buildingAndContents property.
     * 
     */
    public boolean isBuildingAndContents() {
        return buildingAndContents;
    }

    /**
     * Sets the value of the buildingAndContents property.
     * 
     */
    public void setBuildingAndContents(boolean value) {
        this.buildingAndContents = value;
    }

    /**
     * Gets the value of the insuredAddress property.
     * 
     * @return possible object is {@link Address }
     * 
     */
    public Address getInsuredAddress() {
        return insuredAddress;
    }

    /**
     * Sets the value of the insuredAddress property.
     * 
     * @param value
     *            allowed object is {@link Address }
     * 
     */
    public void setInsuredAddress(Address value) {
        this.insuredAddress = value;
    }

    /**
     * Gets the value of the occupancyCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getOccupancyCode() {
        return occupancyCode;
    }

    /**
     * Sets the value of the occupancyCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setOccupancyCode(String value) {
        this.occupancyCode = value;
    }

    /**
     * Gets the value of the yearConstructed property.
     * 
     */
    public int getYearConstructed() {
        return yearConstructed;
    }

    /**
     * Sets the value of the yearConstructed property.
     * 
     */
    public void setYearConstructed(int value) {
        this.yearConstructed = value;
    }

    /**
     * Gets the value of the buildingLayoutCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getBuildingLayoutCode() {
        return buildingLayoutCode;
    }

    /**
     * Sets the value of the buildingLayoutCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setBuildingLayoutCode(String value) {
        this.buildingLayoutCode = value;
    }

    /**
     * Gets the value of the wallConstructionCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getWallConstructionCode() {
        return wallConstructionCode;
    }

    /**
     * Sets the value of the wallConstructionCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setWallConstructionCode(String value) {
        this.wallConstructionCode = value;
    }

    /**
     * Gets the value of the roofConstructionCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getRoofConstructionCode() {
        return roofConstructionCode;
    }

    /**
     * Sets the value of the roofConstructionCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setRoofConstructionCode(String value) {
        this.roofConstructionCode = value;
    }

    /**
     * Gets the value of the businessUsageCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getBusinessUsageCode() {
        return businessUsageCode;
    }

    /**
     * Sets the value of the businessUsageCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setBusinessUsageCode(String value) {
        this.businessUsageCode = value;
    }

    /**
     * Gets the value of the securityAlarmTypeCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getSecurityAlarmTypeCode() {
        return securityAlarmTypeCode;
    }

    /**
     * Sets the value of the securityAlarmTypeCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setSecurityAlarmTypeCode(String value) {
        this.securityAlarmTypeCode = value;
    }

    /**
     * Gets the value of the deadLocksWindowLocksCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getDeadLocksWindowLocksCode() {
        return deadLocksWindowLocksCode;
    }

    /**
     * Sets the value of the deadLocksWindowLocksCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setDeadLocksWindowLocksCode(String value) {
        this.deadLocksWindowLocksCode = value;
    }

    /**
     * Gets the value of the buildingWorksCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getBuildingWorksCode() {
        return buildingWorksCode;
    }

    /**
     * Sets the value of the buildingWorksCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setBuildingWorksCode(String value) {
        this.buildingWorksCode = value;
    }

    /**
     * Gets the value of the goodOrderAndRepair property.
     * 
     */
    public boolean isGoodOrderAndRepair() {
        return goodOrderAndRepair;
    }

    /**
     * Sets the value of the goodOrderAndRepair property.
     * 
     */
    public void setGoodOrderAndRepair(boolean value) {
        this.goodOrderAndRepair = value;
    }

    /**
     * Gets the value of the hardwiredSmokeAlarm property.
     * 
     */
    public boolean isHardwiredSmokeAlarm() {
        return hardwiredSmokeAlarm;
    }

    /**
     * Sets the value of the hardwiredSmokeAlarm property.
     * 
     */
    public void setHardwiredSmokeAlarm(boolean value) {
        this.hardwiredSmokeAlarm = value;
    }

    /**
     * Gets the value of the oldestResidentDob property.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getOldestResidentDob() {
        return oldestResidentDob;
    }

    /**
     * Sets the value of the oldestResidentDob property.
     * 
     * @param value
     *            allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setOldestResidentDob(XMLGregorianCalendar value) {
        this.oldestResidentDob = value;
    }

    /**
     * Gets the value of the mortgagee property.
     * 
     * @return possible object is {@link MortgageeList }
     * 
     */
    public MortgageeList getMortgagee() {
        return mortgagee;
    }

    /**
     * Sets the value of the mortgagee property.
     * 
     * @param value
     *            allowed object is {@link MortgageeList }
     * 
     */
    public void setMortgagee(MortgageeList value) {
        this.mortgagee = value;
    }

    /**
     * Gets the value of the totalBuildingSumInsured property.
     * 
     */
    public int getTotalBuildingSumInsured() {
        return totalBuildingSumInsured;
    }

    /**
     * Sets the value of the totalBuildingSumInsured property.
     * 
     */
    public void setTotalBuildingSumInsured(int value) {
        this.totalBuildingSumInsured = value;
    }

    /**
     * Gets the value of the buildingIndexed property.
     * 
     */
    public boolean isBuildingIndexed() {
        return buildingIndexed;
    }

    /**
     * Sets the value of the buildingIndexed property.
     * 
     */
    public void setBuildingIndexed(boolean value) {
        this.buildingIndexed = value;
    }

    /**
     * Gets the value of the personalEffectsRequired property.
     * 
     */
    public boolean isPersonalEffectsRequired() {
        return personalEffectsRequired;
    }

    /**
     * Sets the value of the personalEffectsRequired property.
     * 
     */
    public void setPersonalEffectsRequired(boolean value) {
        this.personalEffectsRequired = value;
    }

    /**
     * Gets the value of the specifiedPersonalEffectsSumInsured property.
     * 
     */
    public int getSpecifiedPersonalEffectsSumInsured() {
        return specifiedPersonalEffectsSumInsured;
    }

    /**
     * Sets the value of the specifiedPersonalEffectsSumInsured property.
     * 
     */
    public void setSpecifiedPersonalEffectsSumInsured(int value) {
        this.specifiedPersonalEffectsSumInsured = value;
    }

    /**
     * Gets the value of the specifiedPersonalEffects property.
     * 
     * @return possible object is {@link SpecifiedItemList }
     * 
     */
    public SpecifiedItemList getSpecifiedPersonalEffects() {
        return specifiedPersonalEffects;
    }

    /**
     * Sets the value of the specifiedPersonalEffects property.
     * 
     * @param value
     *            allowed object is {@link SpecifiedItemList }
     * 
     */
    public void setSpecifiedPersonalEffects(SpecifiedItemList value) {
        this.specifiedPersonalEffects = value;
    }

    /**
     * Gets the value of the unspecifiedPersonalEffectsCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getUnspecifiedPersonalEffectsCode() {
        return unspecifiedPersonalEffectsCode;
    }

    /**
     * Sets the value of the unspecifiedPersonalEffectsCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setUnspecifiedPersonalEffectsCode(String value) {
        this.unspecifiedPersonalEffectsCode = value;
    }

    /**
     * Gets the value of the totalContentsSumInsured property.
     * 
     */
    public int getTotalContentsSumInsured() {
        return totalContentsSumInsured;
    }

    /**
     * Sets the value of the totalContentsSumInsured property.
     * 
     */
    public void setTotalContentsSumInsured(int value) {
        this.totalContentsSumInsured = value;
    }

    /**
     * Gets the value of the contentsIndexed property.
     * 
     */
    public boolean isContentsIndexed() {
        return contentsIndexed;
    }

    /**
     * Sets the value of the contentsIndexed property.
     * 
     */
    public void setContentsIndexed(boolean value) {
        this.contentsIndexed = value;
    }

    /**
     * Gets the value of the specifiedContentsSumInsured property.
     * 
     */
    public int getSpecifiedContentsSumInsured() {
        return specifiedContentsSumInsured;
    }

    /**
     * Sets the value of the specifiedContentsSumInsured property.
     * 
     */
    public void setSpecifiedContentsSumInsured(int value) {
        this.specifiedContentsSumInsured = value;
    }

    /**
     * Gets the value of the unspecifiedContentsSumInsured property.
     * 
     */
    public int getUnspecifiedContentsSumInsured() {
        return unspecifiedContentsSumInsured;
    }

    /**
     * Sets the value of the unspecifiedContentsSumInsured property.
     * 
     */
    public void setUnspecifiedContentsSumInsured(int value) {
        this.unspecifiedContentsSumInsured = value;
    }

    /**
     * Gets the value of the specifiedContents property.
     * 
     * @return possible object is {@link SpecifiedItemList }
     * 
     */
    public SpecifiedItemList getSpecifiedContents() {
        return specifiedContents;
    }

    /**
     * Sets the value of the specifiedContents property.
     * 
     * @param value
     *            allowed object is {@link SpecifiedItemList }
     * 
     */
    public void setSpecifiedContents(SpecifiedItemList value) {
        this.specifiedContents = value;
    }

    /**
     * Gets the value of the paymentFrequency property.
     * 
     * @return possible object is {@link PaymentFrequency }
     * 
     */
    public PaymentFrequency getPaymentFrequency() {
        return paymentFrequency;
    }

    /**
     * Sets the value of the paymentFrequency property.
     * 
     * @param value
     *            allowed object is {@link PaymentFrequency }
     * 
     */
    public void setPaymentFrequency(PaymentFrequency value) {
        this.paymentFrequency = value;
    }

    /**
     * Gets the value of the subRiskType property.
     * 
     * @return possible object is {@link SubRiskTypeCode }
     * 
     */
    public SubRiskTypeCode getSubRiskType() {
        return subRiskType;
    }

    /**
     * Sets the value of the subRiskType property.
     * 
     * @param value
     *            allowed object is {@link SubRiskTypeCode }
     * 
     */
    public void setSubRiskType(SubRiskTypeCode value) {
        this.subRiskType = value;
    }

    /**
     * Gets the value of the accidentalDamageBuilding property.
     * 
     */
    public boolean isAccidentalDamageBuilding() {
        return accidentalDamageBuilding;
    }

    /**
     * Sets the value of the accidentalDamageBuilding property.
     * 
     */
    public void setAccidentalDamageBuilding(boolean value) {
        this.accidentalDamageBuilding = value;
    }

    /**
     * Gets the value of the fusionBuilding property.
     * 
     */
    public boolean isFusionBuilding() {
        return fusionBuilding;
    }

    /**
     * Sets the value of the fusionBuilding property.
     * 
     */
    public void setFusionBuilding(boolean value) {
        this.fusionBuilding = value;
    }

    /**
     * Gets the value of the stormGatesAndFenceBuilding property.
     * 
     */
    public boolean isStormGatesAndFenceBuilding() {
        return stormGatesAndFenceBuilding;
    }

    /**
     * Sets the value of the stormGatesAndFenceBuilding property.
     * 
     */
    public void setStormGatesAndFenceBuilding(boolean value) {
        this.stormGatesAndFenceBuilding = value;
    }

    /**
     * Gets the value of the domesticWorkersCompBuilding property.
     * 
     */
    public boolean isDomesticWorkersCompBuilding() {
        return domesticWorkersCompBuilding;
    }

    /**
     * Sets the value of the domesticWorkersCompBuilding property.
     * 
     */
    public void setDomesticWorkersCompBuilding(boolean value) {
        this.domesticWorkersCompBuilding = value;
    }

    /**
     * Gets the value of the accidentalDamageContents property.
     * 
     */
    public boolean isAccidentalDamageContents() {
        return accidentalDamageContents;
    }

    /**
     * Sets the value of the accidentalDamageContents property.
     * 
     */
    public void setAccidentalDamageContents(boolean value) {
        this.accidentalDamageContents = value;
    }

    /**
     * Gets the value of the fusionContents property.
     * 
     */
    public boolean isFusionContents() {
        return fusionContents;
    }

    /**
     * Sets the value of the fusionContents property.
     * 
     */
    public void setFusionContents(boolean value) {
        this.fusionContents = value;
    }

    /**
     * Gets the value of the domesticWorkersCompContents property.
     * 
     */
    public boolean isDomesticWorkersCompContents() {
        return domesticWorkersCompContents;
    }

    /**
     * Sets the value of the domesticWorkersCompContents property.
     * 
     */
    public void setDomesticWorkersCompContents(boolean value) {
        this.domesticWorkersCompContents = value;
    }

    /**
     * Gets the value of the fusionInvestorBuilding property.
     * 
     */
    public boolean isFusionInvestorBuilding() {
        return fusionInvestorBuilding;
    }

    /**
     * Sets the value of the fusionInvestorBuilding property.
     * 
     */
    public void setFusionInvestorBuilding(boolean value) {
        this.fusionInvestorBuilding = value;
    }

    /**
     * Gets the value of the fusionInvestorContents property.
     * 
     */
    public boolean isFusionInvestorContents() {
        return fusionInvestorContents;
    }

    /**
     * Sets the value of the fusionInvestorContents property.
     * 
     */
    public void setFusionInvestorContents(boolean value) {
        this.fusionInvestorContents = value;
    }

    /**
     * Gets the value of the stormGatesAndFenceInvestorBuilding property.
     * 
     */
    public boolean isStormGatesAndFenceInvestorBuilding() {
        return stormGatesAndFenceInvestorBuilding;
    }

    /**
     * Sets the value of the stormGatesAndFenceInvestorBuilding property.
     * 
     */
    public void setStormGatesAndFenceInvestorBuilding(boolean value) {
        this.stormGatesAndFenceInvestorBuilding = value;
    }

    /**
     * Gets the value of the pipDiscount property.
     * 
     */
    public boolean isPipDiscount() {
        return pipDiscount;
    }

    /**
     * Sets the value of the pipDiscount property.
     * 
     */
    public void setPipDiscount(boolean value) {
        this.pipDiscount = value;
    }

    /**
     * Gets the value of the staffIndicator property.
     * 
     */
    public boolean isStaffIndicator() {
        return staffIndicator;
    }

    /**
     * Sets the value of the staffIndicator property.
     * 
     */
    public void setStaffIndicator(boolean value) {
        this.staffIndicator = value;
    }

    /**
     * Gets the value of the staffDiscountCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getStaffDiscountCode() {
        return staffDiscountCode;
    }

    /**
     * Sets the value of the staffDiscountCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setStaffDiscountCode(String value) {
        this.staffDiscountCode = value;
    }

    /**
     * Gets the value of the discretionaryDiscountCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getDiscretionaryDiscountCode() {
        return discretionaryDiscountCode;
    }

    /**
     * Sets the value of the discretionaryDiscountCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setDiscretionaryDiscountCode(String value) {
        this.discretionaryDiscountCode = value;
    }

    /**
     * Gets the value of the silverSaverDiscount property.
     * 
     */
    public boolean isSilverSaverDiscount() {
        return silverSaverDiscount;
    }

    /**
     * Sets the value of the silverSaverDiscount property.
     * 
     */
    public void setSilverSaverDiscount(boolean value) {
        this.silverSaverDiscount = value;
    }

    /**
     * Gets the value of the campaignDiscountCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCampaignDiscountCode() {
        return campaignDiscountCode;
    }

    /**
     * Sets the value of the campaignDiscountCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCampaignDiscountCode(String value) {
        this.campaignDiscountCode = value;
    }

    /**
     * Gets the value of the buildingExcessCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getBuildingExcessCode() {
        return buildingExcessCode;
    }

    /**
     * Sets the value of the buildingExcessCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setBuildingExcessCode(String value) {
        this.buildingExcessCode = value;
    }

    /**
     * Gets the value of the investorBuildingExcessCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getInvestorBuildingExcessCode() {
        return investorBuildingExcessCode;
    }

    /**
     * Sets the value of the investorBuildingExcessCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setInvestorBuildingExcessCode(String value) {
        this.investorBuildingExcessCode = value;
    }

    /**
     * Gets the value of the contentsExcessCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getContentsExcessCode() {
        return contentsExcessCode;
    }

    /**
     * Sets the value of the contentsExcessCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setContentsExcessCode(String value) {
        this.contentsExcessCode = value;
    }

    /**
     * Gets the value of the investorContentsExcessCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getInvestorContentsExcessCode() {
        return investorContentsExcessCode;
    }

    /**
     * Sets the value of the investorContentsExcessCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setInvestorContentsExcessCode(String value) {
        this.investorContentsExcessCode = value;
    }

    /**
     * Gets the value of the baseAnnualPremium property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getBaseAnnualPremium() {
        return baseAnnualPremium;
    }

    /**
     * Sets the value of the baseAnnualPremium property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setBaseAnnualPremium(BigDecimal value) {
        this.baseAnnualPremium = value;
    }

    /**
     * Gets the value of the gst property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getGst() {
        return gst;
    }

    /**
     * Sets the value of the gst property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setGst(BigDecimal value) {
        this.gst = value;
    }

    /**
     * Gets the value of the stampDuty property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getStampDuty() {
        return stampDuty;
    }

    /**
     * Sets the value of the stampDuty property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setStampDuty(BigDecimal value) {
        this.stampDuty = value;
    }

    /**
     * Gets the value of the totalAnnualPremium property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getTotalAnnualPremium() {
        return totalAnnualPremium;
    }

    /**
     * Sets the value of the totalAnnualPremium property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setTotalAnnualPremium(BigDecimal value) {
        this.totalAnnualPremium = value;
    }

    /**
     * Gets the value of the installmentAmount property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getInstallmentAmount() {
        return installmentAmount;
    }

    /**
     * Sets the value of the installmentAmount property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setInstallmentAmount(BigDecimal value) {
        this.installmentAmount = value;
    }

    /**
     * Gets the value of the claimHistory property.
     * 
     */
    public int getClaimHistory() {
        return claimHistory;
    }

    /**
     * Sets the value of the claimHistory property.
     * 
     */
    public void setClaimHistory(int value) {
        this.claimHistory = value;
    }

    /**
     * Gets the value of the liabilityClaimHistory property.
     * 
     */
    public int getLiabilityClaimHistory() {
        return liabilityClaimHistory;
    }

    /**
     * Sets the value of the liabilityClaimHistory property.
     * 
     */
    public void setLiabilityClaimHistory(int value) {
        this.liabilityClaimHistory = value;
    }

    /**
     * Gets the value of the claimRefused property.
     * 
     */
    public boolean isClaimRefused() {
        return claimRefused;
    }

    /**
     * Sets the value of the claimRefused property.
     * 
     */
    public void setClaimRefused(boolean value) {
        this.claimRefused = value;
    }

    /**
     * Gets the value of the insuranceRefused property.
     * 
     */
    public boolean isInsuranceRefused() {
        return insuranceRefused;
    }

    /**
     * Sets the value of the insuranceRefused property.
     * 
     */
    public void setInsuranceRefused(boolean value) {
        this.insuranceRefused = value;
    }

    /**
     * Gets the value of the criminalOffences property.
     * 
     */
    public boolean isCriminalOffences() {
        return criminalOffences;
    }

    /**
     * Sets the value of the criminalOffences property.
     * 
     */
    public void setCriminalOffences(boolean value) {
        this.criminalOffences = value;
    }

    /**
     * Gets the value of the criminalOffenceDetails property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public JAXBElement<String> getCriminalOffenceDetails() {
        return criminalOffenceDetails;
    }

    /**
     * Sets the value of the criminalOffenceDetails property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public void setCriminalOffenceDetails(JAXBElement<String> value) {
        this.criminalOffenceDetails = ((JAXBElement<String>) value);
    }

}
