package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetFamiliesInputResult" type="{http://www.ract.com.au/RactPureLink}GetFamiliesResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getFamiliesInputResult" })
@XmlRootElement(name = "GetFamiliesInputResponse")
public class GetFamiliesInputResponse {

    @XmlElementRef(name = "GetFamiliesInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetFamiliesResponse> getFamiliesInputResult;

    /**
     * Gets the value of the getFamiliesInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetFamiliesResponse }{@code >}
     * 
     */
    public JAXBElement<GetFamiliesResponse> getGetFamiliesInputResult() {
        return getFamiliesInputResult;
    }

    /**
     * Sets the value of the getFamiliesInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetFamiliesResponse }{@code >}
     * 
     */
    public void setGetFamiliesInputResult(JAXBElement<GetFamiliesResponse> value) {
        this.getFamiliesInputResult = ((JAXBElement<GetFamiliesResponse>) value);
    }

}
