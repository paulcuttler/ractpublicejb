package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CardType.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="CardType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Visa"/>
 *     &lt;enumeration value="Mastercard"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CardType")
@XmlEnum
public enum CardType {

    @XmlEnumValue("Visa")
    VISA("Visa"), @XmlEnumValue("Mastercard")
    MASTERCARD("Mastercard");
    private final String value;

    CardType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CardType fromValue(String v) {
        for (CardType c : CardType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
