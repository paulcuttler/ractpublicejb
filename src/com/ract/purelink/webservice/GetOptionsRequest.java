package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Java class for GetOptionsRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetOptionsRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="riskTypeCode" type="{http://www.ract.com.au/RactPureLink}RiskTypeCode"/>
 *         &lt;element name="coverTypeCode" type="{http://www.ract.com.au/RactPureLink}CoverTypeCode"/>
 *         &lt;element name="queryDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetOptionsRequest", propOrder = { "riskTypeCode", "coverTypeCode", "queryDate" })
public class GetOptionsRequest {

    @XmlElement(required = true)
    protected RiskTypeCode riskTypeCode;
    @XmlElement(required = true)
    protected CoverTypeCode coverTypeCode;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar queryDate;

    /**
     * Gets the value of the riskTypeCode property.
     * 
     * @return possible object is {@link RiskTypeCode }
     * 
     */
    public RiskTypeCode getRiskTypeCode() {
        return riskTypeCode;
    }

    /**
     * Sets the value of the riskTypeCode property.
     * 
     * @param value
     *            allowed object is {@link RiskTypeCode }
     * 
     */
    public void setRiskTypeCode(RiskTypeCode value) {
        this.riskTypeCode = value;
    }

    /**
     * Gets the value of the coverTypeCode property.
     * 
     * @return possible object is {@link CoverTypeCode }
     * 
     */
    public CoverTypeCode getCoverTypeCode() {
        return coverTypeCode;
    }

    /**
     * Sets the value of the coverTypeCode property.
     * 
     * @param value
     *            allowed object is {@link CoverTypeCode }
     * 
     */
    public void setCoverTypeCode(CoverTypeCode value) {
        this.coverTypeCode = value;
    }

    /**
     * Gets the value of the queryDate property.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getQueryDate() {
        return queryDate;
    }

    /**
     * Sets the value of the queryDate property.
     * 
     * @param value
     *            allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setQueryDate(XMLGregorianCalendar value) {
        this.queryDate = value;
    }

}
