package com.ract.purelink.webservice;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Java class for DriverDetails complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DriverDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="gender" type="{http://www.ract.com.au/RactPureLink}Gender"/>
 *         &lt;element name="givenName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="surname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="yearCommencedDriving" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numberOfAccidents" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="dateOfBirth" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="drivingFrequencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="atFaultClaims" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="maliciousAndTheftClaims" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lossOfLicenceOffences" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ncbAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DriverDetails", propOrder = { "gender", "givenName", "surname", "yearCommencedDriving", "numberOfAccidents", "dateOfBirth", "drivingFrequencyCode", "atFaultClaims", "maliciousAndTheftClaims", "lossOfLicenceOffences", "ncbAmount" })
public class DriverDetails {

    @XmlElement(required = true)
    protected Gender gender;
    @XmlElement(required = true, nillable = true)
    protected String givenName;
    @XmlElement(required = true, nillable = true)
    protected String surname;
    protected int yearCommencedDriving;
    protected int numberOfAccidents;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateOfBirth;
    @XmlElement(required = true, nillable = true)
    protected String drivingFrequencyCode;
    protected int atFaultClaims;
    protected int maliciousAndTheftClaims;
    protected int lossOfLicenceOffences;
    @XmlElement(required = true)
    protected BigDecimal ncbAmount;

    /**
     * Gets the value of the gender property.
     * 
     * @return possible object is {@link Gender }
     * 
     */
    public Gender getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *            allowed object is {@link Gender }
     * 
     */
    public void setGender(Gender value) {
        this.gender = value;
    }

    /**
     * Gets the value of the givenName property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getGivenName() {
        return givenName;
    }

    /**
     * Sets the value of the givenName property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setGivenName(String value) {
        this.givenName = value;
    }

    /**
     * Gets the value of the surname property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets the value of the surname property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setSurname(String value) {
        this.surname = value;
    }

    /**
     * Gets the value of the yearCommencedDriving property.
     * 
     */
    public int getYearCommencedDriving() {
        return yearCommencedDriving;
    }

    /**
     * Sets the value of the yearCommencedDriving property.
     * 
     */
    public void setYearCommencedDriving(int value) {
        this.yearCommencedDriving = value;
    }

    /**
     * Gets the value of the numberOfAccidents property.
     * 
     */
    public int getNumberOfAccidents() {
        return numberOfAccidents;
    }

    /**
     * Sets the value of the numberOfAccidents property.
     * 
     */
    public void setNumberOfAccidents(int value) {
        this.numberOfAccidents = value;
    }

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *            allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setDateOfBirth(XMLGregorianCalendar value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the drivingFrequencyCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getDrivingFrequencyCode() {
        return drivingFrequencyCode;
    }

    /**
     * Sets the value of the drivingFrequencyCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setDrivingFrequencyCode(String value) {
        this.drivingFrequencyCode = value;
    }

    /**
     * Gets the value of the atFaultClaims property.
     * 
     */
    public int getAtFaultClaims() {
        return atFaultClaims;
    }

    /**
     * Sets the value of the atFaultClaims property.
     * 
     */
    public void setAtFaultClaims(int value) {
        this.atFaultClaims = value;
    }

    /**
     * Gets the value of the maliciousAndTheftClaims property.
     * 
     */
    public int getMaliciousAndTheftClaims() {
        return maliciousAndTheftClaims;
    }

    /**
     * Sets the value of the maliciousAndTheftClaims property.
     * 
     */
    public void setMaliciousAndTheftClaims(int value) {
        this.maliciousAndTheftClaims = value;
    }

    /**
     * Gets the value of the lossOfLicenceOffences property.
     * 
     */
    public int getLossOfLicenceOffences() {
        return lossOfLicenceOffences;
    }

    /**
     * Sets the value of the lossOfLicenceOffences property.
     * 
     */
    public void setLossOfLicenceOffences(int value) {
        this.lossOfLicenceOffences = value;
    }

    /**
     * Gets the value of the ncbAmount property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getNcbAmount() {
        return ncbAmount;
    }

    /**
     * Sets the value of the ncbAmount property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setNcbAmount(BigDecimal value) {
        this.ncbAmount = value;
    }

}
