package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for PayByInstalmentsDetails complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PayByInstalmentsDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bankDetails" type="{http://www.ract.com.au/RactPureLink}BankDetails" minOccurs="0"/>
 *         &lt;element name="creditCardDetails" type="{http://www.ract.com.au/RactPureLink}CreditCardDetails" minOccurs="0"/>
 *         &lt;element name="paymentFrequency" type="{http://www.ract.com.au/RactPureLink}PaymentFrequency"/>
 *         &lt;element name="paymentDay" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayByInstalmentsDetails", propOrder = { "bankDetails", "creditCardDetails", "paymentFrequency", "paymentDay" })
public class PayByInstalmentsDetails {

    @XmlElementRef(name = "bankDetails", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<BankDetails> bankDetails;
    @XmlElementRef(name = "creditCardDetails", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<CreditCardDetails> creditCardDetails;
    @XmlElement(required = true)
    protected PaymentFrequency paymentFrequency;
    protected int paymentDay;

    /**
     * Gets the value of the bankDetails property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link BankDetails }{@code >}
     * 
     */
    public JAXBElement<BankDetails> getBankDetails() {
        return bankDetails;
    }

    /**
     * Sets the value of the bankDetails property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link BankDetails }{@code >}
     * 
     */
    public void setBankDetails(JAXBElement<BankDetails> value) {
        this.bankDetails = ((JAXBElement<BankDetails>) value);
    }

    /**
     * Gets the value of the creditCardDetails property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link CreditCardDetails }{@code >}
     * 
     */
    public JAXBElement<CreditCardDetails> getCreditCardDetails() {
        return creditCardDetails;
    }

    /**
     * Sets the value of the creditCardDetails property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link CreditCardDetails }{@code >}
     * 
     */
    public void setCreditCardDetails(JAXBElement<CreditCardDetails> value) {
        this.creditCardDetails = ((JAXBElement<CreditCardDetails>) value);
    }

    /**
     * Gets the value of the paymentFrequency property.
     * 
     * @return possible object is {@link PaymentFrequency }
     * 
     */
    public PaymentFrequency getPaymentFrequency() {
        return paymentFrequency;
    }

    /**
     * Sets the value of the paymentFrequency property.
     * 
     * @param value
     *            allowed object is {@link PaymentFrequency }
     * 
     */
    public void setPaymentFrequency(PaymentFrequency value) {
        this.paymentFrequency = value;
    }

    /**
     * Gets the value of the paymentDay property.
     * 
     */
    public int getPaymentDay() {
        return paymentDay;
    }

    /**
     * Sets the value of the paymentDay property.
     * 
     */
    public void setPaymentDay(int value) {
        this.paymentDay = value;
    }

}
