package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetSuburbValidationResponse complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetSuburbValidationResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="suburbValidationResult" type="{http://www.ract.com.au/RactPureLink}SuburbValidationResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSuburbValidationResponse", propOrder = { "suburbValidationResult" })
public class GetSuburbValidationResponse {

    @XmlElement(required = true)
    protected SuburbValidationResult suburbValidationResult;

    /**
     * Gets the value of the suburbValidationResult property.
     * 
     * @return possible object is {@link SuburbValidationResult }
     * 
     */
    public SuburbValidationResult getSuburbValidationResult() {
        return suburbValidationResult;
    }

    /**
     * Sets the value of the suburbValidationResult property.
     * 
     * @param value
     *            allowed object is {@link SuburbValidationResult }
     * 
     */
    public void setSuburbValidationResult(SuburbValidationResult value) {
        this.suburbValidationResult = value;
    }

}
