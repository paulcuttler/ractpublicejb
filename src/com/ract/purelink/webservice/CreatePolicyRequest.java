package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Java class for CreatePolicyRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreatePolicyRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="personInsured" type="{http://www.ract.com.au/RactPureLink}PersonInsured"/>
 *         &lt;element name="secondaryPolicyHolders" type="{http://www.ract.com.au/RactPureLink}PartyIdentifierList"/>
 *         &lt;element name="homeQuoteDetails" type="{http://www.ract.com.au/RactPureLink}HomeQuoteDetails" minOccurs="0"/>
 *         &lt;element name="motorQuoteDetails" type="{http://www.ract.com.au/RactPureLink}MotorQuoteDetails" minOccurs="0"/>
 *         &lt;element name="coverStartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="coverEndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="paymentMethod" type="{http://www.ract.com.au/RactPureLink}PaymentMethod"/>
 *         &lt;element name="sourceSystemPolicyReference" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreatePolicyRequest", propOrder = { "personInsured", "secondaryPolicyHolders", "homeQuoteDetails", "motorQuoteDetails", "coverStartDate", "coverEndDate", "paymentMethod", "sourceSystemPolicyReference" })
public class CreatePolicyRequest {

    @XmlElement(required = true, nillable = true)
    protected PersonInsured personInsured;
    @XmlElement(required = true, nillable = true)
    protected PartyIdentifierList secondaryPolicyHolders;
    @XmlElementRef(name = "homeQuoteDetails", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<HomeQuoteDetails> homeQuoteDetails;
    @XmlElementRef(name = "motorQuoteDetails", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<MotorQuoteDetails> motorQuoteDetails;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar coverStartDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar coverEndDate;
    @XmlElement(required = true, nillable = true)
    protected PaymentMethod paymentMethod;
    @XmlElement(required = true, nillable = true)
    protected String sourceSystemPolicyReference;

    /**
     * Gets the value of the personInsured property.
     * 
     * @return possible object is {@link PersonInsured }
     * 
     */
    public PersonInsured getPersonInsured() {
        return personInsured;
    }

    /**
     * Sets the value of the personInsured property.
     * 
     * @param value
     *            allowed object is {@link PersonInsured }
     * 
     */
    public void setPersonInsured(PersonInsured value) {
        this.personInsured = value;
    }

    /**
     * Gets the value of the secondaryPolicyHolders property.
     * 
     * @return possible object is {@link PartyIdentifierList }
     * 
     */
    public PartyIdentifierList getSecondaryPolicyHolders() {
        return secondaryPolicyHolders;
    }

    /**
     * Sets the value of the secondaryPolicyHolders property.
     * 
     * @param value
     *            allowed object is {@link PartyIdentifierList }
     * 
     */
    public void setSecondaryPolicyHolders(PartyIdentifierList value) {
        this.secondaryPolicyHolders = value;
    }

    /**
     * Gets the value of the homeQuoteDetails property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link HomeQuoteDetails }{@code >}
     * 
     */
    public JAXBElement<HomeQuoteDetails> getHomeQuoteDetails() {
        return homeQuoteDetails;
    }

    /**
     * Sets the value of the homeQuoteDetails property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link HomeQuoteDetails }{@code >}
     * 
     */
    public void setHomeQuoteDetails(JAXBElement<HomeQuoteDetails> value) {
        this.homeQuoteDetails = ((JAXBElement<HomeQuoteDetails>) value);
    }

    /**
     * Gets the value of the motorQuoteDetails property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link MotorQuoteDetails }{@code >}
     * 
     */
    public JAXBElement<MotorQuoteDetails> getMotorQuoteDetails() {
        return motorQuoteDetails;
    }

    /**
     * Sets the value of the motorQuoteDetails property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link MotorQuoteDetails }{@code >}
     * 
     */
    public void setMotorQuoteDetails(JAXBElement<MotorQuoteDetails> value) {
        this.motorQuoteDetails = ((JAXBElement<MotorQuoteDetails>) value);
    }

    /**
     * Gets the value of the coverStartDate property.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getCoverStartDate() {
        return coverStartDate;
    }

    /**
     * Sets the value of the coverStartDate property.
     * 
     * @param value
     *            allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setCoverStartDate(XMLGregorianCalendar value) {
        this.coverStartDate = value;
    }

    /**
     * Gets the value of the coverEndDate property.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getCoverEndDate() {
        return coverEndDate;
    }

    /**
     * Sets the value of the coverEndDate property.
     * 
     * @param value
     *            allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setCoverEndDate(XMLGregorianCalendar value) {
        this.coverEndDate = value;
    }

    /**
     * Gets the value of the paymentMethod property.
     * 
     * @return possible object is {@link PaymentMethod }
     * 
     */
    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     * 
     * @param value
     *            allowed object is {@link PaymentMethod }
     * 
     */
    public void setPaymentMethod(PaymentMethod value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the sourceSystemPolicyReference property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getSourceSystemPolicyReference() {
        return sourceSystemPolicyReference;
    }

    /**
     * Sets the value of the sourceSystemPolicyReference property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setSourceSystemPolicyReference(String value) {
        this.sourceSystemPolicyReference = value;
    }

}
