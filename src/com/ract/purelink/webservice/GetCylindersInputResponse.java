package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCylindersInputResult" type="{http://www.ract.com.au/RactPureLink}GetCylindersResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getCylindersInputResult" })
@XmlRootElement(name = "GetCylindersInputResponse")
public class GetCylindersInputResponse {

    @XmlElementRef(name = "GetCylindersInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetCylindersResponse> getCylindersInputResult;

    /**
     * Gets the value of the getCylindersInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetCylindersResponse }{@code >}
     * 
     */
    public JAXBElement<GetCylindersResponse> getGetCylindersInputResult() {
        return getCylindersInputResult;
    }

    /**
     * Sets the value of the getCylindersInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetCylindersResponse }{@code >}
     * 
     */
    public void setGetCylindersInputResult(JAXBElement<GetCylindersResponse> value) {
        this.getCylindersInputResult = ((JAXBElement<GetCylindersResponse>) value);
    }

}
