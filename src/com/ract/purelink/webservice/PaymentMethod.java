package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for PaymentMethod complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentMethod">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agencyCollection" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="payByInstalments" type="{http://www.ract.com.au/RactPureLink}PayByInstalmentsDetails" minOccurs="0"/>
 *         &lt;element name="payNow" type="{http://www.ract.com.au/RactPureLink}CreditCardDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentMethod", propOrder = { "agencyCollection", "payByInstalments", "payNow" })
public class PaymentMethod {

    protected boolean agencyCollection;
    @XmlElementRef(name = "payByInstalments", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<PayByInstalmentsDetails> payByInstalments;
    @XmlElementRef(name = "payNow", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<CreditCardDetails> payNow;

    /**
     * Gets the value of the agencyCollection property.
     * 
     */
    public boolean isAgencyCollection() {
        return agencyCollection;
    }

    /**
     * Sets the value of the agencyCollection property.
     * 
     */
    public void setAgencyCollection(boolean value) {
        this.agencyCollection = value;
    }

    /**
     * Gets the value of the payByInstalments property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link PayByInstalmentsDetails }{@code >}
     * 
     */
    public JAXBElement<PayByInstalmentsDetails> getPayByInstalments() {
        return payByInstalments;
    }

    /**
     * Sets the value of the payByInstalments property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link PayByInstalmentsDetails }{@code >}
     * 
     */
    public void setPayByInstalments(JAXBElement<PayByInstalmentsDetails> value) {
        this.payByInstalments = ((JAXBElement<PayByInstalmentsDetails>) value);
    }

    /**
     * Gets the value of the payNow property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link CreditCardDetails }{@code >}
     * 
     */
    public JAXBElement<CreditCardDetails> getPayNow() {
        return payNow;
    }

    /**
     * Sets the value of the payNow property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link CreditCardDetails }{@code >}
     * 
     */
    public void setPayNow(JAXBElement<CreditCardDetails> value) {
        this.payNow = ((JAXBElement<CreditCardDetails>) value);
    }

}
