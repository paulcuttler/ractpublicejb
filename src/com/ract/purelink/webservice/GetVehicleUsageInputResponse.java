package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetVehicleUsageInputResult" type="{http://www.ract.com.au/RactPureLink}GetVehicleUsageResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getVehicleUsageInputResult" })
@XmlRootElement(name = "GetVehicleUsageInputResponse")
public class GetVehicleUsageInputResponse {

    @XmlElementRef(name = "GetVehicleUsageInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetVehicleUsageResponse> getVehicleUsageInputResult;

    /**
     * Gets the value of the getVehicleUsageInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetVehicleUsageResponse }{@code >}
     * 
     */
    public JAXBElement<GetVehicleUsageResponse> getGetVehicleUsageInputResult() {
        return getVehicleUsageInputResult;
    }

    /**
     * Sets the value of the getVehicleUsageInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetVehicleUsageResponse }{@code >}
     * 
     */
    public void setGetVehicleUsageInputResult(JAXBElement<GetVehicleUsageResponse> value) {
        this.getVehicleUsageInputResult = ((JAXBElement<GetVehicleUsageResponse>) value);
    }

}
