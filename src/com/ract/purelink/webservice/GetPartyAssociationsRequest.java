package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetPartyAssociationsRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPartyAssociationsRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ractId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPartyAssociationsRequest", propOrder = { "ractId" })
public class GetPartyAssociationsRequest {

    @XmlElement(required = true, nillable = true)
    protected String ractId;

    /**
     * Gets the value of the ractId property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getRactId() {
        return ractId;
    }

    /**
     * Sets the value of the ractId property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setRactId(String value) {
        this.ractId = value;
    }

}
