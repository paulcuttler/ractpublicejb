package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetFinanceTypeInputResult" type="{http://www.ract.com.au/RactPureLink}GetFinanceTypeResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getFinanceTypeInputResult" })
@XmlRootElement(name = "GetFinanceTypeInputResponse")
public class GetFinanceTypeInputResponse {

    @XmlElementRef(name = "GetFinanceTypeInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetFinanceTypeResponse> getFinanceTypeInputResult;

    /**
     * Gets the value of the getFinanceTypeInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetFinanceTypeResponse }{@code >}
     * 
     */
    public JAXBElement<GetFinanceTypeResponse> getGetFinanceTypeInputResult() {
        return getFinanceTypeInputResult;
    }

    /**
     * Sets the value of the getFinanceTypeInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetFinanceTypeResponse }{@code >}
     * 
     */
    public void setGetFinanceTypeInputResult(JAXBElement<GetFinanceTypeResponse> value) {
        this.getFinanceTypeInputResult = ((JAXBElement<GetFinanceTypeResponse>) value);
    }

}
