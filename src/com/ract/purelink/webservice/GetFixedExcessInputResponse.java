package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetFixedExcessInputResult" type="{http://www.ract.com.au/RactPureLink}GetFixedExcessResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getFixedExcessInputResult" })
@XmlRootElement(name = "GetFixedExcessInputResponse")
public class GetFixedExcessInputResponse {

    @XmlElementRef(name = "GetFixedExcessInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetFixedExcessResponse> getFixedExcessInputResult;

    /**
     * Gets the value of the getFixedExcessInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetFixedExcessResponse }{@code >}
     * 
     */
    public JAXBElement<GetFixedExcessResponse> getGetFixedExcessInputResult() {
        return getFixedExcessInputResult;
    }

    /**
     * Sets the value of the getFixedExcessInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetFixedExcessResponse }{@code >}
     * 
     */
    public void setGetFixedExcessInputResult(JAXBElement<GetFixedExcessResponse> value) {
        this.getFixedExcessInputResult = ((JAXBElement<GetFixedExcessResponse>) value);
    }

}
