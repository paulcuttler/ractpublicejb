package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Java class for GetVehicleByNvicRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetVehicleByNvicRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nvic" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="queryDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetVehicleByNvicRequest", propOrder = { "nvic", "queryDate" })
public class GetVehicleByNvicRequest {

    @XmlElement(required = true, nillable = true)
    protected String nvic;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar queryDate;

    /**
     * Gets the value of the nvic property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getNvic() {
        return nvic;
    }

    /**
     * Sets the value of the nvic property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setNvic(String value) {
        this.nvic = value;
    }

    /**
     * Gets the value of the queryDate property.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getQueryDate() {
        return queryDate;
    }

    /**
     * Sets the value of the queryDate property.
     * 
     * @param value
     *            allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setQueryDate(XMLGregorianCalendar value) {
        this.queryDate = value;
    }

}
