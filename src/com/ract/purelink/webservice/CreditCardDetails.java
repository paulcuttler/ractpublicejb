package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CreditCardDetails complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditCardDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="creditCardType" type="{http://www.ract.com.au/RactPureLink}CardType"/>
 *         &lt;element name="accountNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="accountName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="expiryMonth" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="expiryYear" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="billingAddress" type="{http://www.ract.com.au/RactPureLink}Address"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardDetails", propOrder = { "creditCardType", "accountNumber", "accountName", "expiryMonth", "expiryYear", "billingAddress" })
public class CreditCardDetails {

    @XmlElement(required = true)
    protected CardType creditCardType;
    @XmlElement(required = true, nillable = true)
    protected String accountNumber;
    @XmlElement(required = true, nillable = true)
    protected String accountName;
    @XmlElement(required = true, nillable = true)
    protected String expiryMonth;
    @XmlElement(required = true, nillable = true)
    protected String expiryYear;
    @XmlElement(required = true, nillable = true)
    protected Address billingAddress;

    /**
     * Gets the value of the creditCardType property.
     * 
     * @return possible object is {@link CardType }
     * 
     */
    public CardType getCreditCardType() {
        return creditCardType;
    }

    /**
     * Sets the value of the creditCardType property.
     * 
     * @param value
     *            allowed object is {@link CardType }
     * 
     */
    public void setCreditCardType(CardType value) {
        this.creditCardType = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the accountName property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * Sets the value of the accountName property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setAccountName(String value) {
        this.accountName = value;
    }

    /**
     * Gets the value of the expiryMonth property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getExpiryMonth() {
        return expiryMonth;
    }

    /**
     * Sets the value of the expiryMonth property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setExpiryMonth(String value) {
        this.expiryMonth = value;
    }

    /**
     * Gets the value of the expiryYear property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getExpiryYear() {
        return expiryYear;
    }

    /**
     * Sets the value of the expiryYear property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setExpiryYear(String value) {
        this.expiryYear = value;
    }

    /**
     * Gets the value of the billingAddress property.
     * 
     * @return possible object is {@link Address }
     * 
     */
    public Address getBillingAddress() {
        return billingAddress;
    }

    /**
     * Sets the value of the billingAddress property.
     * 
     * @param value
     *            allowed object is {@link Address }
     * 
     */
    public void setBillingAddress(Address value) {
        this.billingAddress = value;
    }

}
