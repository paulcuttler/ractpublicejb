package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for PersonInsured complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonInsured">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="partyId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ractId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="salutation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="forename" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="surname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonInsured", propOrder = { "partyId", "ractId", "salutation", "forename", "surname" })
public class PersonInsured {

    protected int partyId;
    @XmlElement(required = true, nillable = true)
    protected String ractId;
    @XmlElement(required = true, nillable = true)
    protected String salutation;
    @XmlElement(required = true, nillable = true)
    protected String forename;
    @XmlElement(required = true, nillable = true)
    protected String surname;

    /**
     * Gets the value of the partyId property.
     * 
     */
    public int getPartyId() {
        return partyId;
    }

    /**
     * Sets the value of the partyId property.
     * 
     */
    public void setPartyId(int value) {
        this.partyId = value;
    }

    /**
     * Gets the value of the ractId property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getRactId() {
        return ractId;
    }

    /**
     * Sets the value of the ractId property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setRactId(String value) {
        this.ractId = value;
    }

    /**
     * Gets the value of the salutation property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getSalutation() {
        return salutation;
    }

    /**
     * Sets the value of the salutation property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setSalutation(String value) {
        this.salutation = value;
    }

    /**
     * Gets the value of the forename property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getForename() {
        return forename;
    }

    /**
     * Sets the value of the forename property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setForename(String value) {
        this.forename = value;
    }

    /**
     * Gets the value of the surname property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets the value of the surname property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setSurname(String value) {
        this.surname = value;
    }

}
