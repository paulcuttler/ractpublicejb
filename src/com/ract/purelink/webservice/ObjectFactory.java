package com.ract.purelink.webservice;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the com.ract.purelink.webservice package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content. The Java representation of XML content can consist of schema derived interfaces and classes representing the binding of schema type definitions, element declarations and model groups. Factory methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SpecifiedItemSerialNumber_QNAME = new QName("http://www.ract.com.au/RactPureLink", "serialNumber");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _TestOutput_QNAME = new QName("http://www.ract.com.au/RactPureLink", "TestOutput");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _SuburbValidationResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "SuburbValidationResult");
    private final static QName _GetVehicleMinMaxValueDataResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetVehicleMinMaxValueDataResponse");
    private final static QName _Address_QNAME = new QName("http://www.ract.com.au/RactPureLink", "Address");
    private final static QName _AddUpdateFindPartyResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "AddUpdateFindPartyResponse");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _GetDrivingFrequencyRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetDrivingFrequencyRequest");
    private final static QName _ContactList_QNAME = new QName("http://www.ract.com.au/RactPureLink", "ContactList");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _GetFinancierRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetFinancierRequest");
    private final static QName _PartyIdentifierList_QNAME = new QName("http://www.ract.com.au/RactPureLink", "PartyIdentifierList");
    private final static QName _GetVehicleValueResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetVehicleValueResponse");
    private final static QName _GetExcessResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetExcessResponse");
    private final static QName _GetMatchingVehiclesResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetMatchingVehiclesResponse");
    private final static QName _GetOccupancyResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetOccupancyResponse");
    private final static QName _GetFamiliesRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetFamiliesRequest");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _GetFixedExcessResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetFixedExcessResponse");
    private final static QName _GetVehicleValueRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetVehicleValueRequest");
    private final static QName _GetCylindersResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetCylindersResponse");
    private final static QName _GetBusinessUsageRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetBusinessUsageRequest");
    private final static QName _GetVehicleUsageResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetVehicleUsageResponse");
    private final static QName _GetQuoteResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetQuoteResponse");
    private final static QName _GetOptionsResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetOptionsResponse");
    private final static QName _GetMinMaxInsurableRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetMinMaxInsurableRequest");
    private final static QName _GetVehicleMinMaxValueDataRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetVehicleMinMaxValueDataRequest");
    private final static QName _GetPartyAssociationsResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetPartyAssociationsResponse");
    private final static QName _BankDetails_QNAME = new QName("http://www.ract.com.au/RactPureLink", "BankDetails");
    private final static QName _PersonInsured_QNAME = new QName("http://www.ract.com.au/RactPureLink", "PersonInsured");
    private final static QName _CardType_QNAME = new QName("http://www.ract.com.au/RactPureLink", "CardType");
    private final static QName _GetMakesRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetMakesRequest");
    private final static QName _Party_QNAME = new QName("http://www.ract.com.au/RactPureLink", "Party");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _GetOptionsRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetOptionsRequest");
    private final static QName _PaymentFrequency_QNAME = new QName("http://www.ract.com.au/RactPureLink", "PaymentFrequency");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _GetBusinessUsageResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetBusinessUsageResponse");
    private final static QName _CreditCardDetails_QNAME = new QName("http://www.ract.com.au/RactPureLink", "CreditCardDetails");
    private final static QName _GetFamiliesResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetFamiliesResponse");
    private final static QName _GetHomeAlarmsResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetHomeAlarmsResponse");
    private final static QName _GetSuburbValidationResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetSuburbValidationResponse");
    private final static QName _GetMatchingVehiclesRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetMatchingVehiclesRequest");
    private final static QName _TestConnectivityResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "TestConnectivityResponse");
    private final static QName _TestConnectivityRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "TestConnectivityRequest");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _VehicleAcceptability_QNAME = new QName("http://www.ract.com.au/RactPureLink", "VehicleAcceptability");
    private final static QName _ValidateAgentRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "ValidateAgentRequest");
    private final static QName _MotorQuoteDetails_QNAME = new QName("http://www.ract.com.au/RactPureLink", "MotorQuoteDetails");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _ContactType_QNAME = new QName("http://www.ract.com.au/RactPureLink", "ContactType");
    private final static QName _GetFinanceTypeRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetFinanceTypeRequest");
    private final static QName _Contact_QNAME = new QName("http://www.ract.com.au/RactPureLink", "Contact");
    private final static QName _GetNoClaimDiscountRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetNoClaimDiscountRequest");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _GetFinanceTypeResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetFinanceTypeResponse");
    private final static QName _ValidateAgentResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "ValidateAgentResponse");
    private final static QName _FixedExcessDetail_QNAME = new QName("http://www.ract.com.au/RactPureLink", "FixedExcessDetail");
    private final static QName _GetMinMaxInsurableResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetMinMaxInsurableResponse");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _GetMakesResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetMakesResponse");
    private final static QName _GetVehicleByNvicRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetVehicleByNvicRequest");
    private final static QName _GetWallConstructionResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetWallConstructionResponse");
    private final static QName _State_QNAME = new QName("http://www.ract.com.au/RactPureLink", "State");
    private final static QName _PartyAssociation_QNAME = new QName("http://www.ract.com.au/RactPureLink", "PartyAssociation");
    private final static QName _RactError_QNAME = new QName("http://www.ract.com.au/RactPureLink", "RactError");
    private final static QName _GetNoClaimDiscountResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetNoClaimDiscountResponse");
    private final static QName _DriverDetailsList_QNAME = new QName("http://www.ract.com.au/RactPureLink", "DriverDetailsList");
    private final static QName _GetHomeContentsCategoriesRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetHomeContentsCategoriesRequest");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _GetStylesResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetStylesResponse");
    private final static QName _GetFixedExcessRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetFixedExcessRequest");
    private final static QName _ExcessDetail_QNAME = new QName("http://www.ract.com.au/RactPureLink", "ExcessDetail");
    private final static QName _GetFinancierResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetFinancierResponse");
    private final static QName _GetPartyAssociationsRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetPartyAssociationsRequest");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _GetWallConstructionRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetWallConstructionRequest");
    private final static QName _GetDrivingFrequencyResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetDrivingFrequencyResponse");
    private final static QName _GetStylesRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetStylesRequest");
    private final static QName _GetHomeAlarmsRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetHomeAlarmsRequest");
    private final static QName _UdlRecord_QNAME = new QName("http://www.ract.com.au/RactPureLink", "UdlRecord");
    private final static QName _SpecifiedItemList_QNAME = new QName("http://www.ract.com.au/RactPureLink", "SpecifiedItemList");
    private final static QName _GetVehicleUsageRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetVehicleUsageRequest");
    private final static QName _GlassesVehicle_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GlassesVehicle");
    private final static QName _GetHomeContentsCategoriesResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetHomeContentsCategoriesResponse");
    private final static QName _GetHomeLocksRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetHomeLocksRequest");
    private final static QName _TestResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "TestResult");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _PaymentMethod_QNAME = new QName("http://www.ract.com.au/RactPureLink", "PaymentMethod");
    private final static QName _GetOccupancyRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetOccupancyRequest");
    private final static QName _GetTransmissionsRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetTransmissionsRequest");
    private final static QName _Gender_QNAME = new QName("http://www.ract.com.au/RactPureLink", "Gender");
    private final static QName _GetRoofConstructionRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetRoofConstructionRequest");
    private final static QName _PartyList_QNAME = new QName("http://www.ract.com.au/RactPureLink", "PartyList");
    private final static QName _CreatePolicyRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "CreatePolicyRequest");
    private final static QName _GetRoofConstructionResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetRoofConstructionResponse");
    private final static QName _HomeQuoteDetails_QNAME = new QName("http://www.ract.com.au/RactPureLink", "HomeQuoteDetails");
    private final static QName _AddUpdateFindPartyRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "AddUpdateFindPartyRequest");
    private final static QName _SubRiskTypeCode_QNAME = new QName("http://www.ract.com.au/RactPureLink", "SubRiskTypeCode");
    private final static QName _GetBuildingLayoutRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetBuildingLayoutRequest");
    private final static QName _GetBuildingLayoutResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetBuildingLayoutResponse");
    private final static QName _OptionDetail_QNAME = new QName("http://www.ract.com.au/RactPureLink", "OptionDetail");
    private final static QName _GetCylindersRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetCylindersRequest");
    private final static QName _TestResultList_QNAME = new QName("http://www.ract.com.au/RactPureLink", "TestResultList");
    private final static QName _RiskTypeCode_QNAME = new QName("http://www.ract.com.au/RactPureLink", "RiskTypeCode");
    private final static QName _GetExcessRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetExcessRequest");
    private final static QName _SpecifiedItem_QNAME = new QName("http://www.ract.com.au/RactPureLink", "SpecifiedItem");
    private final static QName _GetTransmissionsResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetTransmissionsResponse");
    private final static QName _GetSuburbValidationRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetSuburbValidationRequest");
    private final static QName _DriverDetails_QNAME = new QName("http://www.ract.com.au/RactPureLink", "DriverDetails");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _MortgageeList_QNAME = new QName("http://www.ract.com.au/RactPureLink", "MortgageeList");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _PartyAssociationList_QNAME = new QName("http://www.ract.com.au/RactPureLink", "PartyAssociationList");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _PayByInstalmentsDetails_QNAME = new QName("http://www.ract.com.au/RactPureLink", "PayByInstalmentsDetails");
    private final static QName _GetQuoteRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetQuoteRequest");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _GetVehicleByNvicResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetVehicleByNvicResponse");
    private final static QName _CreatePolicyResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "CreatePolicyResponse");
    private final static QName _CoverTypeCode_QNAME = new QName("http://www.ract.com.au/RactPureLink", "CoverTypeCode");
    private final static QName _GetHomeLocksResponse_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetHomeLocksResponse");
    private final static QName _GetHomeLocksInputRequest_QNAME = new QName("http://www.ract.com.au/RactPureLink", "request");
    private final static QName _GetRoofConstructionInputResponseGetRoofConstructionInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetRoofConstructionInputResult");
    private final static QName _GetVehicleMinMaxValueDataInputResponseGetVehicleMinMaxValueDataInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetVehicleMinMaxValueDataInputResult");
    private final static QName _GetQuoteInputResponseGetQuoteInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetQuoteInputResult");
    private final static QName _GetOccupancyInputResponseGetOccupancyInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetOccupancyInputResult");
    private final static QName _GetFixedExcessInputResponseGetFixedExcessInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetFixedExcessInputResult");
    private final static QName _GetHomeAlarmsInputResponseGetHomeAlarmsInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetHomeAlarmsInputResult");
    private final static QName _GetExcessInputResponseGetExcessInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetExcessInputResult");
    private final static QName _GetMinMaxInsurableInputResponseGetMinMaxInsurableInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetMinMaxInsurableInputResult");
    private final static QName _TestConnectivityInputResponseTestConnectivityInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "TestConnectivityInputResult");
    private final static QName _GetPartyAssociationsInputResponseGetPartyAssociationsInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetPartyAssociationsInputResult");
    private final static QName _CreatePolicyRequestMotorQuoteDetails_QNAME = new QName("http://www.ract.com.au/RactPureLink", "motorQuoteDetails");
    private final static QName _CreatePolicyRequestHomeQuoteDetails_QNAME = new QName("http://www.ract.com.au/RactPureLink", "homeQuoteDetails");
    private final static QName _GetFinancierInputResponseGetFinancierInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetFinancierInputResult");
    private final static QName _GetWallConstructionInputResponseGetWallConstructionInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetWallConstructionInputResult");
    private final static QName _GetMakesInputResponseGetMakesInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetMakesInputResult");
    private final static QName _GetFinanceTypeInputResponseGetFinanceTypeInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetFinanceTypeInputResult");
    private final static QName _AddUpdateFindPartyRequestRactId_QNAME = new QName("http://www.ract.com.au/RactPureLink", "ractId");
    private final static QName _AddUpdateFindPartyRequestResidentialAddress_QNAME = new QName("http://www.ract.com.au/RactPureLink", "residentialAddress");
    private final static QName _GetSuburbValidationInputResponseGetSuburbValidationInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetSuburbValidationInputResult");
    private final static QName _HomeQuoteDetailsCriminalOffenceDetails_QNAME = new QName("http://www.ract.com.au/RactPureLink", "criminalOffenceDetails");
    private final static QName _CreatePolicyInputResponseCreatePolicyInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "CreatePolicyInputResult");
    private final static QName _GetVehicleByNvicInputResponseGetVehicleByNvicInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetVehicleByNvicInputResult");
    private final static QName _GetCylindersInputResponseGetCylindersInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetCylindersInputResult");
    private final static QName _GetFamiliesInputResponseGetFamiliesInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetFamiliesInputResult");
    private final static QName _GetMatchingVehiclesInputResponseGetMatchingVehiclesInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetMatchingVehiclesInputResult");
    private final static QName _ValidateAgentInputResponseValidateAgentInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "ValidateAgentInputResult");
    private final static QName _AddressPropertyName_QNAME = new QName("http://www.ract.com.au/RactPureLink", "propertyName");
    private final static QName _AddressGnafId_QNAME = new QName("http://www.ract.com.au/RactPureLink", "gnafId");
    private final static QName _AddressDpid_QNAME = new QName("http://www.ract.com.au/RactPureLink", "dpid");
    private final static QName _AddressLongitude_QNAME = new QName("http://www.ract.com.au/RactPureLink", "longitude");
    private final static QName _AddressLatitude_QNAME = new QName("http://www.ract.com.au/RactPureLink", "latitude");
    private final static QName _AddressAusbar_QNAME = new QName("http://www.ract.com.au/RactPureLink", "ausbar");
    private final static QName _TestResultAdditionalInformation_QNAME = new QName("http://www.ract.com.au/RactPureLink", "additionalInformation");
    private final static QName _GetVehicleUsageInputResponseGetVehicleUsageInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetVehicleUsageInputResult");
    private final static QName _GetBuildingLayoutInputResponseGetBuildingLayoutInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetBuildingLayoutInputResult");
    private final static QName _GetTransmissionsInputResponseGetTransmissionsInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetTransmissionsInputResult");
    private final static QName _GetDrivingFrequencyInputResponseGetDrivingFrequencyInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetDrivingFrequencyInputResult");
    private final static QName _AddUpdateFindPartyInputResponseAddUpdateFindPartyInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "AddUpdateFindPartyInputResult");
    private final static QName _PayByInstalmentsDetailsBankDetails_QNAME = new QName("http://www.ract.com.au/RactPureLink", "bankDetails");
    private final static QName _PayByInstalmentsDetailsCreditCardDetails_QNAME = new QName("http://www.ract.com.au/RactPureLink", "creditCardDetails");
    private final static QName _GetBusinessUsageInputResponseGetBusinessUsageInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetBusinessUsageInputResult");
    private final static QName _PaymentMethodPayNow_QNAME = new QName("http://www.ract.com.au/RactPureLink", "payNow");
    private final static QName _PaymentMethodPayByInstalments_QNAME = new QName("http://www.ract.com.au/RactPureLink", "payByInstalments");
    private final static QName _MotorQuoteDetailsAcceptableGoodOrderAndRepairCode_QNAME = new QName("http://www.ract.com.au/RactPureLink", "acceptableGoodOrderAndRepairCode");
    private final static QName _MotorQuoteDetailsFinancier_QNAME = new QName("http://www.ract.com.au/RactPureLink", "financier");
    private final static QName _GetOptionsInputResponseGetOptionsInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetOptionsInputResult");
    private final static QName _GetNoClaimDiscountInputResponseGetNoClaimDiscountInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetNoClaimDiscountInputResult");
    private final static QName _GetVehicleValueInputResponseGetVehicleValueInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetVehicleValueInputResult");
    private final static QName _GetMatchingVehiclesRequestFamily_QNAME = new QName("http://www.ract.com.au/RactPureLink", "family");
    private final static QName _GetMatchingVehiclesRequestCylinder_QNAME = new QName("http://www.ract.com.au/RactPureLink", "cylinder");
    private final static QName _GetMatchingVehiclesRequestStyle_QNAME = new QName("http://www.ract.com.au/RactPureLink", "style");
    private final static QName _GetMatchingVehiclesRequestTransmission_QNAME = new QName("http://www.ract.com.au/RactPureLink", "transmission");
    private final static QName _GetHomeContentsCategoriesInputResponseGetHomeContentsCategoriesInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetHomeContentsCategoriesInputResult");
    private final static QName _GetHomeLocksInputResponseGetHomeLocksInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetHomeLocksInputResult");
    private final static QName _GetVehicleByNvicResponseVehicle_QNAME = new QName("http://www.ract.com.au/RactPureLink", "vehicle");
    private final static QName _GetStylesInputResponseGetStylesInputResult_QNAME = new QName("http://www.ract.com.au/RactPureLink", "GetStylesInputResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ract.purelink.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SpecifiedItem }
     * 
     */
    public SpecifiedItem createSpecifiedItem() {
        return new SpecifiedItem();
    }

    /**
     * Create an instance of {@link SpecifiedItemList }
     * 
     */
    public SpecifiedItemList createSpecifiedItemList() {
        return new SpecifiedItemList();
    }

    /**
     * Create an instance of {@link GetHomeLocksInput }
     * 
     */
    public GetHomeLocksInput createGetHomeLocksInput() {
        return new GetHomeLocksInput();
    }

    /**
     * Create an instance of {@link PartyIdentifierList }
     * 
     */
    public PartyIdentifierList createPartyIdentifierList() {
        return new PartyIdentifierList();
    }

    /**
     * Create an instance of {@link GetRoofConstructionInputResponse }
     * 
     */
    public GetRoofConstructionInputResponse createGetRoofConstructionInputResponse() {
        return new GetRoofConstructionInputResponse();
    }

    /**
     * Create an instance of {@link GetDrivingFrequencyRequest }
     * 
     */
    public GetDrivingFrequencyRequest createGetDrivingFrequencyRequest() {
        return new GetDrivingFrequencyRequest();
    }

    /**
     * Create an instance of {@link GetWallConstructionResponse }
     * 
     */
    public GetWallConstructionResponse createGetWallConstructionResponse() {
        return new GetWallConstructionResponse();
    }

    /**
     * Create an instance of {@link GetBusinessUsageResponse }
     * 
     */
    public GetBusinessUsageResponse createGetBusinessUsageResponse() {
        return new GetBusinessUsageResponse();
    }

    /**
     * Create an instance of {@link GetFamiliesResponse }
     * 
     */
    public GetFamiliesResponse createGetFamiliesResponse() {
        return new GetFamiliesResponse();
    }

    /**
     * Create an instance of {@link GetFixedExcessResponse }
     * 
     */
    public GetFixedExcessResponse createGetFixedExcessResponse() {
        return new GetFixedExcessResponse();
    }

    /**
     * Create an instance of {@link GetMatchingVehiclesInput }
     * 
     */
    public GetMatchingVehiclesInput createGetMatchingVehiclesInput() {
        return new GetMatchingVehiclesInput();
    }

    /**
     * Create an instance of {@link CreditCardDetails }
     * 
     */
    public CreditCardDetails createCreditCardDetails() {
        return new CreditCardDetails();
    }

    /**
     * Create an instance of {@link CreatePolicyResponse }
     * 
     */
    public CreatePolicyResponse createCreatePolicyResponse() {
        return new CreatePolicyResponse();
    }

    /**
     * Create an instance of {@link BankDetails }
     * 
     */
    public BankDetails createBankDetails() {
        return new BankDetails();
    }

    /**
     * Create an instance of {@link GetExcessInputResponse }
     * 
     */
    public GetExcessInputResponse createGetExcessInputResponse() {
        return new GetExcessInputResponse();
    }

    /**
     * Create an instance of {@link GetMinMaxInsurableInputResponse }
     * 
     */
    public GetMinMaxInsurableInputResponse createGetMinMaxInsurableInputResponse() {
        return new GetMinMaxInsurableInputResponse();
    }

    /**
     * Create an instance of {@link GetFinancierResponse }
     * 
     */
    public GetFinancierResponse createGetFinancierResponse() {
        return new GetFinancierResponse();
    }

    /**
     * Create an instance of {@link CreatePolicyRequest }
     * 
     */
    public CreatePolicyRequest createCreatePolicyRequest() {
        return new CreatePolicyRequest();
    }

    /**
     * Create an instance of {@link GetMatchingVehiclesResponse }
     * 
     */
    public GetMatchingVehiclesResponse createGetMatchingVehiclesResponse() {
        return new GetMatchingVehiclesResponse();
    }

    /**
     * Create an instance of {@link GetFinancierInput }
     * 
     */
    public GetFinancierInput createGetFinancierInput() {
        return new GetFinancierInput();
    }

    /**
     * Create an instance of {@link GetDrivingFrequencyInput }
     * 
     */
    public GetDrivingFrequencyInput createGetDrivingFrequencyInput() {
        return new GetDrivingFrequencyInput();
    }

    /**
     * Create an instance of {@link GetWallConstructionInputResponse }
     * 
     */
    public GetWallConstructionInputResponse createGetWallConstructionInputResponse() {
        return new GetWallConstructionInputResponse();
    }

    /**
     * Create an instance of {@link GetCylindersInput }
     * 
     */
    public GetCylindersInput createGetCylindersInput() {
        return new GetCylindersInput();
    }

    /**
     * Create an instance of {@link GetWallConstructionInput }
     * 
     */
    public GetWallConstructionInput createGetWallConstructionInput() {
        return new GetWallConstructionInput();
    }

    /**
     * Create an instance of {@link GetOptionsInput }
     * 
     */
    public GetOptionsInput createGetOptionsInput() {
        return new GetOptionsInput();
    }

    /**
     * Create an instance of {@link GetPartyAssociationsInput }
     * 
     */
    public GetPartyAssociationsInput createGetPartyAssociationsInput() {
        return new GetPartyAssociationsInput();
    }

    /**
     * Create an instance of {@link GetMinMaxInsurableInput }
     * 
     */
    public GetMinMaxInsurableInput createGetMinMaxInsurableInput() {
        return new GetMinMaxInsurableInput();
    }

    /**
     * Create an instance of {@link GlassesVehicle }
     * 
     */
    public GlassesVehicle createGlassesVehicle() {
        return new GlassesVehicle();
    }

    /**
     * Create an instance of {@link GetMakesInputResponse }
     * 
     */
    public GetMakesInputResponse createGetMakesInputResponse() {
        return new GetMakesInputResponse();
    }

    /**
     * Create an instance of {@link GetOccupancyResponse }
     * 
     */
    public GetOccupancyResponse createGetOccupancyResponse() {
        return new GetOccupancyResponse();
    }

    /**
     * Create an instance of {@link GetSuburbValidationInput }
     * 
     */
    public GetSuburbValidationInput createGetSuburbValidationInput() {
        return new GetSuburbValidationInput();
    }

    /**
     * Create an instance of {@link GetFinanceTypeInputResponse }
     * 
     */
    public GetFinanceTypeInputResponse createGetFinanceTypeInputResponse() {
        return new GetFinanceTypeInputResponse();
    }

    /**
     * Create an instance of {@link PersonInsured }
     * 
     */
    public PersonInsured createPersonInsured() {
        return new PersonInsured();
    }

    /**
     * Create an instance of {@link ValidateAgentInput }
     * 
     */
    public ValidateAgentInput createValidateAgentInput() {
        return new ValidateAgentInput();
    }

    /**
     * Create an instance of {@link GetSuburbValidationInputResponse }
     * 
     */
    public GetSuburbValidationInputResponse createGetSuburbValidationInputResponse() {
        return new GetSuburbValidationInputResponse();
    }

    /**
     * Create an instance of {@link AddUpdateFindPartyInput }
     * 
     */
    public AddUpdateFindPartyInput createAddUpdateFindPartyInput() {
        return new AddUpdateFindPartyInput();
    }

    /**
     * Create an instance of {@link TestResultList }
     * 
     */
    public TestResultList createTestResultList() {
        return new TestResultList();
    }

    /**
     * Create an instance of {@link GetVehicleByNvicInputResponse }
     * 
     */
    public GetVehicleByNvicInputResponse createGetVehicleByNvicInputResponse() {
        return new GetVehicleByNvicInputResponse();
    }

    /**
     * Create an instance of {@link GetHomeAlarmsRequest }
     * 
     */
    public GetHomeAlarmsRequest createGetHomeAlarmsRequest() {
        return new GetHomeAlarmsRequest();
    }

    /**
     * Create an instance of {@link GetDrivingFrequencyResponse }
     * 
     */
    public GetDrivingFrequencyResponse createGetDrivingFrequencyResponse() {
        return new GetDrivingFrequencyResponse();
    }

    /**
     * Create an instance of {@link GetHomeContentsCategoriesInput }
     * 
     */
    public GetHomeContentsCategoriesInput createGetHomeContentsCategoriesInput() {
        return new GetHomeContentsCategoriesInput();
    }

    /**
     * Create an instance of {@link GetVehicleMinMaxValueDataRequest }
     * 
     */
    public GetVehicleMinMaxValueDataRequest createGetVehicleMinMaxValueDataRequest() {
        return new GetVehicleMinMaxValueDataRequest();
    }

    /**
     * Create an instance of {@link GetCylindersResponse }
     * 
     */
    public GetCylindersResponse createGetCylindersResponse() {
        return new GetCylindersResponse();
    }

    /**
     * Create an instance of {@link TestConnectivityRequest }
     * 
     */
    public TestConnectivityRequest createTestConnectivityRequest() {
        return new TestConnectivityRequest();
    }

    /**
     * Create an instance of {@link GetHomeContentsCategoriesResponse }
     * 
     */
    public GetHomeContentsCategoriesResponse createGetHomeContentsCategoriesResponse() {
        return new GetHomeContentsCategoriesResponse();
    }

    /**
     * Create an instance of {@link GetFamiliesInputResponse }
     * 
     */
    public GetFamiliesInputResponse createGetFamiliesInputResponse() {
        return new GetFamiliesInputResponse();
    }

    /**
     * Create an instance of {@link GetVehicleUsageInput }
     * 
     */
    public GetVehicleUsageInput createGetVehicleUsageInput() {
        return new GetVehicleUsageInput();
    }

    /**
     * Create an instance of {@link GetBuildingLayoutRequest }
     * 
     */
    public GetBuildingLayoutRequest createGetBuildingLayoutRequest() {
        return new GetBuildingLayoutRequest();
    }

    /**
     * Create an instance of {@link ValidateAgentInputResponse }
     * 
     */
    public ValidateAgentInputResponse createValidateAgentInputResponse() {
        return new ValidateAgentInputResponse();
    }

    /**
     * Create an instance of {@link TestResult }
     * 
     */
    public TestResult createTestResult() {
        return new TestResult();
    }

    /**
     * Create an instance of {@link ContactList }
     * 
     */
    public ContactList createContactList() {
        return new ContactList();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link Party }
     * 
     */
    public Party createParty() {
        return new Party();
    }

    /**
     * Create an instance of {@link GetVehicleUsageRequest }
     * 
     */
    public GetVehicleUsageRequest createGetVehicleUsageRequest() {
        return new GetVehicleUsageRequest();
    }

    /**
     * Create an instance of {@link GetVehicleUsageInputResponse }
     * 
     */
    public GetVehicleUsageInputResponse createGetVehicleUsageInputResponse() {
        return new GetVehicleUsageInputResponse();
    }

    /**
     * Create an instance of {@link GetMinMaxInsurableResponse }
     * 
     */
    public GetMinMaxInsurableResponse createGetMinMaxInsurableResponse() {
        return new GetMinMaxInsurableResponse();
    }

    /**
     * Create an instance of {@link ValidateAgentResponse }
     * 
     */
    public ValidateAgentResponse createValidateAgentResponse() {
        return new ValidateAgentResponse();
    }

    /**
     * Create an instance of {@link GetBuildingLayoutInputResponse }
     * 
     */
    public GetBuildingLayoutInputResponse createGetBuildingLayoutInputResponse() {
        return new GetBuildingLayoutInputResponse();
    }

    /**
     * Create an instance of {@link GetSuburbValidationResponse }
     * 
     */
    public GetSuburbValidationResponse createGetSuburbValidationResponse() {
        return new GetSuburbValidationResponse();
    }

    /**
     * Create an instance of {@link GetStylesInput }
     * 
     */
    public GetStylesInput createGetStylesInput() {
        return new GetStylesInput();
    }

    /**
     * Create an instance of {@link GetTransmissionsInputResponse }
     * 
     */
    public GetTransmissionsInputResponse createGetTransmissionsInputResponse() {
        return new GetTransmissionsInputResponse();
    }

    /**
     * Create an instance of {@link GetVehicleUsageResponse }
     * 
     */
    public GetVehicleUsageResponse createGetVehicleUsageResponse() {
        return new GetVehicleUsageResponse();
    }

    /**
     * Create an instance of {@link AddUpdateFindPartyInputResponse }
     * 
     */
    public AddUpdateFindPartyInputResponse createAddUpdateFindPartyInputResponse() {
        return new AddUpdateFindPartyInputResponse();
    }

    /**
     * Create an instance of {@link GetStylesResponse }
     * 
     */
    public GetStylesResponse createGetStylesResponse() {
        return new GetStylesResponse();
    }

    /**
     * Create an instance of {@link PaymentMethod }
     * 
     */
    public PaymentMethod createPaymentMethod() {
        return new PaymentMethod();
    }

    /**
     * Create an instance of {@link MotorQuoteDetails }
     * 
     */
    public MotorQuoteDetails createMotorQuoteDetails() {
        return new MotorQuoteDetails();
    }

    /**
     * Create an instance of {@link GetMakesResponse }
     * 
     */
    public GetMakesResponse createGetMakesResponse() {
        return new GetMakesResponse();
    }

    /**
     * Create an instance of {@link GetBusinessUsageRequest }
     * 
     */
    public GetBusinessUsageRequest createGetBusinessUsageRequest() {
        return new GetBusinessUsageRequest();
    }

    /**
     * Create an instance of {@link GetMakesRequest }
     * 
     */
    public GetMakesRequest createGetMakesRequest() {
        return new GetMakesRequest();
    }

    /**
     * Create an instance of {@link RactError }
     * 
     */
    public RactError createRactError() {
        return new RactError();
    }

    /**
     * Create an instance of {@link GetOptionsResponse }
     * 
     */
    public GetOptionsResponse createGetOptionsResponse() {
        return new GetOptionsResponse();
    }

    /**
     * Create an instance of {@link GetFixedExcessInput }
     * 
     */
    public GetFixedExcessInput createGetFixedExcessInput() {
        return new GetFixedExcessInput();
    }

    /**
     * Create an instance of {@link GetTransmissionsRequest }
     * 
     */
    public GetTransmissionsRequest createGetTransmissionsRequest() {
        return new GetTransmissionsRequest();
    }

    /**
     * Create an instance of {@link GetHomeAlarmsInput }
     * 
     */
    public GetHomeAlarmsInput createGetHomeAlarmsInput() {
        return new GetHomeAlarmsInput();
    }

    /**
     * Create an instance of {@link PartyAssociation }
     * 
     */
    public PartyAssociation createPartyAssociation() {
        return new PartyAssociation();
    }

    /**
     * Create an instance of {@link GetHomeContentsCategoriesRequest }
     * 
     */
    public GetHomeContentsCategoriesRequest createGetHomeContentsCategoriesRequest() {
        return new GetHomeContentsCategoriesRequest();
    }

    /**
     * Create an instance of {@link UdlRecord }
     * 
     */
    public UdlRecord createUdlRecord() {
        return new UdlRecord();
    }

    /**
     * Create an instance of {@link GetHomeLocksInputResponse }
     * 
     */
    public GetHomeLocksInputResponse createGetHomeLocksInputResponse() {
        return new GetHomeLocksInputResponse();
    }

    /**
     * Create an instance of {@link GetVehicleByNvicResponse }
     * 
     */
    public GetVehicleByNvicResponse createGetVehicleByNvicResponse() {
        return new GetVehicleByNvicResponse();
    }

    /**
     * Create an instance of {@link GetStylesRequest }
     * 
     */
    public GetStylesRequest createGetStylesRequest() {
        return new GetStylesRequest();
    }

    /**
     * Create an instance of {@link GetHomeLocksRequest }
     * 
     */
    public GetHomeLocksRequest createGetHomeLocksRequest() {
        return new GetHomeLocksRequest();
    }

    /**
     * Create an instance of {@link OptionDetail }
     * 
     */
    public OptionDetail createOptionDetail() {
        return new OptionDetail();
    }

    /**
     * Create an instance of {@link GetExcessInput }
     * 
     */
    public GetExcessInput createGetExcessInput() {
        return new GetExcessInput();
    }

    /**
     * Create an instance of {@link GetVehicleMinMaxValueDataResponse }
     * 
     */
    public GetVehicleMinMaxValueDataResponse createGetVehicleMinMaxValueDataResponse() {
        return new GetVehicleMinMaxValueDataResponse();
    }

    /**
     * Create an instance of {@link FixedExcessDetail }
     * 
     */
    public FixedExcessDetail createFixedExcessDetail() {
        return new FixedExcessDetail();
    }

    /**
     * Create an instance of {@link GetNoClaimDiscountRequest }
     * 
     */
    public GetNoClaimDiscountRequest createGetNoClaimDiscountRequest() {
        return new GetNoClaimDiscountRequest();
    }

    /**
     * Create an instance of {@link GetVehicleMinMaxValueDataInputResponse }
     * 
     */
    public GetVehicleMinMaxValueDataInputResponse createGetVehicleMinMaxValueDataInputResponse() {
        return new GetVehicleMinMaxValueDataInputResponse();
    }

    /**
     * Create an instance of {@link GetOccupancyRequest }
     * 
     */
    public GetOccupancyRequest createGetOccupancyRequest() {
        return new GetOccupancyRequest();
    }

    /**
     * Create an instance of {@link GetQuoteInputResponse }
     * 
     */
    public GetQuoteInputResponse createGetQuoteInputResponse() {
        return new GetQuoteInputResponse();
    }

    /**
     * Create an instance of {@link GetFinanceTypeRequest }
     * 
     */
    public GetFinanceTypeRequest createGetFinanceTypeRequest() {
        return new GetFinanceTypeRequest();
    }

    /**
     * Create an instance of {@link GetOccupancyInputResponse }
     * 
     */
    public GetOccupancyInputResponse createGetOccupancyInputResponse() {
        return new GetOccupancyInputResponse();
    }

    /**
     * Create an instance of {@link GetFixedExcessInputResponse }
     * 
     */
    public GetFixedExcessInputResponse createGetFixedExcessInputResponse() {
        return new GetFixedExcessInputResponse();
    }

    /**
     * Create an instance of {@link GetFinanceTypeInput }
     * 
     */
    public GetFinanceTypeInput createGetFinanceTypeInput() {
        return new GetFinanceTypeInput();
    }

    /**
     * Create an instance of {@link GetNoClaimDiscountInput }
     * 
     */
    public GetNoClaimDiscountInput createGetNoClaimDiscountInput() {
        return new GetNoClaimDiscountInput();
    }

    /**
     * Create an instance of {@link GetBuildingLayoutResponse }
     * 
     */
    public GetBuildingLayoutResponse createGetBuildingLayoutResponse() {
        return new GetBuildingLayoutResponse();
    }

    /**
     * Create an instance of {@link GetRoofConstructionResponse }
     * 
     */
    public GetRoofConstructionResponse createGetRoofConstructionResponse() {
        return new GetRoofConstructionResponse();
    }

    /**
     * Create an instance of {@link GetHomeAlarmsInputResponse }
     * 
     */
    public GetHomeAlarmsInputResponse createGetHomeAlarmsInputResponse() {
        return new GetHomeAlarmsInputResponse();
    }

    /**
     * Create an instance of {@link GetPartyAssociationsInputResponse }
     * 
     */
    public GetPartyAssociationsInputResponse createGetPartyAssociationsInputResponse() {
        return new GetPartyAssociationsInputResponse();
    }

    /**
     * Create an instance of {@link TestConnectivityInputResponse }
     * 
     */
    public TestConnectivityInputResponse createTestConnectivityInputResponse() {
        return new TestConnectivityInputResponse();
    }

    /**
     * Create an instance of {@link GetFinancierInputResponse }
     * 
     */
    public GetFinancierInputResponse createGetFinancierInputResponse() {
        return new GetFinancierInputResponse();
    }

    /**
     * Create an instance of {@link GetExcessRequest }
     * 
     */
    public GetExcessRequest createGetExcessRequest() {
        return new GetExcessRequest();
    }

    /**
     * Create an instance of {@link TestConnectivityInput }
     * 
     */
    public TestConnectivityInput createTestConnectivityInput() {
        return new TestConnectivityInput();
    }

    /**
     * Create an instance of {@link GetExcessResponse }
     * 
     */
    public GetExcessResponse createGetExcessResponse() {
        return new GetExcessResponse();
    }

    /**
     * Create an instance of {@link CreatePolicyInput }
     * 
     */
    public CreatePolicyInput createCreatePolicyInput() {
        return new CreatePolicyInput();
    }

    /**
     * Create an instance of {@link GetHomeAlarmsResponse }
     * 
     */
    public GetHomeAlarmsResponse createGetHomeAlarmsResponse() {
        return new GetHomeAlarmsResponse();
    }

    /**
     * Create an instance of {@link TestConnectivityResponse }
     * 
     */
    public TestConnectivityResponse createTestConnectivityResponse() {
        return new TestConnectivityResponse();
    }

    /**
     * Create an instance of {@link GetVehicleMinMaxValueDataInput }
     * 
     */
    public GetVehicleMinMaxValueDataInput createGetVehicleMinMaxValueDataInput() {
        return new GetVehicleMinMaxValueDataInput();
    }

    /**
     * Create an instance of {@link GetTransmissionsResponse }
     * 
     */
    public GetTransmissionsResponse createGetTransmissionsResponse() {
        return new GetTransmissionsResponse();
    }

    /**
     * Create an instance of {@link AddUpdateFindPartyRequest }
     * 
     */
    public AddUpdateFindPartyRequest createAddUpdateFindPartyRequest() {
        return new AddUpdateFindPartyRequest();
    }

    /**
     * Create an instance of {@link Contact }
     * 
     */
    public Contact createContact() {
        return new Contact();
    }

    /**
     * Create an instance of {@link HomeQuoteDetails }
     * 
     */
    public HomeQuoteDetails createHomeQuoteDetails() {
        return new HomeQuoteDetails();
    }

    /**
     * Create an instance of {@link GetMinMaxInsurableRequest }
     * 
     */
    public GetMinMaxInsurableRequest createGetMinMaxInsurableRequest() {
        return new GetMinMaxInsurableRequest();
    }

    /**
     * Create an instance of {@link PartyAssociationList }
     * 
     */
    public PartyAssociationList createPartyAssociationList() {
        return new PartyAssociationList();
    }

    /**
     * Create an instance of {@link CreatePolicyInputResponse }
     * 
     */
    public CreatePolicyInputResponse createCreatePolicyInputResponse() {
        return new CreatePolicyInputResponse();
    }

    /**
     * Create an instance of {@link GetCylindersInputResponse }
     * 
     */
    public GetCylindersInputResponse createGetCylindersInputResponse() {
        return new GetCylindersInputResponse();
    }

    /**
     * Create an instance of {@link GetRoofConstructionInput }
     * 
     */
    public GetRoofConstructionInput createGetRoofConstructionInput() {
        return new GetRoofConstructionInput();
    }

    /**
     * Create an instance of {@link GetCylindersRequest }
     * 
     */
    public GetCylindersRequest createGetCylindersRequest() {
        return new GetCylindersRequest();
    }

    /**
     * Create an instance of {@link GetVehicleValueResponse }
     * 
     */
    public GetVehicleValueResponse createGetVehicleValueResponse() {
        return new GetVehicleValueResponse();
    }

    /**
     * Create an instance of {@link GetHomeLocksResponse }
     * 
     */
    public GetHomeLocksResponse createGetHomeLocksResponse() {
        return new GetHomeLocksResponse();
    }

    /**
     * Create an instance of {@link ExcessDetail }
     * 
     */
    public ExcessDetail createExcessDetail() {
        return new ExcessDetail();
    }

    /**
     * Create an instance of {@link PartyList }
     * 
     */
    public PartyList createPartyList() {
        return new PartyList();
    }

    /**
     * Create an instance of {@link GetFamiliesInput }
     * 
     */
    public GetFamiliesInput createGetFamiliesInput() {
        return new GetFamiliesInput();
    }

    /**
     * Create an instance of {@link GetMatchingVehiclesInputResponse }
     * 
     */
    public GetMatchingVehiclesInputResponse createGetMatchingVehiclesInputResponse() {
        return new GetMatchingVehiclesInputResponse();
    }

    /**
     * Create an instance of {@link GetBuildingLayoutInput }
     * 
     */
    public GetBuildingLayoutInput createGetBuildingLayoutInput() {
        return new GetBuildingLayoutInput();
    }

    /**
     * Create an instance of {@link GetFamiliesRequest }
     * 
     */
    public GetFamiliesRequest createGetFamiliesRequest() {
        return new GetFamiliesRequest();
    }

    /**
     * Create an instance of {@link GetBusinessUsageInput }
     * 
     */
    public GetBusinessUsageInput createGetBusinessUsageInput() {
        return new GetBusinessUsageInput();
    }

    /**
     * Create an instance of {@link DriverDetailsList }
     * 
     */
    public DriverDetailsList createDriverDetailsList() {
        return new DriverDetailsList();
    }

    /**
     * Create an instance of {@link GetFinanceTypeResponse }
     * 
     */
    public GetFinanceTypeResponse createGetFinanceTypeResponse() {
        return new GetFinanceTypeResponse();
    }

    /**
     * Create an instance of {@link GetRoofConstructionRequest }
     * 
     */
    public GetRoofConstructionRequest createGetRoofConstructionRequest() {
        return new GetRoofConstructionRequest();
    }

    /**
     * Create an instance of {@link ValidateAgentRequest }
     * 
     */
    public ValidateAgentRequest createValidateAgentRequest() {
        return new ValidateAgentRequest();
    }

    /**
     * Create an instance of {@link GetQuoteRequest }
     * 
     */
    public GetQuoteRequest createGetQuoteRequest() {
        return new GetQuoteRequest();
    }

    /**
     * Create an instance of {@link GetQuoteInput }
     * 
     */
    public GetQuoteInput createGetQuoteInput() {
        return new GetQuoteInput();
    }

    /**
     * Create an instance of {@link GetFinancierRequest }
     * 
     */
    public GetFinancierRequest createGetFinancierRequest() {
        return new GetFinancierRequest();
    }

    /**
     * Create an instance of {@link DriverDetails }
     * 
     */
    public DriverDetails createDriverDetails() {
        return new DriverDetails();
    }

    /**
     * Create an instance of {@link GetDrivingFrequencyInputResponse }
     * 
     */
    public GetDrivingFrequencyInputResponse createGetDrivingFrequencyInputResponse() {
        return new GetDrivingFrequencyInputResponse();
    }

    /**
     * Create an instance of {@link PayByInstalmentsDetails }
     * 
     */
    public PayByInstalmentsDetails createPayByInstalmentsDetails() {
        return new PayByInstalmentsDetails();
    }

    /**
     * Create an instance of {@link GetMakesInput }
     * 
     */
    public GetMakesInput createGetMakesInput() {
        return new GetMakesInput();
    }

    /**
     * Create an instance of {@link GetBusinessUsageInputResponse }
     * 
     */
    public GetBusinessUsageInputResponse createGetBusinessUsageInputResponse() {
        return new GetBusinessUsageInputResponse();
    }

    /**
     * Create an instance of {@link GetNoClaimDiscountResponse }
     * 
     */
    public GetNoClaimDiscountResponse createGetNoClaimDiscountResponse() {
        return new GetNoClaimDiscountResponse();
    }

    /**
     * Create an instance of {@link GetWallConstructionRequest }
     * 
     */
    public GetWallConstructionRequest createGetWallConstructionRequest() {
        return new GetWallConstructionRequest();
    }

    /**
     * Create an instance of {@link GetOptionsRequest }
     * 
     */
    public GetOptionsRequest createGetOptionsRequest() {
        return new GetOptionsRequest();
    }

    /**
     * Create an instance of {@link GetPartyAssociationsRequest }
     * 
     */
    public GetPartyAssociationsRequest createGetPartyAssociationsRequest() {
        return new GetPartyAssociationsRequest();
    }

    /**
     * Create an instance of {@link GetOptionsInputResponse }
     * 
     */
    public GetOptionsInputResponse createGetOptionsInputResponse() {
        return new GetOptionsInputResponse();
    }

    /**
     * Create an instance of {@link GetVehicleByNvicInput }
     * 
     */
    public GetVehicleByNvicInput createGetVehicleByNvicInput() {
        return new GetVehicleByNvicInput();
    }

    /**
     * Create an instance of {@link GetSuburbValidationRequest }
     * 
     */
    public GetSuburbValidationRequest createGetSuburbValidationRequest() {
        return new GetSuburbValidationRequest();
    }

    /**
     * Create an instance of {@link GetOccupancyInput }
     * 
     */
    public GetOccupancyInput createGetOccupancyInput() {
        return new GetOccupancyInput();
    }

    /**
     * Create an instance of {@link GetVehicleByNvicRequest }
     * 
     */
    public GetVehicleByNvicRequest createGetVehicleByNvicRequest() {
        return new GetVehicleByNvicRequest();
    }

    /**
     * Create an instance of {@link MortgageeList }
     * 
     */
    public MortgageeList createMortgageeList() {
        return new MortgageeList();
    }

    /**
     * Create an instance of {@link GetTransmissionsInput }
     * 
     */
    public GetTransmissionsInput createGetTransmissionsInput() {
        return new GetTransmissionsInput();
    }

    /**
     * Create an instance of {@link GetQuoteResponse }
     * 
     */
    public GetQuoteResponse createGetQuoteResponse() {
        return new GetQuoteResponse();
    }

    /**
     * Create an instance of {@link GetNoClaimDiscountInputResponse }
     * 
     */
    public GetNoClaimDiscountInputResponse createGetNoClaimDiscountInputResponse() {
        return new GetNoClaimDiscountInputResponse();
    }

    /**
     * Create an instance of {@link GetVehicleValueInputResponse }
     * 
     */
    public GetVehicleValueInputResponse createGetVehicleValueInputResponse() {
        return new GetVehicleValueInputResponse();
    }

    /**
     * Create an instance of {@link GetMatchingVehiclesRequest }
     * 
     */
    public GetMatchingVehiclesRequest createGetMatchingVehiclesRequest() {
        return new GetMatchingVehiclesRequest();
    }

    /**
     * Create an instance of {@link GetHomeContentsCategoriesInputResponse }
     * 
     */
    public GetHomeContentsCategoriesInputResponse createGetHomeContentsCategoriesInputResponse() {
        return new GetHomeContentsCategoriesInputResponse();
    }

    /**
     * Create an instance of {@link GetPartyAssociationsResponse }
     * 
     */
    public GetPartyAssociationsResponse createGetPartyAssociationsResponse() {
        return new GetPartyAssociationsResponse();
    }

    /**
     * Create an instance of {@link GetVehicleValueRequest }
     * 
     */
    public GetVehicleValueRequest createGetVehicleValueRequest() {
        return new GetVehicleValueRequest();
    }

    /**
     * Create an instance of {@link GetFixedExcessRequest }
     * 
     */
    public GetFixedExcessRequest createGetFixedExcessRequest() {
        return new GetFixedExcessRequest();
    }

    /**
     * Create an instance of {@link GetStylesInputResponse }
     * 
     */
    public GetStylesInputResponse createGetStylesInputResponse() {
        return new GetStylesInputResponse();
    }

    /**
     * Create an instance of {@link GetVehicleValueInput }
     * 
     */
    public GetVehicleValueInput createGetVehicleValueInput() {
        return new GetVehicleValueInput();
    }

    /**
     * Create an instance of {@link AddUpdateFindPartyResponse }
     * 
     */
    public AddUpdateFindPartyResponse createAddUpdateFindPartyResponse() {
        return new AddUpdateFindPartyResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "serialNumber", scope = SpecifiedItem.class)
    public JAXBElement<String> createSpecifiedItemSerialNumber(String value) {
        return new JAXBElement<String>(_SpecifiedItemSerialNumber_QNAME, String.class, SpecifiedItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestOutput }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "TestOutput")
    public JAXBElement<TestOutput> createTestOutput(TestOutput value) {
        return new JAXBElement<TestOutput>(_TestOutput_QNAME, TestOutput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SuburbValidationResult }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "SuburbValidationResult")
    public JAXBElement<SuburbValidationResult> createSuburbValidationResult(SuburbValidationResult value) {
        return new JAXBElement<SuburbValidationResult>(_SuburbValidationResult_QNAME, SuburbValidationResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVehicleMinMaxValueDataResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetVehicleMinMaxValueDataResponse")
    public JAXBElement<GetVehicleMinMaxValueDataResponse> createGetVehicleMinMaxValueDataResponse(GetVehicleMinMaxValueDataResponse value) {
        return new JAXBElement<GetVehicleMinMaxValueDataResponse>(_GetVehicleMinMaxValueDataResponse_QNAME, GetVehicleMinMaxValueDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "Address")
    public JAXBElement<Address> createAddress(Address value) {
        return new JAXBElement<Address>(_Address_QNAME, Address.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUpdateFindPartyResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "AddUpdateFindPartyResponse")
    public JAXBElement<AddUpdateFindPartyResponse> createAddUpdateFindPartyResponse(AddUpdateFindPartyResponse value) {
        return new JAXBElement<AddUpdateFindPartyResponse>(_AddUpdateFindPartyResponse_QNAME, AddUpdateFindPartyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDrivingFrequencyRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetDrivingFrequencyRequest")
    public JAXBElement<GetDrivingFrequencyRequest> createGetDrivingFrequencyRequest(GetDrivingFrequencyRequest value) {
        return new JAXBElement<GetDrivingFrequencyRequest>(_GetDrivingFrequencyRequest_QNAME, GetDrivingFrequencyRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactList }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "ContactList")
    public JAXBElement<ContactList> createContactList(ContactList value) {
        return new JAXBElement<ContactList>(_ContactList_QNAME, ContactList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFinancierRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetFinancierRequest")
    public JAXBElement<GetFinancierRequest> createGetFinancierRequest(GetFinancierRequest value) {
        return new JAXBElement<GetFinancierRequest>(_GetFinancierRequest_QNAME, GetFinancierRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartyIdentifierList }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "PartyIdentifierList")
    public JAXBElement<PartyIdentifierList> createPartyIdentifierList(PartyIdentifierList value) {
        return new JAXBElement<PartyIdentifierList>(_PartyIdentifierList_QNAME, PartyIdentifierList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVehicleValueResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetVehicleValueResponse")
    public JAXBElement<GetVehicleValueResponse> createGetVehicleValueResponse(GetVehicleValueResponse value) {
        return new JAXBElement<GetVehicleValueResponse>(_GetVehicleValueResponse_QNAME, GetVehicleValueResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetExcessResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetExcessResponse")
    public JAXBElement<GetExcessResponse> createGetExcessResponse(GetExcessResponse value) {
        return new JAXBElement<GetExcessResponse>(_GetExcessResponse_QNAME, GetExcessResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMatchingVehiclesResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetMatchingVehiclesResponse")
    public JAXBElement<GetMatchingVehiclesResponse> createGetMatchingVehiclesResponse(GetMatchingVehiclesResponse value) {
        return new JAXBElement<GetMatchingVehiclesResponse>(_GetMatchingVehiclesResponse_QNAME, GetMatchingVehiclesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOccupancyResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetOccupancyResponse")
    public JAXBElement<GetOccupancyResponse> createGetOccupancyResponse(GetOccupancyResponse value) {
        return new JAXBElement<GetOccupancyResponse>(_GetOccupancyResponse_QNAME, GetOccupancyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFamiliesRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetFamiliesRequest")
    public JAXBElement<GetFamiliesRequest> createGetFamiliesRequest(GetFamiliesRequest value) {
        return new JAXBElement<GetFamiliesRequest>(_GetFamiliesRequest_QNAME, GetFamiliesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFixedExcessResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetFixedExcessResponse")
    public JAXBElement<GetFixedExcessResponse> createGetFixedExcessResponse(GetFixedExcessResponse value) {
        return new JAXBElement<GetFixedExcessResponse>(_GetFixedExcessResponse_QNAME, GetFixedExcessResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVehicleValueRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetVehicleValueRequest")
    public JAXBElement<GetVehicleValueRequest> createGetVehicleValueRequest(GetVehicleValueRequest value) {
        return new JAXBElement<GetVehicleValueRequest>(_GetVehicleValueRequest_QNAME, GetVehicleValueRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCylindersResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetCylindersResponse")
    public JAXBElement<GetCylindersResponse> createGetCylindersResponse(GetCylindersResponse value) {
        return new JAXBElement<GetCylindersResponse>(_GetCylindersResponse_QNAME, GetCylindersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBusinessUsageRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetBusinessUsageRequest")
    public JAXBElement<GetBusinessUsageRequest> createGetBusinessUsageRequest(GetBusinessUsageRequest value) {
        return new JAXBElement<GetBusinessUsageRequest>(_GetBusinessUsageRequest_QNAME, GetBusinessUsageRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVehicleUsageResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetVehicleUsageResponse")
    public JAXBElement<GetVehicleUsageResponse> createGetVehicleUsageResponse(GetVehicleUsageResponse value) {
        return new JAXBElement<GetVehicleUsageResponse>(_GetVehicleUsageResponse_QNAME, GetVehicleUsageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetQuoteResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetQuoteResponse")
    public JAXBElement<GetQuoteResponse> createGetQuoteResponse(GetQuoteResponse value) {
        return new JAXBElement<GetQuoteResponse>(_GetQuoteResponse_QNAME, GetQuoteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOptionsResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetOptionsResponse")
    public JAXBElement<GetOptionsResponse> createGetOptionsResponse(GetOptionsResponse value) {
        return new JAXBElement<GetOptionsResponse>(_GetOptionsResponse_QNAME, GetOptionsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMinMaxInsurableRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetMinMaxInsurableRequest")
    public JAXBElement<GetMinMaxInsurableRequest> createGetMinMaxInsurableRequest(GetMinMaxInsurableRequest value) {
        return new JAXBElement<GetMinMaxInsurableRequest>(_GetMinMaxInsurableRequest_QNAME, GetMinMaxInsurableRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVehicleMinMaxValueDataRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetVehicleMinMaxValueDataRequest")
    public JAXBElement<GetVehicleMinMaxValueDataRequest> createGetVehicleMinMaxValueDataRequest(GetVehicleMinMaxValueDataRequest value) {
        return new JAXBElement<GetVehicleMinMaxValueDataRequest>(_GetVehicleMinMaxValueDataRequest_QNAME, GetVehicleMinMaxValueDataRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPartyAssociationsResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetPartyAssociationsResponse")
    public JAXBElement<GetPartyAssociationsResponse> createGetPartyAssociationsResponse(GetPartyAssociationsResponse value) {
        return new JAXBElement<GetPartyAssociationsResponse>(_GetPartyAssociationsResponse_QNAME, GetPartyAssociationsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankDetails }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "BankDetails")
    public JAXBElement<BankDetails> createBankDetails(BankDetails value) {
        return new JAXBElement<BankDetails>(_BankDetails_QNAME, BankDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonInsured }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "PersonInsured")
    public JAXBElement<PersonInsured> createPersonInsured(PersonInsured value) {
        return new JAXBElement<PersonInsured>(_PersonInsured_QNAME, PersonInsured.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardType }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "CardType")
    public JAXBElement<CardType> createCardType(CardType value) {
        return new JAXBElement<CardType>(_CardType_QNAME, CardType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMakesRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetMakesRequest")
    public JAXBElement<GetMakesRequest> createGetMakesRequest(GetMakesRequest value) {
        return new JAXBElement<GetMakesRequest>(_GetMakesRequest_QNAME, GetMakesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Party }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "Party")
    public JAXBElement<Party> createParty(Party value) {
        return new JAXBElement<Party>(_Party_QNAME, Party.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOptionsRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetOptionsRequest")
    public JAXBElement<GetOptionsRequest> createGetOptionsRequest(GetOptionsRequest value) {
        return new JAXBElement<GetOptionsRequest>(_GetOptionsRequest_QNAME, GetOptionsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentFrequency }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "PaymentFrequency")
    public JAXBElement<PaymentFrequency> createPaymentFrequency(PaymentFrequency value) {
        return new JAXBElement<PaymentFrequency>(_PaymentFrequency_QNAME, PaymentFrequency.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBusinessUsageResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetBusinessUsageResponse")
    public JAXBElement<GetBusinessUsageResponse> createGetBusinessUsageResponse(GetBusinessUsageResponse value) {
        return new JAXBElement<GetBusinessUsageResponse>(_GetBusinessUsageResponse_QNAME, GetBusinessUsageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCardDetails }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "CreditCardDetails")
    public JAXBElement<CreditCardDetails> createCreditCardDetails(CreditCardDetails value) {
        return new JAXBElement<CreditCardDetails>(_CreditCardDetails_QNAME, CreditCardDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFamiliesResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetFamiliesResponse")
    public JAXBElement<GetFamiliesResponse> createGetFamiliesResponse(GetFamiliesResponse value) {
        return new JAXBElement<GetFamiliesResponse>(_GetFamiliesResponse_QNAME, GetFamiliesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHomeAlarmsResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetHomeAlarmsResponse")
    public JAXBElement<GetHomeAlarmsResponse> createGetHomeAlarmsResponse(GetHomeAlarmsResponse value) {
        return new JAXBElement<GetHomeAlarmsResponse>(_GetHomeAlarmsResponse_QNAME, GetHomeAlarmsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSuburbValidationResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetSuburbValidationResponse")
    public JAXBElement<GetSuburbValidationResponse> createGetSuburbValidationResponse(GetSuburbValidationResponse value) {
        return new JAXBElement<GetSuburbValidationResponse>(_GetSuburbValidationResponse_QNAME, GetSuburbValidationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMatchingVehiclesRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetMatchingVehiclesRequest")
    public JAXBElement<GetMatchingVehiclesRequest> createGetMatchingVehiclesRequest(GetMatchingVehiclesRequest value) {
        return new JAXBElement<GetMatchingVehiclesRequest>(_GetMatchingVehiclesRequest_QNAME, GetMatchingVehiclesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestConnectivityResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "TestConnectivityResponse")
    public JAXBElement<TestConnectivityResponse> createTestConnectivityResponse(TestConnectivityResponse value) {
        return new JAXBElement<TestConnectivityResponse>(_TestConnectivityResponse_QNAME, TestConnectivityResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestConnectivityRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "TestConnectivityRequest")
    public JAXBElement<TestConnectivityRequest> createTestConnectivityRequest(TestConnectivityRequest value) {
        return new JAXBElement<TestConnectivityRequest>(_TestConnectivityRequest_QNAME, TestConnectivityRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VehicleAcceptability }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "VehicleAcceptability")
    public JAXBElement<VehicleAcceptability> createVehicleAcceptability(VehicleAcceptability value) {
        return new JAXBElement<VehicleAcceptability>(_VehicleAcceptability_QNAME, VehicleAcceptability.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateAgentRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "ValidateAgentRequest")
    public JAXBElement<ValidateAgentRequest> createValidateAgentRequest(ValidateAgentRequest value) {
        return new JAXBElement<ValidateAgentRequest>(_ValidateAgentRequest_QNAME, ValidateAgentRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MotorQuoteDetails }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "MotorQuoteDetails")
    public JAXBElement<MotorQuoteDetails> createMotorQuoteDetails(MotorQuoteDetails value) {
        return new JAXBElement<MotorQuoteDetails>(_MotorQuoteDetails_QNAME, MotorQuoteDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactType }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "ContactType")
    public JAXBElement<ContactType> createContactType(ContactType value) {
        return new JAXBElement<ContactType>(_ContactType_QNAME, ContactType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFinanceTypeRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetFinanceTypeRequest")
    public JAXBElement<GetFinanceTypeRequest> createGetFinanceTypeRequest(GetFinanceTypeRequest value) {
        return new JAXBElement<GetFinanceTypeRequest>(_GetFinanceTypeRequest_QNAME, GetFinanceTypeRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Contact }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "Contact")
    public JAXBElement<Contact> createContact(Contact value) {
        return new JAXBElement<Contact>(_Contact_QNAME, Contact.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNoClaimDiscountRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetNoClaimDiscountRequest")
    public JAXBElement<GetNoClaimDiscountRequest> createGetNoClaimDiscountRequest(GetNoClaimDiscountRequest value) {
        return new JAXBElement<GetNoClaimDiscountRequest>(_GetNoClaimDiscountRequest_QNAME, GetNoClaimDiscountRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFinanceTypeResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetFinanceTypeResponse")
    public JAXBElement<GetFinanceTypeResponse> createGetFinanceTypeResponse(GetFinanceTypeResponse value) {
        return new JAXBElement<GetFinanceTypeResponse>(_GetFinanceTypeResponse_QNAME, GetFinanceTypeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateAgentResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "ValidateAgentResponse")
    public JAXBElement<ValidateAgentResponse> createValidateAgentResponse(ValidateAgentResponse value) {
        return new JAXBElement<ValidateAgentResponse>(_ValidateAgentResponse_QNAME, ValidateAgentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FixedExcessDetail }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "FixedExcessDetail")
    public JAXBElement<FixedExcessDetail> createFixedExcessDetail(FixedExcessDetail value) {
        return new JAXBElement<FixedExcessDetail>(_FixedExcessDetail_QNAME, FixedExcessDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMinMaxInsurableResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetMinMaxInsurableResponse")
    public JAXBElement<GetMinMaxInsurableResponse> createGetMinMaxInsurableResponse(GetMinMaxInsurableResponse value) {
        return new JAXBElement<GetMinMaxInsurableResponse>(_GetMinMaxInsurableResponse_QNAME, GetMinMaxInsurableResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMakesResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetMakesResponse")
    public JAXBElement<GetMakesResponse> createGetMakesResponse(GetMakesResponse value) {
        return new JAXBElement<GetMakesResponse>(_GetMakesResponse_QNAME, GetMakesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVehicleByNvicRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetVehicleByNvicRequest")
    public JAXBElement<GetVehicleByNvicRequest> createGetVehicleByNvicRequest(GetVehicleByNvicRequest value) {
        return new JAXBElement<GetVehicleByNvicRequest>(_GetVehicleByNvicRequest_QNAME, GetVehicleByNvicRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWallConstructionResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetWallConstructionResponse")
    public JAXBElement<GetWallConstructionResponse> createGetWallConstructionResponse(GetWallConstructionResponse value) {
        return new JAXBElement<GetWallConstructionResponse>(_GetWallConstructionResponse_QNAME, GetWallConstructionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link State }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "State")
    public JAXBElement<State> createState(State value) {
        return new JAXBElement<State>(_State_QNAME, State.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartyAssociation }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "PartyAssociation")
    public JAXBElement<PartyAssociation> createPartyAssociation(PartyAssociation value) {
        return new JAXBElement<PartyAssociation>(_PartyAssociation_QNAME, PartyAssociation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RactError }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "RactError")
    public JAXBElement<RactError> createRactError(RactError value) {
        return new JAXBElement<RactError>(_RactError_QNAME, RactError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNoClaimDiscountResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetNoClaimDiscountResponse")
    public JAXBElement<GetNoClaimDiscountResponse> createGetNoClaimDiscountResponse(GetNoClaimDiscountResponse value) {
        return new JAXBElement<GetNoClaimDiscountResponse>(_GetNoClaimDiscountResponse_QNAME, GetNoClaimDiscountResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DriverDetailsList }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "DriverDetailsList")
    public JAXBElement<DriverDetailsList> createDriverDetailsList(DriverDetailsList value) {
        return new JAXBElement<DriverDetailsList>(_DriverDetailsList_QNAME, DriverDetailsList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHomeContentsCategoriesRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetHomeContentsCategoriesRequest")
    public JAXBElement<GetHomeContentsCategoriesRequest> createGetHomeContentsCategoriesRequest(GetHomeContentsCategoriesRequest value) {
        return new JAXBElement<GetHomeContentsCategoriesRequest>(_GetHomeContentsCategoriesRequest_QNAME, GetHomeContentsCategoriesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStylesResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetStylesResponse")
    public JAXBElement<GetStylesResponse> createGetStylesResponse(GetStylesResponse value) {
        return new JAXBElement<GetStylesResponse>(_GetStylesResponse_QNAME, GetStylesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFixedExcessRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetFixedExcessRequest")
    public JAXBElement<GetFixedExcessRequest> createGetFixedExcessRequest(GetFixedExcessRequest value) {
        return new JAXBElement<GetFixedExcessRequest>(_GetFixedExcessRequest_QNAME, GetFixedExcessRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExcessDetail }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "ExcessDetail")
    public JAXBElement<ExcessDetail> createExcessDetail(ExcessDetail value) {
        return new JAXBElement<ExcessDetail>(_ExcessDetail_QNAME, ExcessDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFinancierResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetFinancierResponse")
    public JAXBElement<GetFinancierResponse> createGetFinancierResponse(GetFinancierResponse value) {
        return new JAXBElement<GetFinancierResponse>(_GetFinancierResponse_QNAME, GetFinancierResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPartyAssociationsRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetPartyAssociationsRequest")
    public JAXBElement<GetPartyAssociationsRequest> createGetPartyAssociationsRequest(GetPartyAssociationsRequest value) {
        return new JAXBElement<GetPartyAssociationsRequest>(_GetPartyAssociationsRequest_QNAME, GetPartyAssociationsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWallConstructionRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetWallConstructionRequest")
    public JAXBElement<GetWallConstructionRequest> createGetWallConstructionRequest(GetWallConstructionRequest value) {
        return new JAXBElement<GetWallConstructionRequest>(_GetWallConstructionRequest_QNAME, GetWallConstructionRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDrivingFrequencyResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetDrivingFrequencyResponse")
    public JAXBElement<GetDrivingFrequencyResponse> createGetDrivingFrequencyResponse(GetDrivingFrequencyResponse value) {
        return new JAXBElement<GetDrivingFrequencyResponse>(_GetDrivingFrequencyResponse_QNAME, GetDrivingFrequencyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStylesRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetStylesRequest")
    public JAXBElement<GetStylesRequest> createGetStylesRequest(GetStylesRequest value) {
        return new JAXBElement<GetStylesRequest>(_GetStylesRequest_QNAME, GetStylesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHomeAlarmsRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetHomeAlarmsRequest")
    public JAXBElement<GetHomeAlarmsRequest> createGetHomeAlarmsRequest(GetHomeAlarmsRequest value) {
        return new JAXBElement<GetHomeAlarmsRequest>(_GetHomeAlarmsRequest_QNAME, GetHomeAlarmsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UdlRecord }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "UdlRecord")
    public JAXBElement<UdlRecord> createUdlRecord(UdlRecord value) {
        return new JAXBElement<UdlRecord>(_UdlRecord_QNAME, UdlRecord.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SpecifiedItemList }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "SpecifiedItemList")
    public JAXBElement<SpecifiedItemList> createSpecifiedItemList(SpecifiedItemList value) {
        return new JAXBElement<SpecifiedItemList>(_SpecifiedItemList_QNAME, SpecifiedItemList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVehicleUsageRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetVehicleUsageRequest")
    public JAXBElement<GetVehicleUsageRequest> createGetVehicleUsageRequest(GetVehicleUsageRequest value) {
        return new JAXBElement<GetVehicleUsageRequest>(_GetVehicleUsageRequest_QNAME, GetVehicleUsageRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlassesVehicle }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GlassesVehicle")
    public JAXBElement<GlassesVehicle> createGlassesVehicle(GlassesVehicle value) {
        return new JAXBElement<GlassesVehicle>(_GlassesVehicle_QNAME, GlassesVehicle.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHomeContentsCategoriesResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetHomeContentsCategoriesResponse")
    public JAXBElement<GetHomeContentsCategoriesResponse> createGetHomeContentsCategoriesResponse(GetHomeContentsCategoriesResponse value) {
        return new JAXBElement<GetHomeContentsCategoriesResponse>(_GetHomeContentsCategoriesResponse_QNAME, GetHomeContentsCategoriesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHomeLocksRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetHomeLocksRequest")
    public JAXBElement<GetHomeLocksRequest> createGetHomeLocksRequest(GetHomeLocksRequest value) {
        return new JAXBElement<GetHomeLocksRequest>(_GetHomeLocksRequest_QNAME, GetHomeLocksRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestResult }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "TestResult")
    public JAXBElement<TestResult> createTestResult(TestResult value) {
        return new JAXBElement<TestResult>(_TestResult_QNAME, TestResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentMethod }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "PaymentMethod")
    public JAXBElement<PaymentMethod> createPaymentMethod(PaymentMethod value) {
        return new JAXBElement<PaymentMethod>(_PaymentMethod_QNAME, PaymentMethod.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOccupancyRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetOccupancyRequest")
    public JAXBElement<GetOccupancyRequest> createGetOccupancyRequest(GetOccupancyRequest value) {
        return new JAXBElement<GetOccupancyRequest>(_GetOccupancyRequest_QNAME, GetOccupancyRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTransmissionsRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetTransmissionsRequest")
    public JAXBElement<GetTransmissionsRequest> createGetTransmissionsRequest(GetTransmissionsRequest value) {
        return new JAXBElement<GetTransmissionsRequest>(_GetTransmissionsRequest_QNAME, GetTransmissionsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Gender }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "Gender")
    public JAXBElement<Gender> createGender(Gender value) {
        return new JAXBElement<Gender>(_Gender_QNAME, Gender.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRoofConstructionRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetRoofConstructionRequest")
    public JAXBElement<GetRoofConstructionRequest> createGetRoofConstructionRequest(GetRoofConstructionRequest value) {
        return new JAXBElement<GetRoofConstructionRequest>(_GetRoofConstructionRequest_QNAME, GetRoofConstructionRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartyList }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "PartyList")
    public JAXBElement<PartyList> createPartyList(PartyList value) {
        return new JAXBElement<PartyList>(_PartyList_QNAME, PartyList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePolicyRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "CreatePolicyRequest")
    public JAXBElement<CreatePolicyRequest> createCreatePolicyRequest(CreatePolicyRequest value) {
        return new JAXBElement<CreatePolicyRequest>(_CreatePolicyRequest_QNAME, CreatePolicyRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRoofConstructionResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetRoofConstructionResponse")
    public JAXBElement<GetRoofConstructionResponse> createGetRoofConstructionResponse(GetRoofConstructionResponse value) {
        return new JAXBElement<GetRoofConstructionResponse>(_GetRoofConstructionResponse_QNAME, GetRoofConstructionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HomeQuoteDetails }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "HomeQuoteDetails")
    public JAXBElement<HomeQuoteDetails> createHomeQuoteDetails(HomeQuoteDetails value) {
        return new JAXBElement<HomeQuoteDetails>(_HomeQuoteDetails_QNAME, HomeQuoteDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUpdateFindPartyRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "AddUpdateFindPartyRequest")
    public JAXBElement<AddUpdateFindPartyRequest> createAddUpdateFindPartyRequest(AddUpdateFindPartyRequest value) {
        return new JAXBElement<AddUpdateFindPartyRequest>(_AddUpdateFindPartyRequest_QNAME, AddUpdateFindPartyRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubRiskTypeCode }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "SubRiskTypeCode")
    public JAXBElement<SubRiskTypeCode> createSubRiskTypeCode(SubRiskTypeCode value) {
        return new JAXBElement<SubRiskTypeCode>(_SubRiskTypeCode_QNAME, SubRiskTypeCode.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBuildingLayoutRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetBuildingLayoutRequest")
    public JAXBElement<GetBuildingLayoutRequest> createGetBuildingLayoutRequest(GetBuildingLayoutRequest value) {
        return new JAXBElement<GetBuildingLayoutRequest>(_GetBuildingLayoutRequest_QNAME, GetBuildingLayoutRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBuildingLayoutResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetBuildingLayoutResponse")
    public JAXBElement<GetBuildingLayoutResponse> createGetBuildingLayoutResponse(GetBuildingLayoutResponse value) {
        return new JAXBElement<GetBuildingLayoutResponse>(_GetBuildingLayoutResponse_QNAME, GetBuildingLayoutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OptionDetail }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "OptionDetail")
    public JAXBElement<OptionDetail> createOptionDetail(OptionDetail value) {
        return new JAXBElement<OptionDetail>(_OptionDetail_QNAME, OptionDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCylindersRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetCylindersRequest")
    public JAXBElement<GetCylindersRequest> createGetCylindersRequest(GetCylindersRequest value) {
        return new JAXBElement<GetCylindersRequest>(_GetCylindersRequest_QNAME, GetCylindersRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestResultList }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "TestResultList")
    public JAXBElement<TestResultList> createTestResultList(TestResultList value) {
        return new JAXBElement<TestResultList>(_TestResultList_QNAME, TestResultList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RiskTypeCode }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "RiskTypeCode")
    public JAXBElement<RiskTypeCode> createRiskTypeCode(RiskTypeCode value) {
        return new JAXBElement<RiskTypeCode>(_RiskTypeCode_QNAME, RiskTypeCode.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetExcessRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetExcessRequest")
    public JAXBElement<GetExcessRequest> createGetExcessRequest(GetExcessRequest value) {
        return new JAXBElement<GetExcessRequest>(_GetExcessRequest_QNAME, GetExcessRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SpecifiedItem }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "SpecifiedItem")
    public JAXBElement<SpecifiedItem> createSpecifiedItem(SpecifiedItem value) {
        return new JAXBElement<SpecifiedItem>(_SpecifiedItem_QNAME, SpecifiedItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTransmissionsResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetTransmissionsResponse")
    public JAXBElement<GetTransmissionsResponse> createGetTransmissionsResponse(GetTransmissionsResponse value) {
        return new JAXBElement<GetTransmissionsResponse>(_GetTransmissionsResponse_QNAME, GetTransmissionsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSuburbValidationRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetSuburbValidationRequest")
    public JAXBElement<GetSuburbValidationRequest> createGetSuburbValidationRequest(GetSuburbValidationRequest value) {
        return new JAXBElement<GetSuburbValidationRequest>(_GetSuburbValidationRequest_QNAME, GetSuburbValidationRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DriverDetails }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "DriverDetails")
    public JAXBElement<DriverDetails> createDriverDetails(DriverDetails value) {
        return new JAXBElement<DriverDetails>(_DriverDetails_QNAME, DriverDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MortgageeList }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "MortgageeList")
    public JAXBElement<MortgageeList> createMortgageeList(MortgageeList value) {
        return new JAXBElement<MortgageeList>(_MortgageeList_QNAME, MortgageeList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartyAssociationList }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "PartyAssociationList")
    public JAXBElement<PartyAssociationList> createPartyAssociationList(PartyAssociationList value) {
        return new JAXBElement<PartyAssociationList>(_PartyAssociationList_QNAME, PartyAssociationList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayByInstalmentsDetails }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "PayByInstalmentsDetails")
    public JAXBElement<PayByInstalmentsDetails> createPayByInstalmentsDetails(PayByInstalmentsDetails value) {
        return new JAXBElement<PayByInstalmentsDetails>(_PayByInstalmentsDetails_QNAME, PayByInstalmentsDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetQuoteRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetQuoteRequest")
    public JAXBElement<GetQuoteRequest> createGetQuoteRequest(GetQuoteRequest value) {
        return new JAXBElement<GetQuoteRequest>(_GetQuoteRequest_QNAME, GetQuoteRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVehicleByNvicResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetVehicleByNvicResponse")
    public JAXBElement<GetVehicleByNvicResponse> createGetVehicleByNvicResponse(GetVehicleByNvicResponse value) {
        return new JAXBElement<GetVehicleByNvicResponse>(_GetVehicleByNvicResponse_QNAME, GetVehicleByNvicResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePolicyResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "CreatePolicyResponse")
    public JAXBElement<CreatePolicyResponse> createCreatePolicyResponse(CreatePolicyResponse value) {
        return new JAXBElement<CreatePolicyResponse>(_CreatePolicyResponse_QNAME, CreatePolicyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CoverTypeCode }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "CoverTypeCode")
    public JAXBElement<CoverTypeCode> createCoverTypeCode(CoverTypeCode value) {
        return new JAXBElement<CoverTypeCode>(_CoverTypeCode_QNAME, CoverTypeCode.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHomeLocksResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetHomeLocksResponse")
    public JAXBElement<GetHomeLocksResponse> createGetHomeLocksResponse(GetHomeLocksResponse value) {
        return new JAXBElement<GetHomeLocksResponse>(_GetHomeLocksResponse_QNAME, GetHomeLocksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHomeLocksRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetHomeLocksInput.class)
    public JAXBElement<GetHomeLocksRequest> createGetHomeLocksInputRequest(GetHomeLocksRequest value) {
        return new JAXBElement<GetHomeLocksRequest>(_GetHomeLocksInputRequest_QNAME, GetHomeLocksRequest.class, GetHomeLocksInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRoofConstructionResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetRoofConstructionInputResult", scope = GetRoofConstructionInputResponse.class)
    public JAXBElement<GetRoofConstructionResponse> createGetRoofConstructionInputResponseGetRoofConstructionInputResult(GetRoofConstructionResponse value) {
        return new JAXBElement<GetRoofConstructionResponse>(_GetRoofConstructionInputResponseGetRoofConstructionInputResult_QNAME, GetRoofConstructionResponse.class, GetRoofConstructionInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetExcessRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetExcessInput.class)
    public JAXBElement<GetExcessRequest> createGetExcessInputRequest(GetExcessRequest value) {
        return new JAXBElement<GetExcessRequest>(_GetHomeLocksInputRequest_QNAME, GetExcessRequest.class, GetExcessInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVehicleMinMaxValueDataResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetVehicleMinMaxValueDataInputResult", scope = GetVehicleMinMaxValueDataInputResponse.class)
    public JAXBElement<GetVehicleMinMaxValueDataResponse> createGetVehicleMinMaxValueDataInputResponseGetVehicleMinMaxValueDataInputResult(GetVehicleMinMaxValueDataResponse value) {
        return new JAXBElement<GetVehicleMinMaxValueDataResponse>(_GetVehicleMinMaxValueDataInputResponseGetVehicleMinMaxValueDataInputResult_QNAME, GetVehicleMinMaxValueDataResponse.class, GetVehicleMinMaxValueDataInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMatchingVehiclesRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetMatchingVehiclesInput.class)
    public JAXBElement<GetMatchingVehiclesRequest> createGetMatchingVehiclesInputRequest(GetMatchingVehiclesRequest value) {
        return new JAXBElement<GetMatchingVehiclesRequest>(_GetHomeLocksInputRequest_QNAME, GetMatchingVehiclesRequest.class, GetMatchingVehiclesInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetQuoteResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetQuoteInputResult", scope = GetQuoteInputResponse.class)
    public JAXBElement<GetQuoteResponse> createGetQuoteInputResponseGetQuoteInputResult(GetQuoteResponse value) {
        return new JAXBElement<GetQuoteResponse>(_GetQuoteInputResponseGetQuoteInputResult_QNAME, GetQuoteResponse.class, GetQuoteInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOccupancyResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetOccupancyInputResult", scope = GetOccupancyInputResponse.class)
    public JAXBElement<GetOccupancyResponse> createGetOccupancyInputResponseGetOccupancyInputResult(GetOccupancyResponse value) {
        return new JAXBElement<GetOccupancyResponse>(_GetOccupancyInputResponseGetOccupancyInputResult_QNAME, GetOccupancyResponse.class, GetOccupancyInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFixedExcessResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetFixedExcessInputResult", scope = GetFixedExcessInputResponse.class)
    public JAXBElement<GetFixedExcessResponse> createGetFixedExcessInputResponseGetFixedExcessInputResult(GetFixedExcessResponse value) {
        return new JAXBElement<GetFixedExcessResponse>(_GetFixedExcessInputResponseGetFixedExcessInputResult_QNAME, GetFixedExcessResponse.class, GetFixedExcessInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFinanceTypeRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetFinanceTypeInput.class)
    public JAXBElement<GetFinanceTypeRequest> createGetFinanceTypeInputRequest(GetFinanceTypeRequest value) {
        return new JAXBElement<GetFinanceTypeRequest>(_GetHomeLocksInputRequest_QNAME, GetFinanceTypeRequest.class, GetFinanceTypeInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNoClaimDiscountRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetNoClaimDiscountInput.class)
    public JAXBElement<GetNoClaimDiscountRequest> createGetNoClaimDiscountInputRequest(GetNoClaimDiscountRequest value) {
        return new JAXBElement<GetNoClaimDiscountRequest>(_GetHomeLocksInputRequest_QNAME, GetNoClaimDiscountRequest.class, GetNoClaimDiscountInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHomeAlarmsResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetHomeAlarmsInputResult", scope = GetHomeAlarmsInputResponse.class)
    public JAXBElement<GetHomeAlarmsResponse> createGetHomeAlarmsInputResponseGetHomeAlarmsInputResult(GetHomeAlarmsResponse value) {
        return new JAXBElement<GetHomeAlarmsResponse>(_GetHomeAlarmsInputResponseGetHomeAlarmsInputResult_QNAME, GetHomeAlarmsResponse.class, GetHomeAlarmsInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetExcessResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetExcessInputResult", scope = GetExcessInputResponse.class)
    public JAXBElement<GetExcessResponse> createGetExcessInputResponseGetExcessInputResult(GetExcessResponse value) {
        return new JAXBElement<GetExcessResponse>(_GetExcessInputResponseGetExcessInputResult_QNAME, GetExcessResponse.class, GetExcessInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMinMaxInsurableResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetMinMaxInsurableInputResult", scope = GetMinMaxInsurableInputResponse.class)
    public JAXBElement<GetMinMaxInsurableResponse> createGetMinMaxInsurableInputResponseGetMinMaxInsurableInputResult(GetMinMaxInsurableResponse value) {
        return new JAXBElement<GetMinMaxInsurableResponse>(_GetMinMaxInsurableInputResponseGetMinMaxInsurableInputResult_QNAME, GetMinMaxInsurableResponse.class, GetMinMaxInsurableInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestConnectivityResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "TestConnectivityInputResult", scope = TestConnectivityInputResponse.class)
    public JAXBElement<TestConnectivityResponse> createTestConnectivityInputResponseTestConnectivityInputResult(TestConnectivityResponse value) {
        return new JAXBElement<TestConnectivityResponse>(_TestConnectivityInputResponseTestConnectivityInputResult_QNAME, TestConnectivityResponse.class, TestConnectivityInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPartyAssociationsResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetPartyAssociationsInputResult", scope = GetPartyAssociationsInputResponse.class)
    public JAXBElement<GetPartyAssociationsResponse> createGetPartyAssociationsInputResponseGetPartyAssociationsInputResult(GetPartyAssociationsResponse value) {
        return new JAXBElement<GetPartyAssociationsResponse>(_GetPartyAssociationsInputResponseGetPartyAssociationsInputResult_QNAME, GetPartyAssociationsResponse.class, GetPartyAssociationsInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MotorQuoteDetails }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "motorQuoteDetails", scope = CreatePolicyRequest.class)
    public JAXBElement<MotorQuoteDetails> createCreatePolicyRequestMotorQuoteDetails(MotorQuoteDetails value) {
        return new JAXBElement<MotorQuoteDetails>(_CreatePolicyRequestMotorQuoteDetails_QNAME, MotorQuoteDetails.class, CreatePolicyRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HomeQuoteDetails }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "homeQuoteDetails", scope = CreatePolicyRequest.class)
    public JAXBElement<HomeQuoteDetails> createCreatePolicyRequestHomeQuoteDetails(HomeQuoteDetails value) {
        return new JAXBElement<HomeQuoteDetails>(_CreatePolicyRequestHomeQuoteDetails_QNAME, HomeQuoteDetails.class, CreatePolicyRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFinancierResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetFinancierInputResult", scope = GetFinancierInputResponse.class)
    public JAXBElement<GetFinancierResponse> createGetFinancierInputResponseGetFinancierInputResult(GetFinancierResponse value) {
        return new JAXBElement<GetFinancierResponse>(_GetFinancierInputResponseGetFinancierInputResult_QNAME, GetFinancierResponse.class, GetFinancierInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFinancierRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetFinancierInput.class)
    public JAXBElement<GetFinancierRequest> createGetFinancierInputRequest(GetFinancierRequest value) {
        return new JAXBElement<GetFinancierRequest>(_GetHomeLocksInputRequest_QNAME, GetFinancierRequest.class, GetFinancierInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDrivingFrequencyRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetDrivingFrequencyInput.class)
    public JAXBElement<GetDrivingFrequencyRequest> createGetDrivingFrequencyInputRequest(GetDrivingFrequencyRequest value) {
        return new JAXBElement<GetDrivingFrequencyRequest>(_GetHomeLocksInputRequest_QNAME, GetDrivingFrequencyRequest.class, GetDrivingFrequencyInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWallConstructionResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetWallConstructionInputResult", scope = GetWallConstructionInputResponse.class)
    public JAXBElement<GetWallConstructionResponse> createGetWallConstructionInputResponseGetWallConstructionInputResult(GetWallConstructionResponse value) {
        return new JAXBElement<GetWallConstructionResponse>(_GetWallConstructionInputResponseGetWallConstructionInputResult_QNAME, GetWallConstructionResponse.class, GetWallConstructionInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCylindersRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetCylindersInput.class)
    public JAXBElement<GetCylindersRequest> createGetCylindersInputRequest(GetCylindersRequest value) {
        return new JAXBElement<GetCylindersRequest>(_GetHomeLocksInputRequest_QNAME, GetCylindersRequest.class, GetCylindersInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestConnectivityRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = TestConnectivityInput.class)
    public JAXBElement<TestConnectivityRequest> createTestConnectivityInputRequest(TestConnectivityRequest value) {
        return new JAXBElement<TestConnectivityRequest>(_GetHomeLocksInputRequest_QNAME, TestConnectivityRequest.class, TestConnectivityInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWallConstructionRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetWallConstructionInput.class)
    public JAXBElement<GetWallConstructionRequest> createGetWallConstructionInputRequest(GetWallConstructionRequest value) {
        return new JAXBElement<GetWallConstructionRequest>(_GetHomeLocksInputRequest_QNAME, GetWallConstructionRequest.class, GetWallConstructionInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePolicyRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = CreatePolicyInput.class)
    public JAXBElement<CreatePolicyRequest> createCreatePolicyInputRequest(CreatePolicyRequest value) {
        return new JAXBElement<CreatePolicyRequest>(_GetHomeLocksInputRequest_QNAME, CreatePolicyRequest.class, CreatePolicyInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOptionsRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetOptionsInput.class)
    public JAXBElement<GetOptionsRequest> createGetOptionsInputRequest(GetOptionsRequest value) {
        return new JAXBElement<GetOptionsRequest>(_GetHomeLocksInputRequest_QNAME, GetOptionsRequest.class, GetOptionsInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPartyAssociationsRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetPartyAssociationsInput.class)
    public JAXBElement<GetPartyAssociationsRequest> createGetPartyAssociationsInputRequest(GetPartyAssociationsRequest value) {
        return new JAXBElement<GetPartyAssociationsRequest>(_GetHomeLocksInputRequest_QNAME, GetPartyAssociationsRequest.class, GetPartyAssociationsInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMinMaxInsurableRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetMinMaxInsurableInput.class)
    public JAXBElement<GetMinMaxInsurableRequest> createGetMinMaxInsurableInputRequest(GetMinMaxInsurableRequest value) {
        return new JAXBElement<GetMinMaxInsurableRequest>(_GetHomeLocksInputRequest_QNAME, GetMinMaxInsurableRequest.class, GetMinMaxInsurableInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMakesResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetMakesInputResult", scope = GetMakesInputResponse.class)
    public JAXBElement<GetMakesResponse> createGetMakesInputResponseGetMakesInputResult(GetMakesResponse value) {
        return new JAXBElement<GetMakesResponse>(_GetMakesInputResponseGetMakesInputResult_QNAME, GetMakesResponse.class, GetMakesInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSuburbValidationRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetSuburbValidationInput.class)
    public JAXBElement<GetSuburbValidationRequest> createGetSuburbValidationInputRequest(GetSuburbValidationRequest value) {
        return new JAXBElement<GetSuburbValidationRequest>(_GetHomeLocksInputRequest_QNAME, GetSuburbValidationRequest.class, GetSuburbValidationInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVehicleMinMaxValueDataRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetVehicleMinMaxValueDataInput.class)
    public JAXBElement<GetVehicleMinMaxValueDataRequest> createGetVehicleMinMaxValueDataInputRequest(GetVehicleMinMaxValueDataRequest value) {
        return new JAXBElement<GetVehicleMinMaxValueDataRequest>(_GetHomeLocksInputRequest_QNAME, GetVehicleMinMaxValueDataRequest.class, GetVehicleMinMaxValueDataInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFinanceTypeResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetFinanceTypeInputResult", scope = GetFinanceTypeInputResponse.class)
    public JAXBElement<GetFinanceTypeResponse> createGetFinanceTypeInputResponseGetFinanceTypeInputResult(GetFinanceTypeResponse value) {
        return new JAXBElement<GetFinanceTypeResponse>(_GetFinanceTypeInputResponseGetFinanceTypeInputResult_QNAME, GetFinanceTypeResponse.class, GetFinanceTypeInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "ractId", scope = AddUpdateFindPartyRequest.class)
    public JAXBElement<String> createAddUpdateFindPartyRequestRactId(String value) {
        return new JAXBElement<String>(_AddUpdateFindPartyRequestRactId_QNAME, String.class, AddUpdateFindPartyRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "residentialAddress", scope = AddUpdateFindPartyRequest.class)
    public JAXBElement<Address> createAddUpdateFindPartyRequestResidentialAddress(Address value) {
        return new JAXBElement<Address>(_AddUpdateFindPartyRequestResidentialAddress_QNAME, Address.class, AddUpdateFindPartyRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateAgentRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = ValidateAgentInput.class)
    public JAXBElement<ValidateAgentRequest> createValidateAgentInputRequest(ValidateAgentRequest value) {
        return new JAXBElement<ValidateAgentRequest>(_GetHomeLocksInputRequest_QNAME, ValidateAgentRequest.class, ValidateAgentInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSuburbValidationResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetSuburbValidationInputResult", scope = GetSuburbValidationInputResponse.class)
    public JAXBElement<GetSuburbValidationResponse> createGetSuburbValidationInputResponseGetSuburbValidationInputResult(GetSuburbValidationResponse value) {
        return new JAXBElement<GetSuburbValidationResponse>(_GetSuburbValidationInputResponseGetSuburbValidationInputResult_QNAME, GetSuburbValidationResponse.class, GetSuburbValidationInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "criminalOffenceDetails", scope = HomeQuoteDetails.class)
    public JAXBElement<String> createHomeQuoteDetailsCriminalOffenceDetails(String value) {
        return new JAXBElement<String>(_HomeQuoteDetailsCriminalOffenceDetails_QNAME, String.class, HomeQuoteDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUpdateFindPartyRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = AddUpdateFindPartyInput.class)
    public JAXBElement<AddUpdateFindPartyRequest> createAddUpdateFindPartyInputRequest(AddUpdateFindPartyRequest value) {
        return new JAXBElement<AddUpdateFindPartyRequest>(_GetHomeLocksInputRequest_QNAME, AddUpdateFindPartyRequest.class, AddUpdateFindPartyInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePolicyResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "CreatePolicyInputResult", scope = CreatePolicyInputResponse.class)
    public JAXBElement<CreatePolicyResponse> createCreatePolicyInputResponseCreatePolicyInputResult(CreatePolicyResponse value) {
        return new JAXBElement<CreatePolicyResponse>(_CreatePolicyInputResponseCreatePolicyInputResult_QNAME, CreatePolicyResponse.class, CreatePolicyInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVehicleByNvicResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetVehicleByNvicInputResult", scope = GetVehicleByNvicInputResponse.class)
    public JAXBElement<GetVehicleByNvicResponse> createGetVehicleByNvicInputResponseGetVehicleByNvicInputResult(GetVehicleByNvicResponse value) {
        return new JAXBElement<GetVehicleByNvicResponse>(_GetVehicleByNvicInputResponseGetVehicleByNvicInputResult_QNAME, GetVehicleByNvicResponse.class, GetVehicleByNvicInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCylindersResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetCylindersInputResult", scope = GetCylindersInputResponse.class)
    public JAXBElement<GetCylindersResponse> createGetCylindersInputResponseGetCylindersInputResult(GetCylindersResponse value) {
        return new JAXBElement<GetCylindersResponse>(_GetCylindersInputResponseGetCylindersInputResult_QNAME, GetCylindersResponse.class, GetCylindersInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHomeContentsCategoriesRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetHomeContentsCategoriesInput.class)
    public JAXBElement<GetHomeContentsCategoriesRequest> createGetHomeContentsCategoriesInputRequest(GetHomeContentsCategoriesRequest value) {
        return new JAXBElement<GetHomeContentsCategoriesRequest>(_GetHomeLocksInputRequest_QNAME, GetHomeContentsCategoriesRequest.class, GetHomeContentsCategoriesInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRoofConstructionRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetRoofConstructionInput.class)
    public JAXBElement<GetRoofConstructionRequest> createGetRoofConstructionInputRequest(GetRoofConstructionRequest value) {
        return new JAXBElement<GetRoofConstructionRequest>(_GetHomeLocksInputRequest_QNAME, GetRoofConstructionRequest.class, GetRoofConstructionInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFamiliesResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetFamiliesInputResult", scope = GetFamiliesInputResponse.class)
    public JAXBElement<GetFamiliesResponse> createGetFamiliesInputResponseGetFamiliesInputResult(GetFamiliesResponse value) {
        return new JAXBElement<GetFamiliesResponse>(_GetFamiliesInputResponseGetFamiliesInputResult_QNAME, GetFamiliesResponse.class, GetFamiliesInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVehicleUsageRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetVehicleUsageInput.class)
    public JAXBElement<GetVehicleUsageRequest> createGetVehicleUsageInputRequest(GetVehicleUsageRequest value) {
        return new JAXBElement<GetVehicleUsageRequest>(_GetHomeLocksInputRequest_QNAME, GetVehicleUsageRequest.class, GetVehicleUsageInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFamiliesRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetFamiliesInput.class)
    public JAXBElement<GetFamiliesRequest> createGetFamiliesInputRequest(GetFamiliesRequest value) {
        return new JAXBElement<GetFamiliesRequest>(_GetHomeLocksInputRequest_QNAME, GetFamiliesRequest.class, GetFamiliesInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMatchingVehiclesResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetMatchingVehiclesInputResult", scope = GetMatchingVehiclesInputResponse.class)
    public JAXBElement<GetMatchingVehiclesResponse> createGetMatchingVehiclesInputResponseGetMatchingVehiclesInputResult(GetMatchingVehiclesResponse value) {
        return new JAXBElement<GetMatchingVehiclesResponse>(_GetMatchingVehiclesInputResponseGetMatchingVehiclesInputResult_QNAME, GetMatchingVehiclesResponse.class, GetMatchingVehiclesInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBuildingLayoutRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetBuildingLayoutInput.class)
    public JAXBElement<GetBuildingLayoutRequest> createGetBuildingLayoutInputRequest(GetBuildingLayoutRequest value) {
        return new JAXBElement<GetBuildingLayoutRequest>(_GetHomeLocksInputRequest_QNAME, GetBuildingLayoutRequest.class, GetBuildingLayoutInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateAgentResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "ValidateAgentInputResult", scope = ValidateAgentInputResponse.class)
    public JAXBElement<ValidateAgentResponse> createValidateAgentInputResponseValidateAgentInputResult(ValidateAgentResponse value) {
        return new JAXBElement<ValidateAgentResponse>(_ValidateAgentInputResponseValidateAgentInputResult_QNAME, ValidateAgentResponse.class, ValidateAgentInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "propertyName", scope = Address.class)
    public JAXBElement<String> createAddressPropertyName(String value) {
        return new JAXBElement<String>(_AddressPropertyName_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "gnafId", scope = Address.class)
    public JAXBElement<String> createAddressGnafId(String value) {
        return new JAXBElement<String>(_AddressGnafId_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "dpid", scope = Address.class)
    public JAXBElement<String> createAddressDpid(String value) {
        return new JAXBElement<String>(_AddressDpid_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "longitude", scope = Address.class)
    public JAXBElement<String> createAddressLongitude(String value) {
        return new JAXBElement<String>(_AddressLongitude_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "latitude", scope = Address.class)
    public JAXBElement<String> createAddressLatitude(String value) {
        return new JAXBElement<String>(_AddressLatitude_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "ausbar", scope = Address.class)
    public JAXBElement<String> createAddressAusbar(String value) {
        return new JAXBElement<String>(_AddressAusbar_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "additionalInformation", scope = TestResult.class)
    public JAXBElement<String> createTestResultAdditionalInformation(String value) {
        return new JAXBElement<String>(_TestResultAdditionalInformation_QNAME, String.class, TestResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "residentialAddress", scope = Party.class)
    public JAXBElement<Address> createPartyResidentialAddress(Address value) {
        return new JAXBElement<Address>(_AddUpdateFindPartyRequestResidentialAddress_QNAME, Address.class, Party.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVehicleUsageResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetVehicleUsageInputResult", scope = GetVehicleUsageInputResponse.class)
    public JAXBElement<GetVehicleUsageResponse> createGetVehicleUsageInputResponseGetVehicleUsageInputResult(GetVehicleUsageResponse value) {
        return new JAXBElement<GetVehicleUsageResponse>(_GetVehicleUsageInputResponseGetVehicleUsageInputResult_QNAME, GetVehicleUsageResponse.class, GetVehicleUsageInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBusinessUsageRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetBusinessUsageInput.class)
    public JAXBElement<GetBusinessUsageRequest> createGetBusinessUsageInputRequest(GetBusinessUsageRequest value) {
        return new JAXBElement<GetBusinessUsageRequest>(_GetHomeLocksInputRequest_QNAME, GetBusinessUsageRequest.class, GetBusinessUsageInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBuildingLayoutResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetBuildingLayoutInputResult", scope = GetBuildingLayoutInputResponse.class)
    public JAXBElement<GetBuildingLayoutResponse> createGetBuildingLayoutInputResponseGetBuildingLayoutInputResult(GetBuildingLayoutResponse value) {
        return new JAXBElement<GetBuildingLayoutResponse>(_GetBuildingLayoutInputResponseGetBuildingLayoutInputResult_QNAME, GetBuildingLayoutResponse.class, GetBuildingLayoutInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStylesRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetStylesInput.class)
    public JAXBElement<GetStylesRequest> createGetStylesInputRequest(GetStylesRequest value) {
        return new JAXBElement<GetStylesRequest>(_GetHomeLocksInputRequest_QNAME, GetStylesRequest.class, GetStylesInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MotorQuoteDetails }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "motorQuoteDetails", scope = GetQuoteRequest.class)
    public JAXBElement<MotorQuoteDetails> createGetQuoteRequestMotorQuoteDetails(MotorQuoteDetails value) {
        return new JAXBElement<MotorQuoteDetails>(_CreatePolicyRequestMotorQuoteDetails_QNAME, MotorQuoteDetails.class, GetQuoteRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HomeQuoteDetails }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "homeQuoteDetails", scope = GetQuoteRequest.class)
    public JAXBElement<HomeQuoteDetails> createGetQuoteRequestHomeQuoteDetails(HomeQuoteDetails value) {
        return new JAXBElement<HomeQuoteDetails>(_CreatePolicyRequestHomeQuoteDetails_QNAME, HomeQuoteDetails.class, GetQuoteRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTransmissionsResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetTransmissionsInputResult", scope = GetTransmissionsInputResponse.class)
    public JAXBElement<GetTransmissionsResponse> createGetTransmissionsInputResponseGetTransmissionsInputResult(GetTransmissionsResponse value) {
        return new JAXBElement<GetTransmissionsResponse>(_GetTransmissionsInputResponseGetTransmissionsInputResult_QNAME, GetTransmissionsResponse.class, GetTransmissionsInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetQuoteRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetQuoteInput.class)
    public JAXBElement<GetQuoteRequest> createGetQuoteInputRequest(GetQuoteRequest value) {
        return new JAXBElement<GetQuoteRequest>(_GetHomeLocksInputRequest_QNAME, GetQuoteRequest.class, GetQuoteInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDrivingFrequencyResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetDrivingFrequencyInputResult", scope = GetDrivingFrequencyInputResponse.class)
    public JAXBElement<GetDrivingFrequencyResponse> createGetDrivingFrequencyInputResponseGetDrivingFrequencyInputResult(GetDrivingFrequencyResponse value) {
        return new JAXBElement<GetDrivingFrequencyResponse>(_GetDrivingFrequencyInputResponseGetDrivingFrequencyInputResult_QNAME, GetDrivingFrequencyResponse.class, GetDrivingFrequencyInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUpdateFindPartyResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "AddUpdateFindPartyInputResult", scope = AddUpdateFindPartyInputResponse.class)
    public JAXBElement<AddUpdateFindPartyResponse> createAddUpdateFindPartyInputResponseAddUpdateFindPartyInputResult(AddUpdateFindPartyResponse value) {
        return new JAXBElement<AddUpdateFindPartyResponse>(_AddUpdateFindPartyInputResponseAddUpdateFindPartyInputResult_QNAME, AddUpdateFindPartyResponse.class, AddUpdateFindPartyInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankDetails }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "bankDetails", scope = PayByInstalmentsDetails.class)
    public JAXBElement<BankDetails> createPayByInstalmentsDetailsBankDetails(BankDetails value) {
        return new JAXBElement<BankDetails>(_PayByInstalmentsDetailsBankDetails_QNAME, BankDetails.class, PayByInstalmentsDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCardDetails }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "creditCardDetails", scope = PayByInstalmentsDetails.class)
    public JAXBElement<CreditCardDetails> createPayByInstalmentsDetailsCreditCardDetails(CreditCardDetails value) {
        return new JAXBElement<CreditCardDetails>(_PayByInstalmentsDetailsCreditCardDetails_QNAME, CreditCardDetails.class, PayByInstalmentsDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBusinessUsageResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetBusinessUsageInputResult", scope = GetBusinessUsageInputResponse.class)
    public JAXBElement<GetBusinessUsageResponse> createGetBusinessUsageInputResponseGetBusinessUsageInputResult(GetBusinessUsageResponse value) {
        return new JAXBElement<GetBusinessUsageResponse>(_GetBusinessUsageInputResponseGetBusinessUsageInputResult_QNAME, GetBusinessUsageResponse.class, GetBusinessUsageInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMakesRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetMakesInput.class)
    public JAXBElement<GetMakesRequest> createGetMakesInputRequest(GetMakesRequest value) {
        return new JAXBElement<GetMakesRequest>(_GetHomeLocksInputRequest_QNAME, GetMakesRequest.class, GetMakesInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCardDetails }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "payNow", scope = PaymentMethod.class)
    public JAXBElement<CreditCardDetails> createPaymentMethodPayNow(CreditCardDetails value) {
        return new JAXBElement<CreditCardDetails>(_PaymentMethodPayNow_QNAME, CreditCardDetails.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayByInstalmentsDetails }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "payByInstalments", scope = PaymentMethod.class)
    public JAXBElement<PayByInstalmentsDetails> createPaymentMethodPayByInstalments(PayByInstalmentsDetails value) {
        return new JAXBElement<PayByInstalmentsDetails>(_PaymentMethodPayByInstalments_QNAME, PayByInstalmentsDetails.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "acceptableGoodOrderAndRepairCode", scope = MotorQuoteDetails.class)
    public JAXBElement<String> createMotorQuoteDetailsAcceptableGoodOrderAndRepairCode(String value) {
        return new JAXBElement<String>(_MotorQuoteDetailsAcceptableGoodOrderAndRepairCode_QNAME, String.class, MotorQuoteDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "financier", scope = MotorQuoteDetails.class)
    public JAXBElement<String> createMotorQuoteDetailsFinancier(String value) {
        return new JAXBElement<String>(_MotorQuoteDetailsFinancier_QNAME, String.class, MotorQuoteDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "additionalInformation", scope = RactError.class)
    public JAXBElement<String> createRactErrorAdditionalInformation(String value) {
        return new JAXBElement<String>(_TestResultAdditionalInformation_QNAME, String.class, RactError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOptionsResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetOptionsInputResult", scope = GetOptionsInputResponse.class)
    public JAXBElement<GetOptionsResponse> createGetOptionsInputResponseGetOptionsInputResult(GetOptionsResponse value) {
        return new JAXBElement<GetOptionsResponse>(_GetOptionsInputResponseGetOptionsInputResult_QNAME, GetOptionsResponse.class, GetOptionsInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOccupancyRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetOccupancyInput.class)
    public JAXBElement<GetOccupancyRequest> createGetOccupancyInputRequest(GetOccupancyRequest value) {
        return new JAXBElement<GetOccupancyRequest>(_GetHomeLocksInputRequest_QNAME, GetOccupancyRequest.class, GetOccupancyInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVehicleByNvicRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetVehicleByNvicInput.class)
    public JAXBElement<GetVehicleByNvicRequest> createGetVehicleByNvicInputRequest(GetVehicleByNvicRequest value) {
        return new JAXBElement<GetVehicleByNvicRequest>(_GetHomeLocksInputRequest_QNAME, GetVehicleByNvicRequest.class, GetVehicleByNvicInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTransmissionsRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetTransmissionsInput.class)
    public JAXBElement<GetTransmissionsRequest> createGetTransmissionsInputRequest(GetTransmissionsRequest value) {
        return new JAXBElement<GetTransmissionsRequest>(_GetHomeLocksInputRequest_QNAME, GetTransmissionsRequest.class, GetTransmissionsInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFixedExcessRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetFixedExcessInput.class)
    public JAXBElement<GetFixedExcessRequest> createGetFixedExcessInputRequest(GetFixedExcessRequest value) {
        return new JAXBElement<GetFixedExcessRequest>(_GetHomeLocksInputRequest_QNAME, GetFixedExcessRequest.class, GetFixedExcessInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHomeAlarmsRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetHomeAlarmsInput.class)
    public JAXBElement<GetHomeAlarmsRequest> createGetHomeAlarmsInputRequest(GetHomeAlarmsRequest value) {
        return new JAXBElement<GetHomeAlarmsRequest>(_GetHomeLocksInputRequest_QNAME, GetHomeAlarmsRequest.class, GetHomeAlarmsInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNoClaimDiscountResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetNoClaimDiscountInputResult", scope = GetNoClaimDiscountInputResponse.class)
    public JAXBElement<GetNoClaimDiscountResponse> createGetNoClaimDiscountInputResponseGetNoClaimDiscountInputResult(GetNoClaimDiscountResponse value) {
        return new JAXBElement<GetNoClaimDiscountResponse>(_GetNoClaimDiscountInputResponseGetNoClaimDiscountInputResult_QNAME, GetNoClaimDiscountResponse.class, GetNoClaimDiscountInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVehicleValueResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetVehicleValueInputResult", scope = GetVehicleValueInputResponse.class)
    public JAXBElement<GetVehicleValueResponse> createGetVehicleValueInputResponseGetVehicleValueInputResult(GetVehicleValueResponse value) {
        return new JAXBElement<GetVehicleValueResponse>(_GetVehicleValueInputResponseGetVehicleValueInputResult_QNAME, GetVehicleValueResponse.class, GetVehicleValueInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "family", scope = GetMatchingVehiclesRequest.class)
    public JAXBElement<String> createGetMatchingVehiclesRequestFamily(String value) {
        return new JAXBElement<String>(_GetMatchingVehiclesRequestFamily_QNAME, String.class, GetMatchingVehiclesRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "cylinder", scope = GetMatchingVehiclesRequest.class)
    public JAXBElement<String> createGetMatchingVehiclesRequestCylinder(String value) {
        return new JAXBElement<String>(_GetMatchingVehiclesRequestCylinder_QNAME, String.class, GetMatchingVehiclesRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "style", scope = GetMatchingVehiclesRequest.class)
    public JAXBElement<String> createGetMatchingVehiclesRequestStyle(String value) {
        return new JAXBElement<String>(_GetMatchingVehiclesRequestStyle_QNAME, String.class, GetMatchingVehiclesRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "transmission", scope = GetMatchingVehiclesRequest.class)
    public JAXBElement<String> createGetMatchingVehiclesRequestTransmission(String value) {
        return new JAXBElement<String>(_GetMatchingVehiclesRequestTransmission_QNAME, String.class, GetMatchingVehiclesRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHomeContentsCategoriesResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetHomeContentsCategoriesInputResult", scope = GetHomeContentsCategoriesInputResponse.class)
    public JAXBElement<GetHomeContentsCategoriesResponse> createGetHomeContentsCategoriesInputResponseGetHomeContentsCategoriesInputResult(GetHomeContentsCategoriesResponse value) {
        return new JAXBElement<GetHomeContentsCategoriesResponse>(_GetHomeContentsCategoriesInputResponseGetHomeContentsCategoriesInputResult_QNAME, GetHomeContentsCategoriesResponse.class, GetHomeContentsCategoriesInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHomeLocksResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetHomeLocksInputResult", scope = GetHomeLocksInputResponse.class)
    public JAXBElement<GetHomeLocksResponse> createGetHomeLocksInputResponseGetHomeLocksInputResult(GetHomeLocksResponse value) {
        return new JAXBElement<GetHomeLocksResponse>(_GetHomeLocksInputResponseGetHomeLocksInputResult_QNAME, GetHomeLocksResponse.class, GetHomeLocksInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlassesVehicle }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "vehicle", scope = GetVehicleByNvicResponse.class)
    public JAXBElement<GlassesVehicle> createGetVehicleByNvicResponseVehicle(GlassesVehicle value) {
        return new JAXBElement<GlassesVehicle>(_GetVehicleByNvicResponseVehicle_QNAME, GlassesVehicle.class, GetVehicleByNvicResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStylesResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "GetStylesInputResult", scope = GetStylesInputResponse.class)
    public JAXBElement<GetStylesResponse> createGetStylesInputResponseGetStylesInputResult(GetStylesResponse value) {
        return new JAXBElement<GetStylesResponse>(_GetStylesInputResponseGetStylesInputResult_QNAME, GetStylesResponse.class, GetStylesInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVehicleValueRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/RactPureLink", name = "request", scope = GetVehicleValueInput.class)
    public JAXBElement<GetVehicleValueRequest> createGetVehicleValueInputRequest(GetVehicleValueRequest value) {
        return new JAXBElement<GetVehicleValueRequest>(_GetHomeLocksInputRequest_QNAME, GetVehicleValueRequest.class, GetVehicleValueInput.class, value);
    }

}
