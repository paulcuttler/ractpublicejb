package com.ract.purelink.webservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetHomeContentsCategoriesResponse complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetHomeContentsCategoriesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="homeContentsCategory" type="{http://www.ract.com.au/RactPureLink}UdlRecord" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetHomeContentsCategoriesResponse", propOrder = { "homeContentsCategory" })
public class GetHomeContentsCategoriesResponse {

    @XmlElement(nillable = true)
    protected List<UdlRecord> homeContentsCategory;

    /**
     * Gets the value of the homeContentsCategory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the homeContentsCategory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getHomeContentsCategory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link UdlRecord }
     * 
     * 
     */
    public List<UdlRecord> getHomeContentsCategory() {
        if (homeContentsCategory == null) {
            homeContentsCategory = new ArrayList<UdlRecord>();
        }
        return this.homeContentsCategory;
    }

}
