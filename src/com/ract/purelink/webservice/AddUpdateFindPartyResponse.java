package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for AddUpdateFindPartyResponse complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddUpdateFindPartyResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="partyId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ractId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="resolvedName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddUpdateFindPartyResponse", propOrder = { "partyId", "ractId", "resolvedName" })
public class AddUpdateFindPartyResponse {

    protected int partyId;
    @XmlElement(required = true, nillable = true)
    protected String ractId;
    @XmlElement(required = true, nillable = true)
    protected String resolvedName;

    /**
     * Gets the value of the partyId property.
     * 
     */
    public int getPartyId() {
        return partyId;
    }

    /**
     * Sets the value of the partyId property.
     * 
     */
    public void setPartyId(int value) {
        this.partyId = value;
    }

    /**
     * Gets the value of the ractId property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getRactId() {
        return ractId;
    }

    /**
     * Sets the value of the ractId property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setRactId(String value) {
        this.ractId = value;
    }

    /**
     * Gets the value of the resolvedName property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getResolvedName() {
        return resolvedName;
    }

    /**
     * Sets the value of the resolvedName property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setResolvedName(String value) {
        this.resolvedName = value;
    }

}
