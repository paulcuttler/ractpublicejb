package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for Address complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Address">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="propertyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="streetNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="streetName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="suburb" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="state" type="{http://www.ract.com.au/RactPureLink}State"/>
 *         &lt;element name="postcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="postcodeIsTasmanian" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="latitude" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="longitude" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="gnafId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dpid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ausbar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Address", propOrder = { "propertyName", "streetNumber", "streetName", "suburb", "state", "postcode", "country", "postcodeIsTasmanian", "latitude", "longitude", "gnafId", "dpid", "ausbar" })
public class Address {

    @XmlElementRef(name = "propertyName", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<String> propertyName;
    @XmlElement(required = true, nillable = true)
    protected String streetNumber;
    @XmlElement(required = true, nillable = true)
    protected String streetName;
    @XmlElement(required = true, nillable = true)
    protected String suburb;
    @XmlElement(required = true)
    protected State state;
    @XmlElement(required = true, nillable = true)
    protected String postcode;
    @XmlElement(required = true, nillable = true)
    protected String country;
    protected boolean postcodeIsTasmanian;
    @XmlElementRef(name = "latitude", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<String> latitude;
    @XmlElementRef(name = "longitude", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<String> longitude;
    @XmlElementRef(name = "gnafId", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<String> gnafId;
    @XmlElementRef(name = "dpid", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<String> dpid;
    @XmlElementRef(name = "ausbar", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<String> ausbar;

    /**
     * Gets the value of the propertyName property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public JAXBElement<String> getPropertyName() {
        return propertyName;
    }

    /**
     * Sets the value of the propertyName property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public void setPropertyName(JAXBElement<String> value) {
        this.propertyName = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the streetNumber property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getStreetNumber() {
        return streetNumber;
    }

    /**
     * Sets the value of the streetNumber property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setStreetNumber(String value) {
        this.streetNumber = value;
    }

    /**
     * Gets the value of the streetName property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getStreetName() {
        return streetName;
    }

    /**
     * Sets the value of the streetName property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setStreetName(String value) {
        this.streetName = value;
    }

    /**
     * Gets the value of the suburb property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getSuburb() {
        return suburb;
    }

    /**
     * Sets the value of the suburb property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setSuburb(String value) {
        this.suburb = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return possible object is {@link State }
     * 
     */
    public State getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *            allowed object is {@link State }
     * 
     */
    public void setState(State value) {
        this.state = value;
    }

    /**
     * Gets the value of the postcode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * Sets the value of the postcode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setPostcode(String value) {
        this.postcode = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the postcodeIsTasmanian property.
     * 
     */
    public boolean isPostcodeIsTasmanian() {
        return postcodeIsTasmanian;
    }

    /**
     * Sets the value of the postcodeIsTasmanian property.
     * 
     */
    public void setPostcodeIsTasmanian(boolean value) {
        this.postcodeIsTasmanian = value;
    }

    /**
     * Gets the value of the latitude property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public JAXBElement<String> getLatitude() {
        return latitude;
    }

    /**
     * Sets the value of the latitude property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public void setLatitude(JAXBElement<String> value) {
        this.latitude = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the longitude property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public JAXBElement<String> getLongitude() {
        return longitude;
    }

    /**
     * Sets the value of the longitude property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public void setLongitude(JAXBElement<String> value) {
        this.longitude = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the gnafId property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public JAXBElement<String> getGnafId() {
        return gnafId;
    }

    /**
     * Sets the value of the gnafId property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public void setGnafId(JAXBElement<String> value) {
        this.gnafId = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the dpid property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public JAXBElement<String> getDpid() {
        return dpid;
    }

    /**
     * Sets the value of the dpid property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public void setDpid(JAXBElement<String> value) {
        this.dpid = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the ausbar property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public JAXBElement<String> getAusbar() {
        return ausbar;
    }

    /**
     * Sets the value of the ausbar property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public void setAusbar(JAXBElement<String> value) {
        this.ausbar = ((JAXBElement<String>) value);
    }

}
