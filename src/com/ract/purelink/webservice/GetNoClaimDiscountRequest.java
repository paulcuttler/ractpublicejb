package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Java class for GetNoClaimDiscountRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetNoClaimDiscountRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="yearStartedDriving" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numberOfAccidents" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="queryDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetNoClaimDiscountRequest", propOrder = { "yearStartedDriving", "numberOfAccidents", "queryDate" })
public class GetNoClaimDiscountRequest {

    protected int yearStartedDriving;
    protected int numberOfAccidents;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar queryDate;

    /**
     * Gets the value of the yearStartedDriving property.
     * 
     */
    public int getYearStartedDriving() {
        return yearStartedDriving;
    }

    /**
     * Sets the value of the yearStartedDriving property.
     * 
     */
    public void setYearStartedDriving(int value) {
        this.yearStartedDriving = value;
    }

    /**
     * Gets the value of the numberOfAccidents property.
     * 
     */
    public int getNumberOfAccidents() {
        return numberOfAccidents;
    }

    /**
     * Sets the value of the numberOfAccidents property.
     * 
     */
    public void setNumberOfAccidents(int value) {
        this.numberOfAccidents = value;
    }

    /**
     * Gets the value of the queryDate property.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getQueryDate() {
        return queryDate;
    }

    /**
     * Sets the value of the queryDate property.
     * 
     * @param value
     *            allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setQueryDate(XMLGregorianCalendar value) {
        this.queryDate = value;
    }

}
