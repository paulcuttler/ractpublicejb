package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SubRiskTypeCode.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="SubRiskTypeCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="HOME"/>
 *     &lt;enumeration value="BINV"/>
 *     &lt;enumeration value="PINV"/>
 *     &lt;enumeration value="STRT"/>
 *     &lt;enumeration value="COMC"/>
 *     &lt;enumeration value="TPPD"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SubRiskTypeCode")
@XmlEnum
public enum SubRiskTypeCode {

    HOME, BINV, PINV, STRT, COMC, TPPD;

    public String value() {
        return name();
    }

    public static SubRiskTypeCode fromValue(String v) {
        return valueOf(v);
    }

}
