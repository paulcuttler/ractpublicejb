package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TestResult complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TestResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="testCase" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="testOutput" type="{http://www.ract.com.au/RactPureLink}TestOutput"/>
 *         &lt;element name="additionalInformation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TestResult", propOrder = { "testCase", "testOutput", "additionalInformation" })
public class TestResult {

    @XmlElement(required = true, nillable = true)
    protected String testCase;
    @XmlElement(required = true)
    protected TestOutput testOutput;
    @XmlElementRef(name = "additionalInformation", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<String> additionalInformation;

    /**
     * Gets the value of the testCase property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getTestCase() {
        return testCase;
    }

    /**
     * Sets the value of the testCase property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setTestCase(String value) {
        this.testCase = value;
    }

    /**
     * Gets the value of the testOutput property.
     * 
     * @return possible object is {@link TestOutput }
     * 
     */
    public TestOutput getTestOutput() {
        return testOutput;
    }

    /**
     * Sets the value of the testOutput property.
     * 
     * @param value
     *            allowed object is {@link TestOutput }
     * 
     */
    public void setTestOutput(TestOutput value) {
        this.testOutput = value;
    }

    /**
     * Gets the value of the additionalInformation property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public JAXBElement<String> getAdditionalInformation() {
        return additionalInformation;
    }

    /**
     * Sets the value of the additionalInformation property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public void setAdditionalInformation(JAXBElement<String> value) {
        this.additionalInformation = ((JAXBElement<String>) value);
    }

}
