package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TestConnectivityResponse complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TestConnectivityResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="testResults" type="{http://www.ract.com.au/RactPureLink}TestResultList"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TestConnectivityResponse", propOrder = { "testResults" })
public class TestConnectivityResponse {

    @XmlElement(required = true, nillable = true)
    protected TestResultList testResults;

    /**
     * Gets the value of the testResults property.
     * 
     * @return possible object is {@link TestResultList }
     * 
     */
    public TestResultList getTestResults() {
        return testResults;
    }

    /**
     * Sets the value of the testResults property.
     * 
     * @param value
     *            allowed object is {@link TestResultList }
     * 
     */
    public void setTestResults(TestResultList value) {
        this.testResults = value;
    }

}
