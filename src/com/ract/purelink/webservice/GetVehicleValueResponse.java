package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetVehicleValueResponse complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetVehicleValueResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vehicleValue" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetVehicleValueResponse", propOrder = { "vehicleValue" })
public class GetVehicleValueResponse {

    protected int vehicleValue;

    /**
     * Gets the value of the vehicleValue property.
     * 
     */
    public int getVehicleValue() {
        return vehicleValue;
    }

    /**
     * Sets the value of the vehicleValue property.
     * 
     */
    public void setVehicleValue(int value) {
        this.vehicleValue = value;
    }

}
