package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TestConnectivityRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TestConnectivityRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="testInternalDependencies" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TestConnectivityRequest", propOrder = { "testInternalDependencies" })
public class TestConnectivityRequest {

    protected boolean testInternalDependencies;

    /**
     * Gets the value of the testInternalDependencies property.
     * 
     */
    public boolean isTestInternalDependencies() {
        return testInternalDependencies;
    }

    /**
     * Sets the value of the testInternalDependencies property.
     * 
     */
    public void setTestInternalDependencies(boolean value) {
        this.testInternalDependencies = value;
    }

}
