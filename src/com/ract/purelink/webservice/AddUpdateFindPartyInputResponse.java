package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddUpdateFindPartyInputResult" type="{http://www.ract.com.au/RactPureLink}AddUpdateFindPartyResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "addUpdateFindPartyInputResult" })
@XmlRootElement(name = "AddUpdateFindPartyInputResponse")
public class AddUpdateFindPartyInputResponse {

    @XmlElementRef(name = "AddUpdateFindPartyInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<AddUpdateFindPartyResponse> addUpdateFindPartyInputResult;

    /**
     * Gets the value of the addUpdateFindPartyInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link AddUpdateFindPartyResponse }{@code >}
     * 
     */
    public JAXBElement<AddUpdateFindPartyResponse> getAddUpdateFindPartyInputResult() {
        return addUpdateFindPartyInputResult;
    }

    /**
     * Sets the value of the addUpdateFindPartyInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link AddUpdateFindPartyResponse }{@code >}
     * 
     */
    public void setAddUpdateFindPartyInputResult(JAXBElement<AddUpdateFindPartyResponse> value) {
        this.addUpdateFindPartyInputResult = ((JAXBElement<AddUpdateFindPartyResponse>) value);
    }

}
