package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for Contact complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Contact">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contactType" type="{http://www.ract.com.au/RactPureLink}ContactType"/>
 *         &lt;element name="contactDetail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Contact", propOrder = { "contactType", "contactDetail" })
public class Contact {

    @XmlElement(required = true)
    protected ContactType contactType;
    @XmlElement(required = true, nillable = true)
    protected String contactDetail;

    /**
     * Gets the value of the contactType property.
     * 
     * @return possible object is {@link ContactType }
     * 
     */
    public ContactType getContactType() {
        return contactType;
    }

    /**
     * Sets the value of the contactType property.
     * 
     * @param value
     *            allowed object is {@link ContactType }
     * 
     */
    public void setContactType(ContactType value) {
        this.contactType = value;
    }

    /**
     * Gets the value of the contactDetail property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getContactDetail() {
        return contactDetail;
    }

    /**
     * Sets the value of the contactDetail property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setContactDetail(String value) {
        this.contactDetail = value;
    }

}
