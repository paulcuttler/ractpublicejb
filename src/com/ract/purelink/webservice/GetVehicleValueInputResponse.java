package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetVehicleValueInputResult" type="{http://www.ract.com.au/RactPureLink}GetVehicleValueResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getVehicleValueInputResult" })
@XmlRootElement(name = "GetVehicleValueInputResponse")
public class GetVehicleValueInputResponse {

    @XmlElementRef(name = "GetVehicleValueInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetVehicleValueResponse> getVehicleValueInputResult;

    /**
     * Gets the value of the getVehicleValueInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetVehicleValueResponse }{@code >}
     * 
     */
    public JAXBElement<GetVehicleValueResponse> getGetVehicleValueInputResult() {
        return getVehicleValueInputResult;
    }

    /**
     * Sets the value of the getVehicleValueInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetVehicleValueResponse }{@code >}
     * 
     */
    public void setGetVehicleValueInputResult(JAXBElement<GetVehicleValueResponse> value) {
        this.getVehicleValueInputResult = ((JAXBElement<GetVehicleValueResponse>) value);
    }

}
