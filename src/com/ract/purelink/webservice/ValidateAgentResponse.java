package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ValidateAgentResponse complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidateAgentResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agentCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="agentName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="agentStatusMessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidateAgentResponse", propOrder = { "agentCode", "agentName", "agentStatusMessage" })
public class ValidateAgentResponse {

    @XmlElement(required = true, nillable = true)
    protected String agentCode;
    @XmlElement(required = true, nillable = true)
    protected String agentName;
    @XmlElement(required = true, nillable = true)
    protected String agentStatusMessage;

    /**
     * Gets the value of the agentCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getAgentCode() {
        return agentCode;
    }

    /**
     * Sets the value of the agentCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setAgentCode(String value) {
        this.agentCode = value;
    }

    /**
     * Gets the value of the agentName property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getAgentName() {
        return agentName;
    }

    /**
     * Sets the value of the agentName property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setAgentName(String value) {
        this.agentName = value;
    }

    /**
     * Gets the value of the agentStatusMessage property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getAgentStatusMessage() {
        return agentStatusMessage;
    }

    /**
     * Sets the value of the agentStatusMessage property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setAgentStatusMessage(String value) {
        this.agentStatusMessage = value;
    }

}
