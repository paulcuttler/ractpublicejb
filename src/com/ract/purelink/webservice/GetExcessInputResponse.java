package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetExcessInputResult" type="{http://www.ract.com.au/RactPureLink}GetExcessResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getExcessInputResult" })
@XmlRootElement(name = "GetExcessInputResponse")
public class GetExcessInputResponse {

    @XmlElementRef(name = "GetExcessInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetExcessResponse> getExcessInputResult;

    /**
     * Gets the value of the getExcessInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetExcessResponse }{@code >}
     * 
     */
    public JAXBElement<GetExcessResponse> getGetExcessInputResult() {
        return getExcessInputResult;
    }

    /**
     * Sets the value of the getExcessInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetExcessResponse }{@code >}
     * 
     */
    public void setGetExcessInputResult(JAXBElement<GetExcessResponse> value) {
        this.getExcessInputResult = ((JAXBElement<GetExcessResponse>) value);
    }

}
