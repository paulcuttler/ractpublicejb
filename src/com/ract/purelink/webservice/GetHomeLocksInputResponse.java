package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetHomeLocksInputResult" type="{http://www.ract.com.au/RactPureLink}GetHomeLocksResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getHomeLocksInputResult" })
@XmlRootElement(name = "GetHomeLocksInputResponse")
public class GetHomeLocksInputResponse {

    @XmlElementRef(name = "GetHomeLocksInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetHomeLocksResponse> getHomeLocksInputResult;

    /**
     * Gets the value of the getHomeLocksInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetHomeLocksResponse }{@code >}
     * 
     */
    public JAXBElement<GetHomeLocksResponse> getGetHomeLocksInputResult() {
        return getHomeLocksInputResult;
    }

    /**
     * Sets the value of the getHomeLocksInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetHomeLocksResponse }{@code >}
     * 
     */
    public void setGetHomeLocksInputResult(JAXBElement<GetHomeLocksResponse> value) {
        this.getHomeLocksInputResult = ((JAXBElement<GetHomeLocksResponse>) value);
    }

}
