package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GlassesVehicle complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GlassesVehicle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nvic" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="year" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="make" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="family" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="style" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="transmission" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cylinder" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="engineCapacity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="engineDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="variant" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="series" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="acceptability" type="{http://www.ract.com.au/RactPureLink}VehicleAcceptability"/>
 *         &lt;element name="crPoints" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tpPoints" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GlassesVehicle", propOrder = { "nvic", "year", "make", "family", "style", "transmission", "cylinder", "description", "engineCapacity", "engineDescription", "variant", "value", "series", "acceptability", "crPoints", "tpPoints" })
public class GlassesVehicle {

    @XmlElement(required = true, nillable = true)
    protected String nvic;
    @XmlElement(required = true, nillable = true)
    protected String year;
    @XmlElement(required = true, nillable = true)
    protected String make;
    @XmlElement(required = true, nillable = true)
    protected String family;
    @XmlElement(required = true, nillable = true)
    protected String style;
    @XmlElement(required = true, nillable = true)
    protected String transmission;
    @XmlElement(required = true, nillable = true)
    protected String cylinder;
    @XmlElement(required = true, nillable = true)
    protected String description;
    @XmlElement(required = true, nillable = true)
    protected String engineCapacity;
    @XmlElement(required = true, nillable = true)
    protected String engineDescription;
    @XmlElement(required = true, nillable = true)
    protected String variant;
    protected int value;
    @XmlElement(required = true, nillable = true)
    protected String series;
    @XmlElement(required = true)
    protected VehicleAcceptability acceptability;
    protected int crPoints;
    protected int tpPoints;

    /**
     * Gets the value of the nvic property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getNvic() {
        return nvic;
    }

    /**
     * Sets the value of the nvic property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setNvic(String value) {
        this.nvic = value;
    }

    /**
     * Gets the value of the year property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getYear() {
        return year;
    }

    /**
     * Sets the value of the year property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setYear(String value) {
        this.year = value;
    }

    /**
     * Gets the value of the make property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getMake() {
        return make;
    }

    /**
     * Sets the value of the make property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setMake(String value) {
        this.make = value;
    }

    /**
     * Gets the value of the family property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getFamily() {
        return family;
    }

    /**
     * Sets the value of the family property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setFamily(String value) {
        this.family = value;
    }

    /**
     * Gets the value of the style property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getStyle() {
        return style;
    }

    /**
     * Sets the value of the style property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setStyle(String value) {
        this.style = value;
    }

    /**
     * Gets the value of the transmission property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getTransmission() {
        return transmission;
    }

    /**
     * Sets the value of the transmission property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setTransmission(String value) {
        this.transmission = value;
    }

    /**
     * Gets the value of the cylinder property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCylinder() {
        return cylinder;
    }

    /**
     * Sets the value of the cylinder property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCylinder(String value) {
        this.cylinder = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the engineCapacity property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getEngineCapacity() {
        return engineCapacity;
    }

    /**
     * Sets the value of the engineCapacity property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setEngineCapacity(String value) {
        this.engineCapacity = value;
    }

    /**
     * Gets the value of the engineDescription property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getEngineDescription() {
        return engineDescription;
    }

    /**
     * Sets the value of the engineDescription property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setEngineDescription(String value) {
        this.engineDescription = value;
    }

    /**
     * Gets the value of the variant property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getVariant() {
        return variant;
    }

    /**
     * Sets the value of the variant property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setVariant(String value) {
        this.variant = value;
    }

    /**
     * Gets the value of the value property.
     * 
     */
    public int getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     */
    public void setValue(int value) {
        this.value = value;
    }

    /**
     * Gets the value of the series property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getSeries() {
        return series;
    }

    /**
     * Sets the value of the series property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setSeries(String value) {
        this.series = value;
    }

    /**
     * Gets the value of the acceptability property.
     * 
     * @return possible object is {@link VehicleAcceptability }
     * 
     */
    public VehicleAcceptability getAcceptability() {
        return acceptability;
    }

    /**
     * Sets the value of the acceptability property.
     * 
     * @param value
     *            allowed object is {@link VehicleAcceptability }
     * 
     */
    public void setAcceptability(VehicleAcceptability value) {
        this.acceptability = value;
    }

    /**
     * Gets the value of the crPoints property.
     * 
     */
    public int getCrPoints() {
        return crPoints;
    }

    /**
     * Sets the value of the crPoints property.
     * 
     */
    public void setCrPoints(int value) {
        this.crPoints = value;
    }

    /**
     * Gets the value of the tpPoints property.
     * 
     */
    public int getTpPoints() {
        return tpPoints;
    }

    /**
     * Sets the value of the tpPoints property.
     * 
     */
    public void setTpPoints(int value) {
        this.tpPoints = value;
    }

}
