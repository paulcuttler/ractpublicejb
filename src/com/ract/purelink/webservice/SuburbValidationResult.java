package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SuburbValidationResult.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="SuburbValidationResult">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OK"/>
 *     &lt;enumeration value="EB"/>
 *     &lt;enumeration value="UK"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SuburbValidationResult")
@XmlEnum
public enum SuburbValidationResult {

    OK, EB, UK;

    public String value() {
        return name();
    }

    public static SuburbValidationResult fromValue(String v) {
        return valueOf(v);
    }

}
