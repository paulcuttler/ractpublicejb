package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetMinMaxInsurableInputResult" type="{http://www.ract.com.au/RactPureLink}GetMinMaxInsurableResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getMinMaxInsurableInputResult" })
@XmlRootElement(name = "GetMinMaxInsurableInputResponse")
public class GetMinMaxInsurableInputResponse {

    @XmlElementRef(name = "GetMinMaxInsurableInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetMinMaxInsurableResponse> getMinMaxInsurableInputResult;

    /**
     * Gets the value of the getMinMaxInsurableInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetMinMaxInsurableResponse }{@code >}
     * 
     */
    public JAXBElement<GetMinMaxInsurableResponse> getGetMinMaxInsurableInputResult() {
        return getMinMaxInsurableInputResult;
    }

    /**
     * Sets the value of the getMinMaxInsurableInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetMinMaxInsurableResponse }{@code >}
     * 
     */
    public void setGetMinMaxInsurableInputResult(JAXBElement<GetMinMaxInsurableResponse> value) {
        this.getMinMaxInsurableInputResult = ((JAXBElement<GetMinMaxInsurableResponse>) value);
    }

}
