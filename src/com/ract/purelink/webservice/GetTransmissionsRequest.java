package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetTransmissionsRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetTransmissionsRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="year" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="make" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="family" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="style" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetTransmissionsRequest", propOrder = { "year", "make", "family", "style" })
public class GetTransmissionsRequest {

    @XmlElement(required = true, nillable = true)
    protected String year;
    @XmlElement(required = true, nillable = true)
    protected String make;
    @XmlElement(required = true, nillable = true)
    protected String family;
    @XmlElement(required = true, nillable = true)
    protected String style;

    /**
     * Gets the value of the year property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getYear() {
        return year;
    }

    /**
     * Sets the value of the year property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setYear(String value) {
        this.year = value;
    }

    /**
     * Gets the value of the make property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getMake() {
        return make;
    }

    /**
     * Sets the value of the make property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setMake(String value) {
        this.make = value;
    }

    /**
     * Gets the value of the family property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getFamily() {
        return family;
    }

    /**
     * Sets the value of the family property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setFamily(String value) {
        this.family = value;
    }

    /**
     * Gets the value of the style property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getStyle() {
        return style;
    }

    /**
     * Sets the value of the style property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setStyle(String value) {
        this.style = value;
    }

}
