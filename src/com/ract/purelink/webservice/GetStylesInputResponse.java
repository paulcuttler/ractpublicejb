package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetStylesInputResult" type="{http://www.ract.com.au/RactPureLink}GetStylesResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getStylesInputResult" })
@XmlRootElement(name = "GetStylesInputResponse")
public class GetStylesInputResponse {

    @XmlElementRef(name = "GetStylesInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetStylesResponse> getStylesInputResult;

    /**
     * Gets the value of the getStylesInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetStylesResponse }{@code >}
     * 
     */
    public JAXBElement<GetStylesResponse> getGetStylesInputResult() {
        return getStylesInputResult;
    }

    /**
     * Sets the value of the getStylesInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetStylesResponse }{@code >}
     * 
     */
    public void setGetStylesInputResult(JAXBElement<GetStylesResponse> value) {
        this.getStylesInputResult = ((JAXBElement<GetStylesResponse>) value);
    }

}
