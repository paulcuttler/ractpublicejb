package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetRoofConstructionInputResult" type="{http://www.ract.com.au/RactPureLink}GetRoofConstructionResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getRoofConstructionInputResult" })
@XmlRootElement(name = "GetRoofConstructionInputResponse")
public class GetRoofConstructionInputResponse {

    @XmlElementRef(name = "GetRoofConstructionInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetRoofConstructionResponse> getRoofConstructionInputResult;

    /**
     * Gets the value of the getRoofConstructionInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetRoofConstructionResponse }{@code >}
     * 
     */
    public JAXBElement<GetRoofConstructionResponse> getGetRoofConstructionInputResult() {
        return getRoofConstructionInputResult;
    }

    /**
     * Sets the value of the getRoofConstructionInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetRoofConstructionResponse }{@code >}
     * 
     */
    public void setGetRoofConstructionInputResult(JAXBElement<GetRoofConstructionResponse> value) {
        this.getRoofConstructionInputResult = ((JAXBElement<GetRoofConstructionResponse>) value);
    }

}
