package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetOccupancyInputResult" type="{http://www.ract.com.au/RactPureLink}GetOccupancyResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getOccupancyInputResult" })
@XmlRootElement(name = "GetOccupancyInputResponse")
public class GetOccupancyInputResponse {

    @XmlElementRef(name = "GetOccupancyInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetOccupancyResponse> getOccupancyInputResult;

    /**
     * Gets the value of the getOccupancyInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetOccupancyResponse }{@code >}
     * 
     */
    public JAXBElement<GetOccupancyResponse> getGetOccupancyInputResult() {
        return getOccupancyInputResult;
    }

    /**
     * Sets the value of the getOccupancyInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetOccupancyResponse }{@code >}
     * 
     */
    public void setGetOccupancyInputResult(JAXBElement<GetOccupancyResponse> value) {
        this.getOccupancyInputResult = ((JAXBElement<GetOccupancyResponse>) value);
    }

}
