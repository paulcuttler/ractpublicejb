package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for State.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="State">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NSW"/>
 *     &lt;enumeration value="QLD"/>
 *     &lt;enumeration value="VIC"/>
 *     &lt;enumeration value="TAS"/>
 *     &lt;enumeration value="SA"/>
 *     &lt;enumeration value="NT"/>
 *     &lt;enumeration value="WA"/>
 *     &lt;enumeration value="ACT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "State")
@XmlEnum
public enum State {

    NSW, QLD, VIC, TAS, SA, NT, WA, ACT;

    public String value() {
        return name();
    }

    public static State fromValue(String v) {
        return valueOf(v);
    }

}
