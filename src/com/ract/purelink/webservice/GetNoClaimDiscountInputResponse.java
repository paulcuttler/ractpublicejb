package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetNoClaimDiscountInputResult" type="{http://www.ract.com.au/RactPureLink}GetNoClaimDiscountResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getNoClaimDiscountInputResult" })
@XmlRootElement(name = "GetNoClaimDiscountInputResponse")
public class GetNoClaimDiscountInputResponse {

    @XmlElementRef(name = "GetNoClaimDiscountInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetNoClaimDiscountResponse> getNoClaimDiscountInputResult;

    /**
     * Gets the value of the getNoClaimDiscountInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetNoClaimDiscountResponse }{@code >}
     * 
     */
    public JAXBElement<GetNoClaimDiscountResponse> getGetNoClaimDiscountInputResult() {
        return getNoClaimDiscountInputResult;
    }

    /**
     * Sets the value of the getNoClaimDiscountInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetNoClaimDiscountResponse }{@code >}
     * 
     */
    public void setGetNoClaimDiscountInputResult(JAXBElement<GetNoClaimDiscountResponse> value) {
        this.getNoClaimDiscountInputResult = ((JAXBElement<GetNoClaimDiscountResponse>) value);
    }

}
