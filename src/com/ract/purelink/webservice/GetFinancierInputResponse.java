package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetFinancierInputResult" type="{http://www.ract.com.au/RactPureLink}GetFinancierResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getFinancierInputResult" })
@XmlRootElement(name = "GetFinancierInputResponse")
public class GetFinancierInputResponse {

    @XmlElementRef(name = "GetFinancierInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetFinancierResponse> getFinancierInputResult;

    /**
     * Gets the value of the getFinancierInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetFinancierResponse }{@code >}
     * 
     */
    public JAXBElement<GetFinancierResponse> getGetFinancierInputResult() {
        return getFinancierInputResult;
    }

    /**
     * Sets the value of the getFinancierInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetFinancierResponse }{@code >}
     * 
     */
    public void setGetFinancierInputResult(JAXBElement<GetFinancierResponse> value) {
        this.getFinancierInputResult = ((JAXBElement<GetFinancierResponse>) value);
    }

}
