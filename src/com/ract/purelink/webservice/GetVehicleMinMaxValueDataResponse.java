package com.ract.purelink.webservice;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetVehicleMinMaxValueDataResponse complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetVehicleMinMaxValueDataResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vehicleValueCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="minValueLimitPercent" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="maxValueLimitPercent" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetVehicleMinMaxValueDataResponse", propOrder = { "vehicleValueCode", "minValueLimitPercent", "maxValueLimitPercent" })
public class GetVehicleMinMaxValueDataResponse {

    @XmlElement(required = true, nillable = true)
    protected String vehicleValueCode;
    @XmlElement(required = true)
    protected BigDecimal minValueLimitPercent;
    @XmlElement(required = true)
    protected BigDecimal maxValueLimitPercent;

    /**
     * Gets the value of the vehicleValueCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getVehicleValueCode() {
        return vehicleValueCode;
    }

    /**
     * Sets the value of the vehicleValueCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setVehicleValueCode(String value) {
        this.vehicleValueCode = value;
    }

    /**
     * Gets the value of the minValueLimitPercent property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getMinValueLimitPercent() {
        return minValueLimitPercent;
    }

    /**
     * Sets the value of the minValueLimitPercent property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setMinValueLimitPercent(BigDecimal value) {
        this.minValueLimitPercent = value;
    }

    /**
     * Gets the value of the maxValueLimitPercent property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getMaxValueLimitPercent() {
        return maxValueLimitPercent;
    }

    /**
     * Sets the value of the maxValueLimitPercent property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setMaxValueLimitPercent(BigDecimal value) {
        this.maxValueLimitPercent = value;
    }

}
