package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CoverTypeCode.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="CoverTypeCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="COMC"/>
 *     &lt;enumeration value="TPPD"/>
 *     &lt;enumeration value="BLDG"/>
 *     &lt;enumeration value="CNTS"/>
 *     &lt;enumeration value="PEFF"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CoverTypeCode")
@XmlEnum
public enum CoverTypeCode {

    COMC, TPPD, BLDG, CNTS, PEFF;

    public String value() {
        return name();
    }

    public static CoverTypeCode fromValue(String v) {
        return valueOf(v);
    }

}
