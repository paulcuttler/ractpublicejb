package com.ract.purelink.webservice;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Java class for MotorQuoteDetails complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MotorQuoteDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="channelCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="riskType" type="{http://www.ract.com.au/RactPureLink}RiskTypeCode"/>
 *         &lt;element name="ratingDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="engineImmobiliser" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="goodOrderAndRepair" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="bypassGlasses" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="bypassRegoLookup" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="glassesData" type="{http://www.ract.com.au/RactPureLink}GlassesVehicle"/>
 *         &lt;element name="registrationState" type="{http://www.ract.com.au/RactPureLink}State"/>
 *         &lt;element name="registrationNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="firstOwner" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="usageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="acceptableGoodOrderAndRepairCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="financier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ratingDriver" type="{http://www.ract.com.au/RactPureLink}DriverDetails"/>
 *         &lt;element name="listedDrivers" type="{http://www.ract.com.au/RactPureLink}DriverDetailsList"/>
 *         &lt;element name="garageAddress" type="{http://www.ract.com.au/RactPureLink}Address"/>
 *         &lt;element name="compCar" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="tpCar" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="minValue" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="averageValue" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="maxValue" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="agreedValue" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="windscreenDeletionOption" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="hireCarOption" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="fireTheftOption" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="pipDiscount" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="silverSaverDiscount" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="subRiskType" type="{http://www.ract.com.au/RactPureLink}SubRiskTypeCode"/>
 *         &lt;element name="maxValueCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="basicExcessCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="baseAnnualPremium" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="gst" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="stampDuty" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="totalAnnualPremium" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="installmentAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="paymentFrequency" type="{http://www.ract.com.au/RactPureLink}PaymentFrequency"/>
 *         &lt;element name="claimRefused" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="insuranceRefused" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="criminalOffences" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MotorQuoteDetails", propOrder = { "channelCode", "riskType", "ratingDate", "engineImmobiliser", "goodOrderAndRepair", "bypassGlasses", "bypassRegoLookup", "glassesData", "registrationState", "registrationNumber", "firstOwner", "usageCode", "acceptableGoodOrderAndRepairCode", "financier", "ratingDriver", "listedDrivers", "garageAddress", "compCar", "tpCar", "minValue", "averageValue", "maxValue", "agreedValue", "windscreenDeletionOption", "hireCarOption", "fireTheftOption", "pipDiscount", "silverSaverDiscount", "subRiskType", "maxValueCode", "basicExcessCode", "baseAnnualPremium", "gst", "stampDuty", "totalAnnualPremium", "installmentAmount", "paymentFrequency", "claimRefused", "insuranceRefused", "criminalOffences" })
public class MotorQuoteDetails {

    @XmlElement(required = true, nillable = true)
    protected String channelCode;
    @XmlElement(required = true)
    protected RiskTypeCode riskType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ratingDate;
    protected boolean engineImmobiliser;
    protected boolean goodOrderAndRepair;
    protected boolean bypassGlasses;
    protected boolean bypassRegoLookup;
    @XmlElement(required = true, nillable = true)
    protected GlassesVehicle glassesData;
    @XmlElement(required = true)
    protected State registrationState;
    @XmlElement(required = true, nillable = true)
    protected String registrationNumber;
    protected boolean firstOwner;
    @XmlElement(required = true, nillable = true)
    protected String usageCode;
    @XmlElementRef(name = "acceptableGoodOrderAndRepairCode", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<String> acceptableGoodOrderAndRepairCode;
    @XmlElementRef(name = "financier", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<String> financier;
    @XmlElement(required = true, nillable = true)
    protected DriverDetails ratingDriver;
    @XmlElement(required = true, nillable = true)
    protected DriverDetailsList listedDrivers;
    @XmlElement(required = true, nillable = true)
    protected Address garageAddress;
    protected boolean compCar;
    protected boolean tpCar;
    protected int minValue;
    protected int averageValue;
    protected int maxValue;
    protected int agreedValue;
    protected boolean windscreenDeletionOption;
    protected boolean hireCarOption;
    protected boolean fireTheftOption;
    protected boolean pipDiscount;
    protected boolean silverSaverDiscount;
    @XmlElement(required = true)
    protected SubRiskTypeCode subRiskType;
    @XmlElement(required = true, nillable = true)
    protected String maxValueCode;
    @XmlElement(required = true, nillable = true)
    protected String basicExcessCode;
    @XmlElement(required = true)
    protected BigDecimal baseAnnualPremium;
    @XmlElement(required = true)
    protected BigDecimal gst;
    @XmlElement(required = true)
    protected BigDecimal stampDuty;
    @XmlElement(required = true)
    protected BigDecimal totalAnnualPremium;
    @XmlElement(required = true)
    protected BigDecimal installmentAmount;
    @XmlElement(required = true)
    protected PaymentFrequency paymentFrequency;
    protected boolean claimRefused;
    protected boolean insuranceRefused;
    protected boolean criminalOffences;

    /**
     * Gets the value of the channelCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getChannelCode() {
        return channelCode;
    }

    /**
     * Sets the value of the channelCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setChannelCode(String value) {
        this.channelCode = value;
    }

    /**
     * Gets the value of the riskType property.
     * 
     * @return possible object is {@link RiskTypeCode }
     * 
     */
    public RiskTypeCode getRiskType() {
        return riskType;
    }

    /**
     * Sets the value of the riskType property.
     * 
     * @param value
     *            allowed object is {@link RiskTypeCode }
     * 
     */
    public void setRiskType(RiskTypeCode value) {
        this.riskType = value;
    }

    /**
     * Gets the value of the ratingDate property.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getRatingDate() {
        return ratingDate;
    }

    /**
     * Sets the value of the ratingDate property.
     * 
     * @param value
     *            allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setRatingDate(XMLGregorianCalendar value) {
        this.ratingDate = value;
    }

    /**
     * Gets the value of the engineImmobiliser property.
     * 
     */
    public boolean isEngineImmobiliser() {
        return engineImmobiliser;
    }

    /**
     * Sets the value of the engineImmobiliser property.
     * 
     */
    public void setEngineImmobiliser(boolean value) {
        this.engineImmobiliser = value;
    }

    /**
     * Gets the value of the goodOrderAndRepair property.
     * 
     */
    public boolean isGoodOrderAndRepair() {
        return goodOrderAndRepair;
    }

    /**
     * Sets the value of the goodOrderAndRepair property.
     * 
     */
    public void setGoodOrderAndRepair(boolean value) {
        this.goodOrderAndRepair = value;
    }

    /**
     * Gets the value of the bypassGlasses property.
     * 
     */
    public boolean isBypassGlasses() {
        return bypassGlasses;
    }

    /**
     * Sets the value of the bypassGlasses property.
     * 
     */
    public void setBypassGlasses(boolean value) {
        this.bypassGlasses = value;
    }

    /**
     * Gets the value of the bypassRegoLookup property.
     * 
     */
    public boolean isBypassRegoLookup() {
        return bypassRegoLookup;
    }

    /**
     * Sets the value of the bypassRegoLookup property.
     * 
     */
    public void setBypassRegoLookup(boolean value) {
        this.bypassRegoLookup = value;
    }

    /**
     * Gets the value of the glassesData property.
     * 
     * @return possible object is {@link GlassesVehicle }
     * 
     */
    public GlassesVehicle getGlassesData() {
        return glassesData;
    }

    /**
     * Sets the value of the glassesData property.
     * 
     * @param value
     *            allowed object is {@link GlassesVehicle }
     * 
     */
    public void setGlassesData(GlassesVehicle value) {
        this.glassesData = value;
    }

    /**
     * Gets the value of the registrationState property.
     * 
     * @return possible object is {@link State }
     * 
     */
    public State getRegistrationState() {
        return registrationState;
    }

    /**
     * Sets the value of the registrationState property.
     * 
     * @param value
     *            allowed object is {@link State }
     * 
     */
    public void setRegistrationState(State value) {
        this.registrationState = value;
    }

    /**
     * Gets the value of the registrationNumber property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getRegistrationNumber() {
        return registrationNumber;
    }

    /**
     * Sets the value of the registrationNumber property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setRegistrationNumber(String value) {
        this.registrationNumber = value;
    }

    /**
     * Gets the value of the firstOwner property.
     * 
     */
    public boolean isFirstOwner() {
        return firstOwner;
    }

    /**
     * Sets the value of the firstOwner property.
     * 
     */
    public void setFirstOwner(boolean value) {
        this.firstOwner = value;
    }

    /**
     * Gets the value of the usageCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getUsageCode() {
        return usageCode;
    }

    /**
     * Sets the value of the usageCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setUsageCode(String value) {
        this.usageCode = value;
    }

    /**
     * Gets the value of the acceptableGoodOrderAndRepairCode property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public JAXBElement<String> getAcceptableGoodOrderAndRepairCode() {
        return acceptableGoodOrderAndRepairCode;
    }

    /**
     * Sets the value of the acceptableGoodOrderAndRepairCode property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public void setAcceptableGoodOrderAndRepairCode(JAXBElement<String> value) {
        this.acceptableGoodOrderAndRepairCode = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the financier property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public JAXBElement<String> getFinancier() {
        return financier;
    }

    /**
     * Sets the value of the financier property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public void setFinancier(JAXBElement<String> value) {
        this.financier = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the ratingDriver property.
     * 
     * @return possible object is {@link DriverDetails }
     * 
     */
    public DriverDetails getRatingDriver() {
        return ratingDriver;
    }

    /**
     * Sets the value of the ratingDriver property.
     * 
     * @param value
     *            allowed object is {@link DriverDetails }
     * 
     */
    public void setRatingDriver(DriverDetails value) {
        this.ratingDriver = value;
    }

    /**
     * Gets the value of the listedDrivers property.
     * 
     * @return possible object is {@link DriverDetailsList }
     * 
     */
    public DriverDetailsList getListedDrivers() {
        return listedDrivers;
    }

    /**
     * Sets the value of the listedDrivers property.
     * 
     * @param value
     *            allowed object is {@link DriverDetailsList }
     * 
     */
    public void setListedDrivers(DriverDetailsList value) {
        this.listedDrivers = value;
    }

    /**
     * Gets the value of the garageAddress property.
     * 
     * @return possible object is {@link Address }
     * 
     */
    public Address getGarageAddress() {
        return garageAddress;
    }

    /**
     * Sets the value of the garageAddress property.
     * 
     * @param value
     *            allowed object is {@link Address }
     * 
     */
    public void setGarageAddress(Address value) {
        this.garageAddress = value;
    }

    /**
     * Gets the value of the compCar property.
     * 
     */
    public boolean isCompCar() {
        return compCar;
    }

    /**
     * Sets the value of the compCar property.
     * 
     */
    public void setCompCar(boolean value) {
        this.compCar = value;
    }

    /**
     * Gets the value of the tpCar property.
     * 
     */
    public boolean isTpCar() {
        return tpCar;
    }

    /**
     * Sets the value of the tpCar property.
     * 
     */
    public void setTpCar(boolean value) {
        this.tpCar = value;
    }

    /**
     * Gets the value of the minValue property.
     * 
     */
    public int getMinValue() {
        return minValue;
    }

    /**
     * Sets the value of the minValue property.
     * 
     */
    public void setMinValue(int value) {
        this.minValue = value;
    }

    /**
     * Gets the value of the averageValue property.
     * 
     */
    public int getAverageValue() {
        return averageValue;
    }

    /**
     * Sets the value of the averageValue property.
     * 
     */
    public void setAverageValue(int value) {
        this.averageValue = value;
    }

    /**
     * Gets the value of the maxValue property.
     * 
     */
    public int getMaxValue() {
        return maxValue;
    }

    /**
     * Sets the value of the maxValue property.
     * 
     */
    public void setMaxValue(int value) {
        this.maxValue = value;
    }

    /**
     * Gets the value of the agreedValue property.
     * 
     */
    public int getAgreedValue() {
        return agreedValue;
    }

    /**
     * Sets the value of the agreedValue property.
     * 
     */
    public void setAgreedValue(int value) {
        this.agreedValue = value;
    }

    /**
     * Gets the value of the windscreenDeletionOption property.
     * 
     */
    public boolean isWindscreenDeletionOption() {
        return windscreenDeletionOption;
    }

    /**
     * Sets the value of the windscreenDeletionOption property.
     * 
     */
    public void setWindscreenDeletionOption(boolean value) {
        this.windscreenDeletionOption = value;
    }

    /**
     * Gets the value of the hireCarOption property.
     * 
     */
    public boolean isHireCarOption() {
        return hireCarOption;
    }

    /**
     * Sets the value of the hireCarOption property.
     * 
     */
    public void setHireCarOption(boolean value) {
        this.hireCarOption = value;
    }

    /**
     * Gets the value of the fireTheftOption property.
     * 
     */
    public boolean isFireTheftOption() {
        return fireTheftOption;
    }

    /**
     * Sets the value of the fireTheftOption property.
     * 
     */
    public void setFireTheftOption(boolean value) {
        this.fireTheftOption = value;
    }

    /**
     * Gets the value of the pipDiscount property.
     * 
     */
    public boolean isPipDiscount() {
        return pipDiscount;
    }

    /**
     * Sets the value of the pipDiscount property.
     * 
     */
    public void setPipDiscount(boolean value) {
        this.pipDiscount = value;
    }

    /**
     * Gets the value of the silverSaverDiscount property.
     * 
     */
    public boolean isSilverSaverDiscount() {
        return silverSaverDiscount;
    }

    /**
     * Sets the value of the silverSaverDiscount property.
     * 
     */
    public void setSilverSaverDiscount(boolean value) {
        this.silverSaverDiscount = value;
    }

    /**
     * Gets the value of the subRiskType property.
     * 
     * @return possible object is {@link SubRiskTypeCode }
     * 
     */
    public SubRiskTypeCode getSubRiskType() {
        return subRiskType;
    }

    /**
     * Sets the value of the subRiskType property.
     * 
     * @param value
     *            allowed object is {@link SubRiskTypeCode }
     * 
     */
    public void setSubRiskType(SubRiskTypeCode value) {
        this.subRiskType = value;
    }

    /**
     * Gets the value of the maxValueCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getMaxValueCode() {
        return maxValueCode;
    }

    /**
     * Sets the value of the maxValueCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setMaxValueCode(String value) {
        this.maxValueCode = value;
    }

    /**
     * Gets the value of the basicExcessCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getBasicExcessCode() {
        return basicExcessCode;
    }

    /**
     * Sets the value of the basicExcessCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setBasicExcessCode(String value) {
        this.basicExcessCode = value;
    }

    /**
     * Gets the value of the baseAnnualPremium property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getBaseAnnualPremium() {
        return baseAnnualPremium;
    }

    /**
     * Sets the value of the baseAnnualPremium property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setBaseAnnualPremium(BigDecimal value) {
        this.baseAnnualPremium = value;
    }

    /**
     * Gets the value of the gst property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getGst() {
        return gst;
    }

    /**
     * Sets the value of the gst property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setGst(BigDecimal value) {
        this.gst = value;
    }

    /**
     * Gets the value of the stampDuty property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getStampDuty() {
        return stampDuty;
    }

    /**
     * Sets the value of the stampDuty property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setStampDuty(BigDecimal value) {
        this.stampDuty = value;
    }

    /**
     * Gets the value of the totalAnnualPremium property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getTotalAnnualPremium() {
        return totalAnnualPremium;
    }

    /**
     * Sets the value of the totalAnnualPremium property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setTotalAnnualPremium(BigDecimal value) {
        this.totalAnnualPremium = value;
    }

    /**
     * Gets the value of the installmentAmount property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getInstallmentAmount() {
        return installmentAmount;
    }

    /**
     * Sets the value of the installmentAmount property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setInstallmentAmount(BigDecimal value) {
        this.installmentAmount = value;
    }

    /**
     * Gets the value of the paymentFrequency property.
     * 
     * @return possible object is {@link PaymentFrequency }
     * 
     */
    public PaymentFrequency getPaymentFrequency() {
        return paymentFrequency;
    }

    /**
     * Sets the value of the paymentFrequency property.
     * 
     * @param value
     *            allowed object is {@link PaymentFrequency }
     * 
     */
    public void setPaymentFrequency(PaymentFrequency value) {
        this.paymentFrequency = value;
    }

    /**
     * Gets the value of the claimRefused property.
     * 
     */
    public boolean isClaimRefused() {
        return claimRefused;
    }

    /**
     * Sets the value of the claimRefused property.
     * 
     */
    public void setClaimRefused(boolean value) {
        this.claimRefused = value;
    }

    /**
     * Gets the value of the insuranceRefused property.
     * 
     */
    public boolean isInsuranceRefused() {
        return insuranceRefused;
    }

    /**
     * Sets the value of the insuranceRefused property.
     * 
     */
    public void setInsuranceRefused(boolean value) {
        this.insuranceRefused = value;
    }

    /**
     * Gets the value of the criminalOffences property.
     * 
     */
    public boolean isCriminalOffences() {
        return criminalOffences;
    }

    /**
     * Sets the value of the criminalOffences property.
     * 
     */
    public void setCriminalOffences(boolean value) {
        this.criminalOffences = value;
    }

}
