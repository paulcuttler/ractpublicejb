package com.ract.purelink.webservice;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for OptionDetail complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OptionDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dollarAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="percentageAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="acceptable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OptionDetail", propOrder = { "code", "description", "dollarAmount", "percentageAmount", "acceptable" })
public class OptionDetail {

    @XmlElement(required = true, nillable = true)
    protected String code;
    @XmlElement(required = true, nillable = true)
    protected String description;
    @XmlElement(required = true)
    protected BigDecimal dollarAmount;
    @XmlElement(required = true)
    protected BigDecimal percentageAmount;
    protected boolean acceptable;

    /**
     * Gets the value of the code property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the dollarAmount property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getDollarAmount() {
        return dollarAmount;
    }

    /**
     * Sets the value of the dollarAmount property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setDollarAmount(BigDecimal value) {
        this.dollarAmount = value;
    }

    /**
     * Gets the value of the percentageAmount property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getPercentageAmount() {
        return percentageAmount;
    }

    /**
     * Sets the value of the percentageAmount property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setPercentageAmount(BigDecimal value) {
        this.percentageAmount = value;
    }

    /**
     * Gets the value of the acceptable property.
     * 
     */
    public boolean isAcceptable() {
        return acceptable;
    }

    /**
     * Sets the value of the acceptable property.
     * 
     */
    public void setAcceptable(boolean value) {
        this.acceptable = value;
    }

}
