package com.ract.purelink.webservice;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetQuoteResponse complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetQuoteResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="baseAnnualPremium" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="gst" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="stampDuty" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="totalAnnualPremium" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="installmentAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="dreResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetQuoteResponse", propOrder = { "baseAnnualPremium", "gst", "stampDuty", "totalAnnualPremium", "installmentAmount", "dreResult" })
public class GetQuoteResponse {

    @XmlElement(required = true)
    protected BigDecimal baseAnnualPremium;
    @XmlElement(required = true)
    protected BigDecimal gst;
    @XmlElement(required = true)
    protected BigDecimal stampDuty;
    @XmlElement(required = true)
    protected BigDecimal totalAnnualPremium;
    @XmlElement(required = true)
    protected BigDecimal installmentAmount;
    @XmlElement(required = true, nillable = true)
    protected String dreResult;

    /**
     * Gets the value of the baseAnnualPremium property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getBaseAnnualPremium() {
        return baseAnnualPremium;
    }

    /**
     * Sets the value of the baseAnnualPremium property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setBaseAnnualPremium(BigDecimal value) {
        this.baseAnnualPremium = value;
    }

    /**
     * Gets the value of the gst property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getGst() {
        return gst;
    }

    /**
     * Sets the value of the gst property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setGst(BigDecimal value) {
        this.gst = value;
    }

    /**
     * Gets the value of the stampDuty property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getStampDuty() {
        return stampDuty;
    }

    /**
     * Sets the value of the stampDuty property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setStampDuty(BigDecimal value) {
        this.stampDuty = value;
    }

    /**
     * Gets the value of the totalAnnualPremium property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getTotalAnnualPremium() {
        return totalAnnualPremium;
    }

    /**
     * Sets the value of the totalAnnualPremium property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setTotalAnnualPremium(BigDecimal value) {
        this.totalAnnualPremium = value;
    }

    /**
     * Gets the value of the installmentAmount property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getInstallmentAmount() {
        return installmentAmount;
    }

    /**
     * Sets the value of the installmentAmount property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setInstallmentAmount(BigDecimal value) {
        this.installmentAmount = value;
    }

    /**
     * Gets the value of the dreResult property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getDreResult() {
        return dreResult;
    }

    /**
     * Sets the value of the dreResult property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setDreResult(String value) {
        this.dreResult = value;
    }

}
