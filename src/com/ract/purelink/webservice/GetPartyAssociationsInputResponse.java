package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetPartyAssociationsInputResult" type="{http://www.ract.com.au/RactPureLink}GetPartyAssociationsResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getPartyAssociationsInputResult" })
@XmlRootElement(name = "GetPartyAssociationsInputResponse")
public class GetPartyAssociationsInputResponse {

    @XmlElementRef(name = "GetPartyAssociationsInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetPartyAssociationsResponse> getPartyAssociationsInputResult;

    /**
     * Gets the value of the getPartyAssociationsInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetPartyAssociationsResponse }{@code >}
     * 
     */
    public JAXBElement<GetPartyAssociationsResponse> getGetPartyAssociationsInputResult() {
        return getPartyAssociationsInputResult;
    }

    /**
     * Sets the value of the getPartyAssociationsInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetPartyAssociationsResponse }{@code >}
     * 
     */
    public void setGetPartyAssociationsInputResult(JAXBElement<GetPartyAssociationsResponse> value) {
        this.getPartyAssociationsInputResult = ((JAXBElement<GetPartyAssociationsResponse>) value);
    }

}
