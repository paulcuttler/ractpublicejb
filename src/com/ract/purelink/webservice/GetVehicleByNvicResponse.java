package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetVehicleByNvicResponse complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetVehicleByNvicResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vehicle" type="{http://www.ract.com.au/RactPureLink}GlassesVehicle" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetVehicleByNvicResponse", propOrder = { "vehicle" })
public class GetVehicleByNvicResponse {

    @XmlElementRef(name = "vehicle", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GlassesVehicle> vehicle;

    /**
     * Gets the value of the vehicle property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GlassesVehicle }{@code >}
     * 
     */
    public JAXBElement<GlassesVehicle> getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GlassesVehicle }{@code >}
     * 
     */
    public void setVehicle(JAXBElement<GlassesVehicle> value) {
        this.vehicle = ((JAXBElement<GlassesVehicle>) value);
    }

}
