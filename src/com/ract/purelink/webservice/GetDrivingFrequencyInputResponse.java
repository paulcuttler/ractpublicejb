package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetDrivingFrequencyInputResult" type="{http://www.ract.com.au/RactPureLink}GetDrivingFrequencyResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getDrivingFrequencyInputResult" })
@XmlRootElement(name = "GetDrivingFrequencyInputResponse")
public class GetDrivingFrequencyInputResponse {

    @XmlElementRef(name = "GetDrivingFrequencyInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetDrivingFrequencyResponse> getDrivingFrequencyInputResult;

    /**
     * Gets the value of the getDrivingFrequencyInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetDrivingFrequencyResponse }{@code >}
     * 
     */
    public JAXBElement<GetDrivingFrequencyResponse> getGetDrivingFrequencyInputResult() {
        return getDrivingFrequencyInputResult;
    }

    /**
     * Sets the value of the getDrivingFrequencyInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetDrivingFrequencyResponse }{@code >}
     * 
     */
    public void setGetDrivingFrequencyInputResult(JAXBElement<GetDrivingFrequencyResponse> value) {
        this.getDrivingFrequencyInputResult = ((JAXBElement<GetDrivingFrequencyResponse>) value);
    }

}
