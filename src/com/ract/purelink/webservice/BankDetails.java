package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for BankDetails complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BankDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bsb" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="accountNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="accountName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankDetails", propOrder = { "bsb", "accountNumber", "accountName" })
public class BankDetails {

    @XmlElement(required = true, nillable = true)
    protected String bsb;
    @XmlElement(required = true, nillable = true)
    protected String accountNumber;
    @XmlElement(required = true, nillable = true)
    protected String accountName;

    /**
     * Gets the value of the bsb property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getBsb() {
        return bsb;
    }

    /**
     * Sets the value of the bsb property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setBsb(String value) {
        this.bsb = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the accountName property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * Sets the value of the accountName property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setAccountName(String value) {
        this.accountName = value;
    }

}
