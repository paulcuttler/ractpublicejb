package com.ract.purelink.webservice;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetNoClaimDiscountResponse complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetNoClaimDiscountResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="noClaimBenefit" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetNoClaimDiscountResponse", propOrder = { "noClaimBenefit" })
public class GetNoClaimDiscountResponse {

    @XmlElement(required = true)
    protected BigDecimal noClaimBenefit;

    /**
     * Gets the value of the noClaimBenefit property.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getNoClaimBenefit() {
        return noClaimBenefit;
    }

    /**
     * Sets the value of the noClaimBenefit property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setNoClaimBenefit(BigDecimal value) {
        this.noClaimBenefit = value;
    }

}
