package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for PartyAssociation complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartyAssociation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="primaryParty" type="{http://www.ract.com.au/RactPureLink}Party"/>
 *         &lt;element name="secondaryParties" type="{http://www.ract.com.au/RactPureLink}PartyList"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyAssociation", propOrder = { "primaryParty", "secondaryParties" })
public class PartyAssociation {

    @XmlElement(required = true, nillable = true)
    protected Party primaryParty;
    @XmlElement(required = true, nillable = true)
    protected PartyList secondaryParties;

    /**
     * Gets the value of the primaryParty property.
     * 
     * @return possible object is {@link Party }
     * 
     */
    public Party getPrimaryParty() {
        return primaryParty;
    }

    /**
     * Sets the value of the primaryParty property.
     * 
     * @param value
     *            allowed object is {@link Party }
     * 
     */
    public void setPrimaryParty(Party value) {
        this.primaryParty = value;
    }

    /**
     * Gets the value of the secondaryParties property.
     * 
     * @return possible object is {@link PartyList }
     * 
     */
    public PartyList getSecondaryParties() {
        return secondaryParties;
    }

    /**
     * Sets the value of the secondaryParties property.
     * 
     * @param value
     *            allowed object is {@link PartyList }
     * 
     */
    public void setSecondaryParties(PartyList value) {
        this.secondaryParties = value;
    }

}
