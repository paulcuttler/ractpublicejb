package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetPartyAssociationsResponse complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPartyAssociationsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="partyAssociations" type="{http://www.ract.com.au/RactPureLink}PartyAssociationList"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPartyAssociationsResponse", propOrder = { "partyAssociations" })
public class GetPartyAssociationsResponse {

    @XmlElement(required = true, nillable = true)
    protected PartyAssociationList partyAssociations;

    /**
     * Gets the value of the partyAssociations property.
     * 
     * @return possible object is {@link PartyAssociationList }
     * 
     */
    public PartyAssociationList getPartyAssociations() {
        return partyAssociations;
    }

    /**
     * Sets the value of the partyAssociations property.
     * 
     * @param value
     *            allowed object is {@link PartyAssociationList }
     * 
     */
    public void setPartyAssociations(PartyAssociationList value) {
        this.partyAssociations = value;
    }

}
