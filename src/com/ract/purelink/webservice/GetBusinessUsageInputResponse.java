package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetBusinessUsageInputResult" type="{http://www.ract.com.au/RactPureLink}GetBusinessUsageResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getBusinessUsageInputResult" })
@XmlRootElement(name = "GetBusinessUsageInputResponse")
public class GetBusinessUsageInputResponse {

    @XmlElementRef(name = "GetBusinessUsageInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetBusinessUsageResponse> getBusinessUsageInputResult;

    /**
     * Gets the value of the getBusinessUsageInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetBusinessUsageResponse }{@code >}
     * 
     */
    public JAXBElement<GetBusinessUsageResponse> getGetBusinessUsageInputResult() {
        return getBusinessUsageInputResult;
    }

    /**
     * Sets the value of the getBusinessUsageInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetBusinessUsageResponse }{@code >}
     * 
     */
    public void setGetBusinessUsageInputResult(JAXBElement<GetBusinessUsageResponse> value) {
        this.getBusinessUsageInputResult = ((JAXBElement<GetBusinessUsageResponse>) value);
    }

}
