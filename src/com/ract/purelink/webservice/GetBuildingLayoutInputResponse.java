package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetBuildingLayoutInputResult" type="{http://www.ract.com.au/RactPureLink}GetBuildingLayoutResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getBuildingLayoutInputResult" })
@XmlRootElement(name = "GetBuildingLayoutInputResponse")
public class GetBuildingLayoutInputResponse {

    @XmlElementRef(name = "GetBuildingLayoutInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetBuildingLayoutResponse> getBuildingLayoutInputResult;

    /**
     * Gets the value of the getBuildingLayoutInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetBuildingLayoutResponse }{@code >}
     * 
     */
    public JAXBElement<GetBuildingLayoutResponse> getGetBuildingLayoutInputResult() {
        return getBuildingLayoutInputResult;
    }

    /**
     * Sets the value of the getBuildingLayoutInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetBuildingLayoutResponse }{@code >}
     * 
     */
    public void setGetBuildingLayoutInputResult(JAXBElement<GetBuildingLayoutResponse> value) {
        this.getBuildingLayoutInputResult = ((JAXBElement<GetBuildingLayoutResponse>) value);
    }

}
