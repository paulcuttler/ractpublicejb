package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetMakesInputResult" type="{http://www.ract.com.au/RactPureLink}GetMakesResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getMakesInputResult" })
@XmlRootElement(name = "GetMakesInputResponse")
public class GetMakesInputResponse {

    @XmlElementRef(name = "GetMakesInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetMakesResponse> getMakesInputResult;

    /**
     * Gets the value of the getMakesInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetMakesResponse }{@code >}
     * 
     */
    public JAXBElement<GetMakesResponse> getGetMakesInputResult() {
        return getMakesInputResult;
    }

    /**
     * Sets the value of the getMakesInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetMakesResponse }{@code >}
     * 
     */
    public void setGetMakesInputResult(JAXBElement<GetMakesResponse> value) {
        this.getMakesInputResult = ((JAXBElement<GetMakesResponse>) value);
    }

}
