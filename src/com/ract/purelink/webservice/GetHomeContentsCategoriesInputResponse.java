package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetHomeContentsCategoriesInputResult" type="{http://www.ract.com.au/RactPureLink}GetHomeContentsCategoriesResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getHomeContentsCategoriesInputResult" })
@XmlRootElement(name = "GetHomeContentsCategoriesInputResponse")
public class GetHomeContentsCategoriesInputResponse {

    @XmlElementRef(name = "GetHomeContentsCategoriesInputResult", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<GetHomeContentsCategoriesResponse> getHomeContentsCategoriesInputResult;

    /**
     * Gets the value of the getHomeContentsCategoriesInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetHomeContentsCategoriesResponse }{@code >}
     * 
     */
    public JAXBElement<GetHomeContentsCategoriesResponse> getGetHomeContentsCategoriesInputResult() {
        return getHomeContentsCategoriesInputResult;
    }

    /**
     * Sets the value of the getHomeContentsCategoriesInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetHomeContentsCategoriesResponse }{@code >}
     * 
     */
    public void setGetHomeContentsCategoriesInputResult(JAXBElement<GetHomeContentsCategoriesResponse> value) {
        this.getHomeContentsCategoriesInputResult = ((JAXBElement<GetHomeContentsCategoriesResponse>) value);
    }

}
