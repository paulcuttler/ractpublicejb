package com.ract.purelink.webservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for RiskTypeCode.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="RiskTypeCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="HOME"/>
 *     &lt;enumeration value="MOTR"/>
 *     &lt;enumeration value="INVS"/>
 *     &lt;enumeration value="STRT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RiskTypeCode")
@XmlEnum
public enum RiskTypeCode {

    HOME, MOTR, INVS, STRT;

    public String value() {
        return name();
    }

    public static RiskTypeCode fromValue(String v) {
        return valueOf(v);
    }

}
