package com.ract.web.temp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import com.ract.common.MailMgrLocal;
import com.ract.common.mail.MailMessage;
import com.ract.util.ConnectionUtil;
import com.ract.util.LogUtil;
import com.ract.util.StringUtil;

@Stateless
public class FootyTippingMgr implements FootyTippingMgrRemote
{
	@Resource(mappedName = "java:/WebDS")
	private DataSource clientDataSource;

	@EJB
	private MailMgrLocal mailMgr;

	private String assembleWelcomeMessage(String userName)
	{
		
		/*
		 * <strong>Welcome to RACT Footy Tipping for 2010.</strong> &nbsp;</p>
		 * <p>RACT is a dedicated supporter of the TSL and big fans of the AFL. And
		 * to prove we're no one-season wonder, we're back for another season with a
		 * bigger, better platform, and loads of prizes to give away.&nbsp;</p>
		 * <p>Win the round in either league and guess the closest points margin -
		 * and you could win $250! Win the season with the best-accumulated score
		 * per round and you could win $2000!&nbsp;</p> 
		 * <p>We've also listened to
		 * our tippers and included a few exciting new features to enhance your
		 * tipping experience.</p> 
		 * <p><strong>RACT Tipster of the
		 * week.</strong></p><p>Each week we'll be featuring a tipster from RACT,
		 * and give you a few insights from the inside. Will you trust their tips?
		 * <strong>&nbsp;</strong></p><p><strong>Tip in
		 * advance.</strong></p><p>Whether you're going away or you're just super
		 * organised, you can now tip rounds in advance. And if you change your
		 * mind, you can always change your tips later.</p><p><strong>Play your
		 * mates.</strong></p><p>If it's bragging rights you're after- start a
		 * league and tip against your friends.\
		 * "My League\" allows you to set-up your own sub competition so you can see how you're tracking against people you know.</p>
		 * <p><strong>Make a change.</strong></p>
		 * <p>We've made it easier to change your user details such as your password, email address or email preferences. Just log in and follow the links on the left hand side of the page.</p><p>And of course, there are all the great features from last season too. With so many great games to look forward to, we can't wait for the season to start.</p><p>Now that you've registered, why not try out the log-in by going to <a href=\"http://www.ract.com.au/footy_tipping\">www.ract.com.au/footy_tipping</a> and check out round one of the AFL and TSL fixtures.</p><p>&nbsp;</p>
		 * <p>Goodluck!</p><p>&nbsp;</p><p>&nbsp;</p><p>
		 * <strong>RACT Tipping Team</strong></p></td></tr></table>"
		 * );
		 */		
		
		StringBuffer innerMessage = new StringBuffer(); 
		innerMessage.append("<table id=\"emailContent\" cellspacing=\"0\"><tr><td><p>Dear " + StringUtil.replace(userName, " ", "") + ",</p>");
		innerMessage.append("<p>If you would like to be part of RACT footy tipping in 2010 you will need to sign up <i>again</i> even if you were part of RACT footy tipping in 2009.<p></p><b>You will not be able to login to footy tipping until you re-register.</b></p>");	
		innerMessage.append("<p>To re-register for RACT footy tipping in 2010 click <a href=\"https://secure.ract.com.au/UserTippingSignup.action\">here</a>.<p>");
		innerMessage.append("<p><b>Re-register before the first TSL game (2nd April 2010, 1:50pm) to be in the draw for the early registration prize of $500.</b><p>");		
		innerMessage.append("<p>Good luck!</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>RACT Tipping Team</strong></p></td></tr></table>");
		
		StringBuffer message = new StringBuffer();
		message.append("<html><head><title>RACT </title><meta content=\"Future Medium - http://www.futuremedium.com.au/\" name=\"author\"><meta content=\"RACT\" name=\"description\"><meta content=\"RACT\" name=\"keywords\"><style type=\"text/css\">");
		message.append("@import url-filtered(\"http://www.ract.com.au/styles/utility.css\");");
		message.append("@import url-filtered(\"http://www.ract.com.au/styles/ract_main.css\");");
		message.append("#email { ");
		message.append("  background: #fff; ");
		message.append("  padding: 20px; ");
		message.append("} ");
		message.append("#emailhead { ");
		message.append("  border-bottom: 2px solid #004990; ");
		message.append("} ");
		message.append("#emailhead span { ");
		message.append("   font-size: 1.6em; ");
		message.append("   font-weight: bold; ");
		message.append("    display: block; ");
		message.append("} ");
		message.append(" #emailContent { ");
		message.append("   border-bottom: 1px solid #D1D1D1; ");
		message.append("   margin: 10px 0; ");
		message.append("   width: 100%; ");
		message.append(" } ");
		message.append("#emailContent td { ");
		message.append("   padding: 20px; ");
		message.append("   font-size: 12px; ");
		message.append(" } ");
		message.append("</style></head><body id=\"email\"><table id=\"emailhead\" cellspacing=\"0\"> ");
		message.append("<tr><td><img alt=\"RACT\" height=\"98\" width=\"116\" src=\"http://www.ract.com.au/images/logo_tipping_email.gif\"><br><br></td><td width=\"99%\"><br></td></tr></table>");

		// message
    message.append(innerMessage);
		message.append("<div id=\"emailfooter\">");
		message.append("Visit us online: <a href=\"http://www.ract.com.au\">http://www.ract.com.au</a><br>");
		message.append("&copy;Copyright 2010 RACT.");
		message.append("</div></body></html>");

		return message.toString();
	}

	public void sendWelcomeEmail()
	{
		LogUtil.debug(this.getClass(), "sendWelcomeEmail START");
		int counter = 0;
		final String SUBJECT = "RACT Footy Tipping 2010";
		PreparedStatement statement = null;
		Connection connection = null;
		try
		{
			connection = clientDataSource.getConnection();
			statement = connection
					.prepareStatement("select userName, emailAddress from Users_20091215 where emailAddress not in (select emailAddress from Users_20100324) order by userName ");
			ResultSet rs = statement.executeQuery();
			String userName = null;
			String emailAddress = null;
			String messageText = null;
//			String EMAIL = "j.holliday@ract.com.au";
			while (rs.next())
			{
				counter++;
				LogUtil.debug(this.getClass(), "counter=" + counter);
				userName = rs.getString(1);
				emailAddress = rs.getString(2);

				LogUtil.debug(this.getClass(), "userName=" + userName);
				LogUtil.debug(this.getClass(), "emailAddress=" + emailAddress);
				LogUtil.debug(this.getClass(), "SUBJECT=" + SUBJECT);				

				messageText = assembleIPadMessage(userName);
				try
				{
					MailMessage message = new MailMessage();
					message.setRecipient(emailAddress);
					message.setSubject(SUBJECT);
					message.setMessage(messageText);
					message.setFrom("footy.tipping@ract.com.au");
					message.setHtml(true);
					
					mailMgr.sendMail(message);
				}
				catch (Exception e)
				{
					LogUtil.debug(this.getClass(), "error=" + e.getMessage());
				}
				LogUtil.debug(this.getClass(), "message=" + messageText);
			}
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			ConnectionUtil.closeConnection(connection, statement);
		}
    LogUtil.log(this.getClass(), "Sent email to "+counter+" footy tippers");
		LogUtil.debug(this.getClass(), "sendWelcomeEmail END");    
	}
	
	private String assembleIPadMessage(String userName)
	{
		
		/*
		 * <strong>Welcome to RACT Footy Tipping for 2010.</strong> &nbsp;</p>
		 * <p>RACT is a dedicated supporter of the TSL and big fans of the AFL. And
		 * to prove we're no one-season wonder, we're back for another season with a
		 * bigger, better platform, and loads of prizes to give away.&nbsp;</p>
		 * <p>Win the round in either league and guess the closest points margin -
		 * and you could win $250! Win the season with the best-accumulated score
		 * per round and you could win $2000!&nbsp;</p> 
		 * <p>We've also listened to
		 * our tippers and included a few exciting new features to enhance your
		 * tipping experience.</p> 
		 * <p><strong>RACT Tipster of the
		 * week.</strong></p><p>Each week we'll be featuring a tipster from RACT,
		 * and give you a few insights from the inside. Will you trust their tips?
		 * <strong>&nbsp;</strong></p><p><strong>Tip in
		 * advance.</strong></p><p>Whether you're going away or you're just super
		 * organised, you can now tip rounds in advance. And if you change your
		 * mind, you can always change your tips later.</p><p><strong>Play your
		 * mates.</strong></p><p>If it's bragging rights you're after- start a
		 * league and tip against your friends.\
		 * "My League\" allows you to set-up your own sub competition so you can see how you're tracking against people you know.</p>
		 * <p><strong>Make a change.</strong></p>
		 * <p>We've made it easier to change your user details such as your password, email address or email preferences. Just log in and follow the links on the left hand side of the page.</p><p>And of course, there are all the great features from last season too. With so many great games to look forward to, we can't wait for the season to start.</p><p>Now that you've registered, why not try out the log-in by going to <a href=\"http://www.ract.com.au/footy_tipping\">www.ract.com.au/footy_tipping</a> and check out round one of the AFL and TSL fixtures.</p><p>&nbsp;</p>
		 * <p>Goodluck!</p><p>&nbsp;</p><p>&nbsp;</p><p>
		 * <strong>RACT Tipping Team</strong></p></td></tr></table>"
		 * );
		 */		
		
		StringBuffer innerMessage = new StringBuffer(); 
		innerMessage.append("<table id=\"emailContent\" cellspacing=\"0\"><tr><td><p>Hi " + StringUtil.replace(userName, " ", "") + ",</p>");
		innerMessage.append("<p><b>Thanks for being a part of the 2010 RACT Footy Tipping Competition.</b></p>");	
		innerMessage.append("<p>As we eagerly await that one day in September, it's time to look back on what a great season it's been. There's been close games and some fierce competition - and not just on the field. With tippers state wide competing in both the AFL and TSL leagues - two have reigned supreme.<p>");
		innerMessage.append("<p>Want to see who won the 2010 RACT Footy Tipping Competition? Click <a href=\"http://www.ract.com.au/footy_tipping\">here</a><p>");		
		innerMessage.append("<p><b>Are you an RACT Roadside member?</b></p>");
		innerMessage.append("<p>Keep us up to date on your contact details <a href=\"https://secure.ract.com.au/Login.action\">here</a>.</p>");
		innerMessage.append("<p><b>Not a Roadside member?</b></p>");
		innerMessage.append("<p>For all the information and benefits of RACT membership, or to join online, click <a href=\"http://www.ract.com.au/membership\">here</a>.</table>");
		
		StringBuffer message = new StringBuffer();
		message.append("<html><head><title>RACT </title><meta content=\"Future Medium - http://www.futuremedium.com.au/\" name=\"author\"><meta content=\"RACT\" name=\"description\"><meta content=\"RACT\" name=\"keywords\"><style type=\"text/css\">");
		message.append("@import url-filtered(\"http://www.ract.com.au/styles/utility.css\");");
		message.append("@import url-filtered(\"http://www.ract.com.au/styles/ract_main.css\");");
		message.append("#email { ");
		message.append("  background: #fff; ");
		message.append("  padding: 20px; ");
		message.append("} ");
		message.append("#emailhead { ");
		message.append("  border-bottom: 2px solid #004990; ");
		message.append("} ");
		message.append("#emailhead span { ");
		message.append("   font-size: 1.6em; ");
		message.append("   font-weight: bold; ");
		message.append("    display: block; ");
		message.append("} ");
		message.append(" #emailContent { ");
		message.append("   border-bottom: 1px solid #D1D1D1; ");
		message.append("   margin: 10px 0; ");
		message.append("   width: 100%; ");
		message.append(" } ");
		message.append("#emailContent td { ");
		message.append("   padding: 20px; ");
		message.append("   font-size: 12px; ");
		message.append(" } ");
		message.append("</style></head><body id=\"email\"><table id=\"emailhead\" cellspacing=\"0\"> ");
		message.append("<tr>" +
				"<td><img alt=\"RACT\" height=\"98\" width=\"116\" src=\"http://www.ract.com.au/images/logo_tipping_email.gif\"></td>" +
				"<td><a href=\"http://www.ract.com.au/promotion\"><img src=\"http://www.ract.com.au/uploaded/9/18008_70tipping_email_banner.jpg\"></a>" +
				"<td width=\"99%\"></td><br></td>" +
				"</tr></table>");

		// message
    message.append(innerMessage);
		message.append("<div id=\"emailfooter\">");
		message.append("Visit us online: <a href=\"http://www.ract.com.au\">http://www.ract.com.au</a><br>");
		message.append("&copy;Copyright 2010 RACT.");
		message.append("</div></body></html>");

		return message.toString();
	}	
	
	public void sendIPadEmail()
	{
		LogUtil.debug(this.getClass(), "sendIPadEmail START");
		int counter = 0;
		final String SUBJECT = "RACT Footy Tipping 2010 Round-up";
		PreparedStatement statement = null;
		Connection connection = null;
		try
		{
			connection = clientDataSource.getConnection();
			statement = connection
					.prepareStatement("select userName, emailAddress from Users where footytipping = ? and emailAddress not like '%ract.com.au' order by userName ");
      statement.setBoolean(1, true);
			ResultSet rs = statement.executeQuery();
			
			String userName = null;
			String emailAddress = null;
			String messageText = null;
			 
			String EMAIL = "g.hankin@ract.com.au";
			while (rs.next())
			{
				counter++;
				LogUtil.debug(this.getClass(), "counter=" + counter);
				userName = rs.getString(1);
				emailAddress = rs.getString(2);

				LogUtil.debug(this.getClass(), "userName=" + userName);
				LogUtil.debug(this.getClass(), "emailAddress=" + emailAddress);
				LogUtil.debug(this.getClass(), "SUBJECT=" + SUBJECT);				

				messageText = assembleIPadMessage(userName);
				try
				{
					MailMessage message = new MailMessage();
					message.setRecipient(emailAddress);
					message.setSubject(SUBJECT);
					message.setMessage(messageText);
					message.setFrom("g.hankin@ract.com.au");
					message.setHtml(true);

					mailMgr.sendMail(message);
				}
				catch (Exception e)
				{
					LogUtil.debug(this.getClass(), "error=" + e.getMessage());
				}
				LogUtil.debug(this.getClass(), "message=" + messageText);
			}
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			ConnectionUtil.closeConnection(connection, statement);
		}
    LogUtil.log(this.getClass(), "Sent email to "+counter+" footy tippers");
		LogUtil.debug(this.getClass(), "sendIPadEmail END");    
	}	
	
}
