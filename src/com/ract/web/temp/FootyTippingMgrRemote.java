package com.ract.web.temp;

import javax.ejb.Remote;

@Remote
public interface FootyTippingMgrRemote
{

	public void sendWelcomeEmail();	
	
	public void sendIPadEmail();	
}
