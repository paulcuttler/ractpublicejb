package com.ract.web.test;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.ract.common.ServiceLocator;
import com.ract.util.DateTime;
import com.ract.web.campaign.Campaign;
import com.ract.web.campaign.CampaignEntry;
import com.ract.web.campaign.CampaignMgrRemote;
import com.ract.web.campaign.CampaignVehicleEntry;

public class CampaignTest {

	CampaignMgrRemote bean;
	private static final String SAFFIRE_CAMPAIGN = "SAFFIRE";
	private static final String FOOTY_CAMPAIGN = "NTHMLB";
	private final Integer TEST_ID = 16;
	private final Integer CLIENT_NO = 432002;
	
	@Before
	public void setUp() throws Exception {
		bean = (CampaignMgrRemote) ServiceLocator.getInstance().getObject("CampaignMgr/remote");        
	}

	//@Test
	public void initCampaigns() throws Exception {
		Campaign c = bean.retrieveCampaign(SAFFIRE_CAMPAIGN);		
		if (c == null) {
			c = new Campaign(SAFFIRE_CAMPAIGN, "Saffire", new DateTime("12/2/2012"));
			bean.createCampaign(c);
		}				
		assertTrue(c.isActive());
		
		c = bean.retrieveCampaign(FOOTY_CAMPAIGN);		
		if (c == null) {
			c = new Campaign(FOOTY_CAMPAIGN, "North Melbourne Free Tickets", new DateTime("12/2/2012"));
			bean.createCampaign(c);
		}				
		assertTrue(c.isActive());
		
		c.setExpiryDate(new DateTime("16/1/2012 17:00:00")); // don't save
		assertTrue(!c.isActive());
	}
		
	//@Test
	public void testCreateVehicle() throws Exception {
		CampaignVehicleEntry c = new CampaignVehicleEntry(SAFFIRE_CAMPAIGN,
				CLIENT_NO,
				"MR",
				"Test",
				"Person",
				true,
				new DateTime("31/05/1980"),
				1234,
				"27",
				"ABC123",
				4,
				"g.newton@ract.com.au",
				"0400000000");
		/*c.setBirthDate(new DateTime("31/05/1980"));
		c.setClientNumber(CLIENT_NO);
		c.setEmail("g.newton@ract.com.au");
		c.setFirstName("Test");
		c.setLastName("Person");
		c.setGender(true); // male
		c.setNvic("ABC123");
		c.setRenewalMonth(5);
		c.setStreetNumber("27");
		c.setStreetSuburbId(1234);
		c.setTitle("MR");
		c.setPhone("0400000000");*/
		
		bean.createEntry(c);
	}

	//@Test
	public void testUpdateVehicle() throws Exception {
		CampaignVehicleEntry c = (CampaignVehicleEntry) bean.retrieveEntry(TEST_ID);
		c.setClientNumber(CLIENT_NO);
		
		bean.updateEntry(c);
		
		System.out.println("Updated entry with ID: " + c.getId());
	}

	//@Test
	public void listVehicleEntries() {
		List<CampaignEntry> entries = bean.getEntriesByReference(SAFFIRE_CAMPAIGN);

		System.out.println("Found " + entries.size() + " entries for campaign: " + SAFFIRE_CAMPAIGN);
		
		assertTrue(!entries.isEmpty());		
	}
	
	@Test
	public void retrieveInvalidCampaign() {
		Campaign c = bean.retrieveCampaign("FUDGE");
		
		assertNull(c);
	}
	
	//@Test
	public void listClientEntries() {
		List<CampaignEntry> entries = bean.getEntriesByReferenceAndClientNumber(SAFFIRE_CAMPAIGN, CLIENT_NO);

		System.out.println("Found " + entries.size() + " entries for campaign: " + SAFFIRE_CAMPAIGN + ", client: " + CLIENT_NO);
		
		assertTrue(!entries.isEmpty());		
	}
	
	//@Test
	public void testCreateMemberPromo() throws RemoteException {
		CampaignEntry c = new CampaignEntry(FOOTY_CAMPAIGN, CLIENT_NO);

		bean.createEntry(c);
		
		boolean createOrphan = false;
		try {
			CampaignEntry f = new CampaignEntry("DUD", 1234);
			bean.createEntry(f);
			createOrphan = true;
		} catch (Exception e) {
			System.out.println(e);
		}
		
		assertFalse(createOrphan);
	}
	
	//@Test
	public void listMemberEntries() {
		List<CampaignEntry> entries = bean.getEntriesByReference(FOOTY_CAMPAIGN);

		System.out.println("Found " + entries.size() + " entries for campaign: " + FOOTY_CAMPAIGN);
		
		assertTrue(!entries.isEmpty());		
	}
	
	//@Test
	public void listMemberClientEntries() {
		List<CampaignEntry> entries = bean.getEntriesByReferenceAndClientNumber(FOOTY_CAMPAIGN, CLIENT_NO);

		System.out.println("Found " + entries.size() + " entries for campaign: " + FOOTY_CAMPAIGN + ", client: " + CLIENT_NO);
		
		assertTrue(!entries.isEmpty());		
	}
	

}
