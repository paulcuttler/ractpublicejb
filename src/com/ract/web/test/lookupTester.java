package com.ract.web.test;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ract.common.ServiceLocator;
import com.ract.util.DateTime;
import com.ract.web.insurance.InRfDet;
import com.ract.web.insurance.InRiskSi;
import com.ract.web.insurance.InRiskSiLineItem;
import com.ract.web.insurance.WebInsMgrRemote;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;


public class lookupTester 
{
	WebInsMgrRemote bean;
	@Before
	public void setUp() throws Exception
	{
	   System.out.println("Set up");
 	   bean = (WebInsMgrRemote) ServiceLocator.getInstance().getObject("WebInsMgr/remote");        
	}
	@After
	public void tearDown() throws Exception
	{
          System.out.println("tearDown");		
	}
	
	//@Test
	public void testGetLookup() throws Exception
	{
		String fName = WebInsQuoteDetail.OPTIONS;
		String selected = "true";
		System.out.println("lookupTester.testGetLookup......");
		ArrayList<InRfDet> list = bean.getLookupData("PIP",
				                            WebInsQuote.COVER_CNTS,
				                            "OPTIONS",
				                            new DateTime());
		InRfDet dt = null;
		for(int xx = 0; xx< list.size();xx++)
		{
			dt = list.get(xx);
			if(selected.equals("true"))selected = "false";
			else selected = "true";
			System.out.println(dt.getrKey3()
         			           + " " + dt.getrValue()
                               + " " + dt.getrUnit()
					           + " " + dt.isAcceptable()
   					           + " " + dt.getrClass()
					           + " " + dt.getrDescription()
					           + " " + dt.getRfDateSeq()
					           + " " + dt.getrKey3()
                               );
			WebInsQuoteDetail det = new WebInsQuoteDetail(new Integer(1234567),
					                                      dt.getrDescription(),
					                                      dt.getrClass(),
					                                      dt);
			bean.setQuoteDetail(det);
		}
	}
	
//	@Test 
	public void testNumberFormat() throws Exception
	{
		String str = "272500";
		NumberFormat nf = NumberFormat.getIntegerInstance();
	    		System.out.println(nf.format(new Integer(str)));
	}
	
	
	
	@Test
	public void testGarthsCode() throws Exception
	{
		
		String coverType = WebInsQuote.COVER_TP;
		String field = WebInsQuoteDetail.OPTIONS;
		System.out.println("Retrieving options for '" + coverType + "': '" + field + "'");
		
		List<InRfDet> options = this.bean.getLookupData("", 
				                                        coverType, 
				                                        field,
				                                        new DateTime());
		for(int xx = 0; xx < options.size();xx++)
		{
			InRfDet thing = options.get(xx);
			System.out.println(thing.getrClass() + ": " + thing.getrDescription() + " = " + thing.getrValue());
		}
		
		coverType = WebInsQuote.COVER_COMP;
		System.out.println("Retrieving options for '" + coverType + "': '" + field + "'");
		
		options = this.bean.getLookupData("", 
				                                        coverType, 
				                                        field,
				                                        new DateTime());
		for(int xx = 0; xx < options.size();xx++)
		{
			InRfDet thing = options.get(xx);
			System.out.println(thing.getrClass() + ": " + thing.getrDescription() + " = " + thing.getrValue());
		}
	}
	
//	@Test
	public void testSetSpecifiedItems() throws Exception
	{
		InRiskSi si = new InRiskSi(new Integer(123456789),
				                   new Integer(1),
				                   "JW",
				                   "Seiko Gold Watch",
				                   "333-TP179Q",
				                   new BigDecimal(950.00));
		bean.setSpecifiedItem(si);
	    si = new InRiskSi(new Integer(123456789),
	    		          new Integer(2),
	    		          "CA",
	    		          "Nikon black body SLR model 4569",
	    		          "6678-2231",
	    		          new BigDecimal(1050.00));
	    si.addLineItem("Tamron 20 - 200mm macro lens",
	    		       "ABT-22100076",
	    		       new BigDecimal(790.00));
	    si.addLineItem("Nikkor 500mm f3.5 telephoto lense",
	    		       "560098-115L",
	    		       new BigDecimal(1890.00));
	    bean.setSpecifiedItem(si);		       
		
	}
	
//	@Test
	public void testGetSpecifiedItems()throws Exception
	{
		InRiskSi item = null;
		InRiskSiLineItem li = null;
		ArrayList<InRiskSi>list = bean.getSpecifiedItems(123456789);
		for(int xx=0;xx<list.size();xx++)	
		{
			item = list.get(xx);
			System.out.println(item.getSiClass());
			for(int yy =0 ;yy<item.getLineCount();yy++)
			{
				System.out.println("     " + item.getDescription(yy)
						         + "  " + item.getSerialNo(yy)
						         + "  " + item.getSumIns(yy));
			}
		}
	}
	
//	@Test
	public void removeSpecItems()throws Exception
	{
		Integer quoteNo = new Integer(123456789);
		bean.removeAllSpecifiedItems(quoteNo);
	}
	

}
