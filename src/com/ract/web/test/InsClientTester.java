package com.ract.web.test;
import java.util.ArrayList;

import javax.naming.InitialContext;

import org.junit.After;
import org.junit.Before;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.common.ServiceLocator;
import com.ract.common.integration.RACTPropertiesProvider;
import com.ract.util.DateTime;
import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsClientDetail;
import com.ract.web.insurance.WebInsClientDetailPK;
import com.ract.web.insurance.WebInsMgrRemote;

public class InsClientTester {
	WebInsMgrRemote bean;
	InitialContext ctx;
	Integer quoteNo = new Integer(6);
	Integer clientNo = new Integer(4);
	
	@Before
	public void setUp() throws Exception
	{
	    RACTPropertiesProvider.setContextURL("localhost", 1099);			
//        RACTPropertiesProvider.setContextURL("203.20.20.50", 1099);		
		bean = (WebInsMgrRemote) ServiceLocator.getInstance().getObject("WebInsMgr/remote");		
	}
//  @Test    
/*    public void testCreateClient()throws Exception
    {
    	WebInsClient clt = bean.createInsuranceClient(quoteNo);
        System.out.println("New Client:\n" + clt.toString());  
    }
*/     
//   @Test
    public void testSetFromCardNo() throws Exception
    {
    	/* membershipId Number 344*/
//    	String cardNo = "3084070106246194";
//    	DateTime bDate = new DateTime("16/03/1951");
    	ArrayList<Card> cards = new ArrayList<Card>();
//    	cards.add(new Card("3084070202000028","16/12/1927"));
//    	cards.add(new Card("3084070102000078","15/08/1959"));
//    	cards.add(new Card("3084070102000151","6/11/1938"));
    	
  
    	Integer quoteNo = new Integer(4);
    	for (int xx = 0; xx< cards.size();xx++)
    	{
    		Card c = cards.get(xx);
    	   	WebInsClient clt = bean.setFromRACTClient(quoteNo, c.cardNo, c.birthDate);
    	    if(clt!=null)
    	    {
    		   System.out.println(clt.toString());
    		   System.out.println("RACT Client no = " + clt.getRactClientNo());
    		}
    	    else
    	    {
    	    	System.out.println("No client found for " + c.cardNo);
    	    }
    	}
    	return;
    }
    
//    @Test
    public void testRetrieveCardNumber() throws Exception
    {
	    RACTPropertiesProvider.setContextURL("localhost", 1099);
	    ServiceLocator slo =ServiceLocator.getInstance();
	    ClientMgr cmgr = ClientEJBHelper.getClientMgr();
	    RACTPropertiesProvider.setContextURL("localhost", 1099);			

	    com.ract.membership.MembershipMgr mgr
		     = (com.ract.membership.MembershipMgr) ServiceLocator.getInstance().getObject("MembershipMgr");		

    	Integer clientNo = new Integer(248998);
    	com.ract.membership.MembershipCardVO mc = mgr.getMembershipCard(clientNo);
    	String cardNo = mc.getCardNumber();
    	com.ract.client.ClientVO clt = cmgr.getClient(clientNo);
    	DateTime birthDate = clt.getBirthDate();
    	System.out.println("Card No = " + cardNo
    			       + "\nBirth Date = " + birthDate.formatShortDate());
    }
    
    
    
//	@Test
/*	public void testSetClient() throws Exception
	{
		System.out.println("Update client records......");
		Integer clientNo = new Integer(2);
		WebInsClient clt = bean.getClient(clientNo);
    	clt.setSurname("Harrington");
		clt.setGivenNames("Gordon James");
		clt.setTitle("Mr");
		clt.setDateOfBirth(new DateTime(15/4/1968));
		clt.setDriver(true);
		clt.setDrivingFrequency(WebInsClient.DRIVE_3_4);
		clt.setYearCommencedDriving(new Integer(1990));
		clt.setCurrentNCD(new Integer(15));
	
		bean.setClient(clt);
	}
*/
//	@Test
/*	public void testMakesSomeClients()throws Exception
	{
    	WebInsClient clt = bean.createInsuranceClient(quoteNo);
    	clt.setSurname("Smythe");
		clt.setGivenNames("Frank");
		clt.setTitle("Mr");
		clt.setDateOfBirth(new DateTime("15/4/1979"));
		clt.setDriver(true);
		clt.setDrivingFrequency(WebInsClient.DRIVE_3_4);
		clt.setCurrentNCD(new Integer(40));
		clt.setGender("Male");
		clt.setNumberOfAccidents(new Integer(0));
		clt.setOwner(true);
		clt.setPostStreetChar("5/69C");
		clt.setPostStreet("Olinda Grove");
		clt.setPostSuburb("Mt Nelson");
		clt.setPreferredAddress(true);
		clt.setYearCommencedDriving(new Integer(1996));
		bean.setClient(clt);

		
	   	clt = bean.createInsuranceClient(quoteNo);

    	clt.setSurname("Smythe");
		clt.setGivenNames("George");
		clt.setTitle("Mr");
		clt.setDateOfBirth(new DateTime("15/4/1990"));
		clt.setDriver(true);
		clt.setDrivingFrequency(WebInsClient.DRIVE_3_4);
		bean.setClient(clt);

		clt = bean.createInsuranceClient(quoteNo);
    	clt.setSurname("Smythe");
		clt.setGivenNames("Michael");
		clt.setTitle("Mr");
		clt.setDateOfBirth(new DateTime("15/4/1991"));
		clt.setDriver(true);
		clt.setDrivingFrequency(WebInsClient.DRIVE_LESS);
		bean.setClient(clt);
	}
*/	
//	@Test
/*	public void testGetClients()throws Exception
	{
		Integer quoteNo = new Integer(5);
		ArrayList<WebInsClient>list = bean.getClients(quoteNo);
		WebInsClient clt = null;
		String dob = null;
		for(int xx = 0; xx < list.size(); xx++)
		{
			clt = list.get(xx);
			if(clt.getDateOfBirth()!=null)dob = clt.getDateOfBirth().formatShortDate();
			else dob = null;
			System.out.println(clt.getWebClientNo()
					          + "  " +clt.getGivenNames()
					          + "  " + dob
					          + "  " + clt.getDrivingFrequency());

		}
	}
*/	
//	@Test
	public void testGetRatingDriver() throws Exception
	{
		WebInsClient clt = bean.getRatingDriver(quoteNo);
		System.out.println("Rating driver: " + clt.getWebClientNo()
				       + "\nDriving frequency: " + clt.getDrivingFrequency()
				       + "\nDate of Birth: " + clt.getBirthDate().formatShortDate()
				       );
	}
	
//	@Test
	public void testGetNCD() throws Exception
	{
		Integer clientNo = new Integer(9);
		WebInsClient clt = bean.getWebInsClient(clientNo);
		clt.setYearCommencedDriving(new Integer(2004));
		clt.setNumberOfAccidents(new Integer(1));
		
		bean.setClient(clt);
		Integer ncd = bean.calculateNCD(clt);
		System.out.println("NCD = " + ncd);
	}
//	@Test
/*	public void testMakesSomeOwners()throws Exception
	{
        Integer quoteNo = new Integer(5);
		WebInsClient clt = bean.createInsuranceClient(quoteNo);
    	clt.setSurname("Smythe");
		clt.setGivenNames("John");
		clt.setTitle("Mr");
		clt.setDateOfBirth(new DateTime("15/4/1949"));
		clt.setDriver(true);
		clt.setOwner(true);
		clt.setDrivingFrequency(WebInsClient.DRIVE_3_4);
		bean.setClient(clt);
		
	   	clt = bean.createInsuranceClient(quoteNo);
    	clt.setSurname("Smythe");
		clt.setGivenNames("Mary");
		clt.setTitle("Miss");
		clt.setDateOfBirth(new DateTime("15/4/1950"));
		clt.setDriver(true);
		clt.setOwner(true);
		clt.setDrivingFrequency(WebInsClient.DRIVE_1_2);
		bean.setClient(clt);

		clt = bean.createInsuranceClient(quoteNo);
    	clt.setSurname("Webber");
		clt.setGivenNames("Michael");
		clt.setTitle("Mr");
		clt.setDateOfBirth(new DateTime("15/4/1991"));
		clt.setDriver(true);
		clt.setOwner(false);
		clt.setDrivingFrequency(WebInsClient.DRIVE_LESS);
		bean.setClient(clt);
	}
*/	
	
	
	
	
//	@Test
	public void testAddDetail()throws Exception
	{
		WebInsClientDetail det = new WebInsClientDetail(clientNo,
				                                        WebInsClient.CLIENT_DETAIL_ACC_THEFT,
				                                        new Integer(11),
				                                        new Integer(2011),
				                                        "This is an accident waiting to happen");
		bean.setClientDetail(det);
		det = new WebInsClientDetail(clientNo,
                WebInsClient.CLIENT_DETAIL_REFUSED,
                new Integer(22),
                new Integer(2022),
                "No insurance until 22/2022");
        bean.setClientDetail(det);

	}
	
//	@Test
	public void testRemoveDetail() throws Exception
	{
		System.out.println("Removing client detail #4");
		System.out.println("Remove detail.........");
		WebInsClient clt = bean.getWebInsClient(this.clientNo);
		WebInsClientDetail det = new WebInsClientDetail();
		WebInsClientDetailPK pk = new WebInsClientDetailPK();
		pk.setWebClientNo(this.clientNo);
		pk.setDetailSeq(new Integer(4));
		det.setPK(pk);
		bean.removeClientDetail(det);
		
	}
//    @Test
    public void testRemoveClient() throws Exception
    {
    	bean.removeClient(clientNo);
    }
 //   @Test
    public void testGetClient()throws Exception
    {
    	System.out.println("\n\nRetrieve and display client.........");
    	Integer clientNo = new Integer(10);
    	WebInsClient clt = bean.getWebInsClient(clientNo);
    	System.out.println(clt.toString());
    	System.out.println(bean.listClientDetails(clientNo));
    }
    
 //   @Test
    public void testCreateDetail()throws Exception
    {
    	Integer cltNo = new Integer(14);
    	WebInsClientDetail det;
    	det = new WebInsClientDetail(cltNo,
    			                     WebInsClient.CLIENT_DETAIL_ACC_THEFT,
    			                     new Integer(5),
    			                     new Integer(2004),
    			                     "At fault accident or claim");
    	det = new WebInsClientDetail(cltNo,
                WebInsClient.CLIENT_DETAIL_REFUSED,
                new Integer(6),
                new Integer(2007),
                "Reason");
    	bean.setClientDetail(det);
    	det = new WebInsClientDetail(cltNo,
                WebInsClient.CLIENT_DETAIL_ACC_THEFT,
                new Integer(3),
                new Integer(2007),
                "NAF accident or claim");
    	bean.setClientDetail(det);
    	det = new WebInsClientDetail(cltNo,
                WebInsClient.CLIENT_DETAIL_SUSPENSION,
                new Integer(5),
                new Integer(2004),
                "Alcohol related");
    	bean.setClientDetail(det);
    	det = new WebInsClientDetail(cltNo,
                WebInsClient.CLIENT_DETAIL_SUSPENSION,
                new Integer(7),
                new Integer(2003),
                "Alcohol related");
    	bean.setClientDetail(det);
   	    det = new WebInsClientDetail(cltNo,
                WebInsClient.CLIENT_DETAIL_SUSPENSION,
                new Integer(8),
                new Integer(2006),
                "Accumulated demerit points");
    	bean.setClientDetail(det);
    	det = new WebInsClientDetail(cltNo,
                WebInsClient.CLIENT_DETAIL_SUSPENSION,
                new Integer(5),
                new Integer(2008),
                "Driving whilst unlicensed");
    	bean.setClientDetail(det);

    }
	
	@After
	public void tearDown() throws Exception
	{
System.out.println("tearDown");		
	}
	
	
//	@Test
	public void testGetDriverStatus()throws Exception
	{
		Integer quoteNo = new Integer(3);
		ArrayList<WebInsClient> cltList = bean.getClients(quoteNo);
		for(int xx=0;xx<cltList.size();xx++)
		{
		   WebInsClient clt = cltList.get(xx);
		   System.out.println("Client " + clt.getWebClientNo() + "  Driver: " + clt.isDriver()
				               +" commenced driving " + clt.getYearCommencedDriving());
		}
	}
	

	
}
    class Card
    {
    	public String cardNo;
    	public DateTime birthDate;
    	public Card(String cNo, String bDate)throws Exception
    	{
    		this.cardNo = cNo;
    		this.birthDate = new DateTime(bDate);
    	}
    }
