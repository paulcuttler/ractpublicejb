package com.ract.web.test;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ract.common.ServiceLocator;
import com.ract.util.DateTime;
import com.ract.web.insurance.GlVehicle;
import com.ract.web.insurance.ListPair;
import com.ract.web.insurance.WebInsGlMgrRemote;

public class InsGLTester {
	WebInsGlMgrRemote bean ;
	@Before
	public void setUp() throws Exception
	{
    System.out.println("go 1");
		bean = (WebInsGlMgrRemote) ServiceLocator.getInstance().getObject("WebInsGlMgr/remote");
	}
//    @Test
    public void testGetMakes()throws Exception
    { 
    	System.out.println("getMakes.....");
    	Integer year = new Integer(2000);
    	GlVehicle veh = new GlVehicle();
    	veh.setVehYear(year);
    	ArrayList<ListPair> makes = bean.getMakes(veh);
    	for(int xx=0;xx<makes.size();xx++)
    	{
    		ListPair p = makes.get(xx);
    		System.out.println(p.getCol1() + "  " + p.getCol2());
    	}
    }
 //   @Test
    public void testGetModels()throws Exception
    {
       System.out.println("\n\n\nget models....\n");
       GlVehicle veh = new GlVehicle();
       veh.setVehYear(new Integer(2000));
       veh.setMake("FOR");
       ArrayList<String> models = bean.getModels(veh);
       for(int xx=0;xx<models.size();xx++)
       {
    	   String m = models.get(xx);
    	   System.out.println(m);
       }
    }
//    @Test 
	public void testGetBodyTypes()throws Exception
	{
		System.out.println("\n\ngetBodyTypes");
		GlVehicle veh = new GlVehicle();
		veh.setVehYear(2000);
		veh.setMake("FOR");
		veh.setModel("Festiva");
		ArrayList<ListPair> bt = bean.getBodyTypes(veh);
		for(int xx=0;xx<bt.size();xx++)
		{
			ListPair lp = bt.get(xx);
			System.out.println(lp.getCol2() + "  " + lp.getCol1());
		}		
	}
//    @Test
    public void testGetTransmission()throws Exception
    {
		System.out.println("\n\ngetTransmissionTypes");
		GlVehicle veh = new GlVehicle();
		veh.setVehYear(2000);
		veh.setMake("FOR");
		veh.setModel("Festiva");
		ArrayList<ListPair> bt = bean.getTransmissionTypes(veh);
		for(int xx=0;xx<bt.size();xx++)
		{
			ListPair lp = bt.get(xx);
			System.out.println(lp.getCol2() + "  " + lp.getCol1());
		}		
    }
    
//    @Test
    public void testGetCylinders()throws Exception
    {
		System.out.println("\n\ngetCylinders");
		GlVehicle veh = new GlVehicle();
		veh.setVehYear(2000);
		veh.setMake("FOR");
		veh.setModel("Fairmont");
		ArrayList<String> bt = bean.getCylinders(veh);
		for(int xx=0;xx<bt.size();xx++)
		{
			String lp = bt.get(xx);
			System.out.println(lp);
		}
    }

//    @Test
    public void testGetVehicles()throws Exception
    {
    	GlVehicle veh = new GlVehicle();
    	veh.setVehYear(2000);
    	veh.setMake("FOR");
    	veh.setModel("Fairmont");
    	ArrayList<GlVehicle> list = bean.getList(veh, new DateTime());
    	for(int xx=0;xx<list.size();xx++)
    	{
    		veh = list.get(xx);
    		System.out.println(veh.getNvic() + " " + veh.getSummary() );    		
    	}
    }
    
    @Test
    public void testGetVehicleCodes() throws Exception
    {
    	GlVehicle veh = bean.getVehicleCodes("o8e", new DateTime());
    	System.out.println(veh.listAttributes());
    	System.out.println("test");
    }
    
//    @Test
    public void testGetValue() throws Exception
    {
    	
    	
    	System.out.println("Value = " + bean.getValue("A6200D",new DateTime()));
    }
    
    @Test
    public void testGetNvicByRego() throws Exception {
    	String rego = "FC1193";
    	String state = "TAS";
    	String nvic = bean.getNvicByRegistration(rego, state);
    	
    	System.out.println("Rego: " + rego + ", State: " + state + ", NVIC: " + nvic);
    }
	
	@After
	public void tearDown() throws Exception
	{
System.out.println("tearDown");		
	}

	
}
