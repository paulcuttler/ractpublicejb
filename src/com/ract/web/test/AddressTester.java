package com.ract.web.test;

import static org.junit.Assert.*;

import java.util.*;

import com.ract.common.ServiceLocator;
import com.ract.common.StreetSuburbVO;
import com.ract.web.address.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AddressTester
{

	AddressMgrRemote addressMgr;
	
	@Before
	public void setUp() throws Exception
	{ 
		addressMgr = (AddressMgrRemote) ServiceLocator.getInstance().getObject("AddressMgr/remote");
	}

	@After
	public void tearDown() throws Exception
	{
		
	}

	//@Test
	public void testGetStreetSuburbsByPostcode()
	{
    Collection list = null; 
    try
		{
			list = addressMgr.getStreetSuburbsByPostcode("7001");
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(e.getMessage());
		}
    Hashtable h = new Hashtable();
    for (Iterator<StreetSuburbVO> i = list.iterator(); i.hasNext();)
    {
    	StreetSuburbVO ss = i.next();
    	System.out.println(ss.getStreetSuburbID()+" ["+ss.getStreet()+"]["+ss.getSuburb()+"]");
    }
		
	}

//	@Test
	public void testGetStreetSuburbsByStreet()
	{
    Collection list = null; 
    try
		{
			list = addressMgr.getStreetSuburbsByStreet("WIG","7000");
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(e.getMessage());
		}
  	System.out.println("-----");
    for (Iterator<StreetSuburbVO> i = list.iterator(); i.hasNext();)
    {
    	System.out.println(i.next().toString());
    }

	}

//	@Test
	public void testGetStreetSuburbsBySuburb()
	{
	    Collection<StreetSuburbVO> list = null; 
	    try
		{
			list = addressMgr.getStreetSuburbsBySuburb("Launceston","7250");
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(e.getMessage());
		}
	  	System.out.println("--x---");		
	    for (Iterator<StreetSuburbVO> i = list.iterator(); i.hasNext();)
	    {
	    	System.out.println("-->"+i.next().toString());
	    }

	}
	
	@Test
	public void testGetUniqueStreetSuburb() {
		StreetSuburbVO result = null;
		
		try {
			result = addressMgr.getUniqueStreetSuburb("WIGNALL ST", "NORTH HOBART", "7000");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		assertNotNull(result);
	}

}
