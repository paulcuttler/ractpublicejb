package com.ract.web.test;

import static junit.framework.Assert.*;
import com.ract.util.DateTime;

import com.ract.web.security.*;
import com.ract.web.security.SecurityHelper;
import com.ract.web.client.*;
import org.junit.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.ract.common.GenericException;
import com.ract.common.ServiceLocator;

public class SecurityTester
{

	UserSecurityMgrRemote userSecurityMgr;
	CustomerMgrRemote customerMgr;

	@Before
	public void setUp() throws Exception
	{
		System.out.println("setUp");		
		userSecurityMgr = (UserSecurityMgrRemote) ServiceLocator.getInstance().getObject("UserSecurityMgr/remote");
		customerMgr = (CustomerMgrRemote) ServiceLocator.getInstance().getObject("CustomerMgr/remote");			
	}

	@After
	public void tearDown() throws Exception
	{
System.out.println("tearDown");		
	}

	final int USER_ID = 1;
	final String USER_NAME = "jyh";	
	final String PASSWORD = "xxx";	

//	@Test
	public void testDeleteARs()
	{
		try
		{
			userSecurityMgr.deleteActivationRequests(USER_ID);
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}	
	
//	@Test	
	public void testCreateUser()
	{
		try
		{
//			User user1 = (MemberUser)UserFactory.getUser(User.TYPE_MEMBER);
			User user1 = new BasicUser();
//			user1.setUserType(User.TYPE_BASIC);
			user1.setUserName(USER_NAME);
			user1.setPassword(SecurityHelper.encryptPassword(PASSWORD));
			user1.setEmailAddress("j.holliday@ract.com.au");
//			user1.setMemberCardNumber("3084073104307418"); 
			user1.setFootyTipping(false);
			user1.setGivenNames("Joe");
			user1.setSurname("Bloggs");
			try 
			{
				user1.setBirthDate(new com.ract.util.DateTime("22/4/1986"));
			}
			catch (ParseException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			user1.setGender(User.GENDER_MALE);
			user1.setHomePhone("62323232");
			user1.setResiStreet("Ryde St");
			user1.setResiSuburb("North Hobart");
			user1.setResiState("Tasmania");
			user1.setResiCountry("Australia");			
			System.out.println("user1"+user1);			
			user1 = userSecurityMgr.createUserAccount(user1);
			System.out.println("user1"+user1);			
			user1.setFootyTipping(true);
			user1 = userSecurityMgr.updateUserAccount(user1);
			System.out.println("user1"+user1);			
			
		}
		catch (Exception e)
		{
      e.printStackTrace();
		}		
	}

//	@Test	
	public void testUpdateUser() throws Exception
	{
		User user = userSecurityMgr.getUser(USER_ID);
		user.setGivenNames("Joe");
		user.setSurname("Bloggs");
		try
		{
			user.setBirthDate(new com.ract.util.DateTime("22/4/1986"));
		}
		catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		user.setGender(User.GENDER_MALE);
		user.setHomePhone("62323232");
		user.setResiStreet("Ryde St");
		user.setResiSuburb("North Hobart");
		user.setResiState("Tasmania");
		user.setResiCountry("Australia");
		System.out.println("user="+user.toString());		
		try
		{
			userSecurityMgr.updateUserAccount(user);
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	@Test
	public void testGetUserSession()
	{
    System.out.println("testGetUserSession");		
    String sessionId = "3513EB24A7A9E532D53E40F16428F94";
    UserSession userSession = userSecurityMgr.getUserSession(sessionId);
    System.out.println("--->userSession="+userSession);
	}
	
//	@Test
	public void testPasswordsMatch() throws GenericException
	{
	  assertEquals(true, userSecurityMgr.passwordsMatch(PASSWORD, USER_ID)); 			
	}
	
	ActivationRequest ar;
	
//  @Test
	public void testActivateMemberAccount() throws GenericException
	{
  	String cardNumber = "3084073104307418";
  	User user = userSecurityMgr.getUserByCardNumber(cardNumber);
		System.out.println("user="+user);  	
		
  	if (user != null)
  	{
//  		userSecurityMgr.removeUserAccount(user.getUserId());
//    	userSecurityMgr.deleteActivationRequests(user.getUserId());  		
  	}

//  	MemberUser us = new MemberUser();
//  	us.setEmailAddress("j.holliday@ract.com.au");
//  	us.setUserName("test123");
//  	us.setFootyTipping(true);
//  	us.setMemberCardNumber(cardNumber);
//  	us.setPassword(SecurityHelper.encryptPassword(PASSWORD));
//  	user = userSecurityMgr.createUserAccount(us);
  	
	  try
		{
			ar = userSecurityMgr.requestMemberAccountActivation(user.getUserId(),cardNumber, "Holliday",
					"7018","","test",null, null,"http://testurl.ract.com.au?test.action=");
			if (ar.getMessage() != null)
			{
				throw new Exception(ar.getMessage());
			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
//	  userSecurityMgr.activateUser(ar.getRequestNumber());
	  
	}
	
//	@Test
	public void testMemberUser()
	{ 
		try
		{
			Collection list = userSecurityMgr.getUsersByEmailAddress("j.holliday@ract.com.au");
			assertNotNull(list);
System.out.println("list="+list);			
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
//	@Test
	public void testCardUpdate()
	{
		try
		{
			
			String currentCard = "3084073104307418";
			String newCard = "3084074104307416";
			

			User user = userSecurityMgr.getUserByCardNumber("3084074104307416", true);
			assertNotNull(user);
			
			System.out.println("user="+user);
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
  
//	@Test
	public void testPasswordReset()
	{
	  try
		{
			User user = userSecurityMgr.getUser(2242);
			user.setPassword(SecurityHelper.encryptPassword("walked4"));
			user.setPasswordReset(false);
			userSecurityMgr.updateUserAccount(user);
			System.out.println("user="+user);
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
		
//	@Test
	public void testGetUser()
	{
		try
		{
			String x = "gnewton008";
			User user = userSecurityMgr.getUserAccount(x);
			System.out.println("user="+user);
			System.out.println("x="+userSecurityMgr.userNameAvailable(x));			
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
//	@Test
	public void testDisableAccount()
	{
		try
		{
			userSecurityMgr.disableUserAccount(USER_ID);
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

//	@Test
	public void testEnableAccount()
	{
		try
		{
			userSecurityMgr.enableUserAccount(USER_ID);
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	

//	@Test
	public void testGetPersonalMembership() throws GenericException
	{
		User user = userSecurityMgr.getUser(USER_ID);
		if (user instanceof MemberUser)
		{
			try
			{
			  // TODO create suitable test
			}
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
//	@Test
	public void changeUserPassword() throws Exception
	{
		final String PW = "tommy"; 
		User user = userSecurityMgr.getUserAccount("ByronHoward");		
		try
		{
			userSecurityMgr.changeUserPassword(user, PW);
//			assertTrue(userSecurityMgr.passwordsMatch(PW, USER_ID));
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}	
	
//	@Test
	public void testUserSession()
	{
		try
		{
			DateTime now = new DateTime();
			SimpleDateFormat sd = new SimpleDateFormat("yyyyMMddHHmmSS");
			userSecurityMgr.createUserSession(sd.format(now), "203.20.20.201", USER_ID, "My PC");
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	@Test
	public void testChangePW() throws GenericException
	{
		User user = userSecurityMgr.getUserAccount("testuser");
		try
		{
			userSecurityMgr.changeUserPassword(user, "test");
			userSecurityMgr.passwordsMatch("testuser", USER_ID);
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	@Test
	public void testUserApproval() throws Exception
	{
		userSecurityMgr.approveUserName(USER_NAME);
		userSecurityMgr.unapproveUserName(USER_NAME);
	}
		
//  @Test
	public void testUpdateUserId() throws Exception
	{
		userSecurityMgr.updateUsersWithUserId();
	}	

//	@Test
	public void testCreateAuditLogin() throws Exception
	{
		LoginAudit la = new LoginAudit();
		la.setCreateDate(new DateTime());
		la.setUserName("dddd");
		la.setLoginFailure(false);
		la.setFailReason(null);
		la.setMemberCardNumber("37282378234879234");
		la.setUserId(null);
		la.setIpAddress("203.20.20.20");
		userSecurityMgr.createLoginAudit(la);
	}
	
//	@Test
	public void testEmailUserDetails() throws Exception
	{
		System.out.println("testUsernameConfirmation");
		try
		{
			userSecurityMgr.emailUserDetails(2);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}	
	}	
	
	//@Test
	public void testMergeDuplicateUserAccounts() throws Exception
	{
		System.out.println("testFixUsers");
		try
		{
			userSecurityMgr.mergeDuplicateUserAccounts(null);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			// TODO Auto-generated catch block
			fail();
		}	
	}
	
}
