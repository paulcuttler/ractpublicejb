package com.ract.web.test;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.junit.After;
import org.junit.Before;

import com.ract.common.ServiceLocator;
import com.ract.util.DateTime;
import com.ract.web.agent.WebAgentStatus;
import com.ract.web.insurance.Premium;
import com.ract.web.insurance.PremiumDiscount;
import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsMgr;
import com.ract.web.insurance.WebInsMgrRemote;
import com.ract.web.insurance.WebInsQuoteDetail;

public class InsTester {
	WebInsMgrRemote bean;
	@Before
	public void setUp() throws Exception
	{
	   System.out.println("Set up");
 	   bean = (WebInsMgrRemote) ServiceLocator.getInstance().getObject("WebInsMgr/remote");        
	}
	@After
	public void tearDown() throws Exception
	{
          System.out.println("tearDown");		
	}

//	@Test
	public 	void 	testAgent()
			throws	Exception
	{
		WebInsMgr wim = new WebInsMgr();
		
		WebAgentStatus was = new WebAgentStatus();
		
		was = wim.validateAgent("AAC", "password");
		System.out.println("Test AAC " + was.getAgentStatusMessage());
		
		was = wim.validateAgent("AAA", "password");
		System.out.println("Test AAA password " + was.getAgentStatusMessage());		
		
		was = wim.validateAgent("AAA", "apassword");
		System.out.println("Test AAA apassword " + was.getAgentStatusMessage());	
		
		was = wim.validateAgent("BBB", "2password");
		System.out.println("Test BBB apassword " + was.getAgentStatusMessage());	
		
		was = wim.validateAgent("CCC", "3password");
		System.out.println("Test CCC 3password " + was.getAgentStatusMessage());	
	}
//	@Test
	public void testGetGroup() throws Exception
	{
		Integer quoteNo = new Integer(7);
		ArrayList<WebInsClient> cltList;
		
		WebInsClient gClt;
		String gString;
		cltList = bean.getClientGroups(quoteNo);
		if(cltList != null)
		{
System.out.println("Client list has " + cltList.size() + " clients");		
		for(int xx = 0; xx<cltList.size();xx++)
		{
			gClt = cltList.get(xx);
			gString = "";
	        for(int yy = 0; yy< gClt.getGroupList().size();yy ++)
	        {
	           gString += " " + gClt.getGroupList().get(yy);	
	        }
			System.out.println("Group client no = " + gClt.getRactClientNo()
					         + " contains " + gString 
					         );
		}
		}
		Boolean fiftyPlus = bean.checkFiftyPlus(quoteNo);
System.out.println("Group has a 50+ member?" + fiftyPlus);		
	}
//	@Test
	public void testGetSetting()throws Exception
	{
		String setting = bean.getInsSetting("MaxSumInsured", "bldg", new DateTime());
		System.out.println("MaxSumInsured = " + setting);
		
		setting = bean.getInsSetting("MinSumInsured", "Cnts", new DateTime());
		System.out.println("MinSumInsured = " + setting);
		setting = bean.getInsSetting("UnacceptableSuburb", "Rossarden", new DateTime());
		System.out.println("Rossarden = " + setting);
		setting = bean.getInsSetting("UnacceptableSuburb", "Hobart", new DateTime());
		System.out.println("Hobart = " + setting);
        
		
	}
	
//	@Test
	public void getPremium() throws Exception
	{
		
		Integer quoteNo = new Integer(475);
		Premium p  = bean.getPremium(quoteNo, "annually", "inv");
		Collection<PremiumDiscount> dl = p.getPremiumDiscounts();
		System.out.println("Premium = " + p.getAnnualPremium()
				           );
		
		Iterator it = dl.iterator();
		while(it.hasNext())
		{
			PremiumDiscount pd = (PremiumDiscount)it.next();
			System.out.println(pd.getDiscountName() + " = " + pd.getDiscountAmount());
		}
		
	}
	
//	@Test
	public void testUpdateDetail()throws Exception
	{
		
		Integer quoteNo = new Integer(438);
		String fieldName = "Fred";
		WebInsQuoteDetail det = null;
		
		det = new WebInsQuoteDetail(quoteNo,
		                            fieldName,
		                            "Thin");
		bean.setQuoteDetail(det);
	}
	
//	@Test
	public void testGetDetail()throws Exception
	{
		Integer quoteNo = new Integer(438);
		String fieldName = "BLDG_AccidentalDamage";
		WebInsQuoteDetail det = bean.getDetail(quoteNo, fieldName);
		System.out.println(det.getFieldName() + " = " + det.getFieldValue()
				         + "\n" + det.getInKey() + "\n" + det.getStringKey());

	}
	
	

}
