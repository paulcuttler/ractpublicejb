package com.ract.web.test;

import static junit.framework.Assert.assertNotNull;

import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ract.common.GenericException;
import com.ract.common.ServiceLocator;
import com.ract.membership.MembershipVO;
import com.ract.membership.ProductVO;
import com.ract.util.DateTime;
import com.ract.web.client.WebClient;
import com.ract.web.membership.WebMembershipClient;
import com.ract.web.membership.WebMembershipMgrRemote;
import com.ract.web.membership.WebMembershipTransactionContainer;
import com.ract.web.payment.WebMembershipPayment;
import com.ract.web.payment.WebPayment;

public class WebMembershipTester
{

	WebMembershipMgrRemote bean;

	WebMembershipClient rsClient;

	@Before
	public void setUp() throws Exception
	{
		bean = (WebMembershipMgrRemote) ServiceLocator.getInstance().getObject(
				"WebMembershipMgr/remote");
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void testCreateWebMembershipClient()
	{
		WebMembershipClient customer = new WebMembershipClient();
		WebPayment wp = null;

		try
		{
			
			DateTime preferredCommenceDate = bean.getDefaultCommenceDate();			
			System.out.println("preferredCommenceDate="+preferredCommenceDate);
			
			//calc fees
			WebMembershipTransactionContainer wmtc = bean
			.createWebMembershipJoinTransaction(preferredCommenceDate, customer,
					ProductVO.PRODUCT_ADVANTAGE, "1:0:0:0:0:0:0", null, false);
	    assertNotNull(wmtc);
	    System.out.println("amountPayable="+wmtc.getPrimeAddressWebMembershipTransaction().getAmountPayable());
	    System.out.println("joinFee="+wmtc.getPrimeAddressWebMembershipTransaction().getJoinFee());
	    System.out.println("product="+wmtc.getPrimeAddressWebMembershipTransaction().getProductCode());
	
	    wmtc = bean.createWebMembershipJoinTransaction(preferredCommenceDate, customer,
			ProductVO.PRODUCT_ULTIMATE, "1:0:0:0:0:0:0", null, false);
      assertNotNull(wmtc);

	    System.out.println("amountPayable="+wmtc.getPrimeAddressWebMembershipTransaction().getAmountPayable());
	    System.out.println("joinFee="+wmtc.getPrimeAddressWebMembershipTransaction().getJoinFee());
	    System.out.println("product="+wmtc.getPrimeAddressWebMembershipTransaction().getProductCode());     

			// STEP 1
			wmtc = bean
					.createWebMembershipJoinTransaction(preferredCommenceDate, customer,
							ProductVO.PRODUCT_ADVANTAGE,"1:0:0:0:0:0:0", null, true);
			assertNotNull(wmtc);

			// STEP 2
			customer.setTitle("Mr");
			customer.setGivenNames("John Gordon");
  		customer.setSurname("Frank");			
  		customer.setGender(WebClient.GENDER_MALE);
  		customer.setEmailAddress("j.holliday@ract.com.au");
  		customer.setBirthDate(new DateTime("10/2/1987"));
  		customer.setHomePhone("0362476876");
  		customer.setResiStreetChar("23");
  		customer.setResiStreet("Wignall St");
  		customer.setResiSuburb("North Hobart");
  		customer.setResiPostcode("7000");
  		customer.setResiCountry("Australia");
  		customer.setResiState("TAS");
  		customer.setPostStreetChar(customer.getResiStreetChar());
  		customer.setPostStreet(customer.getResiStreet());
  		customer.setPostSuburb(customer.getResiSuburb());
  		customer.setPostState(customer.getResiState());
  		customer.setPostPostcode(customer.getResiPostcode()); 
  		customer.setPostCountry(customer.getResiCountry());  	
  		
			customer = bean.createMembershipClient(customer);
			assertNotNull(customer);			

			// STEP 3
			bean.associateWebClient(wmtc, customer);			

			// STEP 4 - use the returned object
//			wmtc = bean.updateWebMembershipJoinTransactionContainer(wmtc.getWebMembershipTransactionHeader().getTransactionHeaderId(),preferredCommenceDate, customer,
//							ProductVO.PRODUCT_ULTIMATE, null,true);
			
			// STEP 5
//			bean.associateWebClient(wmtc, customer);
			
			// STEP 6
//			wp = new WebMembershipPayment(wmtc
//					.getWebMembershipTransactionHeader().getTransactionHeaderId()
//					.toString(), "John Holliday", "VISA", "4111111111111111", "152",
//					"0310");
//			assertNotNull(wp);
//			wmtc.setWebPayment(wp);
			
			// variant
			customer.setSurname("Frankston");
			bean.updateMembershipClient(customer);
			
			// STEP 7 - creates payment as well
			bean.completeWebMembershipTransactions(wmtc);

			// STEP 8
			bean.emailJoinMembershipAdvice(wmtc.getWebMembershipTransactionHeader().getTransactionHeaderId(),"j.holliday@ract.com.au");
			
			// STEP 9 - clear any objects from the session
			
			System.out.println("rsClient=" + customer);
			System.out.println("wmtc=" + wmtc);
			System.out.println("wp=" + wp);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			// TODO Auto-generated catch block
			throw new AssertionError(e.getMessage());
		}
	}

	@Test
	public void testx() throws Exception
	{
		WebMembershipClient customer = new WebMembershipClient();		
		
		DateTime preferredCommenceDate = bean.getDefaultCommenceDate();			
		System.out.println("preferredCommenceDate="+preferredCommenceDate);
		
		//calc fees
		WebMembershipTransactionContainer wmtc = bean
		.createWebMembershipJoinTransaction(preferredCommenceDate, customer,
				ProductVO.PRODUCT_ADVANTAGE, "1:0:0:0:0:0:0", null, false);
    System.out.println("payable="+wmtc.getWebMembershipTransactionHeader().getAmountPayable());
    System.out.println("joinFee="+wmtc.getPrimeAddressWebMembershipTransaction().getJoinFee());    
		
		//calc fees
		wmtc = bean
		.createWebMembershipJoinTransaction(preferredCommenceDate, customer,
				ProductVO.PRODUCT_ULTIMATE, "1:0:0:0:0:0:0", null, false);
    System.out.println("payable="+wmtc.getWebMembershipTransactionHeader().getAmountPayable());
    System.out.println("joinFee="+wmtc.getPrimeAddressWebMembershipTransaction().getJoinFee());    	
	
	}
	
	@Test
	public void testCreateRoadserviceRecords()
	{
		try
		{
			bean.resetTemporaryRoadserviceRecords();
			bean.createTemporaryRoadserviceRecords();
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// @Test
	public void testProducts()
	{
		try
		{
			Collection<String> products = bean.getMembershipProducts();
			assertNotNull(products);
			System.out.println("products=" + products);
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// @Test
	public void testPaymentTypes()
	{
		try
		{
			Collection<String> paymentTypes = bean.getMembershipPaymentTypes();
			assertNotNull(paymentTypes);
			System.out.println("paymentTypes=" + paymentTypes);
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// @Test
	public void testGetPersonalMembership()
	{
		MembershipVO membership = null;
		try
		{
			membership = bean.getPersonalMembership(new Integer(430741));
			assertNotNull(membership);
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(membership.toString());
	}

}
