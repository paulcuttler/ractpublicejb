package com.ract.web.test;

import java.util.*;

import org.junit.Test;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;

import com.ract.common.CommonEJBHelper;
import com.ract.common.schedule.ScheduleMgr;
import com.ract.web.common.PaymentFileGeneratorJob;

public class PayableTest {

	@Test
	public void testWebPayables() throws Exception {

		JobDetail jobDetail = new JobDetail("PaymentFileGeneratorJob",
        Scheduler.DEFAULT_GROUP,
        PaymentFileGeneratorJob.class);

		jobDetail.setDescription("Payment File Generator");

		SimpleTrigger trigger = new SimpleTrigger("PaymentFileGeneratorTrigger", Scheduler.DEFAULT_GROUP, new Date());
		
		try {
			ScheduleMgr scheduleMgr = CommonEJBHelper.getScheduleMgr();
			scheduleMgr.scheduleJob(jobDetail, trigger);
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
