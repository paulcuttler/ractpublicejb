package com.ract.web.test;
import org.junit.*;
import java.util.*;

import com.ract.common.CommonEJBHelper;
import com.ract.common.SequenceMgr;
import com.ract.common.ServiceLocator;
import com.ract.insurance.Risk;

import com.ract.web.common.GnSetting;
import com.ract.web.insurance.*;
import com.ract.util.*;
public class InsQuoteTester {
	
	WebInsMgrRemote bean;
//	Integer quoteNo = new Integer(494);
    Integer quoteNo = new Integer(5);
	@Before
	public void setUp() throws Exception
	{
	System.out.println("Set up");
	
	//Environment is set from a jndi.properties in the classpath
	  bean = (WebInsMgrRemote) ServiceLocator.getInstance().getObject("WebInsMgr/remote");
        
	}
//    @Test
    public void testStartQuote() throws Exception
    {
    	DateTime quoteDate = new DateTime("15/11/2008");
    	String quoteType = WebInsQuote.TYPE_MOTOR;
    	Integer webQuoteNo = bean.startQuote(quoteDate, quoteType);
    	System.out.println("Quote created with number " + webQuoteNo);
    }
	
//    @Test
	public void testGetQuoteNo()throws Exception
	{
        SequenceMgr sMgr = CommonEJBHelper.getSequenceMgr();
        Integer quoteNo = sMgr.getNextID(WebInsQuote.SEQUENCE_QUOTE);
        System.out.println("quoteNo = " + quoteNo);

	}
	
//	@Test
	public void testGetClientPolicies() throws Exception
	{
		final Integer clientNumber = new Integer(248998); //412143
    Collection policies = bean.getClientPolicies(clientNumber, true, true, true);
    com.ract.insurance.Policy p = null;
    Risk r = null;
    
    Collections.sort((List)policies);    
    Collections.reverse((List)policies);
    
    Collection riskList = null;
    for (Iterator<com.ract.insurance.Policy> ip = policies.iterator(); ip.hasNext();)
    {
    	p = ip.next();
      System.out.println("policy: "+p.toString());
      riskList = p.getRiskList();
      Collections.sort((List)riskList);
      for (Iterator<Risk> ir = riskList.iterator(); ir.hasNext();)
      {
      	r = ir.next();
        System.out.println("risk: "+r.toString());      	
      }
    }
     
    
    System.out.println("policies="+policies.toString());
    //585583
	}	
	
//	@Test
	public void testSetAttribute() throws Exception
	{
		Integer quoteNo = new Integer(3);
		bean.setQuoteDetail(quoteNo, "XXX","Have a nice day");
		bean.setQuoteDetail(quoteNo, "type","vegetable");
	}
//	@Test
	public void testGetAttribute()throws Exception
	{
		Integer quoteNo = new Integer(3);
		String det = bean.getQuoteDetail(quoteNo, "50+");
		System.out.println("50+ = " + det);
	}
//	@Test
	public void testGetAllAttributes()throws Exception
	{
		ArrayList<WebInsQuoteDetail> list = bean.getAllQuoteDetails(quoteNo);
		if(list!=null)
		{
			for(int xx = 0; xx<list.size();xx++)
			{
				WebInsQuoteDetail det = list.get(xx);
				System.out.println(det.getWebQuoteNo()
						          + "  " + det.getFieldName()
						          + "  " + det.getFieldValue()); 
			}
		}
	}
	
//	@Test
	public void testGetSecureQuoteNo()throws Exception
	{
		Integer qNo = new Integer(234);
		WebInsQuote qt = bean.getQuote(qNo);
		System.out.println("QuoteNo = " + qNo + "    " + qt.getSecuredQuoteNo() + "  " + qt.getFormattedQuoteNo());
	}

//	@Test
	public void testValidateQuoteNo() throws Exception
	{
		String quoteNo = "40000003";
		Integer webQuoteNo = bean.validateSecuredQuoteNo(quoteNo);
		System.out.println("found quote no " + webQuoteNo);	
	}

//	@Test
	public void testMakeQuote()throws Exception
	{
		Integer quoteNo = bean.startQuote(new DateTime("12/11/2008"),
				                                       "bldg");
		//WebInsQuote quote = bean.getQuote(quoteNo);
		bean.setQuoteDetail(quoteNo, "driverDob","21/09/1971");
		bean.setQuoteDetail(quoteNo, "excess", "500");
		bean.setQuoteDetail(quoteNo, "ncd", "40");
		bean.setQuoteDetail(quoteNo, "nvic", "A6200D");
		bean.setQuoteDetail(quoteNo, "agreedValue", "9800");
		bean.setQuoteDetail(quoteNo, "garagedAt", "20693");
		bean.setQuoteDetail(quoteNo, "finType", "None");
		bean.setQuoteDetail(quoteNo, "windscreen","yes");
		bean.setQuoteDetail(quoteNo, "hireCar", "no");
		bean.setQuoteDetail(quoteNo, "primaryUse", "private");
		bean.setQuoteDetail(quoteNo, "engineImobiliser", "no");
		bean.setQuoteStatus(quoteNo, "quote");
		bean.setQuoteDetail(quoteNo, "50+", "no");
		System.out.println("New Quote created = " + quoteNo);
	}
	
//	@Test
	public void testAddDetails()throws Exception
	{
		Integer quoteNo = new Integer(5);
		bean.setQuoteDetail(quoteNo, "driverDob","21/09/1971");
		bean.setQuoteDetail(quoteNo, "excess", "500");
		bean.setQuoteDetail(quoteNo, "ncd", "40");
		bean.setQuoteDetail(quoteNo, "nvic", "A6200D");
		bean.setQuoteDetail(quoteNo, "agreedValue", "9800");
		bean.setQuoteDetail(quoteNo, "garagedAt", "20693");
		bean.setQuoteDetail(quoteNo, "finType", "None");
		bean.setQuoteDetail(quoteNo,"windscreen","yes");
		bean.setQuoteDetail(quoteNo, "hireCar", "no");
		bean.setQuoteDetail(quoteNo, "primaryUse", "private");
		bean.setQuoteDetail(quoteNo, "engineImobiliser", "no");
		bean.setQuoteStatus(quoteNo, "quote");
		bean.setQuoteDetail(quoteNo, "50+", "no");
		bean.setQuoteDetail(quoteNo, WebInsQuoteDetail.FIN_TYPE, "No");
		bean.setQuoteDetail(quoteNo, WebInsQuoteDetail.PAY_METHOD,WebInsQuoteDetail.PAY_CASH);
        bean.setQuoteStatus(quoteNo, WebInsQuote.COVER_ISSUED);		

	}

//	@Test
	public void testCalculatePremium()throws Exception
	{
	   String coverType = "cnts";
	   String paymentFrequency = "annually";
	   Integer quoteNo = new Integer(438);
	   Premium p = null;
	   WebInsQuote qt = bean.getQuote(quoteNo);
	   ArrayList<WebInsQuoteDetail>list = bean.getAllQuoteDetails(quoteNo);
	   try
	   {
	      p = bean.getPremium(quoteNo,
			                  paymentFrequency,
			                  coverType);
	   }
	   catch(Exception ex)
	   {
		   ex.printStackTrace();
	   }
	   if(p==null)
	   {
		   System.out.println("Premium not retrieved.....");
	   }
	   else
	   {
		   
	      System.out.println("Risk Type = " + coverType
	    		         + "\nPayment Frequency = " + paymentFrequency
	    		         + "\nAnnual premium = $" + p.getAnnualPremium()
			             + "\nPayment =        $" + p.getPayment());
	   }
	   for(int xx=0;xx<list.size();xx++)
	   {
		   WebInsQuoteDetail det = list.get(xx);
		   System.out.println(det.getFieldName() + " =   " + det.getFieldValue());
	   }
	   System.out.println("QuoteDate =  " + qt.getQuoteDate());
	}
	
	@Test
	public void testPrintDocument()throws Exception
	{
	   Integer quoteNo = new Integer(63024);
	   bean.printCoverDoc(quoteNo);
//       bean.printCoverDoc(quoteNo);
	}
	
//	@Test
	public void testQuoteDet()throws Exception
	{
		Integer quoteNo = new Integer(958);
		String det50Plus = bean.getQuoteDetail(quoteNo,"accountType");
        System.out.println("Quote " + quoteNo + " 50+ set to " + det50Plus);
	}
	
	
	
//	@Test
	public void testRetrieveClients()throws Exception
	{
		 String[] sList = {"new","quote","complete","cover","converted"};
		ArrayList<WebInsQuote> list = bean.findQuotesByStatus(sList);
		WebInsQuote quote = null;
		for(int xx = 0;xx<list.size();xx++)
		{
			quote = list.get(xx);
			System.out.println(quote.toString());
		}
	}
//	@Test
	public void testSplit()throws Exception
	{
		String list = "fred|jim|harry|mary|susan|george";
		String[] array = list.split("\\|");
		for(int xx=0;xx<array.length;xx++)
		{
			System.out.println(array[xx]);
		}
	}
	
//	@Test
	public void testGetInsTypeList()throws Exception
	{
		ArrayList<RefType> list = null;
		list = bean.getRefList(WebInsQuoteDetail.REF_TYPE_FINANCIERS,new DateTime());
		for(int xx=0;xx<list.size();xx++)
		{
			RefType rt = list.get(xx);
			System.out.println("\n" + rt.getTypeCode() 
				               + "  " + rt.getTText3()
					           + "  " + rt.getTDescription()
					           );
		}
	}
//	@Test
	public void testRetrieveQuote()throws Exception
	{
		System.out.println("Test look up quote");
		WebInsQuote qt = null;
		for(int xx = 229;xx<240 ; xx++)
		{
		   qt = bean.getQuote(xx);
		   System.out.println(qt.toString());
		   System.out.println("createDate = " + qt.getCreateDate());
		   System.out.println("quoteDate = " + qt.getQuoteDate());
		}
	}
	
//	@Test
	public void testGetLookupValues()throws Exception
	{
		ArrayList<RefType> list = bean.getRefList(WebInsQuoteDetail.REF_TYPE_FINANCIERS, new DateTime());
		for(int xx= 0 ; xx<list.size();xx++)
		{
			RefType rt = list.get(xx);
			System.out.println(rt.getTypeCode() + "   " + rt.getTDescription());
		}
	}
	
	
//	@Test
	public void testGetSystemSetting()throws Exception
	{
		GnSetting setting = bean.getGnSetting("IWS", "CCADD");
		System.out.println("CC Address = " + setting.getDescription());
	}
	
	@After
	public void tearDown() throws Exception
	{
System.out.println("tearDown");		
	}
	
//	 @Test
	 public void testIs50Plus() throws Exception
	 {
		Integer webQuoteNo = new Integer(5);
		boolean result = bean.checkFiftyPlus(webQuoteNo);
		System.out.println("meets 50+ requirements = " + result);
	 }
//		@Test
		public void testAddClientDetail()throws Exception
		{
	System.out.println("testAddClientDetail.........");	
	     try{
			Integer quoteNo = new Integer(3);
		    WebInsClient client = bean.createInsuranceClient(quoteNo); 
System.out.println("New ClientNo " + client.getWebClientNo());		     
		    int numberOfAccident = 5; 
		    for (int j = 1; j <= numberOfAccident; j++) { 
		      String accidentDateString = "15/04/2009" ; //this.getParameter("accidentDate"+i+j); 
		      String accidentType = "Single Vehicle"; //this.getParameter("accidentType"+i+j); 

		      if (accidentType != null && !accidentType.equals("") && accidentDateString != null && !accidentDateString.equals("")) { 
		     /*   WebInsClientDetail detail = new WebInsClientDetail(); */
		        String[] accidentDate = accidentDateString.split("/"); 

		      /*  detail.setDetailType(accidentType); 
		        detail.setDetMonth(Integer.valueOf(accidentDate[0])); 
		        detail.setDetYear(Integer.valueOf(accidentDate[1])); 
		        detail.setDetail("accident"); 

		        detail.setWebClientNo(client.getWebClientNo()); 
		        */
		    	WebInsClientDetail detail = new WebInsClientDetail(client.getWebClientNo(),
		    			                                           accidentType,
		    			                                           new Integer(accidentDate[0]),
		    			                                           new Integer(accidentDate[1]),
		    			                                           "accident");  
		        bean.setClientDetail(detail); 
		      } 
		    } 
	System.out.println("Created new client number " + client.getWebClientNo()
			       + "\n             for quote no " + client.getWebQuoteNo());
		    bean.setClient(client); 
	     }
	     catch(Exception ex)
	     {
	    	 ex.printStackTrace();
	     }

		}
//   @Test
   public void testStartCover() throws Exception
   {
	  Integer quoteNo = new Integer(593);
	  bean.startCover(quoteNo, null, null, null, null, null, null, null);
	   
   }
   
 //  @Test 
   public void testCheckDigits() throws Exception
   {
	   Integer tQuoteNo = new Integer("6");
	   String cd = WebInsQuote.getCheckDigits(tQuoteNo);
	   WebInsQuote qt = new WebInsQuote();
	   qt.setWebQuoteNo(tQuoteNo);
	   String fqn = qt.getFormattedQuoteNo();
	   boolean isOK = WebInsQuote.validateCheckDigits(fqn);
	   Integer wQuoteNo = WebInsQuote.validateQuoteNo(fqn);
	   System.out.println("QuoteNo = " + tQuoteNo
			          + "\ncheckDigits = " + cd
			          + "\nformatted quote No = " + fqn
			          + "\nis valid = " + isOK
			          + "\nRetrieved quote no = " + wQuoteNo); 
	   
   }
   
//   @Test
   public void testGetSecuredQuoteNumbers()throws Exception
   {
	   String[] statusList = {"new","quote","complete","covered","converted","quoteConverted"};
	   ArrayList<WebInsQuote> qList = bean.findQuotesByStatus(statusList);
	   for(int xx=0;xx<qList.size();xx++)
	   {
		   WebInsQuote qt = qList.get(xx);
		   System.out.println(qt.getFormattedQuoteNo() + " " + qt.getQuoteStatus());
	   }
	   
   }
   
 //  @Test
   public void testAllocateBranch() throws Exception
   {
	   for(int xx = 1;xx<=20; xx++)
	   {
		   System.out.println(xx + " " + bean.allocateSalesBranch());
	   }
   }
//   @Test
   public void testFindQuotesBySalesBranch() throws Exception
   {
	   ArrayList<WebInsQuote> list = null;
	   String branch = "bur";
	   list = bean.findQuotesBySalesBranch(branch);
	   if(list.size()==0)System.out.println("None found for " + branch);
	   else
	   {
		   for(int xx=0;xx<list.size();xx++)
		   {
			   WebInsQuote qt = list.get(xx);
			   System.out.println(qt.getFormattedQuoteNo() + " " + qt.getQuoteStatus() 
					          + " " + qt.getQuoteType() + " " + qt.getCoverType()
					          + " " + qt.getCreateDate());
		   }
	   }
   }
   
}
