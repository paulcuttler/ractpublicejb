package com.ract.web.test;


import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ract.client.ClientTitleVO;
import com.ract.client.ClientVO;
import com.ract.common.GenericException;
import com.ract.common.PostalAddressVO;
import com.ract.common.ResidentialAddressVO;
import com.ract.common.ServiceLocator;
import com.ract.common.StreetSuburbVO;
import com.ract.util.DateTime;
import com.ract.web.client.ClientHelper;
import com.ract.web.client.ClientTransaction;
import com.ract.web.client.ClientTransactionPK;
import com.ract.web.client.CustomerMgrRemote;

public class CustomerTester
{

	private static final Integer CLIENT_NUMBER = new Integer(432002);
	private static final String CARD_NUMBER = "3084070104320029";
	CustomerMgrRemote customerMgr;	
	
	@Before
	public void setUp() throws Exception
	{
		customerMgr = (CustomerMgrRemote) ServiceLocator.getInstance().getObject("CustomerMgr/remote");
	}

//	@Test
	public void testGetClient()
	{
		ClientVO client = null;
		try
		{
			client = customerMgr.getClient(CLIENT_NUMBER);
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(CLIENT_NUMBER, client.getClientNumber());
	}
	
	//@Test
	public void testGetClientByCard() {
		ClientVO client = null;
		try
		{
			client = customerMgr.getClientByMembershipCardNumber(CARD_NUMBER);
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(CLIENT_NUMBER, client.getClientNumber());
	}
	
//	@Test
	public void test()
	{
		try
		{
			ClientVO oldClient = customerMgr.getClient(430741);
			ClientVO newClient = customerMgr.getClient(430741);	
			newClient.setSurname("Smith");
			newClient.setTitle("MRS");			
			PostalAddressVO pa = newClient.getPostalAddress();
			pa.setStreetSuburbID(null);
			StreetSuburbVO ss = pa.getStreetSuburb();
			ss.setStreet("TEST ST");
			newClient.setPostalAddress(pa);
//			newClient.setPostStsubId(null);
			newClient.setHomePhone("036236652251");
			newClient.setEmailAddress("j.test@test.com");
			
			ResidentialAddressVO sa = newClient.getResidentialAddress();
			sa.setStreetSuburbID(null);
			StreetSuburbVO ss2 = pa.getStreetSuburb();		
			ss2.setStreet("NOWHERE ST");
			newClient.setResidentialAddress(sa);
			newClient.setSex(false);
			
			
 System.out.println(ClientHelper.reportDifferences(oldClient, newClient));
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//@Test
	public void testGetClientTitles()
	{
	  try
	  {
	  	Collection<ClientTitleVO> titles = customerMgr.getClientTitles(true,false,null);
	  	assertNotNull(titles);

	  	for (ClientTitleVO title : titles)
	  	{	  		
	  		System.out.println("title="+title.getTitleName()+" "+title.getSexSpecific()+" "+title.getSex());
	  	}
	  }
	  catch (GenericException e)
	  {
			// TODO Auto-generated catch block
			e.printStackTrace();	  	
	  }
	}
	
	@Test
	public void testCreateClientTransaction()
	{
		try
		{
			ClientVO client = customerMgr.getClient(new Integer(430741));
			ClientTransaction ct = com.ract.web.client.ClientHelper.convertClient(client);
			System.out.println("ct="+ct.getDisplayName());
			ClientTransactionPK ctPK = ct.getClientTransactionPK();
			ctPK.setTransactionType(ClientTransaction.TRANSACTION_TYPE_UPDATE);
			ctPK.setTransactionDate(new DateTime());
//			ct.setResiStsubid(null);
			ct.setResiStreetChar("99");
			customerMgr.createClientTransaction(ct);
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testProcessClientUpdates()
	{
		try
		{
			customerMgr.processClientUpdates();
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
//	@Test
	public void testGetLastTransaction()
	{
		try
		{
			
			ClientTransaction ct = customerMgr.getLastClientTransaction(new Integer(430741), ClientTransaction.TRANSACTION_TYPE_UPDATE);
      System.out.println(ct.getClientTransactionPK().getTransactionDate().formatLongDate());
      ClientVO client = com.ract.web.client.ClientHelper.convertClientTransaction(ct);
      System.out.println(ct.getSurname());      
      System.out.println("client="+client.getSurname());      
		}
		catch (GenericException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	

	@After
	public void tearDown() throws Exception
	{
	}

}
