package com.ract.web.client;

import java.rmi.RemoteException;
import java.util.Collection;

import javax.ejb.Remote;

import com.ract.client.ClientTitleVO;
import com.ract.client.ClientVO;
import com.ract.common.GenericException;
import com.ract.util.DateTime;

@Remote
public interface CustomerMgrRemote
{

	public ClientVO getClient(Integer clientNumber) throws GenericException;	
	
	public ClientVO getClientByMembershipCardNumber(String membershipCardNumber) throws GenericException;
	
	public WebClient getWebClient(Integer webClientNo) throws GenericException;
	
	public boolean hasEmailNewsletterSubscription(Integer clientNumber) throws GenericException;	
		
	public void processClientUpdate(DateTime now,
			ClientTransaction lastTransaction, String bcc, String fromAddress, String toAddress) throws GenericException,
			RemoteException;
	
	public Collection<ClientTitleVO> getClientTitles(boolean individual, boolean group) throws GenericException;
	public Collection<ClientTitleVO> getClientTitles(boolean individual, boolean group, DateTime deleteDate) throws GenericException;
	
	public ClientTransaction getLastClientTransaction(Integer ractClientNo, String transactionType) throws GenericException;
	
	public void createClientTransaction(ClientTransaction clientTransaction) throws GenericException;
	
//	public void markClientTransactionsProcessed(Integer ractClientNo) throws GenericException;
	
	public WebClient updateWebClient(WebClient webClient) throws GenericException;
	
  public WebClient createWebClient(WebClient webClient) throws GenericException;
  
	public void processClientUpdates() throws GenericException;
	
	public void updateWebClientRACTClientNo(Integer fromClient, Integer toClient) throws GenericException;	
	
}
