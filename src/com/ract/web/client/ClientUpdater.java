package com.ract.web.client;

import com.ract.common.ServiceLocator;

public class ClientUpdater
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		try
		{
			CustomerMgrRemote customerMgr = (CustomerMgrRemote)ServiceLocator.getInstance().getObject("CustomerMgr/remote");
			customerMgr.processClientUpdates();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
