package com.ract.web.client.test;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import com.ract.client.ClientTitleVO;
import com.ract.client.ClientVO;
import com.ract.common.GenericException;
import com.ract.util.DateTime;
import com.ract.web.client.ClientTransaction;
import com.ract.web.client.CustomerMgrRemote;
import com.ract.web.client.WebClient;
import com.ract.web.membership.WebMembershipClient;

public class CustomerMgrStub implements CustomerMgrRemote
{

	@Override
	public boolean hasEmailNewsletterSubscription(Integer clientNumber)
			throws GenericException
	{
		return true;
	}

	@Override
	public WebClient createWebClient(WebClient webClient) throws GenericException
	{
		return webClient;
	}

	@Override
	public WebClient updateWebClient(WebClient webClient) throws GenericException
	{
		return webClient;
	}

	@Override
	public void updateWebClientRACTClientNo(Integer fromClient, Integer toClient) throws GenericException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processClientUpdate(DateTime now,
			ClientTransaction lastTransaction, String bcc, String fromAddress,
			String toAddress) throws GenericException, RemoteException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public WebClient getWebClient(Integer webClientNo) throws GenericException
	{
		return new WebMembershipClient();
	}

	@Override
	public void processClientUpdates() throws GenericException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createClientTransaction(ClientTransaction clientTransaction)
			throws GenericException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public ClientTransaction getLastClientTransaction(Integer ractClientNo,
			String transactionType) throws GenericException
	{
		return new ClientTransaction();
	}

	@Override
	public Collection<ClientTitleVO> getClientTitles(boolean individual, boolean group) throws GenericException
	{
		// TODO Auto-generated method stub
		Collection<ClientTitleVO> titles = new ArrayList<ClientTitleVO>();
		ClientTitleVO title = null;
		title = new ClientTitleVO();
		title.setTitleName("Mr");
		title.setSex(true);
		title.setSexSpecific(true);
		titles.add(title);
		title = new ClientTitleVO();
		title.setTitleName("Mrs");
		title.setSex(false);
		title.setSexSpecific(true);
		titles.add(title);
		title = new ClientTitleVO();
		title.setTitleName("Ms");
		title.setSex(false);
		title.setSexSpecific(true);	
		titles.add(title);
		title = new ClientTitleVO();
		title.setTitleName("Miss");
		title.setSex(false);
		title.setSexSpecific(true);	
		titles.add(title);
		return titles;
	}

	@Override
	public Collection<ClientTitleVO> getClientTitles(boolean individual, boolean group,
			DateTime deleteDate) throws GenericException {
		return getClientTitles(individual, group);
	}

	@Override
	public ClientVO getClient(Integer clientNumber) throws GenericException
	{
		ClientVO client = new ClientVO();
		client.setSurname("Test");
		client.setGivenNames("John");
		return client;
	}

	@Override
	public ClientVO getClientByMembershipCardNumber(String membershipCardNumber)
			throws GenericException {
		// TODO Auto-generated method stub
		return null;
	}

}
