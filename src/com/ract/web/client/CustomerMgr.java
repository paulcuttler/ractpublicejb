package com.ract.web.client;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.ract.client.Client;
import com.ract.client.ClientMgrBean;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientSubscription;
import com.ract.client.ClientTitleVO;
import com.ract.client.ClientVO;
import com.ract.common.CommonMgrLocal;
import com.ract.common.ExceptionHelper;
import com.ract.common.GenericException;
import com.ract.common.MailMgrLocal;
import com.ract.common.Publication;
import com.ract.common.SequenceMgrLocal;
import com.ract.common.SourceSystem;
import com.ract.common.SystemParameterVO;
import com.ract.common.mail.MailMessage;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;

@Stateless
@Remote({CustomerMgrRemote.class})
@Local({CustomerMgrLocal.class})
public class CustomerMgr implements CustomerMgrRemote,CustomerMgrLocal
{
  
	private static final String SUBJECT_RACT_WEB_CLIENT_UPDATE = "RACT Web Client Update";

	private static final String SOURCE_SYSTEM_WEB = "WEB";

	private static final String SALES_BRANCH_WEB = "WB";

	private static final String USER_WEB = "WEB";

	@PersistenceContext
	EntityManager em;	
	
	@EJB
  private ClientMgrLocal clientMgr;
	
	@EJB
	private SequenceMgrLocal sequenceMgrLocal;

	@EJB
	private MailMgrLocal mailMgrLocal;

	@EJB
	private CommonMgrLocal commonMgr;

	@EJB
	private CustomerMgrLocal customerMgr;	
	
  //TODO has travel?
	//TODO update customer subscriptions (what if the user is not a customer???)
  
  /**
   * Get a list of valid RACT client titles.
   * @return A list of client titles.
   */
	public Collection<ClientTitleVO> getClientTitles(boolean individual, boolean group) throws GenericException
	{
		return getClientTitles(individual, group, new DateTime());
	}
	public Collection<ClientTitleVO> getClientTitles(boolean individual, boolean group, DateTime deleteDate) throws GenericException
	{
		Collection<ClientTitleVO> titleList = new ArrayList<ClientTitleVO>();
		Collection<ClientTitleVO> titles = null;
	  try
		{     
      titles = clientMgr.getClientTitles(true); //not cached
      for (ClientTitleVO title : titles)
      {
      	LogUtil.debug(this.getClass(), "title=" + title.getTitleName() + " " + title.getGroupId());
      	if (title.getDeleteDate() != null && title.getDeleteDate().before(deleteDate)) {
      		continue;
      	}
      	if (individual)
      	{
        	LogUtil.debug(this.getClass(), "ind");      		
      		if (!title.getGroupId())
      		{
      			titleList.add(title);
          	LogUtil.debug(this.getClass(), "ind="+title.getTitleName());      			
      		}
      	}
      	if (group)
      	{
        	LogUtil.debug(this.getClass(), "group");      		
      		if (title.getGroupId())
      		{
          	LogUtil.debug(this.getClass(), "group="+title.getTitleName());      			
      			titleList.add(title);
      		}      		
      	}
      }
		}
		catch (Exception e)
		{
      throw new GenericException(e);
		}
		return titleList;
	}
	
	public ClientTitleVO getClientTitle(String titleName)
	{
		return clientMgr.getClientTitle(titleName);
	}
	
	public ClientTransaction getLastClientTransaction(Integer ractClientNo, String transactionType) throws GenericException
	{
		System.out.println("ractClientNo="+ractClientNo);		
		System.out.println("transactionType="+transactionType);		
		ClientTransaction lastTransaction = null;
		List<ClientTransaction> clientTransactions = em.createQuery(
    "from ClientTransaction as ct where ct.clientTransactionPK.ractClientNo = ?1 and ct.clientTransactionPK.transactionType = ?2 and ct.processDate is null order by ct.clientTransactionPK.transactionDate desc")
    .setParameter(1, ractClientNo)
    .setParameter(2, transactionType)    
    .getResultList();
    if (clientTransactions != null)
    {
    	System.out.println("not null"+clientTransactions.size());
    	if (clientTransactions.iterator().hasNext())
    	{
      	System.out.println("last tx");
    		lastTransaction = clientTransactions.iterator().next();
      	System.out.println("last tx"+lastTransaction);    		
    	}
    }
		return lastTransaction;
	}
	
	public void processClientUpdates() throws GenericException
	{
		DateTime now = new DateTime();
		ClientTransaction lastTransaction = null;
		//ingnore processed and manually processed = false
		List<ClientTransaction> clients = em.createQuery(
    "from ClientTransaction as ct where ct.clientTransactionPK.transactionType = ?1 and ct.processDate is null and ct.manuallyProcessed is null order by ct.clientTransactionPK.transactionDate desc")
    .setParameter(1, ClientTransaction.TRANSACTION_TYPE_UPDATE)
    .getResultList();
		Collection<Integer> updates =  new ArrayList();
		ClientVO prevClientState = null;
		ClientVO newClientState = null;		
		
	 	String bcc = ((SystemParameterVO)commonMgr.getSystemParameter(SourceSystem.MEMBERSHIP.getAbbreviation(), "BCCCLIENTUPDATES")).getValue();
	 	String fromAddress = ((SystemParameterVO)commonMgr.getSystemParameter(SourceSystem.MEMBERSHIP.getAbbreviation(), "FROMCLIENTUPDATES")).getValue();
	 	String toAddress = ((SystemParameterVO)commonMgr.getSystemParameter(SourceSystem.MEMBERSHIP.getAbbreviation(), "TOCLIENTUPDATES")).getValue();
		
		for (Iterator<ClientTransaction> i = clients.iterator(); i.hasNext();)
		{
			//get the last client transaction
			lastTransaction = i.next();
			
			String messageText = "";
			
			//skip any that have been previously updated
			if (!updates.contains(lastTransaction.getClientTransactionPK().getRactClientNo()))
			{
				try
				{				
					updates.add(lastTransaction.getClientTransactionPK().getRactClientNo());
					//new transaction
					customerMgr.processClientUpdate(now,lastTransaction,bcc,fromAddress,toAddress);					
				}
				catch (Exception e)
				{
					//email error message
					try
					{
						messageText = "Error updating client: "+e.getMessage();
						MailMessage message = new MailMessage();
						message.setRecipient(toAddress);
						message.setBcc(bcc);
						message.setSubject(SUBJECT_RACT_WEB_CLIENT_UPDATE+" "+lastTransaction.getClientTransactionPK().getRactClientNo());
						message.setMessage(messageText);
						message.setFrom(fromAddress);
						
						mailMgrLocal.sendMail(message);
					}
					catch (RemoteException e1)
					{
						LogUtil.fatal(this.getClass(), "Unable to send email about client update: "+ExceptionHelper.getExceptionStackTrace(e));
					}					
					LogUtil.fatal(this.getClass(), "Unable to update client: "+ExceptionHelper.getExceptionStackTrace(e));
				}
				
			}
			else
			{
				//already processed updates.
			}
		}

		
	}
	
  @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
	public void processClientUpdate(DateTime now,
			ClientTransaction lastTransaction, String bcc, String fromAddress, String toAddress) throws GenericException,
			RemoteException
	{
		ClientVO prevClientState;
		ClientVO newClientState;
		String messageText;
		prevClientState = getClient(lastTransaction.getClientTransactionPK().getRactClientNo());
		
		//TODO consider checking the last history for recent changes.
		
		//copy clientvo
		try
		{
			newClientState = (ClientVO)prevClientState.clone();
			ClientVO newClient = ClientHelper.convertClientTransaction(lastTransaction);
			
			//set changed details
			newClientState.setHomePhone(newClient.getHomePhone());
			newClientState.setWorkPhone(newClient.getWorkPhone());
			newClientState.setMobilePhone(newClient.getMobilePhone());
			newClientState.setEmailAddress(newClient.getEmailAddress());			
		
	  	newClientState.setPostProperty(newClient.getPostProperty());
	  	newClientState.setPostStreetChar(newClient.getPostStreetChar());
		  if (newClient.getPostStsubId() != null)
		  {
		  	newClientState.setPostStsubId(newClient.getPostStsubId());
		  }

			// update (effectively clear) DPID/Ausbar/lat/long/GNAF if post address changed
			if (!prevClientState.getPostalAddress().equals(newClientState.getPostalAddress())) {				
				newClientState.setPostLatitude(newClient.getPostLatitude());
			  newClientState.setPostLongitude(newClient.getPostLongitude());
			  newClientState.setPostGnaf(newClient.getPostGnaf());
			  newClientState.setPostDpid(newClient.getPostDpid());
			  newClientState.setPostAusbar(newClient.getPostAusbar());
			}
			
	  	newClientState.setResiProperty(newClient.getResiProperty());
	  	newClientState.setResiStreetChar(newClient.getResiStreetChar());
		  if (newClient.getResiStsubId() != null)
		  {
		  	newClientState.setResiStsubId(newClient.getResiStsubId());
		  }
		  
		  // update (effectively clear) DPID/Ausbar/lat/long/GNAF if resi address changed
			if (!prevClientState.getResidentialAddress().equals(newClientState.getResidentialAddress())) {
				newClientState.setResiLatitude(newClient.getResiLatitude());
			  newClientState.setResiLongitude(newClient.getResiLongitude());
			  newClientState.setResiGnaf(newClient.getResiGnaf());
			  newClientState.setResiDpid(newClient.getResiDpid());
			  newClientState.setResiAusbar(newClient.getResiAusbar());
			}
		  
		  //set the following to allow the email to be generated with the address details correctly.
			newClientState.setPostalAddress(newClient.getPostalAddress());
      newClientState.setResidentialAddress(newClient.getResidentialAddress());		
      
      // update last update date
      newClientState.setLastUpdate(new DateTime());
		}
		catch (CloneNotSupportedException e)
		{
			throw new GenericException(e.getMessage());
		}
		
		//compare old client details and new client details
		messageText = ClientHelper.reportDifferences(prevClientState, newClientState);

		boolean knownAddress = true;
		
		//if no street suburb then put to one side as an exception
		if (lastTransaction.getPostStsubid() == null ||
				lastTransaction.getResiStsubid() == null)
		{
      knownAddress = false;
		}
		
		if (knownAddress)
		{
			LogUtil.log(this.getClass(), "newClientState="+newClientState);
			//update client, also updates Progress client, Prosper, etc.
			clientMgr.updateClient(newClientState, USER_WEB, Client.HISTORY_UPDATE, SALES_BRANCH_WEB, "", SOURCE_SYSTEM_WEB);
			
			//create a followup if home phone, postal address or residential address has changed.
			if (!prevClientState.getHomePhone().equals(newClientState.getHomePhone()) ||
					!prevClientState.getPostalAddress().equals(newClientState.getPostalAddress()) ||
					!prevClientState.getResidentialAddress().equals(newClientState.getResidentialAddress()))
			{
				int tStamp = new DateTime().getSecondsFromMidnight();
				clientMgr.savePopupRequest(ClientMgrBean.POPUP_ADDRESS_UPDATE, USER_WEB, SALES_BRANCH_WEB, tStamp, prevClientState);
			}
			
		}
		
		String subject = SUBJECT_RACT_WEB_CLIENT_UPDATE+" "+prevClientState.getClientNumber();
		if (!knownAddress)
		{
			subject += " (Manual update required - Unknown address change)";
						
			MailMessage message = new MailMessage();
			message.setRecipient(toAddress);
			message.setBcc(bcc);
			message.setSubject(subject);
			message.setMessage(messageText);
			message.setFrom(fromAddress);
			
			//email confirmation
			mailMgrLocal.sendMail(message);
		}
		
		markClientTransactionsProcessed(lastTransaction.getClientTransactionPK().getRactClientNo(),!knownAddress);

	}
	
	public WebClient getWebClient(Integer webClientNo) throws GenericException
	{
		WebClient webClient = em.find(WebClient.class, webClientNo);
		return webClient;
	}

	public void updateWebClientRACTClientNo(Integer fromClient, Integer toClient) throws GenericException
	{
		WebClient webClient = null;
		Query query = em.createQuery("from WebClient e where e.ractClientNo = ?1");
		query.setParameter(1, fromClient);
		List list = query.getResultList();
		for (Iterator<WebClient> i = list.iterator();i.hasNext();)
		{
			webClient = i.next();
			webClient.setRactClientNo(toClient);
			updateWebClient(webClient);
		}
	}	
	
	public WebClient updateWebClient(WebClient webClient) throws GenericException
	{
		// MX changes
		if (webClient.getGender().equals(WebClient.GENDER_UNSPECIFIED)){
			webClient.setGender(ClientVO.GENDER_FEMALE);
		}
		em.merge(webClient);
		return webClient;
	}	
	
	private void markClientTransactionsProcessed(Integer ractClientNo, boolean manuallyProcessed) throws GenericException
	{
		DateTime currentDate = new DateTime();
		ClientTransaction clientTransaction = null;
		List<ClientTransaction> clientTransactions = em.createQuery(
    "from ClientTransaction as ct where ct.clientTransactionPK.ractClientNo = ?1 and ct.clientTransactionPK.transactionType = ?2 and ct.processDate is null and ct.manuallyProcessed is null")
    .setParameter(1, ractClientNo)
    .setParameter(2, ClientTransaction.TRANSACTION_TYPE_UPDATE)    
    .getResultList();	
    if (clientTransactions != null)
    {
    	System.out.println("not null"+clientTransactions.size());
      for (Iterator<ClientTransaction> i = clientTransactions.iterator(); i.hasNext();)
      {
        clientTransaction = i.next();
        //set process date
        if (manuallyProcessed)
        {
        	clientTransaction.setManuallyProcessed(currentDate);
        }
        else
        {
        	clientTransaction.setProcessDate(currentDate);
        }
        //update the transaction
        em.merge(clientTransaction);
      }
    }
	}

	/**
	 * Create a new web membership client in the database.
	 * @param webClient The web client to save to the database.
	 * @return The saved web membership client including the unique client identifier.
	 */	
  public WebClient createWebClient(WebClient webClient) throws GenericException
  {
		Integer webClientNo = null;
		try
		{
			webClientNo = sequenceMgrLocal.getNextID(WebClient.SEQUENCE_WEB_CLIENT);
		}
		catch (RemoteException e)
		{
			throw new GenericException(e);
			
		}
		webClient.setWebClientNo(webClientNo);
		
		// MX changes
		if (webClient.getGender().equals(WebClient.GENDER_UNSPECIFIED)){
			webClient.setGender(ClientVO.GENDER_FEMALE);
		}

		webClient.setCreateDate(new DateTime());
		em.persist(webClient);
			
		return webClient;
 	
  }
  
  
	
	public void createClientTransaction(ClientTransaction clientTransaction) throws GenericException
	{
	  em.persist(clientTransaction);
	}
	
	public boolean hasEmailNewsletterSubscription(Integer clientNumber) throws GenericException
	{
		boolean emailNewsletterSubscription = false;
	  try
		{
	  	ClientSubscription cs = null;
			Collection clientSubscriptions = clientMgr.getClientSubscriptionList(clientNumber);
			for (Iterator<ClientSubscription> i = clientSubscriptions.iterator(); i.hasNext();)
			{
				cs = i.next();
				if (Publication.RACT_NEWSLETTER.equals(cs.getClientSubscriptionPK().getSubscriptionCode()))
				{
					emailNewsletterSubscription = true;
				}
			}
		}
		catch (RemoteException e)
		{
			throw new GenericException(e);
		}
	  return emailNewsletterSubscription;
	}
	
	/**
	 * Get an RACT client given a unique client number.
	 * @param clientNumber The unique RACT client identifier
	 * @return The RACT client.
	 */
	public ClientVO getClient(Integer clientNumber) throws GenericException
	{
		ClientVO client = null;
		if (clientNumber != null)
		{
		  try
			{      
				client = clientMgr.getClient(clientNumber);
			}
			catch (Exception e)
			{
        throw new GenericException(e);
			}
		}
		return client;
	}	
	
	/**
	 * Get an RACT client given a unique membership card number.
	 * @param membershipCardNumber The unique RACT membership card number
	 * @return The RACT client.
	 */
	public ClientVO getClientByMembershipCardNumber(String membershipCardNumber) throws GenericException {
		ClientVO client = null;
		if (membershipCardNumber != null && !membershipCardNumber.isEmpty()) {
			try {
				client = clientMgr.getClientByMembershipCardNumber(membershipCardNumber);
			} catch (Exception e) {
        throw new GenericException(e);
			}			
		}
		return client;
	}
	
}
