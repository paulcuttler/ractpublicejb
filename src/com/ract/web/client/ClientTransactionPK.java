package com.ract.web.client;

import java.io.Serializable;

import javax.persistence.Embeddable;

import com.ract.util.DateTime;

@Embeddable
public class ClientTransactionPK implements Serializable
{

  /**
	 * 
	 */
	private static final long serialVersionUID = 8863482929301880849L;
	/**
   * The RACT client number
   */
	protected Integer ractClientNo;
	/**
	 * The type of the transaction
	 */
	protected String transactionType;
	/** 
	 * The date and time the transaction was performed
	 */
	protected DateTime transactionDate;
	/** 
	 * The date and time the change has been transferred to the client systems 
	 */
	public Integer getRactClientNo()
	{
		return ractClientNo;
	}
	public void setRactClientNo(Integer ractClientNo)
	{
		this.ractClientNo = ractClientNo;
	}
	public String getTransactionType()
	{
		return transactionType;
	}
	public void setTransactionType(String transactionType)
	{
		this.transactionType = transactionType;
	}
	public DateTime getTransactionDate()
	{
		return transactionDate;
	}
	public void setTransactionDate(DateTime transactionDate)
	{
		this.transactionDate = transactionDate;
	}	

	public boolean equals(Object o)
	{
		ClientTransactionPK pk = (ClientTransactionPK)o;
		return this.ractClientNo.equals(pk.ractClientNo)
		       && this.transactionType.equals(pk.transactionType)
		       && this.transactionDate.equals(pk.transactionDate);
	}
	
	public int hashCode()
	{
		return this.ractClientNo.hashCode() + this.transactionType.hashCode() + this.transactionDate.hashCode();
	}	
	
}
