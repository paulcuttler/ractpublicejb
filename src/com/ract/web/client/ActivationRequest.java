package com.ract.web.client;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.ract.util.DateTime;

/**
 * Represent an account activation - ie. assocation of a user account to a membership.
 * @author hollidayj
 *
 */
 
@Entity
public class ActivationRequest implements Serializable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8437321064806847073L;

	public static String SEQUENCE_ACTIVATION_REQUEST = "WEB_ACTIVATION_REQUEST";	
	
  @Id
  private int requestNumber;
	
	private String clientNumber;
	
	private String membershipCardNumber;
	
	private Integer userId;
	
	private DateTime activationDate;
	
	private String userName;
	
	private boolean created;
	
	private String mobilePhoneNumber;
	
	private String updateEmailAddress;
	
	public String getMobilePhoneNumber()
	{
		return mobilePhoneNumber;
	}

	public void setMobilePhoneNumber(String mobilePhoneNumber)
	{
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	public boolean isCreated()
	{
		return created;
	}

	public void setCreated(boolean created)
	{
		this.created = created;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	private String message;
	
	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public DateTime getActivationDate()
	{
		return activationDate;
	}

	public void setActivationDate(DateTime activationDate)
	{
		this.activationDate = activationDate;
	}

	public Integer getUserId()
	{
		return userId;
	}

	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	@Override
	public String toString()
	{
		return "ActivationRequest [activationDate=" + activationDate + ", clientNumber=" + clientNumber + ", createDate=" + createDate + ", created=" + created + ", dateOfBirth=" + dateOfBirth
				+ ", membershipCardNumber=" + membershipCardNumber + ", message=" + message + ", mobilePhoneNumber=" + mobilePhoneNumber + ", phoneNumber=" + phoneNumber + ", postcode=" + postcode
				+ ", requestNumber=" + requestNumber + ", surname=" + surname + ", userId=" + userId + ", userName=" + userName + "]";
	}
	
	private DateTime createDate;
	
	private String surname;
	
	private String postcode;
	
	private String phoneNumber;
	
	private DateTime dateOfBirth;
	
	public String getSurname()
	{
		return surname;
	}

	public void setSurname(String surname)
	{
		this.surname = surname;
	}

	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(String postcode)
	{
		this.postcode = postcode;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	public DateTime getDateOfBirth()
	{
		return dateOfBirth;
	}

	public void setDateOfBirth(DateTime dateOfBirth)
	{
		this.dateOfBirth = dateOfBirth;
	}

	public int getRequestNumber()
	{
		return requestNumber;
	}

	public void setRequestNumber(int requestNumber)
	{
		this.requestNumber = requestNumber;
	}

	public String getClientNumber()
	{
		return clientNumber;
	}

	public void setClientNumber(String clientNumber)
	{
		this.clientNumber = clientNumber;
	}

	public String getMembershipCardNumber()
	{
		return membershipCardNumber;
	}

	public void setMembershipCardNumber(String membershipCardNumber)
	{
		this.membershipCardNumber = membershipCardNumber;
	}

	public DateTime getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(DateTime createDate)
	{
		this.createDate = createDate;
	}

	public String getUpdateEmailAddress() {
		return updateEmailAddress;
	}

	public void setUpdateEmailAddress(String updateEmailAddress) {
		this.updateEmailAddress = updateEmailAddress;
	}
	
}
