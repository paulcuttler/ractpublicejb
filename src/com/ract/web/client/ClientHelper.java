package com.ract.web.client;

import java.rmi.RemoteException;

import com.ract.client.ClientVO;
import com.ract.common.AddressVO;
import com.ract.common.GenericException;
import com.ract.common.PostalAddressVO;
import com.ract.common.ResidentialAddressVO;
import com.ract.common.StreetSuburbVO;
import com.ract.util.FileUtil;
import com.ract.util.StringUtil;

public class ClientHelper
{

	private static final int ADDRESS_FIELD_LENGTH = 40;

	public static String reportDifferences(ClientVO oldClient, ClientVO newClient)
	{
		
		StringBuffer desc = new StringBuffer();
		desc.append("CLIENT CHANGES: "+newClient.getClientNumber()+FileUtil.NEW_LINE+FileUtil.NEW_LINE);
	
		StringBuffer diff = new StringBuffer();
    diff.append(outputDiff("Client Title",newClient.getTitle(),oldClient.getTitle()));
    diff.append(outputDiff("Given Names",newClient.getGivenNames(),oldClient.getGivenNames()));
    diff.append(outputDiff("Surname",newClient.getSurname(),oldClient.getSurname()));
    diff.append(outputDiff("Gender",newClient.getGender(),oldClient.getGender()));
    diff.append(outputDiff("Birth Date",newClient.getBirthDate(),oldClient.getBirthDate()));
    diff.append(outputDiff("Email Address",newClient.getEmailAddress(),oldClient.getEmailAddress()));
    diff.append(outputDiff("Home Phone",newClient.getHomePhone(),oldClient.getHomePhone()));
    diff.append(outputDiff("Work Phone",newClient.getWorkPhone(),oldClient.getWorkPhone()));
    diff.append(outputDiff("Mobile Phone",newClient.getMobilePhone(),oldClient.getMobilePhone()));
    try
		{
    	diff.append(outputDiff("Residential Address",newClient.getResidentialAddress(),oldClient.getResidentialAddress()));
    	diff.append(outputDiff("Postal Address",newClient.getPostalAddress(),oldClient.getPostalAddress()));
		}
		catch (RemoteException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    
		if (diff.length() == 0)
		{
			diff.append("No differences found.");
		}
		desc.append(diff);
		return desc.toString();
	}
	
	private static String outputDiff(String label, Object newValue, Object oldValue)
	{
		String desc = "";
		if ((!(oldValue == null && newValue == null)) &&
				((oldValue == null &&
				newValue != null) ||
				!oldValue.equals(newValue)))
		{
			if (newValue instanceof AddressVO ||
					oldValue instanceof AddressVO)
			{
				AddressVO newAddress = (AddressVO)newValue;
				AddressVO oldAddress = (AddressVO)oldValue;
				desc += FileUtil.NEW_LINE;
				desc += label+FileUtil.NEW_LINE;				
				desc += "New:"+FileUtil.NEW_LINE;				
				desc += outputAddress(newAddress);
				desc += "Old:"+FileUtil.NEW_LINE;
				desc += outputAddress(oldAddress);	
			}
			else
			{
		    desc += StringUtil.rightPadString(label+":",ADDRESS_FIELD_LENGTH)+(newValue==null?"":newValue)+" (was "+(oldValue==null?"":oldValue)+")"+FileUtil.NEW_LINE;
			}
		}
		return desc;
	}
	
	private static String outputAddress(AddressVO address)
	{
		String add = "";
		add += StringUtil.rightPadString("Property:",ADDRESS_FIELD_LENGTH)+address.getProperty()+FileUtil.NEW_LINE;
		add += StringUtil.rightPadString("Property Qualifier/Street Number:",ADDRESS_FIELD_LENGTH)+address.getPropertyQualifier()+FileUtil.NEW_LINE;
		add += StringUtil.rightPadString("Street:",ADDRESS_FIELD_LENGTH)+address.getStreet()+FileUtil.NEW_LINE;
		add += StringUtil.rightPadString("Suburb:",ADDRESS_FIELD_LENGTH)+address.getSuburb()+FileUtil.NEW_LINE;
		add += StringUtil.rightPadString("State:",ADDRESS_FIELD_LENGTH)+address.getState()+FileUtil.NEW_LINE;
		add += StringUtil.rightPadString("Postcode:",ADDRESS_FIELD_LENGTH)+address.getPostcode()+FileUtil.NEW_LINE;				
		return add;
	}
	
	//populate a ClientTransaction with a ClientVO
  public static ClientTransaction convertClient(ClientVO clientVO) throws GenericException
  {
  	com.ract.web.client.ClientTransaction ct = new com.ract.web.client.ClientTransaction();
  	ClientTransactionPK ctPK = new ClientTransactionPK();
  	ctPK.setRactClientNo(clientVO.getClientNumber());
  	
  	ct.setClientTransactionPK(ctPK);
  	ct.setGivenNames(clientVO.getGivenNames());
  	ct.setSurname(clientVO.getSurname());
  	ct.setTitle(clientVO.getTitle());
  	ct.setGender(clientVO.getGender());
  	
  	ct.setHomePhone(clientVO.getHomePhone());
  	ct.setWorkPhone(clientVO.getWorkPhone());
  	ct.setMobilePhone(clientVO.getMobilePhone());
  	
  	ct.setStatus(clientVO.getStatus());
  	
  	try
		{
  		//sets the other address related fields
			ct.setPostalAddress(clientVO.getPostalAddress());
			ct.setResidentialAddress(clientVO.getResidentialAddress());
			
		}
		catch (RemoteException e)
		{
			throw new GenericException(e);
		}
  	
		ct.setBirthDate(clientVO.getBirthDate());
  	ct.setEmailAddress(clientVO.getEmailAddress());

  	return ct;
  	
  }
	
  public static ClientVO convertClientTransaction(ClientTransaction clientTransaction) throws GenericException
  {
  	ClientVO client = new ClientVO();
  	
  	client.setClientNumber(clientTransaction.getClientTransactionPK().getRactClientNo());

  	client.setTitle(clientTransaction.getTitle());  	
  	client.setGivenNames(clientTransaction.getGivenNames());
  	client.setSurname(clientTransaction.getSurname());
  	client.setSex(clientTransaction.getGender().equals(WebClient.GENDER_MALE));
  	
  	try
		{
			client.setHomePhone(clientTransaction.getHomePhone());
			client.setWorkPhone(clientTransaction.getWorkPhone());
			client.setMobilePhone(clientTransaction.getMobilePhone());
			client.setBirthDate(clientTransaction.getBirthDate());
		}
		catch (Exception e)
		{
			throw new GenericException(e);
		}  	
  	
		client.setEmailAddress(clientTransaction.getEmailAddress());  	
  	
  	if (clientTransaction.getResiStsubid() == null)
  	{
  		ResidentialAddressVO res = new ResidentialAddressVO();
  		res.setProperty(clientTransaction.getResiProperty());
  		res.setPropertyQualifier(clientTransaction.getResiStreetChar());  
  		StreetSuburbVO streetSuburb = new StreetSuburbVO();
  		streetSuburb.setStreet(clientTransaction.getResiStreet());
  		streetSuburb.setSuburb(clientTransaction.getResiSuburb());
  		streetSuburb.setPostcode(clientTransaction.getResiPostcode());
  		streetSuburb.setState(clientTransaction.getResiState());
  		streetSuburb.setCountry(clientTransaction.getResiCountry());
  		res.setStreetSuburb(streetSuburb);
  		client.setResidentialAddress(res);
  	}
  	else
  	{
    	//set up for lazy fetch
  		client.setResiProperty(clientTransaction.getResiProperty());
    	client.setResiStreetChar(clientTransaction.getResiStreetChar());  		
  		client.setResiStsubId(clientTransaction.getResiStsubid());
  	}

  	if (clientTransaction.getPostStsubid() == null)
    {	
 	    PostalAddressVO pos = new PostalAddressVO();
 	    pos.setProperty(clientTransaction.getPostProperty());
 	    pos.setPropertyQualifier(clientTransaction.getPostStreetChar());  	 	    
  		StreetSuburbVO streetSuburb = new StreetSuburbVO();
  		streetSuburb.setStreet(clientTransaction.getPostStreet());
  		streetSuburb.setSuburb(clientTransaction.getPostSuburb());
  		streetSuburb.setPostcode(clientTransaction.getPostPostcode());
  		streetSuburb.setState(clientTransaction.getPostState());
  		streetSuburb.setCountry(clientTransaction.getPostCountry());
  	  pos.setStreetSuburb(streetSuburb);
  	  client.setPostalAddress(pos);
  	}
  	else
  	{
   	  //set up for lazy fetch  		
  	  client.setPostProperty(clientTransaction.getPostProperty());
  	  client.setPostStreetChar(clientTransaction.getPostStreetChar());  	
  		client.setPostStsubId(clientTransaction.getPostStsubid());
  	}
  	return client;
  }  

  public static String getAddressLine1(WebClient webClient)
  {
  	PostalAddressVO add = getPostalAddress(webClient);
  	System.out.println("add1"+add);
  	return add.getAddressLine1();
  }  
  
  public static String getAddressLine2(WebClient webClient)
  {
  	PostalAddressVO add = getPostalAddress(webClient);
  	System.out.println("add2"+add);
  	return add.getAddressLine2();
  }
  
  public static String getAddressLine3(WebClient webClient)
  {
  	PostalAddressVO add = getPostalAddress(webClient);
  	System.out.println("add3"+add);  	
  	return add.getAddressLine3();
  }
  
  public static String getAddressLine4(WebClient webClient)
  {
  	PostalAddressVO add = getPostalAddress(webClient);
  	System.out.println("add4"+add);  	
  	return add.getAddressLine4();
  }

	private static PostalAddressVO getPostalAddress(WebClient webClient)
	{
		StreetSuburbVO ss = new StreetSuburbVO();
  	ss.setCountry(null);
  	ss.setStreet(webClient.getPostStreet());
  	ss.setSuburb(webClient.getPostSuburb());
  	ss.setState(webClient.getPostState());
  	ss.setPostcode(webClient.getPostPostcode());
  	PostalAddressVO add = new PostalAddressVO(webClient.getPostProperty(),webClient.getPostStreetChar(),
  			ss);
		return add;
	}
 
	public static String getPostalName(WebClient webClient)
	{
		ClientVO client = getClient(webClient);
		return client.getPostalName();
	}

	public static String getAddressTitle(WebClient webClient)
	{
		ClientVO client = getClient(webClient);
		return client.getAddressTitle();
	}	
	
	private static ClientVO getClient(WebClient webClient)
	{
		ClientVO client = new ClientVO();
		client.setTitle(webClient.getTitle());
		client.setGivenNames(webClient.getGivenNames());
		client.setSurname(webClient.getSurname());
		client.setDerivedFields();
		return client;
	}
	
}
