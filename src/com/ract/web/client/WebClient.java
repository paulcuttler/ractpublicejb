/**
 * 
 */
package com.ract.web.client;

import java.io.*;
import com.ract.util.*;
import com.ract.common.*;
import javax.persistence.*;

import org.hibernate.annotations.DiscriminatorFormula;

/**
 * Basic WebClient class holding general attributes and method
 * It is expected that this class will be extended for specific uses
 * @author dgk1
 */

@Entity
@DiscriminatorFormula("clientType")
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class WebClient implements Serializable
{
	
	public DateTime getCreateDate()
	{
		return createDate;
	}
	public void setCreateDate(DateTime createDate)
	{
		this.createDate = createDate;
	}
	public String getPreferredContactMethod()
	{
		return preferredContactMethod;
	}
	public void setPreferredContactMethod(String preferredContactMethod)
	{
		this.preferredContactMethod = preferredContactMethod;
	}
	public String getResiState()
	{
		return resiState;
	}
	public void setResiState(String resiState)
	{
		this.resiState = resiState;
	}
	public String getResiCountry()
	{
		return resiCountry;
	}
	public void setResiCountry(String resiCountry)
	{
		this.resiCountry = resiCountry;
	}
	public String getResiPostcode()
	{
		return resiPostcode;
	}
	public void setResiPostcode(String resiPostcode)
	{
		this.resiPostcode = resiPostcode;
	}
	public String getPostProperty()
	{
		return postProperty;
	}
	public void setPostProperty(String postProperty)
	{
		this.postProperty = postProperty;
	}
	public String getPostPostcode()
	{
		return postPostcode;
	}
	public void setPostPostcode(String postPostcode)
	{
		this.postPostcode = postPostcode;
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = -7905731573485516852L;
	public static String CLIENT_TYPE_INSURANCE = "Insurance";
	public static String CLIENT_TYPE_MEMBERSHIP = "Membership";	
	public static String SEQUENCE_WEB_CLIENT = "WEB_CLIENT_SEQ";
	
	public static String GENDER_MALE = "Male";
	public static String GENDER_FEMALE = "Female";	
	public static String GENDER_UNSPECIFIED = "unspecified";	
	
	@Id	
	protected Integer webClientNo;
	protected DateTime createDate;
	protected String  givenNames;
	protected String clientType;
	protected String  surname;
	protected String  title;
	protected String  gender;
	protected DateTime birthDate;
	protected String homePhone;
	protected String workPhone;
 	protected String mobilePhone;
 	protected String faxNo;
	protected String emailAddress;
	protected String preferredContactMethod;
	protected String resiProperty;
	protected String resiStreetChar;
	protected String resiStreet;
	protected String resiSuburb;
	protected String resiState;
	protected String resiCountry;
	protected String resiPostcode;
	protected String resiLatitude;
	protected String resiLongitude;
	protected String resiGnaf;
	protected String resiDpid;
	protected String resiAusbar;
	protected Integer resiStsubid;
	protected String postProperty;	
	protected String postAddress;
	protected String postStreetChar;
	protected String postStreet;
	protected String postSuburb;
	protected String postState;
	protected String postCountry;
	protected String postPostcode;
	protected String postLatitude;
	protected String postLongitude;
	protected String postGnaf;
	protected String postDpid;
	protected String postAusbar;
	protected Integer postStsubid;
	protected Integer ractClientNo;

	@Transient
	protected ResidentialAddressVO residentialAddress;
	@Transient
	protected PostalAddressVO postalAddress;
	
	public Integer getWebClientNo() {
		return webClientNo;
	}
	public void setWebClientNo(Integer webClientNo) {
		this.webClientNo = webClientNo;
	}
	public String getGivenNames() {
		return givenNames;
	}
	public void setGivenNames(String givenNames) {
		this.givenNames = givenNames;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public DateTime getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(DateTime dateOfBirth) {
		this.birthDate = dateOfBirth;
	}
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	public String getWorkPhone() {
		return workPhone;
	}
	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getFaxNo() {
		return faxNo;
	}
	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public ResidentialAddressVO getResidentialAddress() {
		return residentialAddress;
	}
	public void setResidentialAddress(ResidentialAddressVO residentialAddress) {
		this.residentialAddress = residentialAddress;
	}
	public PostalAddressVO getPostalAddress() {
		return postalAddress;
	}
	public void setPostalAddress(PostalAddressVO postalAddress) {
		this.postalAddress = postalAddress;
	}
	public String getClientType() {
		return clientType;
	}
	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
	
	public String getDisplayName()
	{
		StringBuffer displayName = new StringBuffer();
		if(title != null && !title.isEmpty()) {   
            displayName.append(title);
            displayName.append(" ");
        }
		displayName.append(givenNames);
		displayName.append(" ");
		displayName.append(surname);
		return displayName.toString().toUpperCase();
	}
	
	public String getResiStreetChar() {
		return resiStreetChar;
	}
	public void setResiStreetChar(String resiStreetChar) {
		this.resiStreetChar = resiStreetChar;
	}
	public String getResiStreet() {
		return resiStreet;
	}
	public void setResiStreet(String resiStreet) {
		this.resiStreet = resiStreet;
	}
	public String getResiSuburb() {
		return resiSuburb;
	}
	public void setResiSuburb(String resiSuburb) {
		this.resiSuburb = resiSuburb;
	}
	public Integer getResiStsubid() {
		return resiStsubid;
	}
	public void setResiStsubid(Integer resiStsubid) {
		this.resiStsubid = resiStsubid;
	}
	public String getPostStreetChar() {
		return postStreetChar;
	}
	public void setPostStreetChar(String postStreetChar) {
		this.postStreetChar = postStreetChar;
	}
	public String getPostStreet() {
		return postStreet;
	}
	public void setPostStreet(String postStreet) {
		this.postStreet = postStreet;
	}
	public String getPostSuburb() {
		return postSuburb;
	}
	public void setPostSuburb(String postSuburb) {
		this.postSuburb = postSuburb;
	}
	public String getPostState() {
		return postState;
	}
	public void setPostState(String postState) {
		this.postState = postState;
	}
	public String getPostCountry() {
		return postCountry;
	}
	public void setPostCountry(String postCountry) {
		this.postCountry = postCountry;
	}

	public Integer getPostStsubid() {
		return postStsubid;
	}
	public void setPostStsubid(Integer postStsubid) {
		this.postStsubid = postStsubid;
	}
	public Integer getRactClientNo() {
		return ractClientNo;
	}
	public void setRactClientNo(Integer ractClientNo) {
		this.ractClientNo = ractClientNo;
	}
	public String getPostAddress() {
		return postAddress;
	}
	public void setPostAddress(String postAddress) {
		this.postAddress = postAddress;
	}
	
	@Override
	public String toString() {
		return "WebClient [webClientNo=" + webClientNo + ", createDate="
				+ createDate + ", givenNames=" + givenNames + ", clientType="
				+ clientType + ", surname=" + surname + ", title=" + title
				+ ", gender=" + gender + ", birthDate=" + birthDate + ", homePhone="
				+ homePhone + ", workPhone=" + workPhone + ", mobilePhone="
				+ mobilePhone + ", faxNo=" + faxNo + ", emailAddress=" + emailAddress
				+ ", preferredContactMethod=" + preferredContactMethod
				+ ", resiProperty=" + resiProperty + ", resiStreetChar="
				+ resiStreetChar + ", resiStreet=" + resiStreet + ", resiSuburb="
				+ resiSuburb + ", resiState=" + resiState + ", resiCountry="
				+ resiCountry + ", resiPostcode=" + resiPostcode + ", resiLatitude="
				+ resiLatitude + ", resiLongitude=" + resiLongitude + ", resiGnaf="
				+ resiGnaf + ", resiDpid=" + resiDpid + ", resiStsubid=" + resiStsubid
				+ ", postProperty=" + postProperty + ", postAddress=" + postAddress
				+ ", postStreetChar=" + postStreetChar + ", postStreet=" + postStreet
				+ ", postSuburb=" + postSuburb + ", postState=" + postState
				+ ", postCountry=" + postCountry + ", postPostcode=" + postPostcode
				+ ", postDpid=" + postDpid + ", postStsubid=" + postStsubid
				+ ", ractClientNo=" + ractClientNo + ", residentialAddress="
				+ residentialAddress + ", postalAddress=" + postalAddress + "]";
	}
	public String getResiProperty()
	{
		return resiProperty;
	}
	public void setResiProperty(String resiStreetProperty)
	{
		this.resiProperty = resiStreetProperty;
	}
	/**
	 * @return the resiLatitude
	 */
	public String getResiLatitude() {
		return resiLatitude;
	}
	/**
	 * @param resiLatitude the resiLatitude to set
	 */
	public void setResiLatitude(String resiLatitude) {
		this.resiLatitude = resiLatitude;
	}
	/**
	 * @return the resiLongitude
	 */
	public String getResiLongitude() {
		return resiLongitude;
	}
	/**
	 * @param resiLongitude the resiLongitude to set
	 */
	public void setResiLongitude(String resiLongitude) {
		this.resiLongitude = resiLongitude;
	}
	/**
	 * @return the resiGnaf
	 */
	public String getResiGnaf() {
		return resiGnaf;
	}
	/**
	 * @param resiGnaf the resiGnaf to set
	 */
	public void setResiGnaf(String resiGnaf) {
		this.resiGnaf = resiGnaf;
	}
	/**
	 * @return the resiDpid
	 */
	public String getResiDpid() {
		return resiDpid;
	}
	/**
	 * @param resiDpid the resiDpid to set
	 */
	public void setResiDpid(String resiDpid) {
		this.resiDpid = resiDpid;
	}
	/**
	 * @return the postDpid
	 */
	public String getPostDpid() {
		return postDpid;
	}
	/**
	 * @param postDpid the postDpid to set
	 */
	public void setPostDpid(String postDpid) {
		this.postDpid = postDpid;
	}
	/**
	 * @return the postAusbar
	 */
	public String getPostAusbar() {
		return postAusbar;
	}
	/**
	 * @param postAusbar the postAusbar to set
	 */
	public void setPostAusbar(String postAusbar) {
		this.postAusbar = postAusbar;
	}
	/**
	 * @return the resiAusbar
	 */
	public String getResiAusbar() {
		return resiAusbar;
	}
	/**
	 * @param resiAusbar the resiAusbar to set
	 */
	public void setResiAusbar(String resiAusbar) {
		this.resiAusbar = resiAusbar;
	}
	/**
	 * @return the postLatitude
	 */
	public String getPostLatitude() {
		return postLatitude;
	}
	/**
	 * @param postLatitude the postLatitude to set
	 */
	public void setPostLatitude(String postLatitude) {
		this.postLatitude = postLatitude;
	}
	/**
	 * @return the postLongitude
	 */
	public String getPostLongitude() {
		return postLongitude;
	}
	/**
	 * @param postLongitude the postLongitude to set
	 */
	public void setPostLongitude(String postLongitude) {
		this.postLongitude = postLongitude;
	}
	/**
	 * @return the postGnaf
	 */
	public String getPostGnaf() {
		return postGnaf;
	}
	/**
	 * @param postGnaf the postGnaf to set
	 */
	public void setPostGnaf(String postGnaf) {
		this.postGnaf = postGnaf;
	}

	
}
