package com.ract.web.client;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.ract.client.Client;
import com.ract.common.PostalAddressVO;
import com.ract.common.ResidentialAddressVO;
import com.ract.util.DateTime;

/**
 * Transaction performed on RACT clients.
 * @author hollidayj
 *
 */

@Entity
public class ClientTransaction implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6132548577443158168L;
	public static final String TRANSACTION_TYPE_UPDATE = "Update";
	public static final String USER_ID_DEFAULT = "WEB";
	/**
	 * The user who made the change. Default will be "WEB" but this caters for changes
	 * made by RACT users via the web interface.
	 */
	protected String userId;
  protected String comments;
	
	protected DateTime processDate;
	@Id
	protected ClientTransactionPK clientTransactionPK;
	protected String  givenNames;
	protected String  surname;
	protected String  title;
	protected String  gender;
	protected DateTime birthDate;
	protected String homePhone;
	protected String workPhone;
 	protected String mobilePhone;
 	protected String faxNo;
	protected String emailAddress;
	
	protected String resiProperty;
	protected String resiAddress;
	protected String resiStreetChar;
	protected String resiStreet;
	protected String resiSuburb;
	protected String resiState;
	protected String resiCountry;
	protected String resiPostcode;
	protected Integer resiStsubid;
	
	protected String postProperty;
	protected String postAddress;
	protected String postStreetChar;
	protected String postStreet;
	protected String postSuburb;
	protected String postState;
	protected String postCountry;
	protected String postPostcode;
	protected Integer postStsubid;
	
	protected String status;
	
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	protected String preferredContactMethod;
	
	public String getStatusMessage()
	{
		String statusMessage = "";
		if (getStatus() != null)
		{
			if (getStatus().equals(Client.STATUS_ADDRESS_UNKNOWN))
			{
				statusMessage = "Mail returned as address unknown. Please update address details now.";
			}
		}
		return statusMessage;
	}
	
	@Transient
	protected ResidentialAddressVO residentialAddress;
	
	@Transient
	protected PostalAddressVO postalAddress;	
	
	private DateTime manuallyProcessed;
	
	public DateTime getManuallyProcessed()
	{
		return manuallyProcessed;
	}
	public void setManuallyProcessed(DateTime manuallyProcessed)
	{
		this.manuallyProcessed = manuallyProcessed;
	}
	public PostalAddressVO getPostalAddress()
	{
		return postalAddress;
	}
	public void setPostalAddress(PostalAddressVO postalAddress)
	{
		this.postalAddress = postalAddress;
				
		this.postProperty = postalAddress.getProperty();
		this.postStreetChar = postalAddress.getPropertyQualifier();		
		this.postStreet = postalAddress.getStreet();
		this.postState = postalAddress.getState();
		this.postSuburb = postalAddress.getSuburb();
		this.postPostcode = postalAddress.getPostcode();
		this.postCountry = postalAddress.getCountry();
		this.postStsubid = postalAddress.getStreetSuburbID();
		
		this.postAddress = postalAddress.toString();		
		
	}
	public DateTime getProcessDate()
	{
		return processDate;
	}
	public void setProcessDate(DateTime processDate)
	{
		this.processDate = processDate;
	}
	public ClientTransactionPK getClientTransactionPK()
	{
		return clientTransactionPK;
	}
	public void setClientTransactionPK(ClientTransactionPK clientTransactionPK)
	{
		this.clientTransactionPK = clientTransactionPK;
	}
	public String getGivenNames()
	{
		return givenNames;
	}
	public void setGivenNames(String givenNames)
	{
		this.givenNames = givenNames;
	}

	public String getSurname()
	{
		return surname;
	}
	public void setSurname(String surname)
	{
		this.surname = surname;
	}
	public String getTitle()
	{
		return title;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}
	public String getGender()
	{
		return gender;
	}
	public void setGender(String gender)
	{
		this.gender = gender;
	}
	public DateTime getBirthDate()
	{
		return birthDate;
	}
	public void setBirthDate(DateTime birthDate)
	{
		this.birthDate = birthDate;
	}
	public String getHomePhone()
	{
		return homePhone;
	}
	public void setHomePhone(String homePhone)
	{
		this.homePhone = homePhone;
	}
	public String getWorkPhone()
	{
		return workPhone;
	}
	public void setWorkPhone(String workPhone)
	{
		this.workPhone = workPhone;
	}
	public String getMobilePhone()
	{
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone)
	{
		this.mobilePhone = mobilePhone;
	}
	public String getFaxNo()
	{
		return faxNo;
	}
	public void setFaxNo(String faxNo)
	{
		this.faxNo = faxNo;
	}
	public String getEmailAddress()
	{
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress)
	{
		this.emailAddress = emailAddress;
	}
	public String getResiStreetChar()
	{
		return resiStreetChar;
	}
	
	public String getDisplayName()
	{
		return com.ract.client.ClientHelper.formatCustomerName(title, givenNames, surname, false, true, true, true);
	}
	
	public void setResiStreetChar(String resiStreetChar)
	{
		this.resiStreetChar = resiStreetChar;
	}
	public String getResiStreet()
	{
		return resiStreet;
	}
	public void setResiStreet(String resiStreet)
	{
		this.resiStreet = resiStreet;
	}
	public String getResiSuburb()
	{
		return resiSuburb;
	}
	public void setResiSuburb(String resiSuburb)
	{
		this.resiSuburb = resiSuburb;
	}
	public Integer getResiStsubid()
	{
		return resiStsubid;
	}
	public void setResiStsubid(Integer resiStsubid)
	{
		this.resiStsubid = resiStsubid;
	}
	public String getPostAddress()
	{
		return postAddress;
	}
	public void setPostAddress(String postAddress)
	{
		this.postAddress = postAddress;
	}
	public String getPostStreetChar()
	{
		return postStreetChar;
	}
	public void setPostStreetChar(String postStreetChar)
	{
		this.postStreetChar = postStreetChar;
	}
	public String getPostStreet()
	{
		return postStreet;
	}
	public void setPostStreet(String postStreet)
	{
		this.postStreet = postStreet;
	}
	public String getPostSuburb()
	{
		return postSuburb;
	}
	public void setPostSuburb(String postSuburb)
	{
		this.postSuburb = postSuburb;
	}
	public String getPostState()
	{
		return postState;
	}
	public void setPostState(String postState)
	{
		this.postState = postState;
	}
	public String getPostCountry()
	{
		return postCountry;
	}
	public void setPostCountry(String postCountry)
	{
		this.postCountry = postCountry;
	}

	public Integer getPostStsubid()
	{
		return postStsubid;
	}
	public void setPostStsubid(Integer postStsubid)
	{
		this.postStsubid = postStsubid;
	}
	public ResidentialAddressVO getResidentialAddress()
	{
		return residentialAddress;
	}
	public void setResidentialAddress(ResidentialAddressVO residentialAddress)
	{
		this.residentialAddress = residentialAddress;

		this.resiProperty = residentialAddress.getProperty();
		this.resiStreetChar = residentialAddress.getPropertyQualifier();
		this.resiStreet = residentialAddress.getStreet();
		this.resiSuburb = residentialAddress.getSuburb();
	  this.resiState = residentialAddress.getState();
	  this.resiCountry = residentialAddress.getCountry();
	  this.resiPostcode = residentialAddress.getPostcode();	
		this.resiStsubid = residentialAddress.getStreetSuburbID();
		
	  this.resiAddress = residentialAddress.toString();		
		
	}
	public String getComments()
	{
		return comments;
	}
	public void setComments(String comments)
	{
		this.comments = comments;
	}
	public String getUserId()
	{
		return userId;
	}
	public void setUserId(String userId)
	{
		this.userId = userId;
	}
	public String getResiState()
	{
		return resiState;
	}
	public void setResiState(String resiState)
	{
		this.resiState = resiState;
	}
	public String getResiCountry()
	{
		return resiCountry;
	}
	public void setResiCountry(String resiCountry)
	{
		this.resiCountry = resiCountry;
	}
	public String getResiPostcode()
	{
		return resiPostcode;
	}
	public void setResiPostcode(String resiPostcode)
	{
		this.resiPostcode = resiPostcode;
	}
	public String getPostPostcode()
	{
		return postPostcode;
	}
	public void setPostPostcode(String postPostcode)
	{
		this.postPostcode = postPostcode;
	}
	public String getResiProperty()
	{
		return resiProperty;
	}
	public void setResiProperty(String resiProperty)
	{
		this.resiProperty = resiProperty;
	}
	public String getPostProperty()
	{
		return postProperty;
	}
	public void setPostProperty(String postProperty)
	{
		this.postProperty = postProperty;
	}
	public String getResiAddress()
	{
		return resiAddress;
	}
	public void setResiAddress(String resiAddress)
	{
		this.resiAddress = resiAddress;
	}
	public String getPreferredContactMethod()
	{
		return preferredContactMethod;
	}
	public void setPreferredContactMethod(String preferredContactMethod)
	{
		this.preferredContactMethod = preferredContactMethod;
	}
	
}
