package com.ract.web.membership;

import java.io.File;
import java.rmi.RemoteException;
import java.text.DecimalFormat;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.ract.common.ServiceLocator;
import com.ract.common.SystemParameterVO;
import com.ract.common.reporting.ReportDeliveryFormat;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.common.reporting.XMLReportRequest;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.util.XDocument;
import com.ract.web.client.ClientHelper;

public class WebMembershipDocRequest extends XMLReportRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1585358829372351224L;

	private static final String DATASOURCE_DEFAULT_PARAMETER_NAME = "URL";

	private static final String PROPERTIES_FILE_DEFAULT = "master";

	private static final String WORKSPACE_DEFAULT = "/Ract";

	private static final String REPORT_TEMPLATE_MEM_JOIN_ADVICE_ROADSIDE = "mem/RoadsideWebMembershipJoinAdvice";

	private static final String REPORT_TEMPLATE_MEM_JOIN_ADVICE_LIFESTYLE = "mem/LifestyleWebMembershipJoinAdvice";

	private static final String LIFESTYLE = "Lifestyle";

	private WebMembershipMgrRemote memMgr = null;

	private XDocument doc = null;

	public String toXMLString() throws Exception {
		return doc.toXMLString();
	}

	public WebMembershipDocRequest(Integer transactionHeaderId) throws Exception {
		memMgr = (WebMembershipMgrRemote) ServiceLocator.getInstance().getObject("WebMembershipMgr/remote");
		initialSettings();
		// Prepare the data

		doc = new XDocument();
		reportXML = (Element) doc.addNode(doc, "membership");

		WebMembershipTransactionContainer wmtc = memMgr.getWebMembershipTransactionContainer(transactionHeaderId);
		WebMembershipTransactionHeader wmth = wmtc.getWebMembershipTransactionHeader();
		WebMembershipTransaction wmt = wmtc.getPrimeAddressWebMembershipTransaction();

		WebMembershipClient webClient = memMgr.getWebMembershipClient(wmt.getWebClientNo());

		doc.addLeaf(reportXML, "userName", wmth.getUserName());
		doc.addLeaf(reportXML, "clientTitle", webClient.getTitle());
		doc.addLeaf(reportXML, "webClientNo", webClient.getWebClientNo().toString());
		doc.addLeaf(reportXML, "address1", ClientHelper.getAddressLine1(webClient).toUpperCase());
		doc.addLeaf(reportXML, "address2", ClientHelper.getAddressLine2(webClient).toUpperCase());
		doc.addLeaf(reportXML, "address3", ClientHelper.getAddressLine3(webClient).toUpperCase());
		doc.addLeaf(reportXML, "address4", ClientHelper.getAddressLine4(webClient).toUpperCase());
		doc.addLeaf(reportXML, "postalName", ClientHelper.getPostalName(webClient).toUpperCase());
		doc.addLeaf(reportXML, "addressTitle", ClientHelper.getAddressTitle(webClient));
		doc.addLeaf(reportXML, "transactionReference", wmth.getTransactionHeaderId().toString()); // web transaction reference
		doc.addLeaf(reportXML, "transactionId", wmt.getInterimMembershipNo()); // temporary membership number
		doc.addLeaf(reportXML, "productCode", wmt.getProductCode());
		doc.addLeaf(reportXML, "startDate", wmt.getStartDate().formatShortDate());
		doc.addLeaf(reportXML, "endDate", wmt.getEndDate().formatShortDate());
		doc.addLeaf(reportXML, "createDate", wmth.getCreateDate().formatLongDate());

		// This depends on the product type
		this.setReportTemplate(WORKSPACE_DEFAULT, wmt.getProductCode().equals(LIFESTYLE) ? REPORT_TEMPLATE_MEM_JOIN_ADVICE_LIFESTYLE : REPORT_TEMPLATE_MEM_JOIN_ADVICE_ROADSIDE); // constants

		/*
		 * Not required String prodMessage = CommonEJBHelper.getCommonMgr().getGnText(SourceSystem.MEMBERSHIP.getAbbreviation(), "WEBADVICE", wmt.getProductCode().toUpperCase());
		 * 
		 * String termsConditions = CommonEJBHelper.getCommonMgr().getGnText(SourceSystem.MEMBERSHIP.getAbbreviation(), "TANDC", wmt.getProductCode().toUpperCase());
		 * 
		 * doc.addLeaf(reportXML, "prodMessage", prodMessage);
		 * 
		 * doc.addLeaf(reportXML, "termsConditions", termsConditions); terms and conditions in XML format;
		 * 
		 * body text is in xml format to allow flexible formatting merge nodes into main document
		 * 
		 * LogUtil.log(this.getClass(), "termsConditions="+termsConditions);
		 * 
		 * try { DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); factory.setValidating(false); DocumentBuilder builder = factory.newDocumentBuilder(); Document document = builder.parse(new InputSource(new StringReader(termsConditions))); Node node = document.getFirstChild(); if (node != null) { reportXML.appendChild(doc.importNode(node, true)); //must import!!! } } catch (Exception e) { throw new Exception("Terms and conditions are not supplied in XML format. "+e.getMessage()); }
		 */

		Node feeDetails = doc.addNode(reportXML, "feeDetail");
		doc.addLeaf(feeDetails, "title", "Fees");
		doc.addLeaf(feeDetails, "value", wmth.getAmountPayable().toString());

		// format amount
		DecimalFormat amountFormat = new DecimalFormat("\u00A4#########.00");
		DecimalFormat taxFormat = new DecimalFormat(" (inc gst \u00A4#########.00)");
		doc.addLeaf(reportXML, "amountPayable", amountFormat.format(wmth.getAmountPayable()) + taxFormat.format(wmth.getTaxTotal()));
		doc.addLeaf(reportXML, "amount", wmth.getAmountPayable().toString());
		doc.addLeaf(reportXML, "gst", wmth.getTaxTotal().toString());

		LogUtil.log(this.getClass(), doc.toXMLString());
	}

	private void initialSettings() throws RemoteException {
		this.setFormName(REPORT_DELIVERY_METHOD_EMAIL); // constant
		this.setDeliveryFormat(ReportDeliveryFormat.getReportDeliveryFormat(ReportRequestBase.REPORT_DELIVERY_FORMAT_PDF));
		this.setMIMEType(ReportRequestBase.MIME_TYPE_PDF);
		this.setReportName("WebMembershipJoinAdvice");
		// this.setReportTemplate(WORKSPACE_DEFAULT,REPORT_TEMPLATE_MEM_JOIN_ADVICE); //constants set this a bit later so we differentiate between roadside and lifestyle
		try {
			this.constructURL();
		} catch (Exception e) {
			throw new RemoteException(e.getMessage());
		}
		this.addParameter(DATASOURCE_DEFAULT_PARAMETER_NAME, this.getReportDataURL());

		/* write the file to the report cache directory */
		try {
			String reportCache = FileUtil.getProperty(PROPERTIES_FILE_DEFAULT, SystemParameterVO.DIRECTORY_REPORT_CACHE);
			String fileName = reportName + (new java.util.Date().getTime() + "").substring(0, 8) + "." + REPORT_DELIVERY_FORMAT_PDF;
			this.setDestinationFileName(reportCache + File.separator + fileName);
		} catch (Exception rex) {
			rex.printStackTrace();
			throw new RemoteException("" + rex);
		}
	}

}
