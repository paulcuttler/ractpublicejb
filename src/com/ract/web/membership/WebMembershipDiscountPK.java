package com.ract.web.membership;

import java.io.Serializable;

import javax.persistence.Embeddable;

import com.ract.util.DateTime;

@Embeddable
public class WebMembershipDiscountPK implements Serializable
{

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((discountTypeCode == null) ? 0 : discountTypeCode.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof WebMembershipDiscountPK))
			return false;
		WebMembershipDiscountPK other = (WebMembershipDiscountPK) obj;
		if (discountTypeCode == null)
		{
			if (other.discountTypeCode != null)
				return false;
		}
		else
			if (!discountTypeCode.equals(other.discountTypeCode))
				return false;
		if (endDate == null)
		{
			if (other.endDate != null)
				return false;
		}
		else
			if (!endDate.equals(other.endDate))
				return false;
		if (startDate == null)
		{
			if (other.startDate != null)
				return false;
		}
		else
			if (!startDate.equals(other.startDate))
				return false;
		return true;
	}

	private String discountTypeCode;
	
	private DateTime startDate;
	
	private DateTime endDate;

	public String getDiscountTypeCode()
	{
		return discountTypeCode;
	}

	public void setDiscountTypeCode(String discountTypeCode)
	{
		this.discountTypeCode = discountTypeCode;
	}

	public DateTime getStartDate()
	{
		return startDate;
	}

	public void setStartDate(DateTime startDate)
	{
		this.startDate = startDate;
	}

	public DateTime getEndDate()
	{
		return endDate;
	}

	public void setEndDate(DateTime endDate)
	{
		this.endDate = endDate;
	}
}
