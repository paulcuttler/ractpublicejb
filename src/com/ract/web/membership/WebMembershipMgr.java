package com.ract.web.membership;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;

import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientVO;
import com.ract.client.PersonVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgrLocal;
import com.ract.common.GenericException;
import com.ract.common.MailMgr;
import com.ract.common.PostalAddressVO;
import com.ract.common.SequenceMgrLocal;
import com.ract.common.ServiceLocator;
import com.ract.common.ResidentialAddressVO;
import com.ract.common.SourceSystem;
import com.ract.common.SystemParameterVO;
import com.ract.common.cad.CadExportMgrLocal;
import com.ract.common.cad.CadExportVO;
import com.ract.common.mail.MailMessage;
import com.ract.common.reporting.ReportGenerator;
import com.ract.common.reporting.ReportRegister;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.membership.DiscountTypeVO;
import com.ract.membership.FeeTypeVO;
import com.ract.membership.MembershipCardVO;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgr;
import com.ract.membership.MembershipMgrLocal;
import com.ract.membership.MembershipProfileVO;
import com.ract.membership.MembershipRefMgr;
import com.ract.membership.MembershipTransactionTypeVO;
import com.ract.membership.MembershipTransactionVO;
import com.ract.membership.MembershipTypeVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.ProductVO;
import com.ract.membership.TransactionGroup;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMgr;
import com.ract.payment.PaymentMgrLocal;
import com.ract.payment.PaymentTypeVO;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.FileUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.web.client.CustomerMgrLocal;
import com.ract.web.client.WebClient;
import com.ract.web.common.WebBranchAllocation;
import com.ract.web.common.WebBranchAllocationPK;
import com.ract.web.common.WebPayable;
import com.ract.web.payment.WebMembershipPayment;
import com.ract.web.payment.WebPayment;
import com.ract.web.payment.WebPaymentPK;

/**
 * Manage web membership operations.
 * 
 * @author hollidayj
 * 
 */
@Stateless
@Local({ WebMembershipMgrLocal.class })
public class WebMembershipMgr implements WebMembershipMgrRemote {

	private static final String PARAMETER_CAD_TEMP_EFF_PERIOD = "CADTMPEFF";

	private static final String MESSAGE_RACT_JOIN_MEMBERSHIP_ADVICE = "Welcome to the family.\n\nYour membership confirmation is attached.\n\nYour RACT team";

	private static final String SUBJECT_RACT_JOIN_MEMBERSHIP_ADVICE = "Membership confirmation";

	private static final String COMMENCE_DATE_INTERVAL = "commence_date_interval";

	final int SCALE_DEFAULT = 2;

	final String SEQ_WEB_MEM_BRANCH_ALLOCATION = "WEB_MEM_BRANCH_ALLOCATION";

	@PersistenceContext
	EntityManager em;

	@EJB
	private CadExportMgrLocal cadExportMgrLocal;

	@EJB
	private SequenceMgrLocal sequenceMgrLocal;

	@EJB
	private CommonMgrLocal commonMgrLocal;

	@EJB
	private MembershipMgrLocal membershipMgrLocal;

	@EJB
	private PaymentMgrLocal paymentMgrLocal;

	public String allocateBranch(String sourceSystem) throws GenericException {
		if (SourceSystem.MEMBERSHIP.getAbbreviation().equals(sourceSystem)) {
			Integer allocationSequence;
			try {
				allocationSequence = sequenceMgrLocal.getNextID(SEQ_WEB_MEM_BRANCH_ALLOCATION);
				Query branchAllocationQuery = em.createQuery("select e from WebBranchAllocation e where e.webBranchAllocationPK.sourceSystem = ?1");
				branchAllocationQuery.setParameter(1, sourceSystem);
				List allocationList = branchAllocationQuery.getResultList();
				LogUtil.debug(this.getClass(), "allocationList=" + allocationList);
				// reset back to 1 if allocation sequence is greater than the
				// record count
				if (allocationSequence > allocationList.size()) {
					// reset it
					allocationSequence = 1;
					// reset id to 1 as we have used 0 this time
					sequenceMgrLocal.resetTableId(SEQ_WEB_MEM_BRANCH_ALLOCATION, allocationSequence);
				}
			} catch (RemoteException e) {
				LogUtil.fatal(this.getClass(), e);
				throw new GenericException(e.getMessage());
			}
			LogUtil.debug(this.getClass(), "allocationSequence=" + allocationSequence);
			// get branch allocation
			WebBranchAllocationPK pk = new WebBranchAllocationPK();
			pk.setAllocationSequence(allocationSequence);
			pk.setSourceSystem(sourceSystem);
			WebBranchAllocation wb = em.find(WebBranchAllocation.class, pk);
			return wb.getSalesBranchCode();
		} else {
			throw new GenericException("Not implemented for " + sourceSystem);
		}
	}

	/**
	 * Create a new web membership client in the database.
	 * 
	 * @param webMembershipClient The web membership client to save to the database.
	 * @return The saved web membership client including the unique client identifier.
	 */
	public WebMembershipClient createMembershipClient(WebMembershipClient webMembershipClient) throws GenericException {
		return (WebMembershipClient) customerMgr.createWebClient(webMembershipClient);
	}

	public void createTemporaryRoadserviceRecords() throws GenericException {
		Collection webMembershipTransactions = getRoadserviceWebMembershipTransactions();
		for (Iterator<WebMembershipTransaction> i = webMembershipTransactions.iterator(); i.hasNext();) {
			try {
				createRoadserviceRecord(i.next().getWebMembershipTransactionPK().getTransactionId());
			} catch (Exception e) {
				LogUtil.fatal(this.getClass(), e);
			}
		}
	}

	public void resetTemporaryRoadserviceRecords() throws GenericException {
		Collection webMembershipTransactions = getWebMembershipTransactions();
		WebMembershipTransaction wmt = null;
		for (Iterator<WebMembershipTransaction> i = webMembershipTransactions.iterator(); i.hasNext();) {
			wmt = i.next();
			wmt.setCadCreated(null);
			updateWebMembershipTransaction(wmt);
		}
	}

	private Collection getWebMembershipTransactions() {
		Query query = em.createQuery("select e from WebMembershipTransaction e");
		return query.getResultList();
	}

	private Collection getRoadserviceWebMembershipTransactions() {
		Query query = em.createQuery("select e from WebMembershipTransaction e where e.cadCreated is null and e.webClientNo > 0 and e.joinDate > ?1");

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, -6);

		query.setParameter(1, new DateTime(cal.getTime()));

		return query.getResultList();
	}

	private void createRoadserviceRecord(Integer transactionId) throws GenericException {

		DateTime now = new DateTime();

		LogUtil.debug(this.getClass(), "createRoadserviceRecord start");

		WebMembershipTransaction wmt = getWebMembershipTransaction(transactionId);

		Interval temporaryEffectivePeriod = null;

		try {
			temporaryEffectivePeriod = new Interval(commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, PARAMETER_CAD_TEMP_EFF_PERIOD));
		} catch (Exception e1) {
			throw new GenericException(e1);
		}
		DateTime endDate = wmt.getStartDate().add(temporaryEffectivePeriod);
		WebMembershipTransactionHeader wmth = getWebMembershipTransactionHeader(wmt.getWebMembershipTransactionPK().getTransactionHeaderId());
		WebClient webClient = wmt.getWebClient();
		if (webClient == null) {
			throw new GenericException("No client has been attached to the transaction");
		}
		CadExportVO cad = new CadExportVO();

		LogUtil.debug(this.getClass(), "Transaction " + transactionId + " has a header status of '" + wmth.getConversionStatus() + "'.");

		if (wmth.getConversionStatus() == null) {
			LogUtil.debug(this.getClass(), "Ignoring transaction " + transactionId + " as it has not been completed.");
			// ignore as they have not completed the membership
		} else {

			// send only if never to convert - ie. include manually
			// converted/converted. Possible both T prefix and normal
			// membership will be active at the same point in time.
			// only send if current date after membership effective date.
			if (now.compareTo(wmt.getStartDate()) > 0) {
				LogUtil.debug(this.getClass(), "Creating roadservice record for " + transactionId + ".");

				// active
				if (now.compareTo(endDate) < 0 && !WebMembershipTransactionHeader.STATUS_NEVER_CONVERT.equals(wmth.getConversionStatus()))// not never
				// convert
				{
					cad.setDeleted("N");
				} else
				// expired ie. > 14 days
				{
					cad.setDeleted("Y");
				}

				if (wmt.getProductCode().equals(ProductVO.PRODUCT_ULTIMATE)) {
					cad.setPlusFlag("+");
				} else {
					cad.setPlusFlag("");
				}
				cad.setMemberType(wmt.getProductCode().substring(0, 3));

				cad.setExportType("WEB");

				ResidentialAddressVO residentialAddress = webClient.getResidentialAddress();
				if (residentialAddress != null) {
					cad.setAddress(residentialAddress.getProperty() + FileUtil.SEPARATOR_COMMA + residentialAddress.getPropertyQualifier() + FileUtil.SEPARATOR_COMMA + residentialAddress.getStreet() + FileUtil.SEPARATOR_COMMA + residentialAddress.getSuburb() + FileUtil.SEPARATOR_COMMA + residentialAddress.getState() + FileUtil.SEPARATOR_COMMA + residentialAddress.getPostcode());
				} else {
					cad.setAddress(webClient.getResiProperty() + FileUtil.SEPARATOR_COMMA + webClient.getResiStreetChar() + FileUtil.SEPARATOR_COMMA + webClient.getResiStreet() + FileUtil.SEPARATOR_COMMA + webClient.getResiSuburb() + FileUtil.SEPARATOR_COMMA + webClient.getResiState() + FileUtil.SEPARATOR_COMMA + webClient.getResiPostcode());
				}
				PostalAddressVO postalAddress = webClient.getPostalAddress();
				if (postalAddress != null) {
					cad.setAddress2(postalAddress.getProperty() + FileUtil.SEPARATOR_COMMA + postalAddress.getPropertyQualifier() + FileUtil.SEPARATOR_COMMA + postalAddress.getStreet() + FileUtil.SEPARATOR_COMMA + postalAddress.getSuburb() + FileUtil.SEPARATOR_COMMA + postalAddress.getState() + FileUtil.SEPARATOR_COMMA + postalAddress.getPostcode());
				} else {
					cad.setAddress2(webClient.getPostProperty() + FileUtil.SEPARATOR_COMMA + webClient.getPostStreetChar() + FileUtil.SEPARATOR_COMMA + webClient.getPostStreet() + FileUtil.SEPARATOR_COMMA + webClient.getPostSuburb() + FileUtil.SEPARATOR_COMMA + webClient.getPostState() + FileUtil.SEPARATOR_COMMA + webClient.getPostPostcode());
				}

				cad.setBirthDate(webClient.getBirthDate().formatShortDate());

				cad.setClientNo(transactionId);
				cad.setClientTitle(webClient.getTitle());
				cad.setGivenNames(webClient.getGivenNames());
				cad.setCreateDateTime(new DateTime());
				cad.setHomePhone(webClient.getHomePhone());
				cad.setExpiryDate(wmt.getEndDate().formatShortDate());

				cad.setMembershipHold("");
				cad.setMembershipYears(DateUtil.getDateDiff(wmt.getStartDate(), wmt.getEndDate(), Calendar.YEAR));

				cad.setJoinDate(wmt.getStartDate());
				cad.setRemoveRecord(cad.getDeleted());
				cad.setSurname(webClient.getSurname());
				cad.setTranNo(null);
				cad.setEffectiveDate(wmt.getStartDate());

				try {
					cadExportMgrLocal.createCADRecord(cad);
				} catch (Exception e) {
					throw new GenericException(e);
				}
				LogUtil.debug(this.getClass(), "Updating cad created to " + now + " for " + wmt.getWebMembershipTransactionPK().getTransactionId());
				// set cad flag
				wmt.setCadCreated(now);
				updateWebMembershipTransaction(wmt);
			}
		}
		LogUtil.debug(this.getClass(), "createRoadserviceRecord end");
	}

	@EJB
	CustomerMgrLocal customerMgr;

	/**
	 * getLifestyleZeroDates
	 * 
	 * If present get the set of LifeStyle Zero dates to establish the data range
	 * 
	 * @param webMembershipClient
	 * @return
	 * @throws GenericException
	 * @throws RemoteException
	 */
	public HashMap<String, String> getLifestyleZeroDates() throws GenericException, RemoteException {
		HashMap<String, String> lifeStyleZeroDates = new HashMap<String, String>();
		lifeStyleZeroDates.put("start", CommonEJBHelper.getCommonMgr().getSystemParameter("MEM", "LIFESTYLE_FOR_0_START").getValue());
		lifeStyleZeroDates.put("end", CommonEJBHelper.getCommonMgr().getSystemParameter("MEM", "LIFESTYLE_FOR_0_END").getValue());
		return lifeStyleZeroDates;
	}

	/**
	 * Create a new web membership client in the database.
	 * 
	 * @param webMembershipClient The web membership client to save to the database.
	 * @return The saved web membership client including the unique client identifier.
	 */
	public WebMembershipClient updateMembershipClient(WebMembershipClient webMembershipClient) throws GenericException {
		return (WebMembershipClient) customerMgr.updateWebClient(webMembershipClient);
	}

	public WebMembershipTransactionHeader updateWebMembershipTransactionHeader(WebMembershipTransactionHeader webMembershipTransactionHeader) throws GenericException {
		return em.merge(webMembershipTransactionHeader);
	}

	public WebMembershipTransaction updateWebMembershipTransaction(WebMembershipTransaction webMembershipTransaction) throws GenericException {
		return em.merge(webMembershipTransaction);
	}

	public WebMembershipPayment updateWebMembershipPayment(WebMembershipPayment webMembershipPayment) throws GenericException {
		return em.merge(webMembershipPayment);
	}

	/**
	 * Retrieve an existing web membership client record from the database.
	 * 
	 * @param webClientNo The unique web client identifier.
	 * @return The retrieved web membership client.
	 * @throws Exception
	 */
	public WebMembershipClient getWebMembershipClient(Integer webClientNo) throws GenericException {
		WebMembershipClient clt = (WebMembershipClient) em.find(WebMembershipClient.class, webClientNo);
		return clt;
	}

	/**
	 * Record the payment details for a web transaction.
	 * 
	 * @param transactionReference The unique transaction reference for the transaction. For membership, this will be the transactionHeaderId and for Insurance it will be the quote/policy no.
	 * @param cardHolder The name on the card
	 * @param cardType The type of card. Eg. VISA, Mastercard, etc.
	 * @param cvv The secure verification number.
	 * @param expiryDate The card expiry date in the for MMYY. eg. 0110
	 * @return The saved web payment.
	 */
	public WebPayment createWebPayment(WebPayment webPayment) throws GenericException {
		em.persist(webPayment);
		return webPayment;
	}

	public WebMembershipPayment getWebMembershipPayment(String transactionReference) throws GenericException {
		WebMembershipPayment webPayment = (WebMembershipPayment) getWebPayment(WebPayment.TRANSACTION_TYPE_MEMBERSHIP, transactionReference);
		return webPayment;
	}

	/**
	 * Create a web membership transaction header in the database.
	 * 
	 * @param wmth The web membership transaction header
	 * @return The saved web membership transaction header including the unique identifier.
	 * @throws GenericException
	 */
	public WebMembershipTransactionHeader createWebMembershipTransactionHeader(WebMembershipTransactionHeader wmth) throws GenericException {
		Integer transactionHeaderId = null;
		try {
			transactionHeaderId = sequenceMgrLocal.getNextID(WebMembershipTransactionHeader.SEQUENCE_WEB_MEMBERSHIP_TRANSACTION_HEADER);
		} catch (Exception e1) {
			throw new GenericException(e1);
		}
		wmth.setTransactionHeaderId(transactionHeaderId);
		// completed false
		em.persist(wmth);
		return wmth;
	}

	public Collection<WebMembershipTransaction> getWebMembershipTransactionList(Integer transactionHeaderId) throws GenericException {
		LogUtil.info(this.getClass(), "#--# getWebMembershipTransactionList ");

		ArrayList<WebMembershipTransaction> transactionList = new ArrayList<WebMembershipTransaction>();

		LogUtil.info(this.getClass(), "#--# query: select e from WebMembershipTransaction e where e.webMembershipTransactionPK.transactionHeaderId = ?1");
		Query txQuery = em.createQuery("select e from WebMembershipTransaction e where e.webMembershipTransactionPK.transactionHeaderId = ?1");
		txQuery.setParameter(1, transactionHeaderId);

		LogUtil.info(this.getClass(), "#--# query: get result");
		List txList = txQuery.getResultList();

		LogUtil.info(this.getClass(), "#--# query: creating iterator");
		Iterator txIt = txList.listIterator();

		while (txIt.hasNext()) {
			WebMembershipTransaction tx = (WebMembershipTransaction) txIt.next();

			LogUtil.info(this.getClass(), "#--# query: iterating for transactions");

			transactionList.add(tx);
		}

		LogUtil.info(this.getClass(), "#--# getWebMembershipTransactionList COMPLETE");

		return transactionList;
	}

	/**
	 * Mark a web membership transaction header as completed after recording payment details.
	 * 
	 * @param wmth The web membership transaction header to complete.
	 * @return The saved web membership transaction header with completed flag set.
	 */
	public WebMembershipTransactionContainer completeWebMembershipTransactions(WebMembershipTransactionContainer wmtc) throws GenericException {
		WebMembershipTransactionHeader wmth = wmtc.getWebMembershipTransactionHeader();
		if (wmth.getTransactionHeaderId() == null) {
			throw new GenericException("Only transactions with valid headers can be completed.");
		}

		WebPayment webPayment = wmtc.getWebPayment();
		if (webPayment == null) {
			throw new GenericException("No payment attached.");
		}
		createWebPayment(webPayment);

		wmth.setCompletedDate(new DateTime());
		wmth.setConversionStatus(WebMembershipTransactionHeader.STATUS_COVERED);
		em.merge(wmth);

		// send to cad
		// cadExportMgrLocal

		return wmtc;
	}

	/**
	 * Associate customers and identify the prime addressee.
	 */
	public void associateWebClient(WebMembershipTransactionContainer wmtc, WebMembershipClient client) throws GenericException {
		WebMembershipTransactionHeader wmth = wmtc.getWebMembershipTransactionHeader();
		if (wmth.getTransactionHeaderId() == null) {
			throw new GenericException("Only transactions with valid headers can be completed.");
		}
		WebMembershipTransaction wmt = wmtc.getPrimeAddressWebMembershipTransaction();
		// update individual transaction with the web client number.
		wmt.setWebClientNo(client.getWebClientNo());
		em.merge(wmt);
	}

	/**
	 * 
	 * @param membershipId the target membership
	 * @param upgradedProductCode the target product code.
	 * @return
	 * @throws GenericException
	 */
	public WebMembershipTransactionContainer createWebMembershipUpgradeTransaction(Integer membershipId, String upgradedProductCode) throws GenericException {
		return null;
	}

	public WebMembershipTransactionContainer createWebMembershipRejoinTransaction(Integer membershipId) throws GenericException {
		return null;
	}

	/**
	 * 
	 * @param membershipId the target membership
	 * @return
	 * @throws GenericException
	 */
	public MembershipCardVO requestMembershipCard(Integer membershipId) throws GenericException {
		return null;
	}

	/**
	 * Get a list of valid web membership payment types.
	 * 
	 * @return The list of payment types
	 */
	public Collection<String> getMembershipPaymentTypes() throws GenericException {
		Collection<String> validPaymentTypes = new ArrayList<String>();
		Collection<PaymentTypeVO> paymentTypes = null;
		PaymentMgr mgr = null;
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			mgr = PaymentEJBHelper.getPaymentMgr();
			PaymentTypeVO paymentType = null;
			paymentTypes = mgr.getPaymentTypeListForMembership();
			for (Iterator<PaymentTypeVO> i = paymentTypes.iterator(); i.hasNext();) {
				paymentType = i.next();
				if (paymentType.getCashTypeCode().equals(PaymentTypeVO.CASHTYPE_CREDITCARD)) {
					validPaymentTypes.add(paymentType.getDescription().toUpperCase());
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new GenericException(e);
		}
		return validPaymentTypes;
	}

	/**
	 * Get a list of valid web membership products.
	 * 
	 * @return The list of products
	 */
	public Collection<String> getMembershipProducts() throws GenericException {
		MembershipMgr membershipMgr;
		ProductVO product = null;
		Collection<ProductVO> products = null;
		Collection<String> validProducts = new ArrayList<String>();
		try {
			membershipMgr = MembershipEJBHelper.getMembershipMgr();
			products = membershipMgr.getProducts();
			for (Iterator<ProductVO> i = products.iterator(); i.hasNext();) {
				product = i.next();
				if (product.includesRoadsideAssistance()) {
					validProducts.add(product.getProductCode());
				}
			}
		} catch (Exception e) {
			throw new GenericException(e);
		}
		return validProducts;
	}

	/**
	 * Create a web membership transaction record in the database representing an individual transaction record.
	 * 
	 * @param wmt A web membership transaction
	 * @param wmth A web membership transaction header
	 * @return The web membership transaction including the unique transaction identifier.
	 * @throws GenericException
	 */
	public WebMembershipTransaction createWebMembershipTransaction(WebMembershipTransaction wmt, WebMembershipTransactionHeader wmth) throws GenericException {
		Integer transactionId = null;
		try {
			transactionId = sequenceMgrLocal.getNextID(WebMembershipTransaction.SEQUENCE_WEB_MEMBERSHIP_TRANSACTION);
		} catch (Exception e1) {
			throw new GenericException(e1);
		}
		wmt.setWebMembershipTransactionPK(new WebMembershipTransactionPK());
		wmt.getWebMembershipTransactionPK().setTransactionHeaderId(wmth.getTransactionHeaderId());
		wmt.getWebMembershipTransactionPK().setTransactionId(transactionId);
		em.persist(wmt);
		em.flush();
		return wmt;
	}

	public DateTime getDefaultCommenceDate() {
		DateTime commenceDate = new DateTime();
		try {
			// TODO push back to server

			// get the interval from a file
			String defaultCommenceDateInterval = FileUtil.getProperty("master", COMMENCE_DATE_INTERVAL);
			commenceDate = commenceDate.add(new Interval(defaultCommenceDateInterval));
		} catch (Exception e) {
			// ignore
		}
		return commenceDate;
	}

	/**
	 * Create a join transaction consisting of a header record and a single detail record.
	 * 
	 * @param preferredCommenceDate The start date of the membership
	 * @param webClient The entered client details
	 * @param productCode The selected product code
	 * @param comments Any comments
	 * @return The web membership container including the header and detail records.
	 */
	public WebMembershipTransactionContainer createWebMembershipJoinTransaction(DateTime preferredCommenceDate, WebMembershipClient webClient, String productCode, String productTerm, String comments, boolean persist) throws GenericException {
		WebMembershipTransactionContainer wmtc = createWebMembershipTransactionContainer(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE, preferredCommenceDate, webClient, productCode, productTerm, comments, persist);
		return wmtc;
	}

	/**
	 * Generic method for creating and recording membership transactions.
	 * 
	 * @param preferredCommenceDate The start date of the membership
	 * @param webClient The entered client details
	 * @param productCode The selected product code
	 * @param comments Any comments
	 * @return The generic web membership transaction container fully populated.
	 * @throws GenericException
	 */
	private WebMembershipTransactionContainer createWebMembershipTransactionContainer(String groupTransactionTypeCode, DateTime preferredCommenceDate, WebMembershipClient webClient, String productCode, String productTerm, String comments, boolean persist) throws GenericException {

		WebMembershipTransactionContainer wmtc = new WebMembershipTransactionContainer();
		TransactionGroup transGroup = generateMembershipTransactions(groupTransactionTypeCode, preferredCommenceDate, webClient, productCode, productTerm);
		generateHeaderTransactions(webClient, wmtc, transGroup, comments, persist);
		return wmtc;
	}

	private void generateHeaderTransactions(WebMembershipClient webClient, WebMembershipTransactionContainer wmtc, TransactionGroup transGroup, String comments, boolean persist) throws GenericException {
		WebMembershipTransactionHeader wmth = wmtc.getWebMembershipTransactionHeader();
		if (wmth == null) {
			wmth = new WebMembershipTransactionHeader();
		}
		double headerDiscountTotal = 0;
		double discountTotal = 0;
		Collection<MembershipTransactionVO> transactionList = transGroup.getTransactionList();
		MembershipTransactionVO transaction = null;
		WebMembershipTransaction wmt = null;
		MembershipVO newMembership = null;
		int txCount = 0;

		try {
			wmth.setTransactionGroupTypeCode(transGroup.getTransactionGroupTypeCode());
			if (wmth.getTransactionHeaderId() == null) {
				wmth.setCreateDate(transGroup.getTransactionDate());
			} else {
				wmth.setLastUpdate(new DateTime());
			}
			wmth.setAmountPayable(new BigDecimal(transGroup.getAmountPayable()).setScale(SCALE_DEFAULT, BigDecimal.ROUND_HALF_DOWN));
			wmth.setNetTransactionFee(new BigDecimal(transGroup.getNetTransactionFee()).setScale(SCALE_DEFAULT, BigDecimal.ROUND_HALF_DOWN));
			wmth.setGrossTransactionFee(new BigDecimal(transGroup.getGrossTransactionFee()).setScale(SCALE_DEFAULT, BigDecimal.ROUND_HALF_DOWN));
			wmth.setAdjustmentTotal(new BigDecimal(0).setScale(SCALE_DEFAULT, BigDecimal.ROUND_HALF_DOWN));
			headerDiscountTotal = transGroup.getGrossTransactionFee() - transGroup.getNetTransactionFee(); // discount total
			wmth.setDiscountTotal(new BigDecimal(headerDiscountTotal).setScale(SCALE_DEFAULT, BigDecimal.ROUND_HALF_DOWN));
			wmth.setTaxTotal(new BigDecimal(transGroup.getGstAmount()).setScale(SCALE_DEFAULT, BigDecimal.ROUND_HALF_DOWN));
			wmth.setTransactionXML(transGroup.toXML());
			wmth.setUserName(transGroup.getUserID());
			wmth.setComments(comments);

		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			throw new GenericException(e1);
		}

		if (persist) {

			if (wmth.getTransactionHeaderId() == null) {
				// create and return with the id
				wmth = createWebMembershipTransactionHeader(wmth);
			} else {
				// update existing
				wmth = updateWebMembershipTransactionHeader(wmth);
			}

			if (wmth.getTransactionHeaderId() != null) {
				wmtc.getTransactionList().clear();
				// delete attached transactions
				final String DELETE_HQL = "delete from WebMembershipTransaction where webMembershipTransactionPK.transactionHeaderId = ?1";
				Query query = em.createQuery(DELETE_HQL);
				query.setParameter(1, wmth.getTransactionHeaderId());
				int rowsUpdated = query.executeUpdate();
				LogUtil.debug(this.getClass(), "rowsUpdated=" + rowsUpdated);
				em.flush();
			}

		}

		wmtc.setWebMembershipTransactionHeader(wmth);
		// create transactions
		for (Iterator<MembershipTransactionVO> i = transactionList.iterator(); i.hasNext();) {
			txCount++;
			try {
				transaction = i.next();

				newMembership = transaction.getNewMembership();

				wmt = new WebMembershipTransaction();
				wmt.setCreateDate(new DateTime());
				wmt.setMembershipTerm(newMembership.getMembershipTerm().toString());
				wmt.setWebClientNo(webClient.getWebClientNo());
				wmt.setPrimeAddressee(transaction.isPrimeAddressee());
				wmt.setTransactionTypeCode(transaction.getTransactionTypeCode());

				// comments
				wmt.setProfileCode(newMembership.getMembershipProfileCode());
				wmt.setMembershipType(newMembership.getMembershipType().getMembershipTypeCode());

				wmt.setStartDate(transaction.getPreferredCommenceDate().getDateOnly());
				wmt.setEndDate(newMembership.getExpiryDate().getDateOnly());
				wmt.setProductCode(newMembership.getProductCode());
				wmt.setJoinDate(newMembership.getJoinDate());
				// transaction level
				wmt.setAmountPayable(new BigDecimal(transaction.getAmountPayable()));
				wmt.setNetTransactionFee(new BigDecimal(transaction.getNetTransactionFee()));
				wmt.setGrossTransactionFee(new BigDecimal(transaction.getGrossTransactionFee()));
				wmt.setAdjustmentTotal(transaction.getAdjustmentTotal());
				discountTotal = transaction.getGrossTransactionFee() - transaction.getNetTransactionFee(); // discount total
				wmt.setDiscountTotal(new BigDecimal(discountTotal));
				transaction.getGstFee();
				wmt.setTaxTotal(new BigDecimal(transaction.getGstFee()));
				wmt.setJoinFee(new BigDecimal(transaction.getJoinFeeAmount()));
				if (persist) {
					wmt = createWebMembershipTransaction(wmt, wmth);
				}
				wmtc.addTransaction(wmt);

			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				throw new GenericException("Unable to add web membership transaction.", e);
			}

		}
	}

	public WebMembershipTransactionContainer updateWebMembershipJoinTransactionContainer(Integer transactionHeaderId, DateTime preferredCommenceDate, WebMembershipClient webClient, String productCode, String productTerm, String comments, boolean persist) throws GenericException {
		return updateWebMembershipTransactionContainer(transactionHeaderId, MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE, preferredCommenceDate, webClient, productCode, productTerm, comments, persist);
	}

	/**
	 * Used during a membership transaction
	 * 
	 * @param transactionHeaderId
	 * @param preferredCommenceDate
	 * @param webClient
	 * @param productCode
	 * @param comments
	 * @return
	 * @throws GenericException
	 */
	private WebMembershipTransactionContainer updateWebMembershipTransactionContainer(Integer transactionHeaderId, String groupTransactionTypeCode, DateTime preferredCommenceDate, WebMembershipClient webClient, String productCode, String productTerm, String comments, boolean persist) throws GenericException {

		WebMembershipTransactionContainer wmtc = getWebMembershipTransactionContainer(transactionHeaderId);
		TransactionGroup transGroup = generateMembershipTransactions(groupTransactionTypeCode, preferredCommenceDate, webClient, productCode, productTerm);
		generateHeaderTransactions(webClient, wmtc, transGroup, comments, true);

		return wmtc;
	}

	public void emailJoinMembershipAdvice(Integer transactionHeaderId, String recipientAddress) throws GenericException {
		System.out.println("1");
		ReportRequestBase rep;
		String reportKeyName = null;
		try {
			System.out.println("2");
			rep = new WebMembershipDocRequest(transactionHeaderId);
			System.out.println("3");
			OutputStream fStream = FileUtil.openStreamForWriting(rep.getDestinationFileName(), false);
			rep.setOutputStream(fStream);
			System.out.println("4");
			reportKeyName = ReportRegister.addReport(rep);
			rep.setReportKeyName(reportKeyName);
			rep.constructURL();
			rep.addParameter("URL", rep.getReportDataURL());
			System.out.println("URL=" + rep.getReportDataURL());
			System.out.println("file=" + rep.getDestinationFileName());

			new ReportGenerator().runReport(rep);

			Hashtable<String, String> files = new Hashtable<String, String>();
			files.put(WebMembershipHelper.getPDFFileName(transactionHeaderId.toString()), rep.getDestinationFileName());

			String bcc = ((SystemParameterVO) commonMgrLocal.getSystemParameter(SourceSystem.MEMBERSHIP.getAbbreviation(), "BCCWEBADVICE")).getValue();
			String fromAddress = ((SystemParameterVO) commonMgrLocal.getSystemParameter(SourceSystem.MEMBERSHIP.getAbbreviation(), "FROMWEBADVICE")).getValue();

			MailMessage message = new MailMessage();
			message.setRecipient(recipientAddress);
			message.setBcc(bcc);
			message.setSubject(SUBJECT_RACT_JOIN_MEMBERSHIP_ADVICE);
			message.setMessage(MESSAGE_RACT_JOIN_MEMBERSHIP_ADVICE);
			message.setFrom(fromAddress);
			message.setFiles(files);

			MailMgr mailMgr = CommonEJBHelper.getMailMgr();
			mailMgr.sendMail(message);

		} catch (Exception e) {
			e.printStackTrace();
			throw new GenericException(e);
		}

		ReportRegister.removeReport(reportKeyName);

	}

	public WebMembershipTransactionHeader getWebMembershipTransactionHeader(Integer transactionHeaderId) throws GenericException {
		WebMembershipTransactionHeader wmth = em.find(WebMembershipTransactionHeader.class, transactionHeaderId);
		return wmth;
	}

	public Collection<WebMembershipTransactionHeader> getWebMembershipTransactionHeaders() {
		Query txHeaderQuery = em.createQuery("select e from WebMembershipTransactionHeader e");
		List txHeaderList = txHeaderQuery.getResultList();
		return txHeaderList;
	}

	public Collection<WebMembershipTransactionHeader> getCompletedWebMembershipTransactionHeaders() {
		Query txHeaderQuery = em.createQuery("select e from WebMembershipTransactionHeader e where e.completedDate is not null and e.conversionDate is null and conversionStatus = ?1");
		txHeaderQuery.setParameter(1, WebMembershipTransactionHeader.STATUS_COVERED);
		List txHeaderList = txHeaderQuery.getResultList();
		return txHeaderList;
	}

	public Collection<WebMembershipTransaction> getConvertedWebMembershipTransactions(DateTime since) {
		Query txHeaderQuery = em.createQuery("select wt from WebMembershipTransaction wt, WebMembershipTransactionHeader wh where wh.transactionHeaderId = wt.webMembershipTransactionPK.transactionHeaderId and wh.conversionDate >= :conversionDate and wh.conversionStatus = :conversionStatus and wh.conversionReference is not null");
		txHeaderQuery.setParameter("conversionStatus", WebMembershipTransactionHeader.STATUS_CONVERTED);
		txHeaderQuery.setParameter("conversionDate", since);

		List<WebMembershipTransaction> txHeaderList = txHeaderQuery.getResultList();
		return txHeaderList;
	}

	public Collection<WebPayable> getUnreceiptedPayables(DateTime since) {
		Collection<WebMembershipTransaction> webTxns = getConvertedWebMembershipTransactions(since);
		Collection<WebPayable> resultList = new ArrayList<WebPayable>();
		MembershipVO memVO;
		PayableItemVO payableItemVO;

		for (WebMembershipTransaction txn : webTxns) {
			try {
				WebPayable result = new WebPayable();

				result.setSourceSystem(SourceSystem.MEMBERSHIP.getAbbreviation());
				result.setSourceReference(txn.getMembershipTransactionId());
				result.setAmountPaid(txn.getAmountPayable());
				result.setCreateDate(txn.getCreateDate());

				memVO = membershipMgrLocal.getMembership(txn.getMembershipId());
				if (memVO == null) {
					continue;
				}
				result.setClientNo(memVO.getClientNumber());

				payableItemVO = paymentMgrLocal.getPayableItemByClientAndSource(memVO.getClientNumber(), txn.getMembershipTransactionId());
				if (payableItemVO.getReceiptNo() != null && !payableItemVO.getReceiptNo().isEmpty()) {
					continue;
				}
				if (payableItemVO.getAmountOutstanding() <= 0) {
					continue;
				}
				result.setPayRefNo(payableItemVO.getPayableItemID());

				resultList.add(result);

			} catch (RemoteException e) {
				e.printStackTrace();

			} catch (PaymentException e) {
				e.printStackTrace();

			}
		}

		return resultList;
	}

	public WebMembershipTransaction getWebMembershipTransaction(Integer transactionId) throws GenericException {
		WebMembershipTransaction wmt = null;
		Query txQuery = em.createQuery("select e from WebMembershipTransaction e where e.webMembershipTransactionPK.transactionId = ?1");
		txQuery.setParameter(1, transactionId);
		List txList = txQuery.getResultList();
		Iterator txIt = txList.listIterator();
		while (txIt.hasNext()) {
			if (wmt == null) {
				wmt = (WebMembershipTransaction) txIt.next();
			} else {
				throw new GenericException("More than one transaction with id " + transactionId);
			}
		}
		return wmt;
	}

	public WebMembershipTransactionHeader getWebMembershipTransactionHeadersByClient(Integer webClientNo) throws GenericException {

		LogUtil.info(this.getClass(), "#--# getWebMembershipTransactionHeadersByClient");

		WebMembershipTransactionHeader wmh = null;
		Query txQuery = em.createQuery("select wh from WebMembershipTransaction wt, WebMembershipTransactionHeader wh where wh.transactionHeaderId = wt.webMembershipTransactionPK.transactionHeaderId and wt.webClientNo = ?1");
		LogUtil.info(this.getClass(), "#--# createQuery");
		txQuery.setParameter(1, webClientNo);

		LogUtil.info(this.getClass(), "#--# associate client");

		List txList = txQuery.getResultList();
		Iterator txIt = txList.listIterator();

		LogUtil.info(this.getClass(), "#--# get Results");

		while (txIt.hasNext()) {
			if (wmh == null) {
				LogUtil.info(this.getClass(), "#--# load wmh");
				wmh = (WebMembershipTransactionHeader) txIt.next();
			} else {
				LogUtil.info(this.getClass(), "#--# too many wmh");
				throw new GenericException("More than one transaction header with client " + webClientNo);
			}
		}

		LogUtil.info(this.getClass(), "#--# getWebMembershipTransactionHeadersByClient COMPLETE");

		return wmh;
	}

	public WebMembershipTransactionContainer getWebMembershipTransactionContainer(Integer transactionHeaderId) throws GenericException {
		WebMembershipTransactionContainer wmtc = new WebMembershipTransactionContainer();

		WebMembershipTransactionHeader wmth = getWebMembershipTransactionHeader(transactionHeaderId);
		wmtc.setWebMembershipTransactionHeader(wmth);

		Collection<WebMembershipTransaction> transactionList = getWebMembershipTransactionList(transactionHeaderId);
		wmtc.setTransactionList(transactionList);

		WebPayment webPayment = getWebMembershipPayment(transactionHeaderId.toString());
		wmtc.setWebPayment(webPayment);

		return wmtc;
	}

	/**
	 * Get an RACT personal membership.
	 * 
	 * @param clientNumber the unique RACT client number
	 * @return The RACT personal membership.
	 */
	public MembershipVO getPersonalMembership(Integer clientNumber) throws GenericException {
		MembershipVO membership = null;
		try {
			Collection<MembershipVO> membershipList = membershipMgrLocal.findMembershipByClientNumber(clientNumber);
			if (membershipList != null) {
				for (Iterator<MembershipVO> i = membershipList.iterator(); i.hasNext() && membership == null;) {
					membership = i.next();
					if (MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL.equals(membership.getMembershipTypeCode())) {
						return membership;
					} else {
						membership = null;
					}
				}
			}
		} catch (Exception e) {
			throw new GenericException(e);
		}
		return membership;
	}

	public MembershipCardVO getMembershipCard(String cardNumber) throws GenericException {
		MembershipCardVO membershipCard = null;
		try {
			membershipCard = membershipMgrLocal.getMembershipCardByCardNumber(cardNumber);
		} catch (RemoteException e) {
			throw new GenericException(e);
		}
		return membershipCard;
	}

	@EJB
	private ClientMgrLocal clientMgrLocal;

	public Integer getClientNumber(String membershipCardNumber) throws GenericException {
		Integer clientNumber = null;
		try {
			ClientVO client = clientMgrLocal.getClientByMembershipCardNumber(membershipCardNumber);
			if (client != null) {
				clientNumber = client.getClientNumber();
			}
		} catch (RemoteException e) {
			throw new GenericException(e);
		}
		return clientNumber;
	}

	public WebMembershipDiscount getWebMembershipDiscount(String discountTypeCode, DateTime transactionDate) {
		WebMembershipDiscount wmd = null;
		Query query = em.createQuery("select e from WebMembershipDiscount e where e.webMembershipDiscountPK.discountTypeCode = ?1 and ?2 between e.webMembershipDiscountPK.startDate and e.webMembershipDiscountPK.endDate ");
		query.setParameter(1, discountTypeCode);
		query.setParameter(2, transactionDate);
		List<WebMembershipDiscount> list = query.getResultList();
		if (list != null && list.size() > 0) {
			wmd = list.iterator().next();
		}
		return wmd;
	}

	public WebPayment getWebPayment(String paymentType, String transactionReference) throws GenericException {
		WebPaymentPK pk = new WebPaymentPK(paymentType, transactionReference);
		WebPayment wp = null;
		try {
			wp = em.find(WebPayment.class, pk);
		} catch (EncryptionOperationNotPossibleException ex) {
			throw new GenericException(ex);
		}
		return wp;
	}

	/**
	 * Generate membership transactions from web membership data. Uses the central membership fee table and data to generate transactions.
	 * 
	 * @param preferredCommenceDate
	 * @param webClient
	 * @param productCode
	 * @param comments
	 * @return A transaction group containing header details and a transaction list.
	 */
	private TransactionGroup generateMembershipTransactions(String groupTransactionTypeCode, DateTime preferredCommenceDate, WebMembershipClient webClient, String productCode, String productTerm) throws GenericException {

		if (!groupTransactionTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE)) {
			throw new GenericException("Only create transactions are currently supported.");
		}

		TransactionGroup transGroup = null;
		try {
			transGroup = new TransactionGroup(groupTransactionTypeCode, new com.ract.user.User());
			transGroup.setTransactionDate(new DateTime());

			// setup a dummy client - all clients are assumed to be new in this
			// case
			ClientVO client = new PersonVO();
			// temporary client number
			client.setClientNumber(new Integer(webClient.getWebClientNo()));
			// dummy street address to satisfy validation
			client.setResidentialAddress(new ResidentialAddressVO(null, null, -1));

			transGroup.addSelectedClient(client);
			transGroup.setContextClient(client);
			Interval membershipTerm = null;
			// String defaultMembershipTermText =
			// CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP,
			// SystemParameterVO.DEFAULT_MEMBERSHIP_TERM);
			membershipTerm = new Interval(productTerm);
			transGroup.setMembershipTerm(membershipTerm);

			// set membership type - personal
			MembershipTypeVO memTypeVO = MembershipEJBHelper.getMembershipRefMgr().getMembershipType(MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL);
			transGroup.setMembershipType(memTypeVO);
			transGroup.setMembershipProfile(MembershipProfileVO.PROFILE_CLIENT);
			transGroup.setPreferredCommenceDate(preferredCommenceDate);
			DateTime expiryDate = preferredCommenceDate.add(membershipTerm);
			transGroup.setExpiryDate(expiryDate);

			// TODO needed?
			// transGroup.setTransactionReason(reasonVO); //web reason?

			ProductVO product = MembershipEJBHelper.getMembershipRefMgr().getProduct(productCode);
			transGroup.setSelectedProduct(product);

			// create transactions and fees
			transGroup.createTransactionsForSelectedClients();
			transGroup.createTransactionFees();

			transGroup = addWebDiscount(transGroup);

			transGroup.calculateTransactionFee();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return transGroup;

	}

	public TransactionGroup addWebDiscount(TransactionGroup transGroup) throws GenericException {
		// apply discounts
		try {
			Collection distinctFeeTypeList = null;
			Iterator distinctFeeTypeListIterator = null;
			ArrayList selectedDiscountList = new ArrayList();
			ArrayList unselectedDiscountList = new ArrayList();
			Iterator selectedDiscountListIterator = null;
			Iterator unselectedDiscountListIterator = null;
			DiscountTypeVO discTypeVO = null;
			String feeTypeCode = null;
			FeeTypeVO feeTypeVO = null;
			Collection allowedDiscountList = null;
			Iterator allowedDiscountListIterator = null;

			MembershipTransactionVO membershipTxVO = transGroup.getMembershipTransactionForPA();
			distinctFeeTypeList = membershipTxVO.getDistinctFeeTypeList();

			MembershipRefMgr refMgr = MembershipEJBHelper.getMembershipRefMgr();

			// for each membership transaction fee
			if (distinctFeeTypeList != null) {
				distinctFeeTypeListIterator = distinctFeeTypeList.iterator();
				while (distinctFeeTypeListIterator.hasNext()) {
					feeTypeCode = (String) distinctFeeTypeListIterator.next();
					// get the current discounts for this fee code

					feeTypeVO = refMgr.getFeeType(feeTypeCode);
					LogUtil.log(this.getClass(), "feeTypeVO=" + feeTypeVO);

					allowedDiscountList = refMgr.getAllowableDiscountList(feeTypeCode, new DateTime());
					if (allowedDiscountList != null) {
						allowedDiscountListIterator = allowedDiscountList.iterator();
						while (allowedDiscountListIterator.hasNext()) {
							discTypeVO = (DiscountTypeVO) allowedDiscountListIterator.next();

							// lookup active web membership discount
							WebMembershipDiscount wmd = getWebMembershipDiscount(discTypeVO.getDiscountTypeCode(), transGroup.getTransactionDate());
							if (wmd != null) {
								// apply it
								membershipTxVO.addTransactionFeeDiscount(feeTypeVO.getFeeTypeCode(), discTypeVO);
							}
						}
					}
				}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
			throw new GenericException(e.getMessage());
		}
		return transGroup;
	}

}
