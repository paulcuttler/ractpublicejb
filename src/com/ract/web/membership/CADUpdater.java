package com.ract.web.membership;

import com.ract.common.ServiceLocator;

public class CADUpdater
{

	public static String RESET = "reset";	
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		boolean reset = false;
		try
		{
			if (args.length == 1)
			{
				reset = args[0].equals(RESET);
			}
			WebMembershipMgrRemote webMembershipMgr = (WebMembershipMgrRemote) ServiceLocator.getInstance().getObject("WebMembershipMgr/remote");
			if (reset)
			{
				webMembershipMgr.resetTemporaryRoadserviceRecords();
			}
			else
			{
			  webMembershipMgr.createTemporaryRoadserviceRecords();
			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

}
