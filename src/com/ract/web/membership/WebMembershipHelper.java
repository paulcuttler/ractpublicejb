package com.ract.web.membership;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ract.common.GenericException;
import com.ract.common.reporting.ReportGenerator;
import com.ract.common.reporting.ReportRegister;
import com.ract.common.reporting.ReportRequestBase;

public class WebMembershipHelper
{

	/**
	 * Not part of the EJB layer as outputstreams are not serializable.
	 * @param transactionHeaderId
	 * @param request
	 * @param response
	 * @throws GenericException
	 */
	public void displayJoinMembershipAdvice(Integer transactionHeaderId, HttpServletRequest request, HttpServletResponse response)throws GenericException
	{
		ReportRequestBase rep;
		String reportKeyName = null;
		try
		{
			ServletOutputStream sos = response.getOutputStream();		
			
			//TODO call next part from EJB
			
			rep = new WebMembershipDocRequest(transactionHeaderId);

			reportKeyName = ReportRegister.addReport(rep);
			rep.setReportKeyName(reportKeyName);
			rep.constructURL();
			rep.addParameter("URL", rep.getReportDataURL());
			System.out.println("URL="+rep.getReportDataURL());
			System.out.println("file="+rep.getDestinationFileName());					 

			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "Inline;filename="+getPDFFileName(transactionHeaderId.toString()));
      rep.setOutputStream(sos);
      
     	new ReportGenerator().runReport(rep);

  		sos.flush();
  		sos.close();      
		}
		catch (Exception e)
		{
			 throw new GenericException(e);
		}
		finally
		{
  		ReportRegister.removeReport(reportKeyName);
		}
	}		

	public static String getPDFFileName(String transactionHeaderId)
	{
		String fileName = "joinmembershipadvice"+transactionHeaderId+"."+ReportRequestBase.REPORT_DELIVERY_FORMAT_PDF;
		return fileName;
	}
	
	
}
