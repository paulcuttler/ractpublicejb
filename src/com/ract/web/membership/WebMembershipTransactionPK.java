package com.ract.web.membership;

import java.io.Serializable;
import javax.persistence.Embeddable;
import com.ract.web.payment.WebPaymentPK;

/**
 * The primary key class for a WebMembershipTransaction.
 * @author hollidayj
 */
@Embeddable
public class WebMembershipTransactionPK implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8531262966454258579L;
	private Integer transactionHeaderId;
	private Integer transactionId;
	
	public Integer getTransactionHeaderId()
	{
		return transactionHeaderId;
	}

	public void setTransactionHeaderId(Integer transactionHeaderId)
	{
		this.transactionHeaderId = transactionHeaderId;
	}

	public Integer getTransactionId()
	{
		return transactionId;
	}

	public void setTransactionId(Integer transactionId)
	{
		this.transactionId = transactionId;
	}

	public WebMembershipTransactionPK()
	{
		//default
	}
	
  public boolean equals(Object obj)
  {
    if(obj instanceof WebPaymentPK)
    {
    	WebMembershipTransactionPK that = (WebMembershipTransactionPK)obj;
      return this.transactionHeaderId.equals(that.transactionHeaderId) && this.transactionId.equals(that.transactionId);
    }
    return false;
  }

  public int hashCode()
  {
    return this.transactionHeaderId.hashCode() + this.transactionId.hashCode();
  }

  public String toString()
  {
    return this.transactionHeaderId + "/" + transactionId;
  }	
	
}
