package com.ract.web.membership;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.ract.common.ServiceLocator;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.web.client.CustomerMgrRemote;
import com.ract.web.client.WebClient;

/**
 * An individual web membership transaction. Represents a single membership transaction per record.
 * @author hollidayj
 */

@Entity
public class WebMembershipTransaction implements Serializable
{

	public DateTime getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(DateTime createDate)
	{
		this.createDate = createDate;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((webMembershipTransactionPK == null) ? 0
						: webMembershipTransactionPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof WebMembershipTransaction))
			return false;
		WebMembershipTransaction other = (WebMembershipTransaction) obj;
		if (webMembershipTransactionPK == null)
		{
			if (other.webMembershipTransactionPK != null)
				return false;
		}
		else
			if (!webMembershipTransactionPK.equals(other.webMembershipTransactionPK))
				return false;
		return true;
	}

	public Integer getMembershipId()
	{
		return membershipId;
	}

	public void setMembershipId(Integer membershipId)
	{
		this.membershipId = membershipId;
	}

	public Integer getMembershipTransactionId()
	{
		return membershipTransactionId;
	}

	public void setMembershipTransactionId(Integer membershipTransactionId)
	{
		this.membershipTransactionId = membershipTransactionId;
	}

	public BigDecimal getJoinFee()
	{
		return joinFee;
	}

	public void setJoinFee(BigDecimal joinFee)
	{
		this.joinFee = joinFee;
	}

	public void setWebClient(WebClient webClient)
	{
		this.webClient = webClient;
	}

	@Transient
	private WebClient webClient = null;
	
	public WebClient getWebClient()
	{
    if (webClient == null &&
    		this.getWebClientNo() != null)
    {
    	try
			{
      	CustomerMgrRemote customerMgr = (CustomerMgrRemote)ServiceLocator.getInstance().getObject("CustomerMgr/remote");
    		webClient = customerMgr.getWebClient(getWebClientNo());
			}
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }
		return webClient;
	}

	public boolean isPrimeAddressee()
	{
		return primeAddressee;
	}

	public void setPrimeAddressee(boolean primeAddressee)
	{
		this.primeAddressee = primeAddressee;
	}

	public static final String SEQUENCE_WEB_MEMBERSHIP_TRANSACTION = "WEB_MEMBERSHIP_TRANSACTION";
	
	@Id
  private WebMembershipTransactionPK webMembershipTransactionPK;
	
	private Integer webClientNo;

	private String transactionTypeCode;
	
	private String membershipTerm;
	
	// TODO default 48 hours in advance of the transaction date?
	private DateTime startDate; //commence date
	
	private DateTime endDate; //expiry date
	
	private String productCode;
	
	private DateTime joinDate;
	
	private String reasonType;
	
	private String reasonCode;
	
	private String membershipType;
	
	private String profileCode;

	private BigDecimal amountPayable;

	private BigDecimal netTransactionFee;
	
	private BigDecimal discountTotal;

	private BigDecimal adjustmentTotal;
	
	private BigDecimal taxTotal;	
	
	public BigDecimal getGrossTransactionFee()
	{
		return grossTransactionFee;
	}

	public void setGrossTransactionFee(BigDecimal grossTransactionFee)
	{
		this.grossTransactionFee = grossTransactionFee;
	}

	private BigDecimal grossTransactionFee;
	
	private boolean primeAddressee;
	
	private Integer membershipId;
	
	private Integer membershipTransactionId;
		
	public WebMembershipTransactionPK getWebMembershipTransactionPK()
	{
		return webMembershipTransactionPK;
	}

	private BigDecimal joinFee;
	
	public void setWebMembershipTransactionPK(
			WebMembershipTransactionPK webMembershipTransactionPK)
	{
		this.webMembershipTransactionPK = webMembershipTransactionPK;
	}

	public BigDecimal getAmountPayable()
	{
		return amountPayable;
	}

	private DateTime cadCreated;
	
	public DateTime getCadCreated()
	{
		return cadCreated;
	}

	public void setCadCreated(DateTime cadCreated)
	{
		this.cadCreated = cadCreated;
	}

	public void setAmountPayable(BigDecimal amountPayable)
	{
		this.amountPayable = amountPayable;
	}

	public BigDecimal getNetTransactionFee()
	{
		return netTransactionFee;
	}

	public void setNetTransactionFee(BigDecimal netTransactionFee)
	{
		this.netTransactionFee = netTransactionFee;
	}

	public BigDecimal getDiscountTotal()
	{
		return discountTotal;
	}

	public void setDiscountTotal(BigDecimal discountTotal)
	{
		this.discountTotal = discountTotal;
	}

	public BigDecimal getAdjustmentTotal()
	{
		return adjustmentTotal;
	}

	private DateTime createDate;
	
	public void setAdjustmentTotal(BigDecimal adjustmentTotal)
	{
		this.adjustmentTotal = adjustmentTotal;
	}

	public BigDecimal getTaxTotal()
	{
		return taxTotal;
	}

	public void setTaxTotal(BigDecimal taxTotal)
	{
		this.taxTotal = taxTotal;
	}

	public Integer getWebClientNo()
	{
		return webClientNo;
	}

	public void setWebClientNo(Integer webClientNo)
	{
		this.webClientNo = webClientNo;
	}
	


	public String getMembershipTerm()
	{
		return membershipTerm;
	}

	public void setMembershipTerm(String membershipTerm)
	{
		this.membershipTerm = membershipTerm;
	}

	public DateTime getStartDate()
	{
		return startDate;
	}

	public void setStartDate(DateTime startDate)
	{
		this.startDate = startDate;
	}

	public DateTime getEndDate()
	{
		return endDate;
	}

	public void setEndDate(DateTime endDate)
	{
		this.endDate = endDate;
	}

	public String getProductCode()
	{
		return productCode;
	}

	public void setProductCode(String productCode)
	{
		this.productCode = productCode;
	}

	public DateTime getJoinDate()
	{
		return joinDate;
	}

	public void setJoinDate(DateTime joinDate)
	{
		this.joinDate = joinDate;
	}

	public String getMembershipType()
	{
		return membershipType;
	}

	public void setMembershipType(String membershipType)
	{
		this.membershipType = membershipType;
	}

	public String getProfileCode()
	{
		return profileCode;
	}

	public void setProfileCode(String profileCode)
	{
		this.profileCode = profileCode;
	}

	public String getTransactionTypeCode()
	{
		return transactionTypeCode;
	}

	public void setTransactionTypeCode(String transactionTypeCode)
	{
		this.transactionTypeCode = transactionTypeCode;
	}

  public final static String PREFIX_TEMPORARY_MEMBERSHIP_NUMBER = "T";
	
	public String getInterimMembershipNo()
	{
		DecimalFormat format = new DecimalFormat(PREFIX_TEMPORARY_MEMBERSHIP_NUMBER+"0000000");
		return format.format(this.getWebMembershipTransactionPK().getTransactionId());
	}	
	
	public String toString()
	{
		StringBuffer desc = new StringBuffer();
		desc.append(webMembershipTransactionPK);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(transactionTypeCode);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(startDate);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(endDate);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(profileCode);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(productCode);		
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(webClientNo);		
		desc.append(FileUtil.SEPARATOR_COMMA);		
		desc.append(membershipTerm);
		desc.append(FileUtil.SEPARATOR_COMMA);		
		desc.append(membershipType);
		desc.append(FileUtil.SEPARATOR_COMMA);		
		desc.append(joinDate);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(amountPayable);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(adjustmentTotal);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(discountTotal);		
		desc.append(FileUtil.SEPARATOR_COMMA);		
		desc.append(netTransactionFee);		
		desc.append(FileUtil.SEPARATOR_COMMA);		
		desc.append(taxTotal);		
		return desc.toString();
	}

	public String getReasonType()
	{
		return reasonType;
	}

	public void setReasonType(String reasonType)
	{
		this.reasonType = reasonType;
	}

	public String getReasonCode()
	{
		return reasonCode;
	}

	public void setReasonCode(String reasonCode)
	{
		this.reasonCode = reasonCode;
	}	
	
}
