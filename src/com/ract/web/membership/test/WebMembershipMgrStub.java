package com.ract.web.membership.test;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.ract.common.GenericException;
import com.ract.membership.MembershipCardVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.TransactionGroup;
import com.ract.util.DateTime;
import com.ract.web.common.WebPayable;
import com.ract.web.membership.WebMembershipClient;
import com.ract.web.membership.WebMembershipMgrRemote;
import com.ract.web.membership.WebMembershipTransaction;
import com.ract.web.membership.WebMembershipTransactionContainer;
import com.ract.web.membership.WebMembershipTransactionHeader;
import com.ract.web.membership.WebMembershipTransactionPK;
import com.ract.web.payment.WebMembershipPayment;
import com.ract.web.payment.WebPayment;

public class WebMembershipMgrStub implements WebMembershipMgrRemote
{


	@Override
	public MembershipCardVO getMembershipCard(String cardNumber)
			throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void createTemporaryRoadserviceRecords() throws GenericException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public Integer getClientNumber(String membershipCardNumber)
			throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void resetTemporaryRoadserviceRecords() throws GenericException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public WebMembershipTransactionContainer updateWebMembershipJoinTransactionContainer(
			Integer transactionHeaderId, DateTime preferredCommenceDate,
			WebMembershipClient webClient, String productCode, String productTerm, String comments,
			boolean persist) throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WebPayment createWebPayment(WebPayment webPayment)
			throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void associateWebClient(WebMembershipTransactionContainer wmtc,
			WebMembershipClient client) throws GenericException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public WebMembershipTransactionContainer completeWebMembershipTransactions(
			WebMembershipTransactionContainer wmtc) throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String allocateBranch(String sourceSystem) throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WebMembershipTransactionHeader getWebMembershipTransactionHeadersByClient(
			Integer webClientNo) throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WebMembershipTransaction updateWebMembershipTransaction(
			WebMembershipTransaction webMembershipTransaction)
			throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WebMembershipTransactionHeader updateWebMembershipTransactionHeader(
			WebMembershipTransactionHeader webMembershipTransactionHeader)
			throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<WebMembershipTransactionHeader> getCompletedWebMembershipTransactionHeaders()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WebMembershipTransactionContainer createWebMembershipRejoinTransaction(
			Integer membershipId) throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WebMembershipTransactionContainer createWebMembershipUpgradeTransaction(
			Integer membershipId, String upgradedProductCode) throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MembershipCardVO requestMembershipCard(Integer membershipId)
			throws GenericException
	{
		// TODO Auto-generated method stub
		return null; 
	}

	@Override
	public void emailJoinMembershipAdvice(Integer transactionHeaderId,
			String recipientAddress) throws GenericException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public WebMembershipClient updateMembershipClient(
			WebMembershipClient webMembershipClient) throws GenericException
	{
		// TODO Auto-generated method stub
		return null; 
	}

	@Override
	public DateTime getDefaultCommenceDate()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WebMembershipPayment getWebMembershipPayment(
			String transactionReference)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<WebMembershipTransactionHeader> getWebMembershipTransactionHeaders()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WebMembershipTransaction getWebMembershipTransaction(
			Integer transactionId) throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WebMembershipTransactionContainer getWebMembershipTransactionContainer(
			Integer transactionHeaderId) throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WebMembershipTransactionHeader getWebMembershipTransactionHeader(
			Integer transactionHeaderId) throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<WebMembershipTransaction> getWebMembershipTransactionList(
			Integer transactionHeaderId) throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<String> getMembershipPaymentTypes() throws GenericException
	{
		Collection<String> paymentTypes = new ArrayList<String>();
		paymentTypes.add("VISA");
		paymentTypes.add("MASTERCARD");		
		return paymentTypes;
	}

	@Override
	public Collection<String> getMembershipProducts() throws GenericException
	{
		Collection<String> products = new ArrayList<String>();
		products.add("Advantage");
		products.add("Ultimate");
		return products;
	}

	@Override
	public WebMembershipClient getWebMembershipClient(Integer webClientNo) throws GenericException
	{
		WebMembershipClient wmc = new WebMembershipClient();
		wmc.setWebClientNo(webClientNo);
		return wmc;
	}

	@Override
	public WebMembershipClient createMembershipClient(WebMembershipClient memClient)
	 throws GenericException
	{
		
    return memClient;
	}

	@Override
	public WebMembershipTransactionContainer createWebMembershipJoinTransaction(
			DateTime preferredCommenceDate, WebMembershipClient webClient,
			String productCode, String productTerm, String comments, boolean persist) throws GenericException
	{
		final int X = 1;
		final int Y = 1;

		WebMembershipTransactionContainer wmtc = new WebMembershipTransactionContainer();
		WebMembershipTransaction wmt = new WebMembershipTransaction();		
		WebMembershipTransactionHeader wmth = new WebMembershipTransactionHeader();
		wmth.setCreateDate(new DateTime());
		wmth.setComments("Test stub.");
		wmth.setAmountPayable(new BigDecimal(100));
		wmth.setTransactionGroupTypeCode("Test group transaction type");
		wmth.setTransactionHeaderId(new Integer(X));
		
		WebMembershipTransactionPK pk = new WebMembershipTransactionPK();
		pk.setTransactionHeaderId(new Integer(X));
		pk.setTransactionId(new Integer(Y));
		wmt.setWebMembershipTransactionPK(pk);
		wmt.setTransactionTypeCode("Test transactiont type");
		wmt.setProfileCode("King");
		wmt.setStartDate(new DateTime());
		wmtc.addTransaction(wmt);
		return wmtc;
	}

	@Override
	public TransactionGroup addWebDiscount(TransactionGroup transGroup)
			throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MembershipVO getPersonalMembership(Integer clientNumber)
			throws GenericException
	{
		MembershipVO membership =  new MembershipVO();
		try
		{
			membership.setClientNumber(clientNumber);
		}
		catch (RemoteException e)
		{
			throw new GenericException(e);
		}
		return membership;
	}

	@Override
	public Collection<WebMembershipTransaction> getConvertedWebMembershipTransactions(
			DateTime since) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<WebPayable> getUnreceiptedPayables(DateTime since) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WebMembershipPayment updateWebMembershipPayment(
			WebMembershipPayment webMembershipPayment) throws GenericException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<String, String> getLifestyleZeroDates()
		throws GenericException, RemoteException
	{
	    // TODO Auto-generated method stub
	    return null;
	}

	
}
