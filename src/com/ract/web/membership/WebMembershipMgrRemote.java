package com.ract.web.membership;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashMap;

import javax.ejb.Remote;

import com.ract.common.GenericException;
import com.ract.membership.MembershipCardVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.TransactionGroup;
import com.ract.util.DateTime;
import com.ract.web.common.WebPayable;
import com.ract.web.payment.WebMembershipPayment;
import com.ract.web.payment.WebPayment;

@Remote
public interface WebMembershipMgrRemote
{
	
	public MembershipCardVO getMembershipCard(String cardNumber) throws GenericException;	
	
	public WebMembershipTransactionContainer createWebMembershipRejoinTransaction(Integer membershipId) throws GenericException;	
	
	public WebMembershipTransactionContainer createWebMembershipUpgradeTransaction(Integer membershipId, String upgradedProductCode) throws GenericException;

	public MembershipCardVO requestMembershipCard(Integer membershipId) throws GenericException;	
	
	public WebMembershipClient createMembershipClient(WebMembershipClient rsClient) throws GenericException;
  
	public Integer getClientNumber(String membershipCardNumber) throws GenericException;	
	
	public void resetTemporaryRoadserviceRecords() throws GenericException;	
	
	public TransactionGroup addWebDiscount(TransactionGroup transGroup)
	throws GenericException;	
	
	public void createTemporaryRoadserviceRecords() throws GenericException;
	
	public HashMap<String, String> getLifestyleZeroDates() throws GenericException, RemoteException;
	
	public WebMembershipClient updateMembershipClient(WebMembershipClient webMembershipClient)throws GenericException;	
	
  public String allocateBranch(String sourceSystem) throws GenericException;	
	
  public WebMembershipTransactionHeader getWebMembershipTransactionHeadersByClient(Integer webClientNo) throws GenericException;
	
	public WebMembershipTransactionContainer createWebMembershipJoinTransaction(DateTime preferredCommenceDate, WebMembershipClient webClient, String productCode, String productTerm, String comments, boolean persist) throws GenericException;
	
	public WebMembershipTransaction updateWebMembershipTransaction(WebMembershipTransaction webMembershipTransaction)throws GenericException;	
	
	public WebMembershipTransactionHeader updateWebMembershipTransactionHeader(WebMembershipTransactionHeader webMembershipTransactionHeader)throws GenericException;	
	
	public WebMembershipPayment updateWebMembershipPayment(WebMembershipPayment webMembershipPayment)throws GenericException;
	
  public WebMembershipClient getWebMembershipClient(Integer webClientNo) throws GenericException;  	  
  
  public WebMembershipTransactionContainer updateWebMembershipJoinTransactionContainer(Integer transactionHeaderId, DateTime preferredCommenceDate, WebMembershipClient webClient, String productCode, String productTerm, String comments, boolean persist)
  throws GenericException;  
  
	public MembershipVO getPersonalMembership(Integer clientNumber) throws GenericException;

  public Collection<WebMembershipTransactionHeader> getCompletedWebMembershipTransactionHeaders();	
	
  public Collection<WebMembershipTransaction> getConvertedWebMembershipTransactions(DateTime since);
    
  public Collection<WebPayable> getUnreceiptedPayables(DateTime since);
  
	public WebPayment createWebPayment(WebPayment webPayment) throws GenericException;
	
	public WebMembershipTransactionContainer completeWebMembershipTransactions(WebMembershipTransactionContainer wmtc) throws GenericException;

	public void associateWebClient(WebMembershipTransactionContainer wmtc, WebMembershipClient client) throws GenericException;

	public Collection<String> getMembershipPaymentTypes() throws GenericException;	

	public Collection<String> getMembershipProducts() throws GenericException;	
	
	public WebMembershipPayment getWebMembershipPayment(String transactionReference) throws GenericException;	
	
  public WebMembershipTransaction getWebMembershipTransaction(Integer transactionId) throws GenericException;
  
	public WebMembershipTransactionContainer getWebMembershipTransactionContainer(Integer transactionHeaderId) throws GenericException;
	
  public WebMembershipTransactionHeader getWebMembershipTransactionHeader(Integer transactionHeaderId) throws GenericException;
  
	public Collection<WebMembershipTransaction> getWebMembershipTransactionList(Integer transactionHeaderId) throws GenericException;  
		
	//TODO get join reason list
	
	public void emailJoinMembershipAdvice(Integer transactionHeaderId, String recipientAddress)throws GenericException;
	
  public Collection<WebMembershipTransactionHeader> getWebMembershipTransactionHeaders();
  
  public DateTime getDefaultCommenceDate();  
}
