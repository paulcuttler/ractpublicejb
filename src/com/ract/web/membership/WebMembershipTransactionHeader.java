package com.ract.web.membership;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.ract.util.DateTime;
import com.ract.util.FileUtil;

/**
 * Transaction header describing the transaction at a group level.
 * @author hollidayj
 */
@Entity
public class WebMembershipTransactionHeader implements Serializable
{

	public String getConversionUserId()
	{
		return conversionUserId;
	}

	public void setConversionUserId(String conversionUserId)
	{
		this.conversionUserId = conversionUserId;
	}

	public String getConversionComments()
	{
		return conversionComments;
	}

	public void setConversionComments(String conversionComments)
	{
		this.conversionComments = conversionComments;
	}

	public String getConversionReference()
	{
		return conversionReference;
	}

	public void setConversionReference(String conversionReference)
	{
		this.conversionReference = conversionReference;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((transactionHeaderId == null) ? 0 : transactionHeaderId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof WebMembershipTransactionHeader))
			return false;
		WebMembershipTransactionHeader other = (WebMembershipTransactionHeader) obj;
		if (transactionHeaderId == null)
		{
			if (other.transactionHeaderId != null)
				return false;
		}
		else
			if (!transactionHeaderId.equals(other.transactionHeaderId))
				return false;
		return true;
	}

	public DateTime getLastUpdate()
	{
		return lastUpdate;
	}

	public void setLastUpdate(DateTime lastUpdate)
	{
		this.lastUpdate = lastUpdate;
	}

	public static String STATUS_CONVERTED = "Converted";
	
	public static String STATUS_COVERED = "Covered";
	
	public static String STATUS_MANUAL = "Manually Converted";

	public static String STATUS_NEVER_CONVERT = "Never Convert";
	
	private DateTime lastUpdate;
	
	public String getConversionStatus()
	{
		return conversionStatus;
	}

	public void setConversionStatus(String conversionStatus)
	{
		this.conversionStatus = conversionStatus;
	}

	public boolean isAllocated()
	{
		return (this.salesBranch != null);
	}
	
	public DateTime getConversionDate()
	{
		return conversionDate;
	}

	public void setConversionDate(DateTime conversionDate)
	{
		this.conversionDate = conversionDate;
	}

	public DateTime getCompletedDate()
	{
		return completedDate;
	}

	public void setCompletedDate(DateTime completedDate)
	{
		this.completedDate = completedDate;
	}

	public String getSalesBranch()
	{
		return salesBranch;
	}

	public void setSalesBranch(String salesBranch)
	{
		this.salesBranch = salesBranch;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getTransactionXML()
	{
		return transactionXML;
	}

	public void setTransactionXML(String transactionXML)
	{
		this.transactionXML = transactionXML;
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -805538179250034054L;

	public static final String SEQUENCE_WEB_MEMBERSHIP_TRANSACTION_HEADER = "WEB_MEMBERSHIP_TRANSACTION_HEADER";
	
	@Id
	private Integer transactionHeaderId;
	
	private DateTime createDate;	
	
	private BigDecimal amountPayable;

	private BigDecimal netTransactionFee;
	
	private BigDecimal discountTotal;

	private BigDecimal adjustmentTotal;
	
	private BigDecimal grossTransactionFee;	
	
	public BigDecimal getGrossTransactionFee()
	{
		return grossTransactionFee;
	}

	public void setGrossTransactionFee(BigDecimal grossTransactionFee)
	{
		this.grossTransactionFee = grossTransactionFee;
	}

	private BigDecimal taxTotal;
	
	private String comments;
	
	private String transactionGroupTypeCode;
		
	private String salesBranch;
	
	private String transactionXML;	

	
  private DateTime completedDate;
	
  private String conversionStatus;
  
  private DateTime conversionDate;
  
  private String conversionComments;
  
  private String conversionReference;
  
	public String getTransactionGroupTypeCode()
	{
		return transactionGroupTypeCode;
	}

	private String conversionUserId;
	
	public void setTransactionGroupTypeCode(String transactionGroupTypeCode)
	{
		this.transactionGroupTypeCode = transactionGroupTypeCode;
	}

	public Integer getTransactionHeaderId()
	{
		return transactionHeaderId;
	}

	public void setTransactionHeaderId(Integer transactionHeaderId)
	{
		this.transactionHeaderId = transactionHeaderId;
	}


	public DateTime getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(DateTime createDate)
	{
		this.createDate = createDate;
	}

	public BigDecimal getAmountPayable()
	{
		return amountPayable;
	}

	public void setAmountPayable(BigDecimal amountPayable)
	{
		this.amountPayable = amountPayable;
	}

	public BigDecimal getNetTransactionFee()
	{
		return netTransactionFee;
	}

	public void setNetTransactionFee(BigDecimal netTransactionFee)
	{
		this.netTransactionFee = netTransactionFee;
	}

	public BigDecimal getDiscountTotal()
	{
		return discountTotal;
	}

	public void setDiscountTotal(BigDecimal discountTotal)
	{
		this.discountTotal = discountTotal;
	}

	public BigDecimal getAdjustmentTotal()
	{
		return adjustmentTotal;
	}

	public void setAdjustmentTotal(BigDecimal adjustmentTotal)
	{
		this.adjustmentTotal = adjustmentTotal;
	}

	public BigDecimal getTaxTotal()
	{
		return taxTotal;
	}

	public void setTaxTotal(BigDecimal taxTotal)
	{
		this.taxTotal = taxTotal;
	}

	public String getComments()
	{
		return comments;
	}

	public void setComments(String comments)
	{
		this.comments = comments;
	}
	
	public String toString()
	{
		StringBuffer desc = new StringBuffer();
		desc.append(transactionHeaderId);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(transactionGroupTypeCode);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(comments);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(amountPayable);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(adjustmentTotal);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(discountTotal);				
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(userName);		
		desc.append(FileUtil.SEPARATOR_COMMA);			
		desc.append(createDate);		
		desc.append(FileUtil.SEPARATOR_COMMA);		
		desc.append(netTransactionFee);		
		desc.append(FileUtil.SEPARATOR_COMMA);		
		desc.append(taxTotal);
		return desc.toString();
	}
	
	private String userName;
	
}
