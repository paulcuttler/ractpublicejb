package com.ract.web.membership;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.ract.common.GenericException;
import com.ract.web.payment.WebPayment;

/**
 * A container storing a transaction header for summarising the transaction
 * details and detail records representing individual client transactions.
 * 
 * @author hollidayj
 */
public class WebMembershipTransactionContainer implements Serializable
{
//    private boolean isLifeStyleZero;
 
    public WebPayment getWebPayment()
    {
	return webPayment;
    }

    public void setWebPayment(WebPayment webPayment)
    {
	this.webPayment = webPayment;
    }

    private static final long serialVersionUID = 369922511783639045L;

    private WebMembershipTransactionHeader webMembershipTransactionHeader;

    private Collection<WebMembershipTransaction> transactionList = new ArrayList<WebMembershipTransaction>();

    public WebMembershipTransactionHeader getWebMembershipTransactionHeader()
    {
	return webMembershipTransactionHeader;
    }

    private WebPayment webPayment;

    public void setWebMembershipTransactionHeader(
	    WebMembershipTransactionHeader webMembershipTransactionHeader)
    {
	this.webMembershipTransactionHeader = webMembershipTransactionHeader;
    }

    public Collection<WebMembershipTransaction> getTransactionList()
    {
	return transactionList;
    }

    public void setTransactionList(
	    Collection<WebMembershipTransaction> transactionList)
    {
	this.transactionList = transactionList;
    }

    public void addTransaction(WebMembershipTransaction transaction)
	    throws GenericException
    {
	if (transactionList.size() > 1)
	{
	    throw new GenericException(
		    "There should only be one transaction created for the transaction group.");
	}
	transactionList.add(transaction);
    }

    public String toString()
    {
	StringBuffer desc = new StringBuffer();
	desc.append(this.webMembershipTransactionHeader);
	desc.append(this.transactionList);
	return desc.toString();
    }

    public WebMembershipTransaction getPrimeAddressWebMembershipTransaction()
	    throws GenericException
    {
	WebMembershipTransaction wmt = null;
	WebMembershipTransaction pawmt = null;
	for (Iterator i = this.transactionList.iterator(); i.hasNext();)
	{
	    if (pawmt == null)
	    {
		wmt = (WebMembershipTransaction) i.next();
		if (wmt.isPrimeAddressee())
		{
		    pawmt = wmt;
		}
	    }
	    else
	    {
		throw new GenericException(
			"More than one prime addressee has been specified for the transaction.");
	    }
	}
	return pawmt;
    }

//    /**
//     * @return the isLifeStyleZero
//     */
//    public boolean isLifeStyleZero()
//    {
//        return isLifeStyleZero;
//    }
//
//    /**
//     * @param isLifeStyleZero the isLifeStyleZero to set
//     */
//    public void setLifeStyleZero(boolean isLifeStyleZero)
//    {
//        this.isLifeStyleZero = isLifeStyleZero;
//    }

}
