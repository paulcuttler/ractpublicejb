package com.ract.web.membership;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import com.ract.web.client.WebClient;

/**
 * Membership version of a web client.
 * @author hollidayj
 */

@Entity
@DiscriminatorValue("Membership")
public class WebMembershipClient extends WebClient
{
	
	/**
	 * test
	 */
	private static final long serialVersionUID = -5482913220125336341L;

  public WebMembershipClient()
  {
  	setClientType(WebClient.CLIENT_TYPE_MEMBERSHIP);
  	this.setWebClientNo(new Integer(-1));
  }
	
}
