package com.ract.web.common;

import java.io.Serializable;
import java.math.BigDecimal;

import com.ract.util.DateTime;

public class WebPayable implements Serializable {

	private Integer payRefNo;
	private Integer clientNo;
	private Integer sourceReference;
	private BigDecimal amountPaid;
	private String sourceSystem;
	private DateTime createDate;
	
	public WebPayable() {}
		
	public Integer getPayRefNo() {
		return payRefNo;
	}
	
	public void setPayRefNo(Integer payRefNo) {
		this.payRefNo = payRefNo;
	}
	
	public Integer getClientNo() {
		return clientNo;
	}
	
	public void setClientNo(Integer clientNo) {
		this.clientNo = clientNo;
	}
	
	public Integer getSourceReference() {
		return sourceReference;
	}
	
	public void setSourceReference(Integer sourceReference) {
		this.sourceReference = sourceReference;
	}
	
	public BigDecimal getAmountPaid() {
		return amountPaid;
	}
	
	public void setAmountPaid(BigDecimal amountPaid) {
		this.amountPaid = amountPaid;
	}

	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public DateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(DateTime createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "WebPayable [payRefNo=" + payRefNo + ", clientNo=" + clientNo
				+ ", sourceReference=" + sourceReference + ", amountPaid=" + amountPaid
				+ ", sourceSystem=" + sourceSystem + ", createDate=" + createDate + "]";
	}
}
