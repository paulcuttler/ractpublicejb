package com.ract.web.common;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.util.*;
import java.util.Calendar;

import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileFilter;

import au.com.bytecode.opencsv.CSVReader;

import com.ract.common.CommonConstants;
import com.ract.common.CommonEJBHelper;
import com.ract.common.ServiceLocator;
import com.ract.common.SystemParameterVO;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.FileUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.web.insurance.WebInsMgrRemote;
import com.ract.web.membership.WebMembershipMgrRemote;

import org.quartz.*;

public class PaymentFileGeneratorJob implements Job {
  private Hashtable<String,WebPayable> pList = null;
  private static String FILE_PREFIX = "webTxn";
  private boolean isDev = false;
    
  /**
   * Requires general table entries for EXR/WEBTXN - the 
   */
  @Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException 
	{
	   WebMembershipMgrRemote webMembershipMgr;
	   WebInsMgrRemote webInsMgr;
	   int days = 7;
	   pList = new Hashtable<String,WebPayable>();
	   boolean writeFile = false;
	   
	   try 
	   {
		   webMembershipMgr = (WebMembershipMgrRemote) ServiceLocator.getInstance().getObject("WebMembershipMgr/remote");
		   webInsMgr = (WebInsMgrRemote) ServiceLocator.getInstance().getObject("WebInsMgr/remote");       
		   String systemName = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON,SystemParameterVO.TYPE_SYSTEM);
		   if (systemName.equals(CommonConstants.SYSTEM_DEVELOPMENT)) 
		   {
			   isDev = true;// smb is sketchy on Dev, so substring it out
			   days = 365;
		   }
		   
		   String daysOverride = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_EXTRACT_DIRS, "WEBTXNDAYS");
		   try 
		   {
			   days = Integer.parseInt(daysOverride);
		   }
		   catch (Exception e){}

		   Calendar lastWeek = Calendar.getInstance();
		   lastWeek.add(Calendar.DATE, -days);
		   
		   String outputDirectoryName = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_EXTRACT_DIRS, "WEBTXN");
		   if(outputDirectoryName.charAt(outputDirectoryName.length() - 1) != '/') 
		   {
			   outputDirectoryName += "/";
		   }
		   
		   String archiveDirectoryName = CommonEJBHelper.getCommonMgr().getSystemParameterValue(SystemParameterVO.CATEGORY_EXTRACT_DIRS, "WEBTXNARCH");
		   if(archiveDirectoryName.charAt(archiveDirectoryName.length() - 1) != '/') 
		   {
		  	 archiveDirectoryName += "/";
		   }
		   		   		   
		   Collection<WebPayable> resultList = webMembershipMgr.getUnreceiptedPayables(new DateTime(lastWeek.getTime()));		   
		   
		   resultList.addAll(webInsMgr.getUnreceiptedPayables(new DateTime(lastWeek.getTime())));
		   if (resultList != null && !resultList.isEmpty()) 
		   {		   
			   
		  	 makeTable(outputDirectoryName);
			   
			   makeTableFromArchive(archiveDirectoryName);
			   
			   String fileName = outputDirectoryName + FILE_PREFIX + DateUtil.formatDate(new DateTime(), DateUtil.DATE_FORMAT_FULL) + ".csv";
			   //05,MEM|INS,payRefNo,clientNo,amt,reference,YYYYMMddHHmmss,/				
	
			   StringBuilder fileWriter = new StringBuilder();
			   for (WebPayable result : resultList) 
			   {
				   //Check if this has already been written to a file 
				   //pList should contain any pending payables
						
			  	 LogUtil.debug(getClass(), "Looking ---> " + makeKey(result));
				   if(!pList.containsKey(makeKey(result)))
				   {
					   
				  	 LogUtil.debug(getClass(), "Could not find: " + makeKey(result));
				  	 
				  	 fileWriter.append("05,");
					   fileWriter.append(result.getSourceSystem() + ",");
					   fileWriter.append(result.getPayRefNo() + ",");
					   fileWriter.append(result.getClientNo() + ",");
					   fileWriter.append(result.getAmountPaid().multiply(new BigDecimal(100)).setScale(0) + ",");
					   fileWriter.append(result.getSourceReference() + ",");
					   fileWriter.append(DateUtil.formatDate(result.getCreateDate(), DateUtil.DATE_FORMAT_FULL) + ",/\r\n");
					   
					   writeFile = true;
				   }
			   }
			   
			   if (writeFile) {
				   if (isDev) {
					   System.out.println(fileWriter.toString());
				   } else {
				  	 FileUtil.writeFile(fileName, fileWriter.toString().getBytes(), true);
				   }
			   }
		   }	   
	
		} 
	    catch (Exception e) 
	    {
		    LogUtil.fatal(getClass(), "Unable to execute PaymentFileGeneratorJob: " + e);
		    e.printStackTrace();
		}
	   	   
	}
    /**
     * Build temporary hashtable containing payables waiting
     * in any existing files
     * @param inputDirectoryName
     * @throws MalformedURLException 
     * @throws SmbException 
     */
    private void makeTable(String inputDirectoryName) throws MalformedURLException, SmbException
    {
       if(isDev && 
      		 (inputDirectoryName.startsWith("smb:")
        	  || inputDirectoryName.startsWith("SMB:")))
       {
    	  inputDirectoryName = inputDirectoryName.substring(4);   
       }
       
       SmbFile inputDir = new SmbFile(inputDirectoryName);
       SmbFile[] files = inputDir.listFiles();
       String[] nextLine;
       WebPayable tPayable;
       CSVReader reader; 
       
       if (files != null) {
	       for(int xx = 0;xx<files.length;xx++)
	       {
	      	 SmbFile file = files[xx];
	    	   if(file.getName().startsWith(FILE_PREFIX))
	    	   {
	    		  try 
	    		  {
							reader = new CSVReader(new BufferedReader(new InputStreamReader(file.getInputStream())));
							while((nextLine = reader.readNext())!=null)
							{
								tPayable = new WebPayable();
								tPayable.setSourceSystem(nextLine[1]);
								tPayable.setPayRefNo(new Integer(nextLine[2]));
								tPayable.setClientNo(new Integer(nextLine[3]));
								tPayable.setAmountPaid(new BigDecimal(nextLine[4]));
								tPayable.setSourceReference(new Integer(nextLine[5]));
			//					tPayable.setCreateDate(new DateTime());
								pList.put(makeKey(tPayable),tPayable);
								
								LogUtil.debug(getClass(), "Adding ---> " + makeKey(tPayable));
							}
							reader.close();
					  }
	    		  catch (Exception e)
	    		  {
	    		  	e.printStackTrace();
					  }
	    	   }
	       }
       } else {
      	 LogUtil.debug(getClass(), "No files found in: " + inputDirectoryName);
       }
    }
    
    /**
     * Search the archive directory for possible relevant entries
     * All this is to make sure we don't send things twice.
     * Entries are added to the global pList.
     * Similar to maketable, but limit to recent files
     * @param String archive directory name.
     * @throws MalformedURLException 
     * @throws SmbException 
     */
    private void makeTableFromArchive(String inputDirectoryName) throws MalformedURLException, SmbException
    {
       
       if(isDev && 
      		 (inputDirectoryName.startsWith("smb:")
        	  || inputDirectoryName.startsWith("SMB:")))
       {
    	  inputDirectoryName = inputDirectoryName.substring(4);   
       }
       SmbFile inputDir = new SmbFile(inputDirectoryName);
       RecentFileFilter filter = new RecentFileFilter();
       SmbFile[] files = inputDir.listFiles(filter);
       
       LogUtil.debug(getClass(), "Found " + files.length + " recent files.");
       
       String[] nextLine;
       WebPayable tPayable;
       CSVReader reader; 
       
       if (files != null) {
	       for(int xx = 0;xx<files.length;xx++)
	       {
	      	 SmbFile file = files[xx];
	    	   if(file.getName().startsWith(FILE_PREFIX))
	    	   {
    	  		 try 
    	  		 {
								reader = new CSVReader(new BufferedReader(new InputStreamReader(file.getInputStream())));
								while((nextLine = reader.readNext())!=null)
								{
									tPayable = new WebPayable();
									tPayable.setSourceSystem(nextLine[1]);
									tPayable.setPayRefNo(new Integer(nextLine[2]));
									tPayable.setClientNo(new Integer(nextLine[3]));
									tPayable.setAmountPaid(new BigDecimal(nextLine[4]));
									tPayable.setSourceReference(new Integer(nextLine[5]));
								//					tPayable.setCreateDate(new DateTime());
									pList.put(makeKey(tPayable),tPayable);
									
									LogUtil.debug(getClass(), "Adding ---> " + makeKey(tPayable));
								}
								reader.close();
						  }
		    		  catch (Exception e)
		    		  {
		    		  	e.printStackTrace();
						  }
	    	   }
	       }
       }
    }
    
    static class RecentFileFilter implements SmbFileFilter {    	    	
      public boolean accept(SmbFile file) throws SmbException {
      	DateTime today = new DateTime();
        DateTime lookBackTo = new DateTime();
        
        try {
          Interval lookBackPeriod = new Interval(0,0,0,36,0,0,0); // 36 hours old
          lookBackTo = today.subtract(lookBackPeriod);
        } catch(Exception ex) {}
        
      	DateTime fileDate = new DateTime(file.lastModified()); 
      	return (fileDate.after(lookBackTo));
      }
    }
    
    /**
     * Create hashkey from system/client/ref no
     * This is required to provide a unique key for the hashtable
     * @param payable
     * @return hashkey
     */
    private String makeKey(WebPayable payable)
    {
    	return payable.getSourceSystem() 
    			+ "," + payable.getClientNo()
    		  + "," + payable.getPayRefNo();
    }
}
