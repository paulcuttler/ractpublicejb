package com.ract.web.common;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class WebBranchAllocation
{

	public String getSalesBranchCode()
	{
		return salesBranchCode;
	}

	public void setSalesBranchCode(String salesBranchCode)
	{
		this.salesBranchCode = salesBranchCode;
	}

	public WebBranchAllocationPK getWebBranchAllocationPK()
	{
		return webBranchAllocationPK;
	}

	public void setWebBranchAllocationPK(WebBranchAllocationPK webBranchAllocationPK)
	{
		this.webBranchAllocationPK = webBranchAllocationPK;
	}

	@Id
	private WebBranchAllocationPK webBranchAllocationPK; 
	
	private String salesBranchCode;
	
}
