package com.ract.web.common;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.GregorianCalendar;


import com.ract.util.DateTime;

/**
 * Represents a single gn-table record
 * dgk 25/3/09
 * 
 */

public class GnSetting implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8357722845392917545L;
	String tableName;
	String code;
	String description;
	DateTime gnDate;
	boolean logical;
	Integer gnInt;
	BigDecimal gnDec;
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public DateTime getGnDate() {
		return gnDate;
	}
	public void setGnDate(DateTime gnDate) {
		this.gnDate = gnDate;
	}
	public void setGnDate(GregorianCalendar gnDate)
	{
		if(gnDate==null)
		{
			this.gnDate=null;
		}
		else
		{
		    this.gnDate = new DateTime(gnDate.getTime());
		}
	}
	public boolean isLogical() {
		return logical;
	}
	public void setLogical(boolean logical) {
		this.logical = logical;
	}
	public Integer getGnInt() {
		return gnInt;
	}
	public void setGnInt(Integer gnInt) {
		this.gnInt = gnInt;
	}
	public void setGnInt(int gnInt)
	{
		this.gnInt = new Integer(gnInt);
	}
	public BigDecimal getGnDec() {
		return gnDec;
	}
	public void setGnDec(BigDecimal gnDec) {
		this.gnDec = gnDec;
	}
}
