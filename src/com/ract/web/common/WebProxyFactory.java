package com.ract.web.common;

import com.ract.common.pool.ProgressProxyObjectFactory;

public 	class 	WebProxyFactory 
		extends ProgressProxyObjectFactory

{
	
	  public final static String KEY_WEB_INS_PROXY = "WEB_INS_PROXY";
	  
	  public Object makeObject(Object key) throws java.lang.Exception
	  {
	    if (key == null)
	    {
	      throw new Exception("Attempting to make an object with a null key.");
	    }

	    getPoolInfo(WebProxyPool.getInstance(), key);
	    
	    if (key.toString().equals(KEY_WEB_INS_PROXY))
	    {
	      return new com.ract.web.insurance.InsProxy();
	    }
	    else
	    {
	    	//can make any other type
	    	return super.makeObject(key);
	    }
	  }

}
