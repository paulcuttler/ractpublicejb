package com.ract.web.common;

import org.apache.commons.pool.impl.*;


import com.ract.common.SystemParameterVO;
import com.ract.common.pool.ProxyObjectPool;

public 	class 	WebProxyPool  
		extends	ProxyObjectPool
{
	  private static String ProxyPoolKey = SystemParameterVO.CATEGORY_PROXY_POOL_WEB;
	  private static GenericKeyedObjectPool pool;
	  
	  private WebProxyPool()
	  {
	   
	  }
   
	  public synchronized static GenericKeyedObjectPool getInstance()
	  {
		     if (pool == null)
		     {
		      	GenericKeyedObjectPool.Config conf =new WebProxyPool().getProxyPoolConfig();
		       	
//		    	log("WebProxyPool  ","creating pool " );
		    	pool = new GenericKeyedObjectPool	(	new WebProxyFactory(),
		    		  									conf
		    		  	                             );

		     }
		     
//		     log("WebProxyPool ",toString(pool));
		     return pool;

   }

   public static void removeCurrentObjectPool()
   {
     if (pool != null)
     {
       pool.clear();
       try
       {
         pool.close();
       }
       catch(Exception ex)
       {
//         log("WebProxyPool","Unable to close the object pool. "+ex.getMessage());
       }
       pool = null;
     }
   }

@Override
	protected String getKey() 
	{
		return	ProxyPoolKey;
	}

}
