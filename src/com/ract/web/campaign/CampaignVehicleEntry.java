package com.ract.web.campaign;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ract.util.DateTime;

/**
 * Vehicle-specific campaign entry.
 * @author newtong
 *
 */
@Entity
@Table(name="CampaignVehicleEntry")
public class CampaignVehicleEntry extends CampaignEntry implements Serializable {
  		
	private String title;
	
	private String firstName;
	
	private String lastName;
	
	private Boolean gender;
	
	private DateTime birthDate;
	
	private Integer streetSuburbId;
	
	private String streetNumber;
	
	private String nvic;
	
	private Integer renewalMonth;
	
	private String email;
	
	private String phone;

	public CampaignVehicleEntry() {
	}

	public CampaignVehicleEntry(String campaignRef, Integer clientNumber,
			String title, String firstName, String lastName, Boolean gender,
			DateTime birthDate, Integer streetSuburbId, String streetNumber,
			String nvic, Integer renewalMonth, String email, String phone) {
		this.campaignRef = campaignRef;
		this.clientNumber = clientNumber;
		this.title = title;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.birthDate = birthDate;
		this.streetSuburbId = streetSuburbId;
		this.streetNumber = streetNumber;
		this.nvic = nvic;
		this.renewalMonth = renewalMonth;
		this.email = email;
		this.phone = phone;		
	}
		
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public DateTime getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(DateTime birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * Reference to Street Suburb lookup.
	 * @return
	 */
	public Integer getStreetSuburbId() {
		return streetSuburbId;
	}

	public void setStreetSuburbId(Integer streetSuburbId) {
		this.streetSuburbId = streetSuburbId;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	/**
	 * Reference to Glasses Guide vehicle lookup.
	 * @return
	 */
	public String getNvic() {
		return nvic;
	}

	public void setNvic(String nvic) {
		this.nvic = nvic;
	}

	public Integer getRenewalMonth() {
		return renewalMonth;
	}

	public void setRenewalMonth(Integer renewalMonth) {
		this.renewalMonth = renewalMonth;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int hashCode() {
		return (this.id == null) ? 0 : this.id.hashCode();
	}

	public boolean equals(Object object) {
		if (object instanceof CampaignVehicleEntry) {
			final CampaignVehicleEntry obj = (CampaignVehicleEntry) object;
			return (this.id != null) ? this.id.equals(obj.id) : (obj.id == null);
		}
		return false;
	}

	@Override
	public String toString() {
		return "CampaignVehicle [title=" + title + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", gender=" + gender + ", birthDate="
				+ birthDate + ", streetSuburbId=" + streetSuburbId + ", streetNumber="
				+ streetNumber + ", nvic=" + nvic + ", renewalMonth=" + renewalMonth
				+ ", email=" + email + ", phone=" + phone + ", id=" + id
				+ ", createDate=" + createDate + ", campaignRef=" + campaignRef
				+ ", clientNumber=" + clientNumber + "]";
	}
	
}
