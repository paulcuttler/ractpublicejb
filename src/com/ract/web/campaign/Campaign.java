package com.ract.web.campaign;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.ract.util.DateTime;

/**
 * Definition of a Campaign.
 * @author newtong
 *
 */
@Entity
public class Campaign implements Serializable {

	@Id
	private String campaignRef;
	private String friendlyName;
	private DateTime expiryDate;	
	private DateTime drawnDate;
	private String winners;
	
	public Campaign() {}
	
	public Campaign(String campaignRef, String friendlyName, DateTime expiryDate) {
		super();
		this.campaignRef = campaignRef;
		this.friendlyName = friendlyName;
		this.expiryDate = expiryDate;
	}
	
	public String getCampaignRef() {
		return campaignRef;
	}
	public void setCampaignRef(String campaignRef) {
		this.campaignRef = campaignRef;
	}
	
	/**
	 * The date after which entries to the Campaign should not be accepted.
	 * @return
	 */
	public DateTime getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(DateTime expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	/**
	 * A campaign is active if its expiry date is in the future. 
	 * @return
	 */
	public boolean isActive() {
		return this.getExpiryDate().after(new Date());
	}
	
	public String getFriendlyName() {
		return friendlyName;
	}
	public void setFriendlyName(String friendlyName) {
		this.friendlyName = friendlyName;
	}
	
	/**
	 * The date the Campaign was drawn and winners allocated.
	 * @return
	 */
	public DateTime getDrawnDate() {
		return drawnDate;
	}
	public void setDrawnDate(DateTime drawnDate) {
		this.drawnDate = drawnDate;
	}
	
	public String getWinners() {
		return winners;
	}

	public void setWinners(String winners) {
		this.winners = winners;
	}

	@Override
	public String toString() {
		return "Campaign [campaignRef=" + campaignRef + ", friendlyName="
				+ friendlyName + ", expiryDate=" + expiryDate + ", drawnDate="
				+ drawnDate + "]";
	}
	
}
