package com.ract.web.campaign;

import java.io.Serializable;

import javax.persistence.*;

import com.ract.util.DateTime;

/**
 * Basic Campaign entry class holding general attributes and methods
 * It is expected that this class will be extended for specific uses
 * @author newtong
 *
 */
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class CampaignEntry implements Serializable {

	public CampaignEntry() {}
	
  public CampaignEntry(String campaignRef, Integer clientNumber) {
  	this.campaignRef = campaignRef;
		this.clientNumber = clientNumber;
	}

	public static final String SEQUENCE_CAMPAIGN = "WEB_CAMPAIGN";
  
	@Id
	@Column(name="entryId")
	protected Integer id;
	
	@Column(insertable=true, updatable=false)
	protected DateTime createDate;
	
	protected String campaignRef;
	
	protected Integer clientNumber;
	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	

	public DateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(DateTime createDate) {
		this.createDate = createDate;
	}
	
	public String getCampaignRef() {
		return campaignRef;
	}

	public void setCampaignRef(String campaignRef) {
		this.campaignRef = campaignRef;
	}

	public Integer getClientNumber() {
		return clientNumber;
	}

	public void setClientNumber(Integer clientNumber) {
		this.clientNumber = clientNumber;
	}

	@Override
	public String toString() {
		return "Campaign [id=" + id + ", createDate=" + createDate
				+ ", campaignRef=" + campaignRef + ", clientNumber=" + clientNumber + "]";
	}

}
