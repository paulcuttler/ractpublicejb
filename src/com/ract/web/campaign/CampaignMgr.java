package com.ract.web.campaign;

import java.rmi.RemoteException;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.ract.common.SequenceMgrLocal;
import com.ract.common.SystemException;
import com.ract.util.DateTime;

@Stateless
@Remote({CampaignMgrRemote.class})
@Local({CampaignMgrLocal.class})
public class CampaignMgr implements CampaignMgrRemote {

	@PersistenceContext
	EntityManager em;

	@EJB
	private SequenceMgrLocal sequenceMgrLocal;
	
	/**
	 * Persist a Campaign to the database.
	 */
	public void createCampaign(Campaign campaign) {
		this.em.persist(campaign);
	}
	
	/**
	 * Retrieve a Campaign matching ref from the database.
	 * @param ref Campaign reference
	 */
	public Campaign retrieveCampaign(String ref) {
		return this.em.find(Campaign.class, ref);
	}
	
	/**
	 * Update an existing Campaign in the database.
	 */
	public void updateCampaign(Campaign campaign) {
		this.em.merge(campaign);
	}
	
	/**
	 * Delete a Campaign from the database.
	 */
	public void deleteCampaign(Campaign campaign) {
		this.em.remove(campaign);
	}	
	
	/**
	 * Persist a new CampaignEntry to the database.
	 * @param campaignEntry CampaignEntry to persist.
	 * @throws RemoteException if an invalid campaignRef is provided with CampaignEntry.
	 */
	public void createEntry(CampaignEntry campaignEntry) throws RemoteException {
		String ref = campaignEntry.getCampaignRef();
		if (this.retrieveCampaign(ref) == null) {
			throw new SystemException("Cannot create a campaign entry for non-existant campaign reference: " + ref);
		}
		
    Integer campaignId = sequenceMgrLocal.getNextID(CampaignVehicleEntry.SEQUENCE_CAMPAIGN);
    campaignEntry.setId(campaignId);
		campaignEntry.setCreateDate(new DateTime());
		
		this.em.persist(campaignEntry);
	}
	
	/**
	 * Retrieve a CampaignEntry matching id from the database.
	 * @param id CampaignEntry unique identifier.
	 */
	public CampaignEntry retrieveEntry(Integer id) {
		return this.em.find(CampaignEntry.class, id);
	}
	
	/**
	 * Update an existing CampaignEntry in the database.
	 * @param campaignEntry CampaignEntry to update.
	 * @throws RemoteException if an invalid campaignRef is provided with CampaignEntry.
	 */	
	public void updateEntry(CampaignEntry campaignEntry) throws RemoteException {
		String ref = campaignEntry.getCampaignRef();
		if (this.retrieveCampaign(ref) == null) {
			throw new SystemException("Cannot update a campaign entry for non-existant campaign reference: " + ref);
		}
		
		this.em.merge(campaignEntry);
	}
	
	/**
	 * Remove a CampaignEntry from the database.
	 */
	public void deleteEntry(CampaignEntry campaignEntry) {
		this.em.remove(campaignEntry);
	}	
	
	/**
	 * Retrieves a list of Campaign entries by reference.
	 * @param ref Campaign reference code
	 */
	@SuppressWarnings("unchecked")
	public List<CampaignEntry> getEntriesByReference(String ref) {
		Query query = this.em.createQuery("select e from CampaignEntry e where e.campaignRef = :ref");
		query.setParameter("ref", ref);
		
		return (List<CampaignEntry>) query.getResultList();
	}
	
	/**
	 * Retrieves a list of Campaign entries by reference and client number.
	 * @param ref Campaign reference code
	 * @param clientNo Client Number
	 */
	@SuppressWarnings("unchecked")
	public List<CampaignEntry> getEntriesByReferenceAndClientNumber(String ref, Integer clientNo) {
		Query query = this.em.createQuery("select e from CampaignEntry e where e.campaignRef = :ref and e.clientNumber = :clientNo");
		query.setParameter("ref", ref);
		query.setParameter("clientNo", clientNo);
		
		return (List<CampaignEntry>) query.getResultList();
	}
	
	/**
	 * Retrieve a list of Campaigns that are ready to be drawn (i.e. expired and having no drawn date).
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Campaign> getUndrawnCampaigns() {
		Query query = this.em.createQuery("select e from Campaign e where e.expiryDate <= :today and e.drawnDate is null");
		query.setParameter("today", new DateTime());

		return (List<Campaign>) query.getResultList();		
	}
	
	/**
	 * Retrieve a list of Campaigns that have been drawn.
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Campaign> getDrawnCampaigns() {
		Query query = this.em.createQuery("select e from Campaign e where e.expiryDate <= :today and e.drawnDate is not null");
		query.setParameter("today", new DateTime());

		return (List<Campaign>) query.getResultList();		
	}
	
}
