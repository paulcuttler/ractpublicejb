package com.ract.web.campaign;

import java.rmi.RemoteException;
import java.util.List;

import javax.ejb.Remote;

@Remote
public interface CampaignMgrRemote {
	
	public void createCampaign(Campaign campaign);

	public Campaign retrieveCampaign(String ref);
	
	public void updateCampaign(Campaign campaign);
	
	public void deleteCampaign(Campaign campaign);
	
	public void createEntry(CampaignEntry campaignEntry) throws RemoteException;
	
	public CampaignEntry retrieveEntry(Integer id);
	
	public void updateEntry(CampaignEntry campaignEntry) throws RemoteException;
	
	public void deleteEntry(CampaignEntry campaignEntry);
	
	public List<CampaignEntry> getEntriesByReference(String ref);
	
	public List<CampaignEntry> getEntriesByReferenceAndClientNumber(String ref, Integer clientNo);

	public List<Campaign> getUndrawnCampaigns();
	
	public List<Campaign> getDrawnCampaigns();
}
