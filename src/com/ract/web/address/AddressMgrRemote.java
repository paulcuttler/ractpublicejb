package com.ract.web.address;

import java.util.Collection;

import javax.ejb.Remote;

import com.ract.common.GenericException;
import com.ract.common.StreetSuburbVO;

@Remote
public interface AddressMgrRemote
{

  public Collection<StreetSuburbVO> getStreetSuburbsByPostcode(String postcode)
  throws GenericException;
  
  public Collection<StreetSuburbVO> getStreetSuburbsByStreet(String street, String postcode)
  throws GenericException;  
	
  public Collection<StreetSuburbVO> getStreetSuburbsBySuburb(String suburb, String postcode)
  throws GenericException;  
  
  public StreetSuburbVO getUniqueStreetSuburb(String street, String suburb, String postcode)
  throws GenericException; 
  
  public Integer createStreetSuburb(String street, String suburb, String postcode)
  throws GenericException;
  
}
