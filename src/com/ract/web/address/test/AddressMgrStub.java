package com.ract.web.address.test;

import java.util.ArrayList;
import java.util.Collection;

import com.ract.common.*;
import com.ract.web.address.AddressMgrRemote;

public class AddressMgrStub implements AddressMgrRemote
{

	@Override
	public Collection getStreetSuburbsByPostcode(String postcode)
			throws GenericException
	{
		// TODO Auto-generated method stub
		return getDefaultStreetSuburbList();
	}

	@Override
	public Collection getStreetSuburbsByStreet(String street, String postcode)
			throws GenericException
	{
		// TODO Auto-generated method stub
		return getDefaultStreetSuburbList();
	}

	private ArrayList<StreetSuburbVO> getDefaultStreetSuburbList()
	{
		ArrayList<StreetSuburbVO> streetSuburbList = new ArrayList<StreetSuburbVO>();
		StreetSuburbVO test1 = new StreetSuburbVO();
		test1.setStreet("Wignall St");
		test1.setSuburb("North Hobart");
		test1.setStreetSuburbID(new Integer(1));
		streetSuburbList.add(test1);
		test1 = new StreetSuburbVO();
		test1.setStreet("Ryde St");
		test1.setSuburb("North Hobart");
		test1.setStreetSuburbID(new Integer(2));		
		streetSuburbList.add(test1);		
		test1 = new StreetSuburbVO();
		test1.setStreet("Zenith Crt");
		test1.setSuburb("Howrah");
		test1.setStreetSuburbID(new Integer(3));
		streetSuburbList.add(test1);		
		return streetSuburbList;
	}

	@Override
	public Collection getStreetSuburbsBySuburb(String suburb, String postcode)
			throws GenericException
	{
		// TODO Auto-generated method stub
		return getDefaultStreetSuburbList();
	}

	@Override
	public StreetSuburbVO getUniqueStreetSuburb(String street, String suburb,
			String postcode) throws GenericException {
		// TODO Auto-generated method stub
		return getDefaultStreetSuburbList().get(0);
	}

	@Override
	public Integer createStreetSuburb(String street, String suburb,
			String postcode) throws GenericException {
		// TODO Auto-generated method stub
		return null;
	}

}
