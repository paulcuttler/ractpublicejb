package com.ract.web.address;

import java.util.Collection;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import com.ract.common.*;
import com.ract.common.qas.QASAddress;
import com.ract.common.streetsuburb.StreetSuburbFactory;
import com.ract.security.SecurityHelper;

@Stateless
@Remote({AddressMgrRemote.class})
@Local({AddressMgrLocal.class})
public class AddressMgr implements AddressMgrRemote
{
  
  public Collection<StreetSuburbVO> getStreetSuburbsByPostcode(String postcode)
    throws GenericException
  {
  	Collection<StreetSuburbVO> streetSuburbList = null;
		try
		{
			StreetSuburbMgr streetSuburbMgr = CommonEJBHelper.getStreetSuburbMgr();
			streetSuburbList = streetSuburbMgr.getStreetSuburbsByPostcode(postcode);
		}
		catch (Exception e)
		{
      throw new GenericException(e);
		}
  	return streetSuburbList;
  }

  public Collection<StreetSuburbVO> getStreetSuburbsByStreet(String street, String postcode)
    throws GenericException  
  {
  	Collection<StreetSuburbVO> streetSuburbList = null;
		try
		{
			StreetSuburbMgr streetSuburbMgr = CommonEJBHelper.getStreetSuburbMgr();
			streetSuburbList = streetSuburbMgr.getStreetSuburbsByStreet(street, postcode);
		}
		catch (Exception e)
		{
      throw new GenericException(e);
		}
  	return streetSuburbList;
  }
  
  public Collection<StreetSuburbVO> getStreetSuburbsBySuburb(String suburb, String postcode)
    throws GenericException  
  {
  	Collection<StreetSuburbVO> streetSuburbList = null;
		try
		{
			StreetSuburbMgr streetSuburbMgr = CommonEJBHelper.getStreetSuburbMgr();
			streetSuburbList = streetSuburbMgr.getStreetSuburbsBySuburb(suburb, postcode);
		}
		catch (Exception e)
		{
      throw new GenericException(e);
		}
  	return streetSuburbList;
  }  
  
  public StreetSuburbVO getUniqueStreetSuburb(String street, String suburb, String postcode)
  	throws GenericException  
  {
		try
		{
			StreetSuburbMgr streetSuburbMgr = CommonEJBHelper.getStreetSuburbMgr();
			return streetSuburbMgr.getUniqueStreetSuburb(street, suburb, postcode);
		}
		catch (Exception e)
		{
			throw new GenericException(e);
		}
  }  
	
  public Integer createStreetSuburb(String street, String suburb, String postcode) 
  	throws GenericException {
  	
		Integer result = null;
		
  	try {
	  	QASAddress q = new QASAddress();
	  	q.setPostcode(postcode);
			q.setStreet(street);
			q.setSuburb(suburb);
			q.setState(AddressVO.STATE_TAS);
			q.setUserid(SecurityHelper.getSystemUser().getUserID());
			
			result = StreetSuburbFactory.getStreetSuburbAdapter().createProgressStreetSuburb(q);
			
  	} catch (Exception e) {
  		throw new GenericException(e);		
  	}
		
		return result;
  }
}
