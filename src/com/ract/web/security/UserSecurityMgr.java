package com.ract.web.security;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgrLocal;
import com.ract.common.ExceptionHelper;
import com.ract.common.GenericException;
import com.ract.common.MailMgr;
import com.ract.common.MailMgrLocal;
import com.ract.common.SequenceMgr;
import com.ract.common.SequenceMgrLocal;
import com.ract.common.SourceSystem;
import com.ract.common.SystemParameterVO;
import com.ract.common.mail.MailMessage;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;
import com.ract.web.client.ActivationRequest;
import com.ract.web.membership.WebMembershipMgrLocal;

/**
 * Manage web security operations.
 * @author hollidayj
 */

@Stateless
@Remote({UserSecurityMgrRemote.class})
@Local({UserSecurityMgrLocal.class})
public class UserSecurityMgr implements UserSecurityMgrRemote,UserSecurityMgrLocal
{

	private static final String SUBJECT_RACT_MEMBER_ONLY_ACCESS = "RACT Member Only Access";
	private static final String SEQUENCE_WEB_USER = "WEB_USER_SEQ";
	private static final String SEQUENCE_LOGIN_AUDIT = "LOGIN_AUDIT_SEQ";	
//	private static final String HSQL_ACTIVATION_REQUEST_BY_CARD_NUMBER = "select e from ActivationRequest e where clientNumber = ?1 and activationDate is not null";
	private static final String HSQL_ACTIVATION_REQUEST_BY_USERID = "select e from ActivationRequest e where userId = ?1";	
	
	public static final String MESSAGE_EMAIL_PASSWORD_RESET = "Your RACT member access account password has been reset to ";
	public static final String SUBJECT_EMAIL_PASSWORD_RESET = "RACT Member Access Password Reset";
	public static final String SUBJECT_EMAIL_USERNAME_CONF = "RACT user account details";
	
	public static final String MESSAGE_NO_CLIENT = "There is no RACT client record for the entered membership card number.";
	public static final String MESSAGE_INVALID_EMAIL_ADDRESS = "The email address does not contain an \"@\" or a \".\".";
//	public static final String MESSAGE_NO_EMAIL_ADDRESS = "No email address has been defined for the RACT customer.";
	public static final String MESSAGE_USER_DOES_NOT_EXIST = "User does not exist.";
	public static final String MESSAGE_ACCOUNT_ALREADY_ASSOCIATED = "Member access account is already associated with a membership.";

	@EJB
	private SequenceMgrLocal sequenceMgr;

	@EJB
	private MailMgrLocal mailMgr;

	@EJB
	private CommonMgrLocal commonMgr;	
	
	@EJB
	private UserSecurityMgrLocal userSecurityMgr;	
	
	@PersistenceContext
	EntityManager em;
	
	//TODO admin function required to enter a client number directly on a user record
	// where user does not have a valid email address recorded against their client
	// record. Simple update attribute and call updateUserAccount	
	
	//user in session = logged in
	//jsession cookie = logged in
	
	/**
	 * Create an audit record consisting of user and session details.
	 * @param sessionId The session identifier
	 * @param ipAddress The client IP address
	 * @param userId The user id of the logged in user if it is available
	 * @param initialContext The area of the system they were initially in when accessing the system. Constants in the web tier.
	 * @return The saved user session. 
	 */
	public UserSession createUserSession(String sessionId, String ipAddress,
			Integer userId, String initialContext) throws GenericException
	{
		UserSession us = new UserSession();
		us.setCreateDate(new Date());
		us.setInitialContext(initialContext);
		us.setIpAddress(ipAddress);
    us.setUserId(userId);
		us.setSessionId(sessionId);
		em.persist(us);
		return us;
	}
	
	/**
	 * Create a record to track login attempts
	 */
	public LoginAudit createLoginAudit(LoginAudit loginAudit) throws GenericException
	{
		Integer auditId;
		try
		{
			auditId = sequenceMgr.getNextID(SEQUENCE_LOGIN_AUDIT);
		}
		catch (RemoteException e)
		{
			throw new GenericException("Unable to get next login audit Id: "+e.getMessage());
		}
		loginAudit.setAuditId(auditId);
		em.persist(loginAudit);
		return loginAudit;
	}
	
	public void updateUsersWithUserId() throws GenericException
	{

		Query users = em.createQuery("select e from User e");
		Collection<User> list = users.getResultList();
		User user = null;
		for (Iterator<User> i = list.iterator();i.hasNext();)
		{
			user = i.next();
			if (user.getUserId() == null)
			{
				try
				{
					Integer userId = sequenceMgr.getNextID(SEQUENCE_WEB_USER);
					user.setUserId(userId);
					updateUserAccount(user);
				}
				catch (RemoteException e)
				{
					e.printStackTrace();
					throw new GenericException(e.getMessage());
				}
			}
			
		}
		
	}
	
	public UserSession getUserSession(String sessionId)
	{
		UserSession userSession = em.find(UserSession.class, sessionId);
		return userSession;
	}
		
	/**
	 * Create a member account activation request record and send an activation email to the Web User's email address.
	 * @param userId The web user id to activate.
	 * @param membershipCardNumber The RACT membership card number.
	 * @param surname Member surname to match.
	 * @param postcode Member postcode to match.
	 * @param phoneNumber Member home/work phone number to match.
	 * @param mobilePhoneNumber Member mobile number to match.
	 * @param dateOfBirth Member DOB to match.
	 * @param updateEmailAddress Place holder for Web User email address to be updated on Client record.
	 * @param activationURL Link for Member to click in activation notification email.  
	 */
	public ActivationRequest requestMemberAccountActivation(Integer userId,
			String membershipCardNumber, String surname,
			String postcode, String phoneNumber, String mobilePhoneNumber, DateTime dateOfBirth, String updateEmailAddress, String activationURL)
	  throws GenericException
	{
		String errorMessage = "";
		boolean create = false;
		
		LogUtil.debug(this.getClass(),"requestMemberAccountActivation start");
		//Has the account been activate previously
		Query activatedQuery = em.createQuery("select e from ActivationRequest e where membershipCardNumber = ?1 and activationDate is not null").setParameter(1, membershipCardNumber);
		List activationRequestList = activatedQuery.getResultList();
		LogUtil.debug(this.getClass(),"activationRequestList="+activationRequestList);
		if (activationRequestList != null &&
				activationRequestList.size() > 0)
		{
			throw new GenericException(MESSAGE_ACTIVATION_REQUEST_PROCESSED);
		}
		//limit the number of activation requests.
		DateTime createDate = new DateTime();
		int attemptCount = 0;
		Query query = em.createQuery("select e from ActivationRequest e where membershipCardNumber = ?1 and activationDate is null and created = ?2");
		query.setParameter(1, membershipCardNumber);
		query.setParameter(2, false);
		List<ActivationRequest> attemptList = query.getResultList();	
		ActivationRequest attempt = null;
		for (Iterator<ActivationRequest> i = attemptList.iterator();i.hasNext();)
		{
			attempt = i.next();
			LogUtil.debug(this.getClass(), "attempt="+attempt);
			if (attempt.getCreateDate().getDateOnly().equals(createDate.getDateOnly())) //attempts on the same day
			{
				LogUtil.debug(this.getClass(), "on same day");
				attemptCount++;
			}
		}
		//3?
				
   	int maxAttemptCount = Integer.parseInt(((SystemParameterVO)commonMgr.getSystemParameter(SourceSystem.MEMBERSHIP.getAbbreviation(), "USERACTMAXCOUNT")).getValue());		
   	LogUtil.debug(this.getClass(), "attemptCount="+attemptCount);
   	LogUtil.debug(this.getClass(), "maxAttemptCount="+maxAttemptCount);
   	if (attemptCount >= maxAttemptCount)
		{
			throw new GenericException("You have exceeded the maximum number of registration attempts.");
		}
		
		LogUtil.debug(this.getClass(),"surname="+surname);
		LogUtil.debug(this.getClass(),"postcode="+postcode);	
		LogUtil.debug(this.getClass(),"phoneNumber="+phoneNumber);	
		LogUtil.debug(this.getClass(),"mobilePhoneNumber="+mobilePhoneNumber);	
		LogUtil.debug(this.getClass(),"dateOfBirth="+(dateOfBirth==null?"":dateOfBirth.formatMediumDate()));
		
		if (surname == null ||
				surname.trim().length() == 0)
		{
			throw new GenericException("Surname must be entered.");
		}
		
		if (postcode == null ||
				postcode.trim().length() == 0)
		{
			throw new GenericException("Postcode must be entered.");			
		}
		
		//get client using membership card number
		ClientMgr clientMgr = null;
		ClientVO client = null;
		try
		{			
  		clientMgr = ClientEJBHelper.getClientMgr();
	  	client = clientMgr.getClientByMembershipCardNumber(membershipCardNumber);
	  	LogUtil.debug(this.getClass(), "client="+client);
		}
		catch (Exception e)
		{
			throw new GenericException(e);
		}
		
		if (client==null)
		{
			throw new GenericException(MESSAGE_NO_CLIENT);
		}	
		
		boolean validDOB = (client.getBirthDate()!=null); //non-null DOB (NA, etc stored in another attribute)
		
    boolean validHomePhone = false;
    try
		{
			validHomePhone = (NumberUtil.validatePhoneNumber(NumberUtil.PHONE_TYPE_HOME, client.getHomePhone())!= null);
		}
		catch (Exception e)
		{
			LogUtil.warn(this.getClass(), e.getMessage());
		}
    boolean validWorkPhone = false;
    try
		{
			validWorkPhone = (NumberUtil.validatePhoneNumber(NumberUtil.PHONE_TYPE_WORK, client.getWorkPhone())!= null);
		}
		catch (Exception e)
		{
			LogUtil.warn(this.getClass(), e.getMessage());
		}					
    boolean validMobilePhone = false;
    try
		{
    	validMobilePhone = (NumberUtil.validatePhoneNumber(NumberUtil.PHONE_TYPE_MOBILE, client.getMobilePhone())!= null);
		}
		catch (Exception e)
		{
			LogUtil.warn(this.getClass(), e.getMessage());
		}					
		
		if (!validDOB && !validHomePhone && !validWorkPhone && !validMobilePhone)
		{
			throw new GenericException("There are insufficient details on your RACT client record to allow matching. You will need to contact us to create a member access account.");
		}
		
		String clientPostcode = null;
		try
		{
			clientPostcode = client.getResidentialAddress().getPostcode();
		}
		catch (RemoteException e)
		{
			LogUtil.warn(this.getClass(), "Unable to get residential address postcode. "+ExceptionHelper.getExceptionStackTrace(e));
		}
		LogUtil.debug(this.getClass(),"client.getSurname()="+client.getSurname().toUpperCase());
		LogUtil.debug(this.getClass(),"clientPostcode="+clientPostcode);
		LogUtil.debug(this.getClass(),"client.getHomePhone()="+client.getHomePhone());
		LogUtil.debug(this.getClass(),"client.getWorkPhone()="+client.getWorkPhone());
		LogUtil.debug(this.getClass(),"client.getMobilePhone()="+client.getMobilePhone());		
		LogUtil.debug(this.getClass(),"client.getBirthDate()="+(client.getBirthDate()==null?"":client.getBirthDate().formatMediumDate()));		
				
		//verify membership details
		if (!surname.toUpperCase().equals(client.getSurname().toUpperCase()))
		{
			errorMessage += "Surname entered does not match your RACT client record. ";
		}
		if (!postcode.equals(clientPostcode))
		{
			errorMessage += "Postcode entered does not match your RACT client record. ";
		}
		
		boolean mandEntered = false;
		if (dateOfBirth != null)
		{
			mandEntered = true;	
			if (client.getBirthDate() == null)
			{
				errorMessage += "Unable to match date of birth as there is no date of birth on your RACT client record.";
			}
			else if (!dateOfBirth.formatShortDate().equals(client.getBirthDate().formatShortDate()))
			{
				LogUtil.info(this.getClass(), client.getClientNumber() + ": In: " + dateOfBirth + " vs. Current: " + client.getBirthDate());
				
				errorMessage += "Date of birth entered does not match your RACT client record.";
			}
		}		
		else if (phoneNumber != null &&
				     phoneNumber.length() > 0)
		{
	    mandEntered = true;	

			if (!phoneNumber.equals(client.getHomePhone()) &&			
			    !phoneNumber.equals(client.getWorkPhone()))
	    {
	      errorMessage += "Phone number entered does not match home or work phone numbers on your RACT client record. ";
	      boolean invalidHome = (client.getHomePhone() == null || client.getHomePhone().trim().length() == 0 || !validHomePhone);
	      boolean invalidWork = (client.getWorkPhone() == null || client.getWorkPhone().trim().length() == 0 ||	!validWorkPhone);
	    	//no match
	      if (invalidHome && invalidWork)
	      {
	      	errorMessage += "Note: There is no valid home or work phone number on your RACT client record. ";
	      }
	      else //valid but no match or one invalid
	      {
	    		if (invalidHome)
	    		{
	    			errorMessage += "Note: There is no valid home phone number on your RACT client record. ";
	    		}				
	    		else if (invalidWork)
	    		{
	    			errorMessage += "Note: There is no valid work phone number on your RACT client record. ";
	    		}
	      }
    		
	    } //else a match
		}
		else if (mobilePhoneNumber != null &&
				     mobilePhoneNumber.length() > 0)
		{
			mandEntered = true;
	
			if (client.getMobilePhone() == null ||
					!validMobilePhone)
			{
				errorMessage += "Unable to match mobile phone number as there is no valid mobile phone number on your RACT client record. ";
			}
			else if (!mobilePhoneNumber.equals(client.getMobilePhone()))
	    {
		    errorMessage += "Phone number entered does not match mobile phone number on your RACT client record. ";
	    }			
		}
    
		LogUtil.debug(this.getClass(),"mandEntered="+mandEntered);		
	
		//append a hint to the error message.
		
		if (!mandEntered)
		{
			throw new GenericException("At least one of home/work phone number, mobile number or date of birth must be entered to allow matching.");
		}
		
		//do we send an activation request?
		create = (errorMessage == null || errorMessage.trim().length() == 0);
		
		//record request record including client number, present user with a request number.
		SequenceMgr sequenceMgr = CommonEJBHelper.getSequenceMgr();		
		Integer requestNumber = null;
		try
		{
			requestNumber = sequenceMgr.getNextID(ActivationRequest.SEQUENCE_ACTIVATION_REQUEST);
		}
		catch (RemoteException e)
		{
      throw new GenericException(e);
		}
		
		ActivationRequest activationRequest = new ActivationRequest();
		activationRequest.setRequestNumber(requestNumber);
		activationRequest.setCreateDate(createDate);
		activationRequest.setMembershipCardNumber(membershipCardNumber);
		activationRequest.setClientNumber(client.getClientNumber().toString());
		activationRequest.setUserId(userId);
		activationRequest.setDateOfBirth(dateOfBirth);
		activationRequest.setPhoneNumber(phoneNumber);
		activationRequest.setMobilePhoneNumber(mobilePhoneNumber);
		activationRequest.setPostcode(postcode);
		activationRequest.setSurname(surname);
		activationRequest.setUpdateEmailAddress(updateEmailAddress);
		activationRequest.setCreated(create);
		//save null rather than empty string in the message field
		if (errorMessage != null &&
				errorMessage.trim().length() == 0)
		{
			errorMessage = null;
		}
		activationRequest.setMessage(errorMessage);
		//save the request
		em.persist(activationRequest);
		
		LogUtil.debug(this.getClass(),"ar="+activationRequest);		
		
		LogUtil.debug(this.getClass(),"create="+create);		
		if (create)
		{
		  //update user type
			
	    //returned as a basic user
	    User user = getUser(activationRequest.getUserId());
	    LogUtil.debug(this.getClass(),"user="+user);    
	    if (user instanceof MemberUser)
	    {
	    	if (user.getActivationDate() != null)
	    	{
	    	  throw new GenericException(MESSAGE_ACCOUNT_ALREADY_ASSOCIATED);
	    	}
	    	
	    	LogUtil.debug(this.getClass(),"member user");    	
	    	MemberUser memUser = (MemberUser)user;
	  		//update user record with client number
	      //get client number from activation request.    
	      memUser.setClientNumber(activationRequest.getClientNumber());
	      memUser.setMemberCardNumber(activationRequest.getMembershipCardNumber());
	      memUser.setLastUpdate(new DateTime());
	      LogUtil.debug(this.getClass(),"memUser="+memUser);      
	      em.merge(memUser);
	    }  
	    else
	    {
	    	throw new GenericException("Only a member can request activation.");
	    }
			
	   	String fromAddress = ((SystemParameterVO)commonMgr.getSystemParameter(SourceSystem.MEMBERSHIP.getAbbreviation(), "FROMUSER")).getValue();    
	    
	   	activationURL += requestNumber; //add parameter to URL
	   	
	   	StringBuffer emailMessage = new StringBuffer();
	   	emailMessage.append("<p>");
	   	emailMessage.append("Click <a href=\"");
	   	emailMessage.append(activationURL);
	   	emailMessage.append("\"/>");
	   	emailMessage.append("here</a>");
	   	emailMessage.append(" to activate your RACT member only access account.");
	   	emailMessage.append("</p>");   	
	   	emailMessage.append("<p>");
	   	emailMessage.append("Alternatively copy and paste "+activationURL+" into your web browser.");
	   	emailMessage.append("</p>");   	
	   	
	    //send email
	    try
			{
        MailMessage message = new MailMessage();
        message.setRecipient(user.getEmailAddress());
        message.setSubject(SUBJECT_RACT_MEMBER_ONLY_ACCESS);
        message.setMessage(emailMessage.toString());
        message.setFrom(fromAddress);
        message.setHtml(true);
        
				mailMgr.sendMail(message);
			}
			catch (RemoteException e)
			{
				e.printStackTrace();
				throw new GenericException(e.getMessage());
			}
		}
		LogUtil.debug(this.getClass(),"requestMemberAccountActivation end");    
		return activationRequest;
	}
	
	
	
	public User emailUserDetails(Integer userId) throws GenericException
  {
		User user = getUser(userId);
		
		if (user == null)
		{
			throw new GenericException(MESSAGE_USER_DOES_NOT_EXIST);
		}
	
		//email user
	  try
    {
     	String bcc = ((SystemParameterVO)commonMgr.getSystemParameter(SourceSystem.MEMBERSHIP.getAbbreviation(), "BCCUSER")).getValue();
     	String fromAddress = ((SystemParameterVO)commonMgr.getSystemParameter(SourceSystem.MEMBERSHIP.getAbbreviation(), "FROMUSER")).getValue();
	  	
			MailMgr mailMgr = CommonEJBHelper.getMailMgr();
			
			MailMessage message = new MailMessage();
			message.setRecipient(user.getEmailAddress());
			message.setBcc(bcc);
			message.setSubject(SUBJECT_EMAIL_USERNAME_CONF);
			
			StringBuffer body = new StringBuffer();
			body.append("User account created: "+user.getCreateDate().formatMediumDate()+FileUtil.NEW_LINE+FileUtil.NEW_LINE);
			
			if (user.getUserName() != null && user.getUserName().trim().length() > 0)
			{
			  body.append("RACT footy tipping username: "+user.getUserName()+FileUtil.NEW_LINE+FileUtil.NEW_LINE);
			  body.append("Favourite AFL team: "+user.getAflTeam()+FileUtil.NEW_LINE+FileUtil.NEW_LINE);				  
			}
			
			if (user instanceof BasicUser)
			{
				body.append("Given Names: "+user.getGivenNames()+FileUtil.NEW_LINE+FileUtil.NEW_LINE);
				body.append("Surname: "+user.getSurname()+FileUtil.NEW_LINE+FileUtil.NEW_LINE);
			}
			else if (user instanceof MemberUser)
			{
				MemberUser memUser = (MemberUser)user;
				if (memUser.getMemberCardNumber() != null && memUser.getMemberCardNumber().trim().length() > 0)
				{
					body.append("Member card number: "+memUser.getMemberCardNumber()+FileUtil.NEW_LINE+FileUtil.NEW_LINE);
					if (memUser.getActivationDate() != null)
					{
					  body.append("Member account activated: "+memUser.getActivationDate().formatMediumDate()+FileUtil.NEW_LINE+FileUtil.NEW_LINE);
					}
				}
			}

			message.setMessage(body.toString());
			
			message.setFrom(fromAddress);
			
  		mailMgr.sendMail(message);
	  }
		catch (Exception e)
  	{
		  e.printStackTrace();
		  throw new GenericException("Unable to email user details. "+e.getMessage());
	  }
		return user;
	}	
	
	
	/**
	 * Reset a users password
	 * @param userId The web user id
	 * @param length The length limit for the new random password
	 * @param notifyUser Flags whether the new password is emailed to the user
	 * @return A new web user object with updated password details.
	 */
	public User resetUserPassword(Integer userId, int length, boolean notifyUser)
  	throws GenericException
	{
		User user = getUser(userId);
		
		if (user == null)
		{
			throw new GenericException(MESSAGE_USER_DOES_NOT_EXIST);
		}
		
		//random password generator algorithm
		RandomPassword rp = new RandomPassword(length);
		String password = rp.getPassword();
		
		//update password on account
		user.setPassword(SecurityHelper.encryptPassword(password));
		
		//flag on user that password has been reset
		user.setPasswordReset(true);
		
		user.setLastUpdate(new DateTime());		
		
    if (notifyUser)
    {
  		//email user
		  try
	    {
	     	String bcc = ((SystemParameterVO)commonMgr.getSystemParameter(SourceSystem.MEMBERSHIP.getAbbreviation(), "BCCUSER")).getValue();
	     	String fromAddress = ((SystemParameterVO)commonMgr.getSystemParameter(SourceSystem.MEMBERSHIP.getAbbreviation(), "FROMUSER")).getValue();
		  	
  			MailMgr mailMgr = CommonEJBHelper.getMailMgr();
  			
  			MailMessage message = new MailMessage();
  			message.setRecipient(user.getEmailAddress());
  			message.setBcc(bcc);
  			message.setSubject(SUBJECT_EMAIL_PASSWORD_RESET);
  			message.setMessage(MESSAGE_EMAIL_PASSWORD_RESET+password);
  			message.setFrom(fromAddress);
  			
	  		mailMgr.sendMail(message);
		  }
  		catch (Exception e)
	  	{
		  	e.printStackTrace();
			  throw new GenericException("Unable to notify password change: "+e.getMessage());
		  }
    }
		return user;
	}
	
	public final static String MESSAGE_ACTIVATION_REQUEST_PROCESSED = "A member access account has already been registered and activated for that card number.";	
	
	/**
	 * Create a user account. A user can be nominated as a particular user type when
	 * it is created and also have the footy tipping preference set.
	 * @param userName The unique web user name
	 * @param password The user's password
	 * @param userType The type of user. Eg. Basic, Membership
	 * @param emailAddress The email address of the user
	 * @param clientNumber The RACT client number if they are an existing RACT customer.
	 * @param footyTipping Flags whether they are registered for footy tipping.
	 * 
	 */
	public User createUserAccount(User user)
    throws GenericException
	{
		LogUtil.debug(this.getClass(),"createUserAccount start");		
		LogUtil.debug(this.getClass(),"user="+user);
		if (user.getUserId() == null)
		{
			Integer userId = null;
			try
			{
				userId = sequenceMgr.getNextID(SEQUENCE_WEB_USER);
				LogUtil.debug(this.getClass(),"userId="+userId);
			}
			catch (RemoteException e)
			{
				throw new GenericException(e);
			}
			user.setUserId(userId);
		}
		
		if (user.getCreateDate() == null)
		{
		  user.setCreateDate(new DateTime());
		}
		if (user.getPasswordReset() == null)
		{
		  user.setPasswordReset(false);
		}
		if (user.getUserStatus() == null)
		{
		  user.setUserStatus(User.STATUS_ACTIVE);
		}
		em.persist(user);
    LogUtil.debug(this.getClass(),"createUserAccount end");    
		return user;
	}

	/**
	 * Has the user name already been used
	 * @param userName The user name to check if it exists
	 * @return Whether the user name is available or not
	 */
  public boolean userNameAvailable(String userName) throws GenericException
  {
  	LogUtil.debug(this.getClass(),"userNameAvailable start");  	
  	User user = getUserAccount(userName);
  	if (user != null)
  	{
    	LogUtil.debug(this.getClass(),"userNameAvailable end false");
  		return false;
  	}
  	else
  	{
    	LogUtil.debug(this.getClass(),"userNameAvailable end true");  		
  	  return true;
  	}
  }
	
  /**
   * Get a user account.
   * @param userName The user name of the user to retrieve
   * @return The retrieved user
   */
	public User getUser(Integer userId) throws GenericException
	{
		LogUtil.debug(this.getClass(),"getUser start");		
		User user = (User)em.find(User.class, userId);
		LogUtil.debug(this.getClass(),"getUser end");		
		return user;
	}
	
	@EJB
	private WebMembershipMgrLocal webMembershipMgrLocal;
	
	public User getUserByClientNumber(Integer clientNumber) throws GenericException
	{
		LogUtil.debug(this.getClass(),"getUserByClientNumber start");
		LogUtil.debug(this.getClass(),"clientNumber="+clientNumber);		
		User user = null;
    Query query = em.createQuery("select e from MemberUser e where e.clientNumber = ?1 and e.userStatus = ?2 and e.activationDate is not null");
    query.setParameter(1, clientNumber.toString());
    query.setParameter(2, User.STATUS_ACTIVE);
    Collection<User> list = query.getResultList();
    if (list.size() > 1)
    {
    	throw new GenericException("There is more than one active account registered with entered client number.");
    }
    else if (list.size() == 1)
    {
    	user = list.iterator().next();
    }
    LogUtil.debug(this.getClass(),"user="+user);
		LogUtil.debug(this.getClass(),"getUserByClientNumber end");    
    return user;
	}	
	
	public User getUserByCardNumber(String membershipCardNumber) throws GenericException	
	{
		return getUserByCardNumber(membershipCardNumber, false);
	}
	
	public User getUserByCardNumber(String membershipCardNumber, boolean updateCard) throws GenericException
	{
		LogUtil.debug(this.getClass(),"getUserByCardNumber start");			
		LogUtil.debug(this.getClass(),"membershipCardNumber="+membershipCardNumber);		
		LogUtil.debug(this.getClass(),"updateCard="+updateCard);		
		User user = null;
    Query query = em.createQuery("select e from MemberUser e where e.memberCardNumber = ?1 and e.userStatus = ?2");
    query.setParameter(1, membershipCardNumber);
    query.setParameter(2, User.STATUS_ACTIVE);
    Collection<User> list = query.getResultList();
    if (list.size() > 1)
    {
    	throw new GenericException("There is more than one active account registered with entered card number.");
    }
    else if (list.size() == 1)
    {
    	user = list.iterator().next();
    }
    
    if (user == null &&
    		updateCard)
    {
			MemberUser currentUser = null;
  		try
			{
				ClientMgr clientMgr = ClientEJBHelper.getClientMgr();
				ClientVO client = clientMgr.getClientByMembershipCardNumber(membershipCardNumber);
				if (client != null)
				{
					//get user by client number
					currentUser = (MemberUser)getUserByClientNumber(client.getClientNumber());
					if (currentUser != null &&
							!currentUser.getMemberCardNumber().equals(membershipCardNumber)) //ensure it matches entered card number
					{
						//no match
            currentUser.setMemberCardNumber(membershipCardNumber);
            updateUserAccount(currentUser);
						LogUtil.log(this.getClass(),"no match");
            return currentUser;
					}
					else
					{
						//no match
						LogUtil.log(this.getClass(),"match - no action required");	
					}
				}
			}
			catch (RemoteException e)
			{
				throw new GenericException(e);
			}
    }
		LogUtil.debug(this.getClass(),"getUserByCardNumber end");    
    return user;
	}
	
	public Collection getUsersByEmailAddress(String emailAddress) throws GenericException
	{
		LogUtil.debug(this.getClass(),"getUsersByEmailAddress start");
		LogUtil.debug(this.getClass(),"emailAddress="+emailAddress);		
    Query query = em.createQuery("select e from User e where e.emailAddress = ?1 and e.userStatus = ?2 and (e.activationDate is not null or e.userName is not null)");
    query.setParameter(1, emailAddress);
    query.setParameter(2, User.STATUS_ACTIVE);
    Collection<User> list = query.getResultList();
    LogUtil.debug(this.getClass(),"getUsersByEmailAddress end");    
    return list;
	}	
	
	public void mergeDuplicateUserAccounts(String emailAddress) throws Exception
	{
		LogUtil.log(this.getClass(), "mergeDuplicateUserAccounts start");
		
		//get list of users grouped on email address where there is more than one active record.
		
		String queryString = "select e from User e where e.emailAddress in (select u.emailAddress from User u where u.userStatus = ?1 group by u.emailAddress having count(u.userId) > 1) ";
		if (emailAddress != null)
		{
			queryString += "where e.emailAddress = ?2 ";
		}
		queryString += "order by e.emailAddress desc, e.createDate desc";
		
		Query query = em.createQuery(queryString);
    query.setParameter(1, User.STATUS_ACTIVE);
    if (emailAddress != null)
    {
      query.setParameter(1, emailAddress);
    }
    Collection<User> list = query.getResultList();
    
    LogUtil.log(this.getClass(), "records="+list.size());
    
    User ref = null;
    User tmp = null;
    
    boolean updatedRef = false;
    
    int counter = 0;
    
		//get the most recent user record
		for (Iterator<User> i = list.iterator();i.hasNext();)
		{
			counter++;
			
			LogUtil.debug(this.getClass(), "counter="+counter);
			
      //get the user record
			tmp = i.next();
			
	    LogUtil.debug(this.getClass(), "tmp="+tmp);			
			LogUtil.debug(this.getClass(), "tmp.getEmailAddress()="+tmp.getEmailAddress());
			
			LogUtil.debug(this.getClass(), "updatedRef="+updatedRef);
	    
			LogUtil.debug(this.getClass(), "ref="+ref);			
			
			//check if email address has changed
			if (ref != null &&
				 !tmp.getEmailAddress().equals(ref.getEmailAddress()))
			{
				LogUtil.debug(this.getClass(), "change of email");	
				//email address has changed - need a new ref
				ref = null;
				updatedRef = false;
			}	
			
			LogUtil.debug(this.getClass(), "ref="+ref);
			
			//first is reference copy - what type is it?
			if (ref == null &&
					(tmp != null && (tmp.getUserName() != null && tmp.getUserName().trim().length() > 0 || tmp.getActivationDate() != null)))
			{
				
			  //keep this reference copy
				ref = tmp;
			}
			
			LogUtil.debug(this.getClass(), "ref="+ref);					
			
			if (ref != null) //others 
			{
				LogUtil.debug(this.getClass(), "ref not null");
				if (tmp != null)
				{
					
					LogUtil.debug(this.getClass(), "tmp not null");					
					
					//have we just set this?
					if (!tmp.equals(ref))
					{
					
						LogUtil.debug(this.getClass(), "not the same");						
						
						if (!updatedRef)
						{
							
							LogUtil.debug(this.getClass(), "!updatedRef");						
							
							//others
							if (ref instanceof MemberUser) //ref not null
							{
								LogUtil.debug(this.getClass(), "ref is a member user");							
								
								//if ref a member user only try to get footy tipping details copied across
								if ((ref.getUserName() == null || ref.getUserName().trim().length() == 0) &&
										 tmp.getUserName() != null && tmp.getUserName().trim().length() > 0)
								{
									LogUtil.debug(this.getClass(), "setting footy tipping attributes");								
																	
									ref.setUserName(tmp.getUserName());
								  ref.setFootyTipping(tmp.getFootyTipping());
									ref.setApproved(tmp.getApproved());
								  ref.setAflTeam(tmp.getAflTeam());
									ref.setSflTeam(tmp.getSflTeam());									
									
								}
								
								if (ref.getActivationDate() == null && tmp.getActivationDate() != null)
								{
									LogUtil.debug(this.getClass(), "setting activation date");
									
									ref.setActivationDate(tmp.getActivationDate());
								}
								
							}
							else //basic user - footy tipping only
							{
								
								LogUtil.debug(this.getClass(), "ref is a basic user");	
								
								//any member details on any of the other records?
								if (tmp instanceof MemberUser)
								{
									LogUtil.debug(this.getClass(), "tmp is a member user");									
									
									//only copy if activated
									if (tmp.getActivationDate() != null)
									{
										
										User refCopy = ref;										

										LogUtil.debug(this.getClass(), "tmp="+tmp);													
										LogUtil.debug(this.getClass(), "before ref="+ref);										
										
										removeUserAccount(ref.getUserId());										
																												
										LogUtil.debug(this.getClass(), "refCopy="+refCopy);
										
										//member user is currently basic
										ref = new MemberUser();
										
										//copy attributes form the reference copy where possible
										
										//retain user id
		                ref.setUserId(refCopy.getUserId());
		                
										LogUtil.debug(this.getClass(), "yyy");
		                //member details come from the current record
										if (((MemberUser)tmp).getMemberCardNumber() != null)
										{
											LogUtil.debug(this.getClass(), "setting member details");
											LogUtil.debug(this.getClass(), "setting member details");											
											
											((MemberUser)ref).setClientNumber(((MemberUser)tmp).getClientNumber());
											((MemberUser)ref).setMemberCardNumber(((MemberUser)tmp).getMemberCardNumber());						
											ref.setActivationDate(tmp.getActivationDate());
										}                
		                
										//retain generic details
										ref.setCreateDate(refCopy.getCreateDate());
										ref.setEmailAddress(refCopy.getEmailAddress());
										ref.setLastLoggedIn(refCopy.getLastLoggedIn());
										ref.setPasswordReset(refCopy.getPasswordReset());
										ref.setLastPasswordChange(refCopy.getLastPasswordChange());
										ref.setLastUpdate(new DateTime());
										ref.setPassword(refCopy.getPassword());
										ref.setSurname(refCopy.getSurname());
										ref.setGivenNames(refCopy.getGivenNames());
										
										ref.setHomePhone(refCopy.getHomePhone());
										ref.setWorkPhone(refCopy.getWorkPhone());
										ref.setMobilePhone(refCopy.getMobilePhone());
										
										ref.setResiPostcode(refCopy.getResiPostcode());

										ref.setUserName(refCopy.getUserName());
									  ref.setFootyTipping(refCopy.getFootyTipping());
										ref.setApproved(refCopy.getApproved());
									  ref.setAflTeam(refCopy.getAflTeam());
										ref.setSflTeam(refCopy.getSflTeam());				
										
									  
										LogUtil.debug(this.getClass(), "xxx"+refCopy.getUserName());
										LogUtil.debug(this.getClass(), "xxx"+tmp.getUserName());		
										LogUtil.debug(this.getClass(), "xxx"+ref.getUserName());										
										
										//use footy tipping details
										if (refCopy.getUserName() == null && tmp.getUserName() != null)
										{
											LogUtil.debug(this.getClass(), "setting footy tipping details");
											ref.setUserName(tmp.getUserName());
										  ref.setFootyTipping(tmp.getFootyTipping());
											ref.setApproved(tmp.getApproved());
										  ref.setAflTeam(tmp.getAflTeam());
											ref.setSflTeam(tmp.getSflTeam());
										}
										
										createUserAccount(ref);
										
										LogUtil.debug(this.getClass(), "after ref="+ref);
										
										updatedRef = true;
									}
									else
									{
										//already a footy tipping record so ignore.
									}
								}
								
							}
						
							//update ref once
							if (!updatedRef)
							{
								LogUtil.debug(this.getClass(), "updating ref "+ref);
								updateUserAccount(ref);
							  updatedRef = true;
							}									
						}
						
            if (!tmp.equals(ref))
            {
						  try
							{
								//disable all non-ref account
								LogUtil.debug(this.getClass(), "disabling tmp "+tmp);						
								disableUserAccount(tmp.getUserId());
							}
							catch (Exception e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}						
						}
					
					}
						
				} //tmp is not null				
			}	
			else
			{
			  try
				{
			  	if (tmp != null)
			  	{
						//disable all non-ref account
						LogUtil.debug(this.getClass(), "disabling tmp "+tmp);						
						disableUserAccount(tmp.getUserId());
			  	}
				}
				catch (Exception e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}							
			}
						
		}
		
		LogUtil.log(this.getClass(), "mergeDuplicateUserAccounts end");			
		
		
	}
	
	public User getUserAccount(String userName) throws GenericException
	{
		LogUtil.debug(this.getClass(),"getUserAccount start");
		LogUtil.debug(this.getClass(),"userName="+userName);			
		User user = null;
    Query query = em.createQuery("select e from User e where e.userName = ?1 and e.userStatus = ?2");
    query.setParameter(1, userName);
    query.setParameter(2, User.STATUS_ACTIVE);
    Collection<User> list = query.getResultList();
    if (list.size() > 1)
    {
    	throw new GenericException("There is more than one active account registered with entered username.");
    }
    else if (list.size() == 1)
    {
    	user = list.iterator().next();
    }
    LogUtil.debug(this.getClass(),"getUserAccount end");    
    return user;
	}	
	
	public void updateUserClientNumber(Integer fromClient, Integer toClient) throws GenericException
	{
		Query query = em.createQuery("from User e where e.clientNumber = ?1");
		query.setParameter(1, fromClient);
		List<User> list = query.getResultList();
		for (User user: list)
		{
			((MemberUser) user).setClientNumber(toClient.toString());
			updateUserAccount(user);
		}
	}
		
	/**
	 * Update the user account with the supplied user details. The password needs to be
	 * encrypted before sending to this method!
	 * @param user the user object to update
	 * @return the user version from the database
	 */
	public User updateUserAccount(User user)
    throws GenericException	
	{
    LogUtil.debug(this.getClass(),"updateUserAccount start");
    LogUtil.debug(this.getClass(),"user="+user);
		user.setLastUpdate(new DateTime());
		em.merge(user);
    LogUtil.debug(this.getClass(),"updateUserAccount end");		
		return user;
	}
	
	/**
	 * Unapprove a user name for footy tipping.
	 * @param userName The user name to unapprove
	 * @return The unapproved user object
	 * @throws GenericException
	 */
	public User unapproveUserName(String userName) throws GenericException
	{
		User user = getUserAccount(userName);
		if (!user.isApproved())
		{
			throw new GenericException("User name is already unapproved.");
		}
		user.setApproved(false);
		em.persist(user);
		return user;
	}	
	
	public void activateUser(Integer activationRequestNumber) throws GenericException
	{
		ActivationRequest ar = em.find(ActivationRequest.class, activationRequestNumber);
		if (ar == null)
		{
			throw new GenericException("Activation request does not exist for request number "+activationRequestNumber);
		}
		else if (!ar.isCreated())
		{
			throw new GenericException("Attempting to activate a failed activation request for request number "+activationRequestNumber);			
		} 
		else
		{
			if (ar.getActivationDate() != null)
			{
				throw new GenericException("Account already activated.");
			}
			
			DateTime now = new DateTime();
			
			//update activation request
			ar.setActivationDate(now);
			em.merge(ar);
			
			//update user
			User user = getUser(ar.getUserId());
			if (!user.isActive())
			{
				throw new GenericException("The user account is no longer active. You will need to register again.");
			}
			user.setActivationDate(now);
			updateUserAccount(user);			
		}
		
	}
	
	/**
	 * Approve a user name for footy tipping.
	 * @param userName The user name to approve
	 * @return The approved user object
	 * @throws GenericException
	 */
	public User approveUserName(String userName) throws GenericException
	{
		User user = getUserAccount(userName);
		if (user.isApproved())
		{
			throw new GenericException("User name is already approved.");
		}
		user.setApproved(true);
		em.persist(user);
		return user;
	}
	
	/**
	 * Disable the old user name and transfer existing details to a new user name. This is 
	 * typically used when a user name is not given approval.
	 * @param fromUserName The original user name
	 * @param toUserName The new user name
	 * @return The new user
	 */
	/*
	public User changeUserName(String fromUserName, String toUserName) throws GenericException
	{
		if (fromUserName.equals(toUserName))
		{
			throw new GenericException("From and to user name are the same. "+fromUserName+" "+toUserName);
		}
		
		if (!userNameAvailable(toUserName))
		{
			throw new GenericException("To user name already exists.");
		}
		//disable existing user		
    User fromUser = getUserAccount(fromUserName);
    
    //TODO ensure that web tier recognises this. 
    disableUserAccount(fromUser.getUserId());

		//create new user
    
    //consider a copy function if more than one usage
    User toUser = UserFactory.getUser(fromUser.getUserType());
    toUser.setUserName(toUserName);
    toUser.setPassword(fromUser.getPassword());
    toUser.setUserStatus(User.STATUS_ACTIVE);
    toUser.setSurname(fromUser.getSurname());
    toUser.setGender(fromUser.getGender());
    toUser.setFirstName(fromUser.getFirstName());
    toUser.setBirthDate(fromUser.getBirthDate());
    toUser.setCreateDate(new DateTime());   
    toUser.setFootyTipping(fromUser.getFootyTipping());
    toUser.setHomePhone(fromUser.getHomePhone());
    toUser.setLastUpdate(null);
    toUser.setPasswordReset(false);
    toUser.setEmailAddress(fromUser.getEmailAddress());
    toUser.setAddressLine1(fromUser.getAddressLine1());
    toUser.setAddressLine2(fromUser.getAddressLine2());
    toUser.setState(fromUser.getState());
    toUser.setPostcode(fromUser.getPostcode());
    toUser.setCountry(fromUser.getCountry());    
    
    //set member specific attributes
    if (fromUser instanceof MemberUser &&
    		toUser instanceof MemberUser)
    {
    	//set the RACT client number
    	((MemberUser)toUser).setClientNumber(((MemberUser)fromUser).getClientNumber());
    }
    
    em.persist(toUser);
		
    return toUser;
	}
	*/
	
	/**
	 * @todo Validation required to be exposed to client 
	 * @param user
	 * @param password
	 */
	public User changeUserPassword(User user, String password)
	  throws GenericException
	{
		if (password != null)
		{
			
			String encryptedPassword = SecurityHelper.encryptPassword(password);
			
			if (encryptedPassword.equals(user.getPassword()))
			{
				throw new GenericException("Old and new passwords are the same.");
			}
			
		  user.setPassword(encryptedPassword);
		  user.setPasswordReset(false);

		  //update last password change
		  user.setLastPasswordChange(new DateTime());
		  
		  em.merge(user);
		}
		return user;
	}
	
	/**
	 * Remove a user account from the database. This would
	 * be an admin function. Normally disable account would be used.
	 */
	public void removeUserAccount(Integer userId)
    throws GenericException	
	{ 
    User user = getUser(userId);
		if (user != null)
		{ 
		  em.remove(user);
		}
	}
	
	/**
	 * Disable a user account by updating the status to disabled.
	 * to be disabled
	 * @param userName
	 */
	public User disableUserAccount(Integer userId)
    throws GenericException	
	{
    User user = getUser(userId);
		if (user != null)
		{ 
			user.setUserStatus(User.STATUS_DISABLED);
			user.setLastUpdate(new DateTime());			
      em.persist(user);
		}
		return user;
	}

	/**
	 * Enable a user account after it has been disabled.
	 * @param userName
	 */
	public User enableUserAccount(Integer userId)
  throws GenericException	
	{
    User user = getUser(userId);
		if (user != null && 
				User.STATUS_DISABLED.equals(user.getUserStatus()))
		{ 
			user.setUserStatus(User.STATUS_ACTIVE);
			user.setLastUpdate(new DateTime());			
      em.merge(user);
		}
		return user;		
	}
	
	/**
	 * Checks whether an entered password matches the password stored against
	 * the nominated user name.
	 */
	public boolean passwordsMatch(String enteredPassword, Integer userId)
    throws GenericException	
	{
		boolean passwordsMatch = false;
		User user = getUser(userId);
		LogUtil.debug(this.getClass(), "passwordsMatch user="+user);
		if (user != null)
		{
			LogUtil.debug(this.getClass(), "enteredPassword="+enteredPassword);			
			String encryptedPassword = SecurityHelper.encryptPassword(enteredPassword);
			LogUtil.debug(this.getClass(), "enc="+encryptedPassword);
			LogUtil.debug(this.getClass(), "sto="+user.getPassword());			
			if (encryptedPassword.equals(user.getPassword()))
			{
				passwordsMatch = true;		
			}
		}
		LogUtil.debug(this.getClass(), "passwordsMatch="+passwordsMatch);		
		return passwordsMatch; 
	}

	@Override
	public void deleteActivationRequests(Integer userId) throws GenericException
	{
		LogUtil.debug(this.getClass(), "userId="+userId);
		Query query = em.createQuery(HSQL_ACTIVATION_REQUEST_BY_USERID).setParameter(1, userId);
		List activationRequestList = query.getResultList();
		ActivationRequest ar = null;
    for (Iterator i = activationRequestList.iterator(); i.hasNext();)
    {
    	ar = (ActivationRequest)i.next();
    	em.remove(ar);
    }		
	}
	
	/**
	 * Retrieve an ActivationRequest by requestNumber.
	 * @param requestNumber Unique request number.
	 */
	@Override
	public ActivationRequest getActivationRequest(Integer requestNumber) throws GenericException {
		return this.em.find(ActivationRequest.class, requestNumber);
	}
	
}
