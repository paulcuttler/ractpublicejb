package com.ract.web.security;

import java.security.MessageDigest;

import com.ract.common.GenericException;

import sun.misc.BASE64Encoder;

public class SecurityHelper
{

	/*
	 * Encrypt password.
	 */
	public static String encryptPassword(String password) throws GenericException
	{
    String hash = null;
		try
		{
			MessageDigest md = MessageDigest.getInstance("SHA");
			md.update(password.getBytes("UTF-8"));
			System.out.println("md="+md.toString());
			byte[] raw = md.digest(); 			
			hash = (new BASE64Encoder()).encode(raw);
		}
		catch (Exception e)
		{
			throw new GenericException(e);
		}
		System.out.println("hash="+hash);		
		return hash;
	}

}
