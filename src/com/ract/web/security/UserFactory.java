package com.ract.web.security;

import com.ract.common.GenericException;

public class UserFactory
{

	public static User getUser(String userType)
	  throws GenericException
	{
		if (User.TYPE_BASIC.equals(userType))
		{
			return new BasicUser();
		}
		else if (User.TYPE_MEMBER.equals(userType))
		{
			return new MemberUser();
		}
		else
		{
			throw new GenericException("User type "+userType+" is not recognised.");
		}
	}
	
}
