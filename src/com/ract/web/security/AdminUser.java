package com.ract.web.security;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Admin")
public class AdminUser extends User
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7689668568155314504L;

}
