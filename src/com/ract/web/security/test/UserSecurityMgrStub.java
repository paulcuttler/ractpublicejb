package com.ract.web.security.test;

import java.util.Collection;

import com.ract.common.GenericException;
import com.ract.util.DateTime;
import com.ract.web.client.ActivationRequest;
import com.ract.web.security.BasicUser;
import com.ract.web.security.LoginAudit;
import com.ract.web.security.User;
import com.ract.web.security.UserSecurityMgrRemote;
import com.ract.web.security.UserSession;

public class UserSecurityMgrStub implements UserSecurityMgrRemote
{

	@Override
	public LoginAudit createLoginAudit(LoginAudit loginAudit) throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUserByClientNumber(Integer clientNumber)
			throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection getUsersByEmailAddress(String emailAddress)
			throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User emailUserDetails(Integer userId) throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void mergeDuplicateUserAccounts(String emailAddress) throws Exception
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public User createUserAccount(User user) throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUserByCardNumber(String membershipCardNumber,
			boolean updateCard) throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User approveUserName(String userName) throws GenericException
	{
    User user = new BasicUser();
    user.setApproved(true);
		return user;

	}

	@Override
	public void activateUser(Integer activationRequestNumber)
			throws GenericException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public User getUserByCardNumber(String membershipCardNumber)
			throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateUsersWithUserId() throws GenericException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public User unapproveUserName(String userName) throws GenericException
	{
    User user = new BasicUser();
    user.setApproved(false);
		return user;
	}

	@Override
	public User changeUserPassword(User user, String password)
	{
		user.setPassword(password);
		user.setPasswordReset(true);
		return user;
	}

	@Override
	public void deleteActivationRequests(Integer userId) throws GenericException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public User getUser(Integer userId)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean userNameAvailable(String userName)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public UserSession getUserSession(String sessionId)
	{
		UserSession userSession = new UserSession();
		userSession.setSessionId(sessionId);
		return userSession;
	}

	@Override
	public UserSession createUserSession(String sessionId, String ipAddress,
			Integer userId, String initialContext) throws GenericException
	{
		UserSession userSession = new UserSession();
		userSession.setSessionId(sessionId);
		userSession.setIpAddress(ipAddress);
		userSession.setUserId(userId);
		userSession.setInitialContext(initialContext);
		return userSession;
	}

	@Override
	public User disableUserAccount(Integer userId)
	{
		User user = getUser(userId);
		user.setUserStatus(User.STATUS_DISABLED);
		return user;
	}

	@Override
	public User enableUserAccount(Integer userId)
	{
		User user = getUser(userId);
		user.setUserStatus(User.STATUS_ACTIVE);
		return user;
	}

	@Override
	public User getUserAccount(String userName)
	{
		User user = new BasicUser();
		user.setUserName(userName);		
		return user;
	}

	@Override
	public boolean passwordsMatch(String enteredPassword, Integer userId)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void removeUserAccount(Integer userName)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public ActivationRequest requestMemberAccountActivation(Integer userId,
			String membershipCardNumber, String surname,
			String postcode, String phoneNumber, String mobilePhoneNumber, DateTime dateOfBirth, String updateEmailAddress, String activationURL)
	  throws GenericException
	{
		ActivationRequest activationRequest = new ActivationRequest();
		activationRequest.setMembershipCardNumber(membershipCardNumber);
		activationRequest.setUserId(userId);
		activationRequest.setSurname(surname);
		activationRequest.setPostcode(postcode);
		activationRequest.setPhoneNumber(phoneNumber);
		activationRequest.setMobilePhoneNumber(mobilePhoneNumber);
		activationRequest.setDateOfBirth(dateOfBirth);
		activationRequest.setRequestNumber(1);
		// TODO Auto-generated method stub
		return new ActivationRequest();
	}

	@Override
	public ActivationRequest getActivationRequest(Integer requestNumber) throws GenericException {
		return new ActivationRequest();
	}

	@Override
	public User resetUserPassword(Integer userId, int length, boolean notifyUser)
			throws GenericException
	{
		// TODO Auto-generated method stub
		User user = new BasicUser();
		user.setUserId(userId);
		user.setPasswordReset(true);
		return user;
	}

	@Override
	public User updateUserAccount(User user)
	{
		return user;
	}

	@Override
	public void updateUserClientNumber(Integer fromClient, Integer toClient)
			throws GenericException {
		// TODO Auto-generated method stub
		
	}


}
