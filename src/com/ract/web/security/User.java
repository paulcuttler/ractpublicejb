package com.ract.web.security;

import java.io.*;
import javax.persistence.*;

import org.hibernate.annotations.DiscriminatorFormula;
import com.ract.util.*;

@Entity
@Table(name="Users") //User is reserved word
@DiscriminatorFormula("userType")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public abstract class User implements Serializable 
{
	
	public User()
	{
		setUserStatus(User.STATUS_ACTIVE);
	}
	
	public Boolean getApproved()
	{
		return approved;
	}

	public void setApproved(Boolean approved)
	{
		this.approved = approved;
	}

	public Boolean getPasswordReset()
	{
		return passwordReset;
	}

	public void setPasswordReset(Boolean passwordReset)
	{
		this.passwordReset = passwordReset;
	}

	public final static String TYPE_BASIC = "Basic";
	
	public final static String TYPE_MEMBER = "Member";	
	
	public final static String TYPE_ADMIN = "Admin";		

	public final static String STATUS_ACTIVE = "A";	

	public final static String STATUS_DISABLED = "D";
	
	public final static String GENDER_MALE = "M";
	
	public final static String GENDER_FEMALE = "F";

  private String userStatus;
  
	private Boolean approved = false;
  
  private DateTime activationDate;
	
  public DateTime getActivationDate()
	{
		return activationDate;
	}

	public void setActivationDate(DateTime activationDate)
	{
		this.activationDate = activationDate;
	}
	
	@Id
  private Integer userId;
  
	public Integer getUserId()
	{
		return userId;
	}

	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	
	private DateTime lastLoggedIn;
	
	private DateTime lastPasswordChange;
	
	public DateTime getLastLoggedIn()
	{
		return lastLoggedIn;
	}

	public void setLastLoggedIn(DateTime lastLoggedIn)
	{
		this.lastLoggedIn = lastLoggedIn;
	}

	public DateTime getLastPasswordChange()
	{
		return lastPasswordChange;
	}

	public void setLastPasswordChange(DateTime lastPasswordChange)
	{
		this.lastPasswordChange = lastPasswordChange;
	}

	private String userName;

	private String password;
   
	private String userType;

  private DateTime createDate; 
  
  private String givenNames;
  
	private String surname;
	private String gender;
	private DateTime birthDate;
	private String homePhone;
  private String mobilePhone;
  
  private String workPhone;
  
  private String aflTeam;
  
  private String sflTeam;

  private Boolean passwordReset;
  
  private String title;
  
  private String resiCountry; //Australia
  private String resiPostcode; //7000
  private String resiProperty; //"The Elms"
  private String resiSuburb; //North Hobart
  public String getGivenNames()
	{
		return givenNames;
	}

	public void setGivenNames(String givenNames)
	{
		this.givenNames = givenNames;
	}

	public String getMobilePhone()
	{
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone)
	{
		this.mobilePhone = mobilePhone;
	}

	public String getWorkPhone()
	{
		return workPhone;
	}

	public void setWorkPhone(String workPhone)
	{
		this.workPhone = workPhone;
	}

	public String getAflTeam()
	{
		return aflTeam;
	}

	public void setAflTeam(String aflTeam)
	{
		this.aflTeam = aflTeam;
	}

	public String getSflTeam()
	{
		return sflTeam;
	}

	public void setSflTeam(String sflTeam)
	{
		this.sflTeam = sflTeam;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getResiCountry()
	{
		return resiCountry;
	}

	public void setResiCountry(String resiCountry)
	{
		this.resiCountry = resiCountry;
	}

	public String getResiPostcode()
	{
		return resiPostcode;
	}

	public void setResiPostcode(String resiPostcode)
	{
		this.resiPostcode = resiPostcode;
	}

	public String getResiProperty()
	{
		return resiProperty;
	}

	public void setResiProperty(String resiProperty)
	{
		this.resiProperty = resiProperty;
	}

	public String getResiSuburb()
	{
		return resiSuburb;
	}

	public void setResiSuburb(String resiSuburb)
	{
		this.resiSuburb = resiSuburb;
	}

	public String getResiState()
	{
		return resiState;
	}

	public void setResiState(String resiState)
	{
		this.resiState = resiState;
	}

	public String getResiStreet()
	{
		return resiStreet;
	}

	public void setResiStreet(String resiStreet)
	{
		this.resiStreet = resiStreet;
	}

	public String getResiStreetChar()
	{
		return resiStreetChar;
	}

	public void setResiStreetChar(String resiStreetChar)
	{
		this.resiStreetChar = resiStreetChar;
	}

	private String resiState; //Tasmania
  private String resiStreet; //Elizabeth St
  private String resiStreetChar; //unit 1, 23
	
	public boolean isActive()
	{
		return STATUS_ACTIVE.equals(userStatus);
	}

	public DateTime getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(DateTime createDate)
	{
		this.createDate = createDate;
	}

	public String getUserType()
	{
		return userType;
	}

  private String emailAddress;

  private Boolean footyTipping; 
  
  private DateTime lastUpdate;
  
	public DateTime getLastUpdate()
	{
		return lastUpdate;
	}

	public void setLastUpdate(DateTime lastUpdate)
	{ 
		this.lastUpdate = lastUpdate;
	}

	public Boolean getFootyTipping()
	{
		return footyTipping;
	}

	public void setFootyTipping(Boolean footyTipping)
	{
		this.footyTipping = footyTipping;
	}

	public String getEmailAddress()
	{
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress)
	{
		this.emailAddress = emailAddress;
	}

	public void setUserType(String userType)
	{
		this.userType = userType;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public boolean isPasswordReset()
	{
		return passwordReset;
	}

	public String getUserStatus()
	{
		return userStatus;
	}

	public void setUserStatus(String userStatus)
	{
		this.userStatus = userStatus;
	}

	public String getSurname()
	{
		return surname;
	}

	public void setSurname(String surname)
	{
		this.surname = surname;
	}

	public String getGender()
	{
		return gender;
	}

	public void setGender(String gender)
	{
		this.gender = gender;
	}

	public DateTime getBirthDate()
	{
		return birthDate;
	}

	public void setBirthDate(DateTime dateOfBirth)
	{
		this.birthDate = dateOfBirth;
	}

	public String getHomePhone()
	{
		return homePhone;
	}

	public void setHomePhone(String homePhone)
	{
		this.homePhone = homePhone;
	}

	@Override
	public String toString()
	{
		return "User [activationDate=" + activationDate + ", aflTeam=" + aflTeam + ", approved=" + approved + ", birthDate=" + birthDate + ", createDate=" + createDate + ", emailAddress=" + emailAddress
				+ ", footyTipping=" + footyTipping + ", gender=" + gender + ", givenNames=" + givenNames + ", homePhone=" + homePhone + ", lastLoggedIn=" + lastLoggedIn + ", lastPasswordChange="
				+ lastPasswordChange + ", lastUpdate=" + lastUpdate + ", mobilePhone=" + mobilePhone + ", password=" + password + ", passwordReset=" + passwordReset + ", resiCountry=" + resiCountry
				+ ", resiPostcode=" + resiPostcode + ", resiProperty=" + resiProperty + ", resiState=" + resiState + ", resiStreet=" + resiStreet + ", resiStreetChar=" + resiStreetChar + ", resiSuburb="
				+ resiSuburb + ", sflTeam=" + sflTeam + ", surname=" + surname + ", title=" + title + ", userId=" + userId + ", userName=" + userName + ", userStatus=" + userStatus + ", userType=" + userType
				+ ", workPhone=" + workPhone + "]";
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof User))
			return false;
		User other = (User) obj;
		if (userId == null)
		{
			if (other.userId != null)
				return false;
		}
		else if (!userId.equals(other.userId))
			return false;
		return true;
	}

	public boolean isApproved()
	{
		return approved;
	}

	public void setApproved(boolean approved)
	{
		this.approved = approved;
	}
	
}
