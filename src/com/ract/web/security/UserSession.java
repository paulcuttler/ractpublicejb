package com.ract.web.security;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

/**
 * An audit object representing the client and the session. Created after a user has successfully logged in.
 * @author hollidayj
 */

@Entity
public class UserSession implements Serializable
{

	//session scoped
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8965534161945429868L;

	@Id
	private String sessionId;
	
	private Date createDate;
	
	private Integer userId;
	
	//getUser
	
	public Integer getUserId()
	{
		return userId;
	}

	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	private String ipAddress;
	
	/**
	 * Context that user initally logged in from. eg. Footy tipping, etc.
	 */
	private String initialContext;

	public String getSessionId()
	{
		return sessionId;
	}

	public void setSessionId(String sessionId)
	{
		this.sessionId = sessionId;
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}

	public String getIpAddress()
	{
		return ipAddress;
	}

	public void setIpAddress(String ipAddress)
	{
		this.ipAddress = ipAddress;
	}

	public String getInitialContext()
	{
		return initialContext;
	}

	public void setInitialContext(String initialContext)
	{
		this.initialContext = initialContext;
	}
	
	public String toString()
	{
		StringBuffer desc = new StringBuffer();
		desc.append(sessionId);
		desc.append(":");
		desc.append(userId);
		return desc.toString();
	}
	
}
