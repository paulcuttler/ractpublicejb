package com.ract.web.security;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.ract.util.DateTime;

@Entity
public class LoginAudit implements Serializable
{
	@Id
  private Integer auditId;
	
	@Override
	public String toString()
	{
		return "LoginAudit [auditId=" + auditId + ", createDate=" + createDate + ", failReason=" + failReason + ", ipAddress=" + ipAddress + ", loginFailure=" + loginFailure + ", memberCardNumber="
				+ memberCardNumber + ", userId=" + userId + ", userName=" + userName + "]";
	}

	public Integer getAuditId()
	{
		return auditId;
	}

	public void setAuditId(Integer auditId)
	{
		this.auditId = auditId;
	}

	public DateTime getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(DateTime createDate)
	{
		this.createDate = createDate;
	}

	public Integer getUserId()
	{
		return userId;
	}

	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getMemberCardNumber()
	{
		return memberCardNumber;
	}

	public void setMemberCardNumber(String memberCardNumber)
	{
		this.memberCardNumber = memberCardNumber;
	}

	public boolean isLoginFailure()
	{
		return loginFailure;
	}

	public void setLoginFailure(boolean loginFailure)
	{
		this.loginFailure = loginFailure;
	}

	public String getFailReason()
	{
		return failReason;
	}

	public void setFailReason(String failReason)
	{
		this.failReason = failReason;
	}

	private DateTime createDate;
	
	private Integer userId;
	
	private String userName;
	
	private String memberCardNumber;
	
	private boolean loginFailure;
	
	private String failReason;
	
	private String ipAddress;

	public String getIpAddress()
	{
		return ipAddress;
	}

	public void setIpAddress(String ipAddress)
	{
		this.ipAddress = ipAddress;
	}
	
}
