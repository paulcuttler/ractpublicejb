package com.ract.web.security;

import javax.persistence.*;

@Entity
@DiscriminatorValue("Basic")
public class BasicUser extends User
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 240316014698232518L;

	public BasicUser()
	{
		setUserType(User.TYPE_BASIC);		
	}

}
