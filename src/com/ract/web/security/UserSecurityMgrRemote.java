package com.ract.web.security;

import java.util.Collection;

import javax.ejb.*;

import com.ract.util.DateTime;
import com.ract.web.client.*;
import com.ract.common.GenericException;

@Remote
public interface UserSecurityMgrRemote
{
	
	public User createUserAccount(User user)
    throws GenericException;

	public LoginAudit createLoginAudit(LoginAudit loginAudit) throws GenericException;	
	
	public void activateUser(Integer activationRequestNumber) throws GenericException;	
	
	public User getUserByCardNumber(String membershipCardNumber, boolean updateCard) throws GenericException;	
	
	public boolean passwordsMatch(String enteredPassword, Integer userId) throws GenericException;
	
	public User getUserByClientNumber(Integer clientNumber) throws GenericException;	
	
	public User resetUserPassword(Integer userId, int length, boolean notifyUser)
	  throws GenericException;	
	
	public Collection getUsersByEmailAddress(String emailAddress) throws GenericException;
	
	public void updateUsersWithUserId() throws GenericException;
	
	public User getUserByCardNumber(String membershipCardNumber) throws GenericException;
	
	public UserSession getUserSession(String sessionId);	
	
	public void removeUserAccount(Integer userId) throws GenericException;

	public ActivationRequest requestMemberAccountActivation(Integer userId,
			String membershipCardNumber, String surname,
			String postcode, String phoneNumber, String mobilePhoneNumber, DateTime dateOfBirth, String updateEmailAddress, String activationURL)
	  throws GenericException;		

	public User getUser(Integer userId) throws GenericException;	
	
	public User getUserAccount(String userName) throws GenericException;
	
	public UserSession createUserSession(String sessionId, String ipAddress,
			Integer userId, String initialContext) throws GenericException;	

	public void updateUserClientNumber(Integer fromClient, Integer toClient) throws GenericException;
	
	public User updateUserAccount(User user) throws GenericException;
	
  public boolean userNameAvailable(String userName) throws GenericException;
  
	public User enableUserAccount(Integer userId) throws GenericException;
	
	public User disableUserAccount(Integer userId)	throws GenericException;	
	
	public User changeUserPassword(User user, String password) throws GenericException;
	
	public void deleteActivationRequests(Integer userId) throws GenericException;
	
	public User unapproveUserName(String userName) throws GenericException;

	public User approveUserName(String userName) throws GenericException;

	public void mergeDuplicateUserAccounts(String emailAddress) throws Exception;
	
	public User emailUserDetails(Integer userId) throws GenericException;
	
	public ActivationRequest getActivationRequest(Integer requestNumber) throws GenericException;	
}
