package com.ract.web.insurance;
import javax.persistence.*;
import java.io.*;

@Embeddable
public class WebInsClientDetailPK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 709133313287356049L;
	public Integer webClientNo;
	public Integer detailSeq;
	
	public WebInsClientDetailPK()
	{
		
	}
	public WebInsClientDetailPK(Integer webClientNo)
	{
		this.webClientNo = webClientNo;
//		this.detailSeq = new Integer(WebInsClient.lastDetailSeq(webClientNo).intValue() + 1);
	}
	public WebInsClientDetailPK(Integer webClientNo,
			                    Integer detailSeq)
	{
		this.webClientNo = webClientNo;
		this.detailSeq = detailSeq;
	}

	public Integer getWebClientNo() {
		return webClientNo;
	}
	public void setWebClientNo(Integer webClientNo) {
		this.webClientNo = webClientNo;
	} 
	public Integer getDetailSeq() {
		return detailSeq;
	}
	public void setDetailSeq(Integer detailSeq) {
		this.detailSeq = detailSeq;
	}
	public boolean equals(Object det)
	{
		WebInsClientDetailPK pk = (WebInsClientDetailPK)det;
		return this.webClientNo.equals(pk.webClientNo)
		       && this.detailSeq.equals(pk.detailSeq);
	}
	public int hashCode()
	{
	    return this.webClientNo.hashCode() + this.detailSeq;
	}

}
