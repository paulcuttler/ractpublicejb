package com.ract.web.insurance;

import java.util.ArrayList;

import javax.ejb.Remote;

import com.ract.common.SystemException;
import com.ract.util.DateTime;

@Remote
public interface WebInsGlMgrRemote {
    public ArrayList<ListPair> getMakes(GlVehicle veh) throws SystemException;

    public ArrayList<ListPair> getBodyTypes(GlVehicle veh) throws SystemException;

    public ArrayList<ListPair> getTransmissionTypes(GlVehicle veh) throws SystemException;

    public ArrayList<String> getCylinders(GlVehicle veh) throws SystemException;

    public ArrayList<GlVehicle> getList(GlVehicle veh, DateTime quoteDate) throws SystemException;

    public ArrayList<String> getModels(GlVehicle veh) throws SystemException;

    public Integer getValue(String nvic, DateTime quoteDate) throws SystemException;

    public GlVehicle getVehicle(String nvic, DateTime quoteDate) throws Exception;

    public String getShortDescription(String nvic) throws Exception;

    public GlVehicle getVehicleCodes(String nvic, DateTime quoteDate) throws Exception;

    public String getNvicByRegistration(String rego, String state) throws Exception;
}
