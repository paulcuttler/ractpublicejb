package com.ract.web.insurance;
/**
 * Insurance version of Web client (Extends from Web client
 * 
 */
import java.util.ArrayList;

import com.ract.util.DateTime;
import com.ract.util.LogUtil;
import com.ract.web.client.*;
import javax.persistence.*;

@Entity
@DiscriminatorValue("Insurance")
public class WebInsClient extends WebClient implements Comparable{
	private static final long serialVersionUID = -9209312397502062247L;

	public WebInsClient()
	{
		setClientType(WebClient.CLIENT_TYPE_INSURANCE);		
	}
	
	public static String CLIENT_DETAIL_SUSPENSION = "suspension";
	public static String CLIENT_DETAIL_ACC_THEFT = "acc_theft";
	public static String CLIENT_DETAIL_REFUSED = "refused";
	public static String DRIVE_DAILY = "D";
	public static String DRIVE_3_4 = "E";
	public static String DRIVE_1_2 = "F";
	public static String DRIVE_LESS = "G";

	boolean driver = false;
	boolean owner = false;
	boolean preferredAddress = false;

	boolean cltGroup;
	String  drivingFrequency;
	Integer yearCommencedDriving;
	Integer numberOfAccidents;
	Integer currentNCD;
	Integer webQuoteNo;
	String groupListString;
	String groupDescription;
	
    @Transient
	ArrayList<Integer> groupList;
	
	@Transient
	String  tData;

	public boolean isDriver() {
		return driver;
	}
	public void setDriver(boolean isDriver) {
		this.driver = isDriver;
	}
	public boolean isOwner() {
		return owner;
	}
	public void setOwner(boolean isOwner) {
		this.owner = isOwner;
	}
	public boolean isPreferredAddress() {
		return preferredAddress;
	}
	public void setPreferredAddress(boolean isPreferredAddress) {
		this.preferredAddress = isPreferredAddress;
	}
	public String getDrivingFrequency() {
		return drivingFrequency;
	}
	public void setDrivingFrequency(String drivingFrequency) {
		this.drivingFrequency = drivingFrequency;
	}
	public Integer getYearCommencedDriving() {
		return yearCommencedDriving;
	}
	public void setYearCommencedDriving(Integer yearCommencedDriving) {
		this.yearCommencedDriving = yearCommencedDriving;
	}
	public Integer getNumberOfAccidents() {
		return numberOfAccidents;
	}
	public void setNumberOfAccidents(Integer numberOfAccidents) {
		this.numberOfAccidents = numberOfAccidents;
	}
	public Integer getCurrentNCD() {
		return currentNCD;
	}
	public void setCurrentNCD(Integer currentNCB) {
		this.currentNCD = currentNCB;
	}
    /**
     * getWebQuoteNo
     * Return the quote number of the quote with which this WebClient is associated
     * @return Integer webQuoteNo
     */
	public Integer getWebQuoteNo() {
		return webQuoteNo;
	}

	/**
	 * setWebQuoteNo
	 * Sets the webQuote number of this client
	 * @param webQuoteNo
	 */
	public void setWebQuoteNo(Integer webQuoteNo) {
		this.webQuoteNo = webQuoteNo;
	}
	public boolean isCltGroup() {
		return cltGroup;
	}
	/**
	 * setGroup
	 * Sets the group flag to true/false
	 * true => this record represents a group
	 * @param cltGroup (boolean)
	 */
	public void setCltGroup(boolean cltGroup) {
		this.cltGroup = cltGroup;
	}
	
	
	/**
	 * toString
	 * Returns a String representation of this client entity
	 * Return String
	 */
	public String toString()
	{
		String cltString = 
			  "\nwebClientNo              = " + webClientNo         
			+ "\nclientType               = " + clientType
			+ "\ngivenNames               = " + givenNames          
          	+ "\nsurname                  = " + surname             
			+ "\ntitle                    = " + title               
			+ "\ngender                   = " + gender              
			+ "\ndateOfBirth              = " + birthDate         
			+ "\nhomePhone                = " + homePhone           
			+ "\nworkPhone                = " + workPhone           
			+ "\nmobilePhone              = " + mobilePhone         
			+ "\nfaxNo                    = " + faxNo               
			+ "\nemailAddress             = " + emailAddress        
			+ "\nresiStreetChar           = " + resiStreetChar      
			+ "\nresiStreet               = " + resiStreet          
			+ "\nresiSuburb               = " + resiSuburb          
			+ "\nresiStsubid              = " + resiStsubid         
			+ "\npostStreetChar           = " + postStreetChar      
			+ "\npostStreet               = " + postStreet          
			+ "\npostSuburb               = " + postSuburb          
			+ "\npostState                = " + postState           
			+ "\npostCountry              = " + postCountry         
			+ "\npostCode                 = " + postPostcode            
			+ "\npostStsubid              = " + postStsubid
			+ "\npostProperty             = " + postProperty
			+ "\ndriver                   = " + driver              
			+ "\nowner                    = " + owner               
			+ "\npreferredAddress         = " + preferredAddress    
			+ "\ngroup                    = " + cltGroup               
			+ "\ndrivingFrequency         = " + drivingFrequency    
			+ "\nyearCommencedDriving     = " + yearCommencedDriving
			+ "\nnumberOfAccidents        = " + numberOfAccidents   
			+ "\ncurrentNCD               = " + currentNCD          
			+ "\nwebQuoteNo               = " + webQuoteNo 
			+ "\ngroupListString          = " + groupListString 
			+ "\ngroupDescription         = " + groupDescription 
			;
		
		return cltString;
		
	}

	public int compareTo(Object c2)
	{
		WebInsClient clt = (WebInsClient)c2;
		return this.getWebClientNo().intValue() - clt.getWebClientNo().intValue();
	}
	
	public String getTData() {
		return tData;
	}

	public void setTData(String data) {
		tData = data;
	}

	public ArrayList<Integer> getGroupList() {
	    if(null == groupList && null != groupListString) {
	        setGroupList(groupListString);
	    }
	    
		return groupList;
	}

	public void setGroupList(String groupListString) 
	{
		String[] gList;
		Integer aNum;
		this.groupList = new ArrayList<Integer>();
		if(groupListString!=null
		  && !groupListString.equals(""))
		{
		   gList = groupListString.split(",");
		   for(int xx = 0; xx<gList.length;xx++)
		   { 
			  aNum = new Integer(gList[xx]);
			  this.groupList.add(aNum);
		   }
		}
		
		setGroupListString(groupListString);
	}

    public String getGroupListString() {
        if(null == groupListString && null != groupList && !groupList.isEmpty())
        {
            StringBuilder sb = new StringBuilder();
            for(int partyId : getGroupList()) {
                sb.append(partyId);
                sb.append(",");
            }
            sb.deleteCharAt(sb.lastIndexOf(","));
            setGroupListString(sb.toString());
        } else {
            // nothing to return
        }
        LogUtil.warn(this.getClass(), "Returning group list string: " + groupListString);
        return groupListString;
    }

    public void setGroupListString(String groupListString) {
        this.groupListString = groupListString;
    }
    
    public String getGroupDescription() {
        return groupDescription;
    }
    
    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }
    
    public String getSelectionDescription() {
        if(isCltGroup()) {
            return getGroupDescription();
        }
        return getDisplayName();
    }
    
    public WebInsClient createShallowCopy(WebInsClient copy) {
        if(null == copy) {
            return copy;
        }
        
        copy.setBirthDate(getBirthDate());
        copy.setClientType(getClientType());
        copy.setCltGroup(isCltGroup());
        copy.setCreateDate(new DateTime());
        copy.setCurrentNCD(getCurrentNCD());
        copy.setDriver(isDriver());
        copy.setDrivingFrequency(getDrivingFrequency());
        copy.setEmailAddress(getEmailAddress());
        copy.setFaxNo(getFaxNo());
        copy.setGender(getGender());
        copy.setGivenNames(getGivenNames());
        copy.setGroupDescription(getGroupDescription());
        //copy.setGroupList(getGroupListString());
        copy.setGroupListString(getGroupListString());
        copy.setHomePhone(getHomePhone());
        copy.setMobilePhone(getMobilePhone());
        copy.setNumberOfAccidents(getNumberOfAccidents());
        copy.setOwner(isOwner());
        copy.setPostAddress(getPostAddress());
        copy.setPostalAddress(getPostalAddress());
        copy.setPostAusbar(getPostAusbar());
        copy.setPostCountry(getPostCountry());
        copy.setPostDpid(getPostDpid());
        copy.setPostGnaf(getPostGnaf());
        copy.setPostLatitude(getPostLatitude());
        copy.setPostLongitude(getPostLongitude());
        copy.setPostPostcode(getPostPostcode());
        copy.setPostProperty(getPostProperty());
        copy.setPostState(getPostState());
        copy.setPostStreet(getPostStreet());
        copy.setPostStreetChar(getPostStreetChar());
        copy.setPostStsubid(getPostStsubid());
        copy.setPostSuburb(getPostSuburb());
        copy.setPreferredAddress(isPreferredAddress());
        copy.setPreferredContactMethod(getPreferredContactMethod());
        copy.setRactClientNo(getRactClientNo());
        copy.setResiAusbar(getResiAusbar());
        copy.setResiCountry(getResiCountry());
        copy.setResidentialAddress(getResidentialAddress());
        copy.setResiDpid(getResiDpid());
        copy.setResiGnaf(getResiGnaf());
        copy.setResiLatitude(getResiLatitude());
        copy.setResiLongitude(getResiLongitude());
        copy.setResiPostcode(getResiPostcode());
        copy.setResiProperty(getResiProperty());
        copy.setResiState(getResiState());
        copy.setResiStreet(getResiStreet());
        copy.setResiStreetChar(getResiStreetChar());
        copy.setResiStsubid(getResiStsubid());
        copy.setResiSuburb(getResiSuburb());
        copy.setSurname(getSurname());
        copy.setTData(getTData());
        copy.setTitle(getTitle());
        //copy.setWebClientNo(currentNCD);
        //copy.setWebQuoteNo(webQuoteNo);
        copy.setWorkPhone(getWorkPhone());
        copy.setYearCommencedDriving(getYearCommencedDriving());
                
        return copy;
    }
}
