package com.ract.web.insurance;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.ract.common.GenericException;
import com.ract.common.SystemException;
import com.ract.insurance.Policy;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.web.agent.WebAgentStatus;
import com.ract.web.common.GnSetting;

public class InsAdapter{
	private PureLinkAdapter pureLinkAdapter = null;

    /**
     * calculatePremium
     * @param quote
     * @param ractClientNo
     * @param paymentFrequency
     * @param quoteDetailList
     * @param specifiedItemsList
     * @param clientList
     * @param coverType
     * @param discountTypeList
     * @param discountList
     * @return Premium - calculated premium for the data supplied
     * @throws Exception
     * Connects to the DRE premium calculation module,
     * passes all details, and returns a Premium object
     */
    public Premium calculatePremium(WebInsQuote quote, Integer ractClientNo, String paymentFrequency, ArrayList<WebInsQuoteDetail> quoteDetailList, ArrayList<InRiskSi> specifiedItemsList, List<WebInsClient> clientList, String coverType, StringBuffer discountTypeList, StringBuffer discountList) throws Exception {
//        LogUtil.warn(this.getClass(), "****************************************************");
//        LogUtil.warn(this.getClass(), "calculatePremium invoked with parameters:");
//        LogUtil.warn(this.getClass(), "WebInsQuote: " + quote.toString());
//        LogUtil.warn(this.getClass(), "ractClientNo: " + ractClientNo);
//        LogUtil.warn(this.getClass(), "paymentFrequency: " + paymentFrequency);
//        LogUtil.warn(this.getClass(), "ArrayList<WebInsQuoteDetail> size: " + quoteDetailList.size());
//        LogUtil.warn(this.getClass(), "ArrayList<InRiskSi> size: " + (null == specifiedItemsList ? "null" : specifiedItemsList.size()));
//        LogUtil.warn(this.getClass(), "coverType: " + coverType);
//        LogUtil.warn(this.getClass(), "discountTypeList: " + discountTypeList);
//        LogUtil.warn(this.getClass(), "discountList: " + discountList);
//        LogUtil.warn(this.getClass(), "****************************************************");
//        LogUtil.warn(this.getClass(), "Dumping WebInsQuoteDetail list:");
//        LogUtil.warn(this.getClass(), "webQuoteNo / fieldName (stringKey / intKey) = fieldValue");
//        LogUtil.warn(this.getClass(), "****************************************************");
//        for (WebInsQuoteDetail detail : quoteDetailList) {
//            LogUtil.warn(this.getClass(), detail.toString());
//        }
//        LogUtil.warn(this.getClass(), "****************************************************");

        return new PureLinkAdapter().getPremium(quote, paymentFrequency, quoteDetailList, specifiedItemsList, clientList);
    }
	
	/**
	 * getClientPolicies
	 * @param clientNumber
	 * @param loadRisks
	 * @param includeCancelled
	 * @param includeRenewal
	 * @return Collection of Policy objects
	 * @throws GenericException
	 * Returns a collection of Policy objects representing all policies owned by the client represented by the
	 * clientNumber.  Includes policies owned individually or jointly by this client.
	 */
	@Deprecated
	public Collection<Policy> getClientPolicies(Integer clientNumber, boolean loadRisks, boolean includeCancelled, boolean includeRenewal)
	  throws GenericException
	{
        LogUtil.warn(this.getClass(), "****************************************************");
        LogUtil.warn(this.getClass(), "getClientPolicies invoked with parameters:");
        LogUtil.warn(this.getClass(), "clientNumber: " + clientNumber);
        LogUtil.warn(this.getClass(), "loadRisks: " + loadRisks);
        LogUtil.warn(this.getClass(), "includeCancelled: " + includeCancelled);
        LogUtil.warn(this.getClass(), "includeRenewal: " + includeRenewal);
        LogUtil.warn(this.getClass(), "****************************************************");
	    
        throw new GenericException("getClientPolicies invoked with unexpected parameters - refer to logs for details.");
	}

	/**
	 * getMessageText
	 * @param subSystem
	 * @param noticeType
	 * @param location
	 * @param prodType
	 * @param subType
	 * @param effDate
	 * @return MessageText object holding the required string data
	 * @throws Exception
	 * Retrieves textual information from the progress system.
	 * Looks up the gn_text table according to the parameters provided
	 * Returns a single MessageText object
	 */
	public MessageText getMessageText(String subSystem,
			                     String noticeType,
			                     String location,
			                     String prodType,
			                     String subType,
			                     DateTime effDate)throws Exception
	{
        if("QUOTE".equalsIgnoreCase(noticeType) && "RISK HEADER".equalsIgnoreCase(location) && "BLDG".equalsIgnoreCase(subType)) {
            MessageText message = new MessageText("Home Building","The terms of your Building insurance are set out in your current policy booklet.\n\nThis policy is based on the information you gave us.  It is in your interest to check it carefully.  You must tell us if anything is incorrect or incomplete.");
            return message;
        }
        
        if("QUOTE".equalsIgnoreCase(noticeType) && "RISK HEADER".equalsIgnoreCase(location) && "inv".equalsIgnoreCase(subType)) {
            MessageText message = new MessageText("Home Building","The terms of your Building insurance are set out in your current policy booklet.\n\nThis policy is based on the information you gave us.  It is in your interest to check it carefully.  You must tell us if anything is incorrect or incomplete.");
            return message;
        }

        if("QUOTE".equalsIgnoreCase(noticeType) && "RISK HEADER".equalsIgnoreCase(location) && "cnts".equalsIgnoreCase(subType)) {
            MessageText message = new MessageText("Home Contents","The terms of your Contents insurance are set out in your current policy booklet.\n\nThis policy is based on the information you gave us.  It is in your interest to check it carefully.  You must tell us if anything is incorrect or incomplete.");
            return message;
        }
        
        if("QUOTE".equalsIgnoreCase(noticeType) && "RISK HEADER".equalsIgnoreCase(location) && "bldg_cnts".equalsIgnoreCase(subType)) {
            MessageText message = new MessageText("Building and Contents","The terms of your  Building and Contents insurance are set out in your current policy booklet.\n\nThis policy is based on the information you gave us.  It is in your interest to check it carefully.  You must tell us if anything is incorrect or incomplete.");
            return message;
        }
        
        if("QUOTE".equalsIgnoreCase(noticeType) && "RISK HEADER".equalsIgnoreCase(location) && "car".equalsIgnoreCase(subType)) {
            MessageText message = new MessageText("Comprehensive Motor Vehicle","The terms of your Comprehensive Motor Vehicle insurance are set out in your current policy booklet.\n\nThis policy is based on the information you gave us.  It is in your interest to check it carefully.  You must tell us if anything is incorrect or incomplete.\n\n");
            return message;
        }
        
        if("QUOTE".equalsIgnoreCase(noticeType) && "RISK HEADER".equalsIgnoreCase(location) && "tp".equalsIgnoreCase(subType)) {
            MessageText message = new MessageText("Third Party Property Damage","The terms of your motor vehicle Third Party Property Damage insurance are set out in your current policy booklet.\n\nThis policy is based on the information you gave us.  It is in your interest to check it carefully.  You must tell us if anything is incorrect or incomplete.\n\n");
            return message;
        }
        
        if("QUOTE".equalsIgnoreCase(noticeType) && "Wording".equalsIgnoreCase(location) && "HPV300".equalsIgnoreCase(subType)) {
            MessageText message = new MessageText("High Performance Vehicle","HIGH PERFORMANCE VEHICLE - It is noted that no cover is provided under this policy if any driver under 25 is driving this vehicle.");
            return message;
        }
        
        if("QUOTE".equalsIgnoreCase(noticeType) && "PREMIUM MESSAGE".equalsIgnoreCase(location) && "DD".equalsIgnoreCase(subType)) {
            MessageText message = new MessageText("","As you have elected to pay by direct debit, no further action is required on your behalf.");
            return message;
        }
        
        if("QUOTE".equalsIgnoreCase(noticeType) && "PREMIUM MESSAGE".equalsIgnoreCase(location) && "NONDD".equalsIgnoreCase(subType)) {
            MessageText message = new MessageText("","If you elected to receive an invoice this will be provided with your documents within 5 working days - you will then have 14 days to pay the premium.\n\nIf you have already elected to pay up-front, there is nothing further you need to do.  You will receive your documents within 5 working days.\n\n");
            return message;
        }
        
        LogUtil.warn(this.getClass(), "****************************************************" );
        LogUtil.warn(this.getClass(), "getMessageText invoked with parameters:" );
        LogUtil.warn(this.getClass(), "subSystem: " +  subSystem);
        LogUtil.warn(this.getClass(), "noticeType: " +  noticeType);
        LogUtil.warn(this.getClass(), "location: " +  location);
        LogUtil.warn(this.getClass(), "prodType: " +  prodType);
        LogUtil.warn(this.getClass(), "subType: " +  subType);
        LogUtil.warn(this.getClass(), "effDate: " +  effDate);
        LogUtil.warn(this.getClass(), "****************************************************" );
        
        throw new SystemException("getMessageText invoked with unexpected parameters - refer to logs for details.");
	}

	/**
	 * getBasicXs
	 * @param riskType
	 * @param effDate
	 * @return Integer - excess amount
	 * @throws Exception
	 * Returns the default (Excess/Optional Excess) excess 
	 * for the given risk type.
	 */
	@Deprecated
	public Integer getBasicXs(String riskType,
			                  DateTime effDate)throws Exception
	{
        LogUtil.warn(this.getClass(), "****************************************************" );
        LogUtil.warn(this.getClass(), "getBasicXs invoked with parameters:" );
        LogUtil.warn(this.getClass(), "type: " +  riskType);
        LogUtil.warn(this.getClass(), "effDate: " +  effDate);
        LogUtil.warn(this.getClass(), "****************************************************" );
	    
        throw new SystemException("getBasicXs invoked with unexpected parameters - refer to logs for details.");
	}
	
	/**
	 * getNotices
	 * @param docType
	 * @param prodType
	 * @param riskType
	 * @param effDate
	 * @return ArrayList of MessageText objects
	 * @throws Exception
	 * Looks up progress gn_text table and returns a collection of 
	 * MessageText objects for all notices specific to WEBINS/NOTICES
	 */
	public ArrayList<MessageText> getNotices(String docType,
			                                 String prodType,
			                                 String riskType,
			                                 DateTime effDate)throws Exception
    {
		ArrayList<MessageText> notices = null;
		
		if("QUOTE".equalsIgnoreCase(docType) && "NPIP".equalsIgnoreCase(prodType) && "BLDG".equalsIgnoreCase(riskType)) {
		    notices = new ArrayList<MessageText>();
		    MessageText message = new MessageText("Third Party Property Damage","Thank you for insuring with RACT Insurance.\n\nDetails of your new insurance policy, together with our product disclosure statement and privacy policy  will be mailed to you within 5 working days.\n\n");
		    notices.add(message);
		    
		    message = new MessageText("","A discounted premium is available if you have three products insured with RACT (comprehensive motor vehicle, home building and home contents).  If you think you may be eligible, contact one of our consultants to have your policies combined into a Personal Insurance Portfolio.\n");
            notices.add(message);

            message = new MessageText("Bushfire exclusion","BUSHFIRE EXCLUSION -  Bushfire is not covered for the first 48 hours after the start of your policy.\n");
            notices.add(message);
            
		    return notices;
		}
		
		if("QUOTE".equalsIgnoreCase(docType) && "NPIP".equalsIgnoreCase(prodType) && "cnts".equalsIgnoreCase(riskType)) {
		    notices = new ArrayList<MessageText>();
		    MessageText message = new MessageText("Third Party Property Damage","Thank you for insuring with RACT Insurance.\n\nDetails of your new insurance policy, together with our product disclosure statement and privacy policy  will be mailed to you within 5 working days.\n\n");
		    notices.add(message);
		    
		    message = new MessageText("","A discounted premium is available if you have three products insured with RACT (comprehensive motor vehicle, home building and home contents).  If you think you may be eligible, contact one of our consultants to have your policies combined into a Personal Insurance Portfolio.\n");
		    notices.add(message);
		    
		    message = new MessageText("Bushfire exclusion","BUSHFIRE EXCLUSION -  Bushfire is not covered for the first 48 hours after the start of your policy.\n");
		    notices.add(message);
		    
		    return notices;
		}
		
		if("QUOTE".equalsIgnoreCase(docType) && "NPIP".equalsIgnoreCase(prodType) && "bldg_cnts".equalsIgnoreCase(riskType)) {
		    notices = new ArrayList<MessageText>();
		    MessageText message = new MessageText("Third Party Property Damage","Thank you for insuring with RACT Insurance.\n\nDetails of your new insurance policy, together with our product disclosure statement and privacy policy  will be mailed to you within 5 working days.\n\n");
		    notices.add(message);
		    
		    message = new MessageText("","A discounted premium is available if you have three products insured with RACT (comprehensive motor vehicle, home building and home contents).  If you think you may be eligible, contact one of our consultants to have your policies combined into a Personal Insurance Portfolio.\n");
		    notices.add(message);
		    
		    message = new MessageText("Bushfire exclusion","BUSHFIRE EXCLUSION -  Bushfire is not covered for the first 48 hours after the start of your policy.\n");
		    notices.add(message);
		    
		    return notices;
		}
		
		if("QUOTE".equalsIgnoreCase(docType) && "NPIP".equalsIgnoreCase(prodType) && "inv".equalsIgnoreCase(riskType)) {
		    notices = new ArrayList<MessageText>();
		    MessageText message = new MessageText("Third Party Property Damage","Thank you for insuring with RACT Insurance.\n\nDetails of your new insurance policy, together with our product disclosure statement and privacy policy  will be mailed to you within 5 working days.\n\n");
		    notices.add(message);
		    
		    message = new MessageText("","A discounted premium is available if you have three products insured with RACT (comprehensive motor vehicle, home building and home contents).  If you think you may be eligible, contact one of our consultants to have your policies combined into a Personal Insurance Portfolio.\n");
		    notices.add(message);
		    
		    message = new MessageText("Bushfire exclusion","BUSHFIRE EXCLUSION -  Bushfire is not covered for the first 48 hours after the start of your policy.\n");
		    notices.add(message);
		    
		    return notices;
		}
		
		if("QUOTE".equalsIgnoreCase(docType) && "NPIP".equalsIgnoreCase(prodType) && "car".equalsIgnoreCase(riskType)) {
		    notices = new ArrayList<MessageText>();
		    MessageText message = new MessageText("Third Party Property Damage","Thank you for insuring with RACT Insurance.\n\nDetails of your new insurance policy, together with our product disclosure statement and privacy policy  will be mailed to you within 5 working days.\n\n");
		    notices.add(message);
		    
		    message = new MessageText("","A discounted premium is available if you have three products insured with RACT (comprehensive motor vehicle, home building and home contents).  If you think you may be eligible, contact one of our consultants to have your policies combined into a Personal Insurance Portfolio.\n");
		    notices.add(message);
		    
		    message = new MessageText("CAR","");
		    notices.add(message);
		    
		    return notices;
		}
		
		if("QUOTE".equalsIgnoreCase(docType) && "NPIP".equalsIgnoreCase(prodType) && "tp".equalsIgnoreCase(riskType)) {
		    notices = new ArrayList<MessageText>();
		    MessageText message = new MessageText("Third Party Property Damage","Thank you for insuring with RACT Insurance.\n\nDetails of your new insurance policy, together with our product disclosure statement and privacy policy  will be mailed to you within 5 working days.\n\n");
		    notices.add(message);
		    
		    message = new MessageText("","A discounted premium is available if you have three products insured with RACT (comprehensive motor vehicle, home building and home contents).  If you think you may be eligible, contact one of our consultants to have your policies combined into a Personal Insurance Portfolio.\n");
		    notices.add(message);
		    
		    return notices;
		}

		LogUtil.warn(this.getClass(), "****************************************************" );
		LogUtil.warn(this.getClass(), "getNotices invoked with parameters:" );
		LogUtil.warn(this.getClass(), "docType: " +  docType);
		LogUtil.warn(this.getClass(), "prodType: " +  prodType);
		LogUtil.warn(this.getClass(), "riskType: " +  riskType);
		LogUtil.warn(this.getClass(), "effDate: " +  effDate);
		LogUtil.warn(this.getClass(), "****************************************************" );

		throw new SystemException("getNotices invoked with unexpected parameters - refer to logs for details.");
    }
	
	/**
	 * getNotionalProdType
	 * @param clientNo
	 * @param fiftyPlus
	 * @param riskType
	 * @param effDate
	 * @return String - PIP,NPIP,50+
	 * @throws Exception
	 * Uses the policy rules to distinguish between qualifying PIP, 
	 * and non-qualifying PIP.
	 */
	public String getNotionalProdType(Integer clientNo,
			                          String fiftyPlus,
			                          String  riskType,
			                          DateTime effDate)throws Exception
	{
//        LogUtil.warn(this.getClass(), "****************************************************" );
//        LogUtil.warn(this.getClass(), "getNotionalProdType invoked with parameters:" );
//        LogUtil.warn(this.getClass(), "clientNo: " +  clientNo);
//        LogUtil.warn(this.getClass(), "fiftyPlus: " +  fiftyPlus);
//        LogUtil.warn(this.getClass(), "riskType: " +  riskType);
//        LogUtil.warn(this.getClass(), "effDate: " +  effDate);
//        LogUtil.warn(this.getClass(), "****************************************************" );
	    
        // Since we are asking them if they qualify for PIP or not on the forms for Pure integration,
        // this method only needs to determine if they qualify for 50+
        if(fiftyPlus.equalsIgnoreCase("Yes") || fiftyPlus.equalsIgnoreCase("true")) {
            return WebInsQuoteDetail.FIFTY_PLUS;
        } else {
            return "NPIP";
        }
	}
	
    /**
     * @return Company - RACT company data
     * Returns hard coded contact information for RACT
     */
    public Company getCompanyData() {
        Company company = new Company();
        company.setAbn("96 068 167 804");
        company.setAddress1("Cnr Murray & Patrick Streets");
        company.setAddress2("Hobart 7000");
        company.setAFSNumber("229076");
        company.setCompanyName("RACT Insurance Pty Ltd");
        company.setCompanyPhone("03 6232 6300");
        company.setPhonePayNumber("13 27 22");
        company.setPostalAddress("PO Box 1292 Hobart 7001");
        company.setServicePhoneNo("13 27 22");
        company.setWebPhoneNo("13 27 22");
        return company;
    }
	
	/**
	 * getAssociatedGroups
	 * @param ractClientNo
	 * @return Collection of WebInsClient
	 * @throws Exception
	 * Assembles a list of all clientGroups of which the given client (ractClientNo) is
	 * a member.
	 */
	public ArrayList<WebInsClient> getAssociatedGroups(Integer ractClientNo)throws Exception
	{
        return new PureLinkAdapter().getPartyAssociations(ractClientNo);
	}
	
	/**
	 * getGroupMembers
	 * @param ractClientNo
	 * @return collection of client numbers
	 * @throws Exception
	 * Returns a collect of client numbers representing all the members of the
	 * group represented by the given client number.  If the client number supplied
	 * is not a group client, return an empty collection.
	 */
	@Deprecated
	public ArrayList<Integer> getGroupMembers(Integer ractClientNo)throws Exception
	{
	    LogUtil.warn(this.getClass(), "****************************************************");
        LogUtil.warn(this.getClass(), "getGroupMembers invoked with parameters:");
        LogUtil.warn(this.getClass(), "ractClientNo: " + ractClientNo);
        LogUtil.warn(this.getClass(), "****************************************************");
	    
        throw new SystemException("getGroupMembers invoked with unexpected parameters - refer to logs for details.");
	}
	
	/**
	 * getTypeList
	 * @param type
	 * @param effDate
	 * @return ArrayList of RefType
	 * @throws Exception
	 * Returns a list extracted from a single insType record
	 * (valid values for type:
	 *              BoatConstType       
	 *				BoatType            
     *				claimDiaryType      
	 *				ddLoadings          
	 *				Financiers          
	 *				officerAssignment   
	 *				recoveryAssignment  
	 *				siLimit             
	 *				VehicleUsage        
	 * 				WebExtras)           
	 */
    public ArrayList<RefType> getTypeList(String type, DateTime effDate) throws Exception {
        if (WebInsQuoteDetail.REF_TYPE_USAGE.equalsIgnoreCase(type)) {
            return new PureLinkAdapter().getVehicleUsages(effDate);
        }

        if (WebInsQuoteDetail.REF_TYPE_DRIVING_FREQUENCY.equalsIgnoreCase(type)) {
            return new PureLinkAdapter().getDrivingFrequencies(effDate);
        }
        
        LogUtil.warn(this.getClass(), "****************************************************");
        LogUtil.warn(this.getClass(), "getTypeList invoked with parameters:");
        LogUtil.warn(this.getClass(), "type: " + type);
        LogUtil.warn(this.getClass(), "effDate: " + effDate);
        LogUtil.warn(this.getClass(), "****************************************************");

        ArrayList<RefType> list = new ArrayList<RefType>();
        return list;
    }
	
	/**
	 * getSysSetting
	 * @param tName
	 * @param tCode
	 * @return GnSetting 
	 * @throws Exception
	 * Look up master configuration for given table name and key value.
	 * Return a single GnSetting value representing the retrieved record.
	 * If no such record exists, throws an Exception.
	 */
	public GnSetting getSysSetting(String tName, String tCode)throws Exception
	{
	    if("IWS".equalsIgnoreCase(tName) && "CCADD".equalsIgnoreCase(tCode)) {
            GnSetting gn = new GnSetting();
            gn.setTableName(tName);
            gn.setCode(tCode);
            gn.setDescription(FileUtil.getProperty("master", "email.coverNote.bcc"));
            return gn;
	    }
	    
	    if("IWS".equalsIgnoreCase(tName) && "FromAdd".equalsIgnoreCase(tCode)) {
	        GnSetting gn = new GnSetting();
	        gn.setTableName(tName);
	        gn.setCode(tCode);
	        gn.setDescription(FileUtil.getProperty("master", "email.coverNote.from"));
	        return gn;
	    }
	    
	    
        LogUtil.warn(this.getClass(), "****************************************************" );
        LogUtil.warn(this.getClass(), "getSysSetting invoked with parameters:" );
        LogUtil.warn(this.getClass(), "tName: " +  tName);
        LogUtil.warn(this.getClass(), "tCode: " +  tCode);
        LogUtil.warn(this.getClass(), "****************************************************" );
	    
        throw new SystemException("getSysSetting invoked with unexpected parameters - refer to logs for details.");
	}
	
	/**
	 * allocateSalesBranch
	 * @return String salesBranch
	 * Allocates a sales branch according to a preset pattern
	 * held in gn-table "IWB".
	 */
	@Deprecated
	public String allocateSalesBranch(String agentCode)
	{
        LogUtil.warn(this.getClass(), "****************************************************" );
        LogUtil.warn(this.getClass(), "allocateSalesBranch invoked with parameters:" );
        LogUtil.warn(this.getClass(), "agentCode: " +  agentCode);
        LogUtil.warn(this.getClass(), "****************************************************" );
	    
        String salesBranch = null;
        return salesBranch;
	} 
	
    /**
     * getLookupData
     * @param prodType
     * @param riskType
     * @param fieldName
     * @param effDate
     * @return ArrayList<InRfDet> reference data returned from Pure
     * @throws Exception
     * Extracts lookup data from Pure
     */
    public ArrayList<InRfDet> getLookupData(String prodType, String riskType, String fieldName, DateTime effDate) throws Exception {
        boolean isSilverSaver = WebInsQuoteDetail.FIFTY_PLUS.equalsIgnoreCase(prodType);
        
        if (WebInsQuoteDetail.FIN_TYPE.equalsIgnoreCase(fieldName)) {
            return getPureLinkAdapter().getFinanceTypes(effDate);
        }

        if (WebInsQuoteDetail.FINANCIER.equalsIgnoreCase(fieldName)) {
            return getPureLinkAdapter().getFinanciers(effDate);
        }

        if (WebInsQuoteDetail.BUSINESS_USE.equalsIgnoreCase(fieldName)) {
            return getPureLinkAdapter().getBusinessUsages(effDate);
        }

        if (WebInsQuoteDetail.BUILDING_TYPE.equalsIgnoreCase(fieldName)) {
            return getPureLinkAdapter().getBuildingLayout(effDate);
        }

        if (WebInsQuoteDetail.CONSTRUCTION.equalsIgnoreCase(fieldName)) {
            return getPureLinkAdapter().getWallConstruction(effDate);
        }

        if (WebInsQuoteDetail.ROOF.equalsIgnoreCase(fieldName)) {
            return getPureLinkAdapter().getRoofConstruction(effDate);
        }

        if (WebInsQuoteDetail.SECURITY_LOCK.equalsIgnoreCase(fieldName)) {
            return getPureLinkAdapter().getHomeLocks(effDate);
        }

        if (WebInsQuoteDetail.SECURITY_ALARM.equalsIgnoreCase(fieldName)) {
            return getPureLinkAdapter().getHomeAlarms(effDate);
        }

        if (WebInsQuoteDetail.RENTAL_MANAGER.equalsIgnoreCase(fieldName)) {
            return getManagerInRfDet();
        }

        if (WebInsQuoteDetail.OCCUPANCY.equalsIgnoreCase(fieldName) && WebInsQuote.COVER_INV.equalsIgnoreCase(riskType)) {
            return getPureLinkAdapter().getOccupancy(effDate, "INVS", true);
        }

        if (WebInsQuoteDetail.OCCUPANCY.equalsIgnoreCase(fieldName) && WebInsQuote.COVER_BLDG.equalsIgnoreCase(riskType)) {
            return getPureLinkAdapter().getOccupancy(effDate, "HOME", true);
        }

        if (WebInsQuoteDetail.OCCUPANCY.equalsIgnoreCase(fieldName) && WebInsQuote.COVER_CNTS.equalsIgnoreCase(riskType)) {
            return getPureLinkAdapter().getOccupancy(effDate, "HOME", false);
        }

        if (WebInsQuoteDetail.EXCESS_OPTIONS.equalsIgnoreCase(fieldName) && WebInsQuote.COVER_INV.equalsIgnoreCase(riskType)) {
            return getPureLinkAdapter().getExcess(effDate, "INVS", "BLDG", false);
        }

        if (WebInsQuoteDetail.EXCESS_OPTIONS.equalsIgnoreCase(fieldName) && WebInsQuote.COVER_BLDG.equalsIgnoreCase(riskType)) {
            return getPureLinkAdapter().getExcess(effDate, "HOME", "BLDG", false);
        }

        if (WebInsQuoteDetail.EXCESS_OPTIONS.equalsIgnoreCase(fieldName) && WebInsQuote.COVER_CNTS.equalsIgnoreCase(riskType)) {
            return getPureLinkAdapter().getExcess(effDate, "HOME", "CNTS", false);
        }
        if (WebInsQuoteDetail.EXCESS_OPTIONS.equalsIgnoreCase(fieldName) && WebInsQuote.COVER_COMP.equalsIgnoreCase(riskType)) {
            return getPureLinkAdapter().getExcess(effDate, "MOTR", "COMC", isSilverSaver);
        }
        if (WebInsQuoteDetail.EXCESS_OPTIONS.equalsIgnoreCase(fieldName) && WebInsQuote.COVER_TP.equalsIgnoreCase(riskType)) {
            return getPureLinkAdapter().getExcess(effDate, "MOTR", "TPPD", isSilverSaver);
        }
        
        if (WebInsQuoteDetail.OPTIONS.equalsIgnoreCase(fieldName) && WebInsQuote.COVER_INV.equalsIgnoreCase(riskType)) {
            return getPureLinkAdapter().getOptions(effDate, "INVS", "BLDG");
        }
        
        if (WebInsQuoteDetail.OPTIONS.equalsIgnoreCase(fieldName) && WebInsQuote.COVER_BLDG.equalsIgnoreCase(riskType)) {
            return getPureLinkAdapter().getOptions(effDate, "HOME", "BLDG");
        }
        
        if (WebInsQuoteDetail.OPTIONS.equalsIgnoreCase(fieldName) && WebInsQuote.COVER_CNTS.equalsIgnoreCase(riskType)) {
            return getPureLinkAdapter().getOptions(effDate, "HOME", "CNTS");
        }
        
        if (WebInsQuoteDetail.OPTIONS.equalsIgnoreCase(fieldName) && WebInsQuote.COVER_COMP.equalsIgnoreCase(riskType)) {
            return getPureLinkAdapter().getOptions(effDate, "MOTR", "COMC");
        }
        
        if (WebInsQuoteDetail.OPTIONS.equalsIgnoreCase(fieldName) && WebInsQuote.COVER_TP.equalsIgnoreCase(riskType)) {
            return getPureLinkAdapter().getOptions(effDate, "MOTR", "TPPD");
        }

        if (WebInsQuote.COVER_CNTS.equalsIgnoreCase(fieldName) && WebInsQuote.COVER_INV.equalsIgnoreCase(riskType)) {
            return getPureLinkAdapter().getOptions(effDate, "INVS", "CNTS");
        }

        if (WebInsQuoteDetail.CNTS_SUM_INSURED.equalsIgnoreCase(fieldName) && WebInsQuote.COVER_INV.equalsIgnoreCase(riskType)) {
            return getInvestorContentsInRfDet();
        }

        if (WebInsQuoteDetail.SPEC_CNTS.equalsIgnoreCase(fieldName)) {
            return getPureLinkAdapter().getHomeContentsCategories(effDate);
        }

        if (WebInsQuoteDetail.NO_CONVERT.equalsIgnoreCase(fieldName)) {
            return getNoConvertInRfDet();
        }
        
        
        ArrayList<InRfDet> list = new ArrayList<InRfDet>();
        LogUtil.warn(this.getClass(), "****************************************************");
        LogUtil.warn(this.getClass(), "getLookupData invoked with parameters:");
        LogUtil.warn(this.getClass(), "prodType: " + prodType);
        LogUtil.warn(this.getClass(), "riskType: " + riskType);
        LogUtil.warn(this.getClass(), "fieldName: " + fieldName);
        LogUtil.warn(this.getClass(), "effDate: " + effDate);
        LogUtil.warn(this.getClass(), "****************************************************");
        return list;
    }

    /**
	 * getNcd
	 * @param accidentList
	 * @param numberOfAccidents
	 * @param yearCommencedDriving
	 * @return Integer no claim discount %
	 * @throws Exception
	 * Given all known driver information, assesses no claim discount
	 */
	public Integer getNcd(ArrayList<WebInsClientDetail> accidentList,
			              Integer numberOfAccidents,
			              Integer yearCommencedDriving)throws Exception
	{
	    return getPureLinkAdapter().getNoClaimDiscount(numberOfAccidents, yearCommencedDriving);
	}
	
    /**
     * getInsSetting
     * @param tName
     * @param tKey
     * @param effDate
     * @return String setting value
     * @throws Exception
     * Extracts setting value equivalents from Pure
     */
    public String getInsSetting(String tName, String tKey, DateTime effDate) throws Exception {
        if (WebInsQuoteDetail.SUBURB_UNACCEPTABLE.equalsIgnoreCase(tName)) {
            return getPureLinkAdapter().getSuburbValidation(effDate, tKey);
        }

        if (WebInsQuoteDetail.MIN_SUM_INSURED.equalsIgnoreCase(tName) && WebInsQuote.COVER_INV.equalsIgnoreCase(tKey)) {
            return String.valueOf(getPureLinkAdapter().getMinInsurable(effDate, "INVS", "BLDG"));
        }
        if (WebInsQuoteDetail.MAX_SUM_INSURED.equalsIgnoreCase(tName) && WebInsQuote.COVER_INV.equalsIgnoreCase(tKey)) {
            return String.valueOf(getPureLinkAdapter().getMaxInsurable(effDate, "INVS", "BLDG"));
        }

        if (WebInsQuoteDetail.MIN_SUM_INSURED.equalsIgnoreCase(tName) && WebInsQuote.COVER_BLDG.equalsIgnoreCase(tKey)) {
            return String.valueOf(getPureLinkAdapter().getMinInsurable(effDate, "HOME", "BLDG"));
        }
        if (WebInsQuoteDetail.MAX_SUM_INSURED.equalsIgnoreCase(tName) && WebInsQuote.COVER_BLDG.equalsIgnoreCase(tKey)) {
            return String.valueOf(getPureLinkAdapter().getMaxInsurable(effDate, "HOME", "BLDG"));
        }

        if (WebInsQuoteDetail.MIN_SUM_INSURED.equalsIgnoreCase(tName) && WebInsQuote.COVER_CNTS.equalsIgnoreCase(tKey)) {
            return String.valueOf(getPureLinkAdapter().getMinInsurable(effDate, "HOME", "CNTS"));
        }
        if (WebInsQuoteDetail.MAX_SUM_INSURED.equalsIgnoreCase(tName) && WebInsQuote.COVER_CNTS.equalsIgnoreCase(tKey)) {
            return String.valueOf(getPureLinkAdapter().getMaxInsurable(effDate, "HOME", "CNTS"));
        }
        
        if (WebInsQuoteDetail.MIN_SUM_INSURED.equalsIgnoreCase(tName) && WebInsQuote.COVER_COMP.equalsIgnoreCase(tKey)) {
            return String.valueOf(getPureLinkAdapter().getMinInsurable(effDate, "MOTR", "COMC"));
        }
        if (WebInsQuoteDetail.MAX_SUM_INSURED.equalsIgnoreCase(tName) && WebInsQuote.COVER_COMP.equalsIgnoreCase(tKey)) {
            return String.valueOf(getPureLinkAdapter().getMaxInsurable(effDate, "MOTR", "COMC"));
        }

        LogUtil.warn(this.getClass(), "****************************************************");
        LogUtil.warn(this.getClass(), "getInsSetting invoked with parameters:");
        LogUtil.warn(this.getClass(), "tName: " + tName);
        LogUtil.warn(this.getClass(), "tKey: " + tKey);
        LogUtil.warn(this.getClass(), "effDate: " + effDate);
        LogUtil.warn(this.getClass(), "****************************************************");
        throw new SystemException("getInsSetting invoked with unexpected parameters - refer to logs for details.");
    }

	/**
	 * getLookupDescription
	 * @param rfSeqNo
	 * @param pKey
	 * @param pSubKey
	 * @return String lookup description
	 * @throws Exception
	 * Deprecated - depends on progress implementation to return lookup description
	 */
    @Deprecated
	public String getLookupDescription(Integer rfSeqNo,
			                           String pKey,
			                           String pSubKey)throws Exception
	{
        LogUtil.warn(this.getClass(), "****************************************************" );
        LogUtil.warn(this.getClass(), "getLookupDescription invoked with parameters:" );
        LogUtil.warn(this.getClass(), "rfSeqNo: " +  rfSeqNo);
        LogUtil.warn(this.getClass(), "pKey: " +  pKey);
        LogUtil.warn(this.getClass(), "pSubKey: " +  pSubKey);
        LogUtil.warn(this.getClass(), "****************************************************" );
	    
        throw new SystemException("getLookupDescription invoked with unexpected parameters - refer to logs for details.");
	}

    /**
     * validateAgent
     * @param agentCode
     * @param agentPassword
     * @return WebAgentStatus agent validation result
     * @throws Exception
     * Validates agent details against Pure data
     */
    public WebAgentStatus validateAgent(String agentCode, String agentPassword) throws Exception {
        WebAgentStatus was = new WebAgentStatus();

        try {
            was = getPureLinkAdapter().validateAgent(agentCode, agentPassword);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return was;
    }
	
    /**
     * getFixedExcesses
     * @param prodType
     * @param riskType
     * @param effDate
     * @return ArrayList<ListPair> fixed excess options applicable
     * @throws Exception
     * Extracts fixed excess information from Pure for specified inputs
     */
    public ArrayList<ListPair> getFixedExcesses(String prodType, String riskType, DateTime effDate) throws Exception {
        boolean isSilverSaver = WebInsQuoteDetail.FIFTY_PLUS.equalsIgnoreCase(prodType);
        
        if (WebInsQuote.COVER_BLDG.equalsIgnoreCase(riskType)) {
            return (ArrayList<ListPair>) getPureLinkAdapter().getFixedExcess(effDate, "HOME", "HOME", isSilverSaver);
        }
        if (WebInsQuote.COVER_CNTS.equalsIgnoreCase(riskType)) {
            return (ArrayList<ListPair>) getPureLinkAdapter().getFixedExcess(effDate, "HOME", "HOME", isSilverSaver);
        }
        if (WebInsQuote.COVER_COMP.equalsIgnoreCase(riskType)) {
            return (ArrayList<ListPair>) getPureLinkAdapter().getFixedExcess(effDate, "MOTR", "COMC", isSilverSaver);
        }
        if (WebInsQuote.COVER_TP.equalsIgnoreCase(riskType)) {
            return (ArrayList<ListPair>) getPureLinkAdapter().getFixedExcess(effDate, "MOTR", "TPPD", isSilverSaver);
        }
	    if(WebInsQuote.COVER_INV.equalsIgnoreCase(riskType)) {
	        return (ArrayList<ListPair>) getPureLinkAdapter().getFixedExcess(effDate, "INVS", "PINV", isSilverSaver);
	    }
        
        LogUtil.warn(this.getClass(), "****************************************************" );
        LogUtil.warn(this.getClass(), "getFixedExcesses invoked with parameters:" );
        LogUtil.warn(this.getClass(), "prodType: " +  prodType);
        LogUtil.warn(this.getClass(), "riskType: " +  riskType);
        LogUtil.warn(this.getClass(), "effDate: " +  effDate);
        LogUtil.warn(this.getClass(), "****************************************************" );
        
        throw new SystemException("getFixedExcess invoked with unexpected parameters - refer to logs for details.");
    }
    
    /**
     * savePolicyToPure
     * @param quote
     * @param quoteDetailList
     * @param specifiedItemsList
     * @param clientList
     * @param cardType
     * @param bsb
     * @param accountNumber
     * @param cardExpiry
     * @param accountName
     * @param paymentDay
     * @param paymentFrequency
     * @return String policy reference for saved policy
     * @throws Exception
     * Persists policy information to Pure 
     */
    public String savePolicyToPure(WebInsQuote quote, List<WebInsQuoteDetail> quoteDetailList, List<InRiskSi> specifiedItemsList, List<WebInsClient> clientList, String cardType, String bsb, String accountNumber, String cardExpiry, String accountName, String paymentDay, String paymentFrequency) throws Exception {
        try {
            return getPureLinkAdapter().savePolicyToPure(quote, quoteDetailList, specifiedItemsList, clientList, cardType, bsb, accountNumber, cardExpiry, accountName, paymentDay, paymentFrequency);
        } catch (Exception e) {
            LogUtil.warn(getClass(), e);
            throw e;
        }
    }
    
    public RefType getVehicleMinMaxValueData(DateTime queryDate, int vehicleAge) throws RemoteException {
        return getPureLinkAdapter().getVehicleMinMaxValueData(queryDate, vehicleAge);
    }
    
    private PureLinkAdapter getPureLinkAdapter() {
        if (this.pureLinkAdapter == null) {
            pureLinkAdapter = new PureLinkAdapter();
        }
        return pureLinkAdapter;
    }
    
    private ArrayList<InRfDet> getManagerInRfDet() {
        ArrayList<InRfDet> response = new ArrayList<InRfDet>();
        
        InRfDet detail = new InRfDet();
        detail.setAcceptable(true);
        detail.setrDescription("OWNER");
        detail.setrKey3("IPM");
        detail.setrClass("1");
        response.add(detail);
        
        detail = new InRfDet();
        detail.setAcceptable(true);
        detail.setrDescription("REAL ESTATE");
        detail.setrKey3("IPM");
        detail.setrClass("2");
        response.add(detail);
        
        detail = new InRfDet();
        detail.setAcceptable(true);
        detail.setrDescription("SOLICITOR");
        detail.setrKey3("IPM");
        detail.setrClass("3");
        response.add(detail);
        
        detail = new InRfDet();
        detail.setAcceptable(true);
        detail.setrDescription("OTHER");
        detail.setrKey3("IPM");
        detail.setrClass("4");
        response.add(detail);
        
        return response;
    }
    
    private ArrayList<InRfDet> getInvestorContentsInRfDet() {
        ArrayList<InRfDet> response = new ArrayList<InRfDet>();
        
        InRfDet detail = new InRfDet();
        detail.setAcceptable(true);
        detail.setrDescription("Investor Contents Cover $10000");
        detail.setrKey3("10000");
        detail.setrClass("INV_ContentsCover10");
        detail.setrValue(BigDecimal.ZERO);
        detail.setrUnit("$");
        response.add(detail);
        
        detail = new InRfDet();
        detail.setAcceptable(true);
        detail.setrDescription("Investor Contents Cover $30000");
        detail.setrKey3("30000");
        detail.setrClass("INV_ContentsCover30");
        detail.setrValue(new BigDecimal(140));
        detail.setrUnit("$");
        response.add(detail);
        
        detail = new InRfDet();
        detail.setAcceptable(true);
        detail.setrDescription("Investor Contents Cover $50000");
        detail.setrKey3("50000");
        detail.setrClass("INV_ContentsCover50");
        detail.setrValue(new BigDecimal(170));
        detail.setrUnit("$");
        response.add(detail);
        
        return response;
    }
 
    private ArrayList<InRfDet> getNoConvertInRfDet() {
        ArrayList<InRfDet> response = new ArrayList<InRfDet>();
        
        InRfDet detail = new InRfDet();
        detail.setAcceptable(true);
        detail.setrDescription("Test only");
        detail.setrKey3("NCV");
        detail.setrClass("1");
        response.add(detail);
        
        detail = new InRfDet();
        detail.setAcceptable(true);
        detail.setrDescription("Converted manually");
        detail.setrKey3("NCV");
        detail.setrClass("2");
        response.add(detail);
        
        detail = new InRfDet();
        detail.setAcceptable(true);
        detail.setrDescription("Policy not required");
        detail.setrKey3("NCV");
        detail.setrClass("3");
        response.add(detail);
        
        return response;
    }
}
