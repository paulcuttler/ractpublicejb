package com.ract.web.insurance;



import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import com.ract.util.*;
import java.io.*;

@Entity
public class WebInsQuote implements Serializable{
	private static final long serialVersionUID = 217046011916386616L;

	@PersistenceContext
	private static EntityManager em;	

	public static String NEW = "new";
	public static String QUOTE = "quote";
	public static String COMPLETE = "complete";
	public static String COVER_ISSUED = "covered";
	public static String CONVERTED = "converted";
	public static String SEQUENCE_QUOTE = "WEB_QUOTE_SEQ";
	public static String TYPE_MOTOR="motor";
	public static String COVER_COMP = "car";
	public static String COVER_TP = "tp";
	public static String QUOTE_CONVERTED = "quoteConverted";
	public static String TYPE_HOME = "home";
	public static String COVER_BLDG = "bldg";
	public static String COVER_CNTS = "cnts";
	public static String COVER_INV  = "inv";
	public static String COVER_PEFF = "peff";
	
	public static String COVER_BLDG_AND_CNTS = COVER_BLDG + "_" + COVER_CNTS;
	
	/**
	 * Unique identifier for Web Insurance quote
	 */
	@Id
	private Integer 	webQuoteNo;
	/**
	 * The date given by the user for cover to start
	 */
	private DateTime  	quoteDate;
	/**
	 * The date & time that this quote record was created
	 */
	private DateTime  	createDate;
	/**
	 * Indicates what type of risk this quote will be converted to.
	 * Currently allow "car" & "tp"
	 */
	private String 		coverType;
	/**
	 * An indicator to show to what point this quote has been taken
	 */
	private String 		quoteStatus;
	/**
	 * Currently a quote may only be a "motor" quote, which can become
	 * either a "car" or "tp" risk.
	 */
	private String 		quoteType;
	/**
	 * The date and time that cover is actually commenced
	 */
	private DateTime    startCover;

	public DateTime getStartCover() {
		return startCover;
	}
	public void setStartCover(DateTime startCover) {
		this.startCover = startCover;
	}
	public Integer getWebQuoteNo() {
		return webQuoteNo;
	}
	public void setWebQuoteNo(Integer webQuoteNo) {
		this.webQuoteNo = webQuoteNo;
	}
	public DateTime getQuoteDate() {
		return quoteDate;
	}
	public void setQuoteDate(DateTime quoteDate) {
		this.quoteDate = quoteDate;
	}
	public DateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(DateTime createDate) {
		this.createDate = createDate;
	}
	public String getCoverType() {
		return coverType;
	}
	public void setCoverType(String coverType) {
		this.coverType = coverType;
	}
	public String getQuoteStatus() {
		return quoteStatus;
	}
	public void setQuoteStatus(String status) {
		this.quoteStatus = status;
	}
	public String getQuoteType() {
		return quoteType;
	}
	public void setQuoteType(String quoteType) {
		this.quoteType = quoteType;
	}
	public String toString()
	{
		String str = null;
		try
		{
		  str = this.getSecuredQuoteNo()
		          + " " + this.createDate
		          + " " + this.quoteType
		          + " " + this.coverType
		          + " " + this.quoteStatus;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return str;
	}
	
	/**
	 * Creates a secured quote number - padded to 8 digits including
	 * a leading checkdigit
	 * @return
	 * @throws Exception
	 */
/*	public String getSecuredQuoteNo()throws Exception
	{
		String cd = checkDigit(this.webQuoteNo + "",7);
        String qStr = this.webQuoteNo + "";
        int startLength = (this.webQuoteNo + "").length();
		for(int xx= startLength;xx < 7;xx++)
		{
			qStr = "0" + qStr;
		}
		return cd + qStr;
	}
	*/
	/* version with 2 check digits appended*/
	public String getSecuredQuoteNo() throws Exception
	{
		String cd = getCheckDigits(this.webQuoteNo+"");
        String qStr = this.webQuoteNo + cd;
		return qStr;
	}
	
	/* validate quote number */
	public static boolean validateCheckDigits(String fQuoteNo)throws Exception
	{
		String cQuoteNo = "";
		String cCD = "";
		int l = fQuoteNo.length();
		for(int xx = l-1;xx>=0; xx--)
		{
		  if(xx > l-3)cCD = fQuoteNo.charAt(xx) + cCD;
		  else if(fQuoteNo.charAt(xx)!=' ')cQuoteNo = fQuoteNo.charAt(xx) + cQuoteNo;
		}
		String newCd = getCheckDigits(cQuoteNo);
		return newCd.equals(cCD);
	}
	
	public static Integer validateQuoteNo(String fQuoteNo)throws Exception
	{
		String cQuoteNo = "";
		String cCD = "";
		int l = fQuoteNo.length();
		for(int xx = l-1;xx>=0; xx--)
		{
		  if(xx > l-3)cCD = fQuoteNo.charAt(xx) + cCD;
		  else if(fQuoteNo.charAt(xx)!=' ')cQuoteNo = fQuoteNo.charAt(xx) + cQuoteNo;
		}
		String newCd = getCheckDigits(cQuoteNo);
		if(newCd.equals(cCD))
		{ 
			return new Integer(cQuoteNo);
		}
		else return null;
	}
	
	/* double check digit */
	public static String getCheckDigits(String numString)throws Exception
	{
		String cd = checkDigit(numString,8);
		String cd2 = checkDigit(numString + cd,8);
		return cd+cd2;
	}
	public static String getCheckDigits(Integer num)throws Exception
	{
		return getCheckDigits(num + "");
	}
	
	/**
	 * Format secured quote number as ## ### ###
	 * @return String
	 * @throws Exception
	 */
/*	public String getFormattedQuoteNo()throws Exception
	{
		String qs = this.getSecuredQuoteNo();
		StringBuffer bs = new StringBuffer(qs);
		bs.insert(5, ' ');
		bs.insert(2, ' ');
		return bs.toString();
	}
	*/
	
	/*format from right hand end */
	public String getFormattedQuoteNo() throws Exception
	{
		String qs = this.webQuoteNo + getCheckDigits(webQuoteNo + "");
		StringBuffer bs = new StringBuffer(qs);
		int l = qs.length();
		if(l>3)
		{
			bs.insert(l-3, ' ');			
		}
		if(l>6)
		{
			bs.insert(l-6, ' ');
		}
		return bs.toString();
	}
	
	
	
	
    public static String checkDigit(String instr,int digits)
    {
    	//pad out to the current number of digits
    	String inCh = instr;
    	for(int xx = instr.length();xx<= digits;xx++)
    	{
    		inCh = "0" + inCh;
    	}
    	inCh = inCh + "0";
    	String t1 = "";
    	int digit = 0;
    	int unit = 0;
    	int ten = 0;
    	int sum = 0;
    	int cd = 0;
        for(int ind = inCh.length()-1 ;ind>=0;ind--)
        {  
           t1 = inCh.substring(ind,ind+1);

    	   if(ind % 2 != 0)
    	   {
    		  digit = 2 * Integer.parseInt(t1);
    		  if(digit>9)digit = digit - 9;
    	   }
    	   else
    	   {
    		   digit = Integer.parseInt(t1);
    	   }
     	   sum = sum + digit; 
        }
        cd = 10 - sum % 10;
        if(cd==10)cd = 0;
    	return cd + "";
    }


}
