package com.ract.web.insurance;
import java.io.*;
/**
 * Represents a motor vehicle type as described in ins-gl-main
 * Where attributes represent coded fields, the code is generally used.
 * @author dgk1
 *
 */

public class GlVehicle implements Serializable{
	
 	/**
	 * 
	 */
	private static final long serialVersionUID = -3856547784082132301L;
	private Integer vehYear;
    private String make;
    private String model;
    private String bodyType;
    private String transmission;
    private String cylinders;
//    private String description;
    private String nvic;
    private String engineCap;
    private String engineDesc;
    private String variant;
    private Integer value;
    private String acceptable;
    private String seriesDesc;
    
    private int crPoints;
    private int tpPoints;
    
	public String getSeriesDesc() {
		return seriesDesc;
	}
	public void setSeriesDesc(String seriesDesc) {
		this.seriesDesc = seriesDesc;
	}
	public String getAcceptable() {
		return acceptable;
	}
	public void setAcceptable(String acceptable) {
		this.acceptable = acceptable;
	}
	public Integer getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = new Integer(value);
	}
	public void setValue(Integer value)
	{
		this.value = value;
	}
	public String getNvic() {
		return nvic;
	}
	public void setNvic(String nvic) {
		this.nvic = nvic;
	}
	public String getEngineCap() {
		return engineCap;
	}
	public void setEngineCap(String engineCap) {
		this.engineCap = engineCap;
	}
	public String getEngineDesc() {
		return engineDesc;
	}
	public void setEngineDesc(String engineDesc) {
		this.engineDesc = engineDesc;
	}
	public String getVariant() {
		return variant;
	}
	public void setVariant(String variant) {
		this.variant = variant;
	}
	public Integer getVehYear() {
		return vehYear;
	}
	public void setVehYear(Integer vehYear) {
		this.vehYear = vehYear;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getBodyType() {
		return bodyType;
	}
	public void setBodyType(String bodyType) {
		this.bodyType = bodyType;
	}
	public String getTransmission() {
		return transmission;
	}
	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}
	public String getCylinders() {
		return cylinders;
	}
	public void setCylinders(String cylinders) {
		this.cylinders = cylinders;
	}
	public void setVehYear(int year)
	{
		this.vehYear = new Integer(year);
	}
	//Nvic, variant,engine capacity and engine type
	public String getSummary()
	{
		return this.variant
		        + " " + this.seriesDesc
		        + " " + this.engineCap 
		        + " " + this.engineDesc;
	}
//	public String getDescription() {
//		return description;
//	}
//	public void setDescription(String description) {
//		this.description = description;
//	}
	public String getExtendedDescription()
	{
		String desc = this.vehYear + " " + this.make + " " + this.model 
		       + "\n" + this.variant + " " + this.seriesDesc + " " + this.bodyType
		       + "\n" + this.engineCap + " " + this.cylinders + " " + this.transmission
		       + "\n" + this.engineDesc 
		       + "\n NVIC = " + this.nvic;
		return desc;
	}
	public String listAttributes()
	{
		String desc = "vehYear      = " + vehYear
				    + "\n make         = " + make
				    + "\n model        = " + model
				    + "\n bodyType     = " + bodyType
				    + "\n transmission = " + transmission
				    + "\n cylinders    = " + cylinders
				    + "\n nvic         = " + nvic
				    + "\n engineCap    = " + engineCap
				    + "\n engineDesc   = " + engineDesc
				    + "\n variant      = " + variant
				    + "\n  value       = " + value
				    + "\n acceptable   = " + acceptable
				    + "\n seriesDesc   = " + seriesDesc;
       return desc;
	}
	
    public int getCrPoints() {
        return crPoints;
    }
    
    public void setCrPoints(int crPoints) {
        this.crPoints = crPoints;
    }
    
    public int getTpPoints() {
        return tpPoints;
    }
    
    public void setTpPoints(int tpPoints) {
        this.tpPoints = tpPoints;
    }
}
