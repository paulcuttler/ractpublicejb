package com.ract.web.insurance;

import com.ract.util.*;
import java.io.*;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class WebInsQuoteDetail implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 4245378483347338098L;
	public static String TRUE = "yes";
    public static String FALSE = "no";
    public static String FIFTY_PLUS = "50+";
    public static String DRIVER_DOB = "driverDob";
    public static String EXCESS = "excess";
    public static String NO_CLAIM_DISCOUNT = "ncd";
    public static String NVIC = "nvic";        
    public static String AGREED_VALUE = "agreedValue"; 
    public static String SITUATION_ID = "situationId"; 
    public static String SITUATION_NO = "situationStreetNo";
    public static String SITUATION_LATITUDE = "situationLatitude";
    public static String SITUATION_LONGITUDE = "situationLongitude";
    public static String SITUATION_GNAF = "situationGnaf";
    public static String SITUATION_STREET = "situationStreetName";
    public static String SITUATION_SUBURB = "situationSuburb";
    public static String SITUATION_POSTCODE = "situationPostcode";
    
    public static String FIN_TYPE = "finType"; 
/*    public static String FIN_TYPE_NONE = "n";
    public static String FIN_TYPE_LEASE = "l";
    public static String FIN_TYPE_SECURED = "s";
    public static String FIN_TYPE_OTHER = "o";*/
    public static String WINDSCREEN = "windscreen"; 
    public static String HIRE_CAR = "hireCar";
    public static String USEAGE = "primaryUse";
    public static String IMOBILISER = "engineImobiliser";
    public static String FIRE_THEFT = "fireAndTheft";
    public static String PREMIUM = "grossPremium";
    public static String PAYMENT = "payment";
    public static String REG_NO = "regNo";
    public static String DD_TYPE = "accountType";
    public static String DD_NAME = "accountName";
    public static String DD_BSB = "bsbNo";
    public static String DD_NO = "accountNo";
    public static String DD_EXP = "accExpiry";
    public static String DD_DAY = "ddDay";
    public static String PAY_METHOD = "payMethod";
    public static String PAY_FREQ = "payFreq";
    public static String COMP_DATE = "completionDate";
    public static String COVER_START = "coverStart";
    public static String PAY_DD = "DD";
    public static String PAY_CASH = "nonDD";
    public static String CARD_NO = "cardNo";
    public static String CLIENT_DOB = "clientDob";
    public static String RACT_CLIENT_NO = "ractClientNo";
    public static String PIP_DISCOUNT = "pipDiscount";
    public static String PAY_MONTHLY = "monthly";
    public static String PAY_ANNUALLY = "annually";
    public static String ACC_TYPE_VISA = "Visa";
    public static String ACC_TYPE_MASTERCARD = "Mastercard";
    public static String ACC_TYPE_DEBIT = "debit";
    public static String REF_TYPE_USAGE = "vehicleUsage";
    public static String REF_TYPE_DRIVING_FREQUENCY = "drivingFrequency";
    public static String REF_TYPE_FINANCIERS = "financiers";
    public static String CONFIRMATION_EMAIL_ADDRESS = "confirmationEmailAddress";
    public static String FINANCIER = "financier";
    public static String SALES_BRANCH = "salesBranch";
    public static String BUILDING_TYPE = "BuildingType";
    public static String CONSTRUCTION = "Construction";
    public static String YEAR_CONST = "yearConstructed";
    public static String ROOF = "Roof";
    public static String OCCUPANCY = "Occupancy";
    public static String SECURITY_LOCK = "Security Lock";
    public static String SECURITY_ALARM = "Security Alarm";
    public static String BUSINESS_USE = "BusinessUse";
    public static String BUILDING_WORK = "BuildingWork";

    public static String WORK_UNDERTAKEN = "workUndertaken";
    public static String EXCEED_50K = "exceed50K";
    
    public static String UNRELATED_PERSONS = "UnrelatedPer";
    public static String CRIMINAL_CONV = "CriminalConv";
    public static String CLAIM = "Claims";
    public static String FINANCE_TYPE = "FinType";
    public static String OPTIONS = "Options";
    public static String SPEC_CNTS = "SpecCnts";
    public static String SPEC_PEFF = "SpecPeff";
    public static String UNSPEC_PEFF = "UnspecPeff";
    public static String SMOKE_ALARM = "SmokeAlarm";
    public static String CONTENTS_COVER_20 = "contentsCover20";
    public static String CONTENTS_COVER_40 = "contentsCover40";
    public static String BLDG_SUM_INSURED = "bldgSumIns";
    public static String CNTS_SUM_INSURED = "cntsSumIns";
    public static String BLDG_EXCESS = "bldgExcess";
    public static String CNTS_EXCESS = "cntsExcess";
    public static String BLDG_OPTION = "bldgOption";
    public static String CNTS_OPTION = "cntsOptions";
    public static String INV_SUM_INSURED = "invSumIns";
    public static String INV_EXCESS = "invExcess";
    public static String INV_OPTION = "invOption";
    public static String MAX_SUM_INSURED = "MaxSumInsured";
    public static String MIN_SUM_INSURED = "MinSumInsured";
    public static String SUBURB_UNACCEPTABLE = "UnacceptableSuburb";
    public static String EXCESS_OPTIONS = "Excess_opt";
    public static String RENTAL_MANAGER = "Manager";
    public static String IS_HPV = "isHPV";
    
    public static String AGENT_CODE	=	"agentCode";
    public static String AGENT_USER	=	"agentUser";

    public static String ANNUAL_PAYMENT_BASE_ANNUAL_PERMIUM = "annualPaymentBaseAnnualPremium";
    public static String ANNUAL_PAYMENT_GST = "annualPaymentGst";
    public static String ANNUAL_PAYMENT_STAMP_DUTY = "annualPaymentStampDuty";
    public static String ANNUAL_PAYMENT_TOTAL_ANNUAL_PREMIUM = "annualPaymentTotalAnnualPremium";
    public static String ANNUAL_PAYMENT_INSTALMENT_AMOUNT = "annualPaymentInstalmentAmount";
    public static String MONTHLY_PAYMENT_BASE_ANNUAL_PERMIUM = "monthlyPaymentBaseAnnualPremium";
    public static String MONTHLY_PAYMENT_GST = "monthlyPaymentGst";
    public static String MONTHLY_PAYMENT_STAMP_DUTY = "monthlyPaymentStampDuty";
    public static String MONTHLY_PAYMENT_TOTAL_ANNUAL_PREMIUM = "monthlyPaymentTotalAnnualPremium";
    public static String MONTHLY_PAYMENT_INSTALMENT_AMOUNT = "monthlyPaymentInstalmentAmount";
    
    public static String AGREED_VALUE_LOWER = "agreedValueLower"; 
    public static String AGREED_VALUE_UPPER = "agreedValueUpper"; 
    public static String AGREED_VALUE_DEFAULT = "agreedValueDefault"; 
    
    public static String TERMS_AND_CONDITIONS_ACCEPTED = "termsAndConditionsAccepted";
    
    public static String PURE_POLICY_REFERENCE = "purePolicyReference";
    public static String WEB_QUOTE_TYPE = "webQuoteType";
    public static String WEB_QUOTE_TYPE_HOME = "webQuoteTypeHome";
    public static String WEB_QUOTE_TYPE_MOTOR = "webQuoteTypeMotor";
    
    public static String NO_CONVERT = "NoConvert";
    
	@Id
	private WebInsQuoteDetailPK pk;
	private String fieldValue;
	private DateTime tStamp;
	private Integer intKey;
	private String stringKey;
	
	public WebInsQuoteDetail(Integer webInsQuoteNo,
			                 String fieldName,
			                 InRfDet rfDet)
	{
		this.pk = new WebInsQuoteDetailPK();
		this.pk.fieldName = fieldName;
		this.pk.webQuoteNo = webInsQuoteNo;
		this.tStamp = new DateTime();
		this.intKey = rfDet.getRfDateSeq();
		this.stringKey = rfDet.getrKey3();
		this.fieldValue = rfDet.getrClass();
	}
	
	public WebInsQuoteDetail(Integer quoteNo,
			                 String fName,
			                 String fValue,
			                 InRfDet det)
	{
		this.pk = new WebInsQuoteDetailPK();
		this.pk.fieldName = fName;
		this.pk.webQuoteNo = quoteNo;
		this.tStamp = new DateTime();
		this.intKey = det.getRfDateSeq();
		this.stringKey = det.getrKey3();
		this.fieldValue = fValue;
	}

	public Integer getInKey() {
		return intKey;
	}

	public void setInKey(Integer intKey) {
		this.intKey = intKey;
	}

	public String getStringKey() {
		return this.stringKey;
	}

	public void setStringKey(String stringKey) {
		this.stringKey = stringKey;
	}

	public WebInsQuoteDetail()
	{
       pk = new WebInsQuoteDetailPK();
	}

	public WebInsQuoteDetail(Integer webQuoteNo, 
			String fieldName, 
			String fieldValue) {
		pk = new WebInsQuoteDetailPK(webQuoteNo,fieldName);
		this.fieldValue = fieldValue;
		this.tStamp = new DateTime();
		this.pk = new WebInsQuoteDetailPK(webQuoteNo,
				fieldName);
	}
	public Integer getWebQuoteNo() {
		return this.pk.webQuoteNo;
	}
	public void setWebQuoteNo(Integer webQuoteNo) {
		this.pk.webQuoteNo = webQuoteNo;
	}
	public String getFieldName() {
		return this.pk.fieldName;
	}
	public void setFieldName(String fieldName) {
		this.pk.fieldName = fieldName;
	}
	public String getFieldValue() {
		return fieldValue;
	}
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
	public DateTime getTStamp() {
		return tStamp;
	}
	public void setTStamp(DateTime stamp) {
		tStamp = stamp;
	}
	public String toString()
	{
		return this.pk.webQuoteNo + "/" + this.pk.fieldName
		       + "(" + this.stringKey + "/" + this.intKey + ")"       
		       + " = " + fieldValue;		 
	}
}
