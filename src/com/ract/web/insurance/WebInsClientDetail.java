package com.ract.web.insurance;
import javax.persistence.*;
import java.io.*;
@Entity
public class WebInsClientDetail implements Serializable, Comparable {
	private static final long serialVersionUID = -9029784087707419798L;
   @EmbeddedId
   WebInsClientDetailPK pk;
   String  detailType;
   Integer detMonth;
   Integer detYear;
   String  detail;
   
    public WebInsClientDetail()
    {
    	pk = new WebInsClientDetailPK();
    }
	public WebInsClientDetail(Integer webClientNo, 
			                  String detailType, 
			                  Integer month,
			                  Integer year, 
			                  String detail) {
		this.pk = new WebInsClientDetailPK(webClientNo);
		this.detailType = detailType;
		this.detMonth = month;
		this.detYear = year;
		this.detail = detail;
	}
	public Integer getWebClientNo() {
		return this.pk.webClientNo;
	}
	public void setWebClientNo(Integer webClientNo) {
		this.pk.webClientNo = webClientNo;
	}
	public String getDetailType() {
		return detailType;
	}
	public void setDetailType(String detType) {
		this.detailType = detType;
	}
	public Integer getDetMonth() {
		return detMonth;
	}
	public void setDetMonth(Integer detMonth) {
		this.detMonth = detMonth;
	}
	public Integer getDetYear() {
		return detYear;
	}
	public void setDetYear(Integer detYear) {
		this.detYear = detYear;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
    public void setPK(WebInsClientDetailPK pk)
    {
    	this.pk = pk;
    }
 	public int compareTo(Object det)
 	{
 	    WebInsClientDetail clDet = (WebInsClientDetail)det;
 	    int compType = this.detailType.compareTo(clDet.detailType);
 		if(compType!=0)return compType;
  		else
 		{
 			if(this.detYear<clDet.detYear)return -1;
 			else if(this.detYear>clDet.detYear)return 1;
 			else
 			{
 				if(this.detMonth<clDet.detMonth)return -1;
 				else if(this.detMonth>clDet.detMonth)return 1;
 				else return 0;
 			}
 		}
 	}
}
