package com.ract.web.insurance;
import java.io.*;
import javax.persistence.*;

@Embeddable
public class InRiskSiLineItemPk implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -2195746040589531016L;
	protected Integer webQuoteNo;
    protected Integer siSeqNo;
    protected Integer siLineNo;

    public InRiskSiLineItemPk(){}

	public Integer getWebQuoteNo() {
		return webQuoteNo;
	}

	public void setWebQuoteNo(Integer webQuoteNo) {
		this.webQuoteNo = webQuoteNo;
	}

	public Integer getSiSeqNo() {
		return siSeqNo;
	}

	public void setSiSeqNo(Integer siSeqNo) {
		this.siSeqNo = siSeqNo;
	}

	public Integer getSiLineNo() {
		return siLineNo;
	}

	public void setSiLineNo(Integer siLineNo) {
		this.siLineNo = siLineNo;
	}

	public InRiskSiLineItemPk(Integer webQuoteNo, 
			                  Integer siSeqNo,
			                  Integer siLineNo) {
		this.webQuoteNo = webQuoteNo;
		this.siSeqNo = siSeqNo;
		this.siLineNo = siLineNo;
	}
	
	public boolean equals(InRiskSiLineItemPk other)
	{
		return this.webQuoteNo.equals(other.webQuoteNo)
		       && this.siSeqNo.equals(other.siSeqNo)
		       && this.siLineNo.equals(other.siLineNo);
	}
    
}
