package com.ract.web.insurance;

import java.io.Serializable;
import java.math.BigDecimal;

public class PremiumDiscount implements Serializable
{
    private static final long serialVersionUID = -4020340289642036538L;

    public final static String TYPE_PIP = "PIP";
	
	public final static String TYPE_SILVER_SAVER = "Silver Saver";
	
	private String discountName;
	
	private BigDecimal discountAmount;

	@Override
	public String toString()
	{
		return "PremiumDiscount [discountAmount=" + discountAmount + ", discountName=" + discountName + "]";
	}

	public String getDiscountName()
	{
		return discountName;
	}

	public void setDiscountName(String discountName)
	{
		this.discountName = discountName;
	}

	public BigDecimal getDiscountAmount()
	{
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount)
	{
		this.discountAmount = discountAmount;
	}
	
	
	
}
