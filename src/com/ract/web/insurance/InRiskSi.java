package com.ract.web.insurance;
 
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.persistence.*;

/**
 * InRiskSI
 * @author dgk1
 * Represents a single Specified item in either a 
 * contents quote/risk or a personal Effects quote/risk
 * Allows for multiple line items within an item (for example
 * a camera with multiple lenses and accessories)
 * 
 */
@Entity
public class InRiskSi implements Serializable
{
	private static final long serialVersionUID = 6682216131958513362L;
@Id 
   private InRiskSiPk pk;
   private String siClass;
   @Transient
   private ArrayList<InRiskSiLineItem> itemList;
   
   /**
    * InRiskSi()
    * Default constructor
    */
   public InRiskSi()
   {
	   itemList = new ArrayList<InRiskSiLineItem>();
   }
   
   /**
    * InRiskSi 
    * Constructor 
    * 
    * @param quoteNo
    * @param seqNo
    * @param siClass
    * @param description
    * @param serialNo
    * @param sumIns
    * 
    * Creates Initial InRiskSiLineItem
    */
   public InRiskSi(Integer quoteNo,
		           Integer seqNo,
		           String siClass,
		           String description,
		           String serialNo,
		           BigDecimal sumIns)
   {
	   InRiskSiLineItem line = new InRiskSiLineItem(quoteNo,
			                                        seqNo,
			                                        new Integer(1),
			                                        description,
			                                        serialNo,
			                                        sumIns);
	   itemList = new ArrayList<InRiskSiLineItem>();
	   itemList.add(line);
	   this.pk = new InRiskSiPk(quoteNo,
			                     seqNo);
	   this.siClass = siClass;
	   
   }
   /**
    * addLineItem
    * @param item
    * Adds an existing InRiskSiLineItem to this InRiskSi
    */
   public void addLineItem(InRiskSiLineItem item)
   {
	   this.itemList.add(item);
   }
   
   /**
    * addLineItem
    * @param description
    * @param serialNo
    * @param sumIns
    * Creates a new InRiskSiLineItem and appends to this InRiskSi
    */
   public void addLineItem(String description,
		                   String serialNo,
		                   BigDecimal sumIns)
   {
	   InRiskSiLineItem item = new InRiskSiLineItem(this.pk.webQuoteNo,
			                                        this.pk.siSeqNo,
			                                        this.itemList.size() + 1,
			                                        description,
			                                        serialNo,
			                                        sumIns);
	   this.itemList.add(item);
   }
   
   /**
    * getLineItem
    * @param x
    * @return InRiskSiLineItem
    * Returns the x'th line item from the list in this instance
    */
   public InRiskSiLineItem getLineItem(Integer x)
   {
	   InRiskSiLineItem item = this.itemList.get(x.intValue());
	   return item;
   }
   
   public InRiskSiPk getPk() {
	   return pk;
   }

   public void setPk(InRiskSiPk pk) {
	   this.pk = pk;
   }

   public String getSiClass() {
	   return siClass;
   }

   public void setSiClass(String siClass) {
	   this.siClass = siClass;
   }

   public ArrayList<InRiskSiLineItem> getItemList() {
	   return itemList;
   }

   public void setItemList(ArrayList<InRiskSiLineItem> itemList) {
	   this.itemList = itemList;
   }
   /**
    * getDescription
    * @param x
    * @return String
    * Returns the description of the x'th line item
    */
   public String getDescription(Integer x)
   {
	   InRiskSiLineItem item = this.itemList.get(x);
	   return item.getDescription();
   }
   /**
    * getSerialNo
    * @param x
    * @return String
    * Returns the serialNo of the x'th line item
    */
   public String getSerialNo(Integer x)
   {
	   InRiskSiLineItem item = this.itemList.get(x);
	   return item.getSerialNo();
   }
   /**
    * getSumIns
    * @param x
    * @return BigDecimal
    * Returns the sumIns of the x'th line item
    */
   public BigDecimal getSumIns(Integer x)
   {
	   InRiskSiLineItem item = this.itemList.get(x);
       return item.getSumIns();	   
   }
   /**
    * getLineCount
    * @return Integer
    * Return the number of line items for this InRiskSi (that is, the number
    * of attached InRiskSiLineItem s
    */
   public Integer getLineCount()
   {
	   return new Integer(this.itemList.size());
   }
   
   public Integer getSiSeqNo()
   {
	   return pk.siSeqNo;
   }
   public Integer getWebQuoteNo()
   {
	   return pk.webQuoteNo;
   }
   
   public String getFormattedLineItem(Integer lineNo)
   {
	   InRiskSiLineItem li = this.itemList.get(lineNo);
	   String op = li.getDescription();
	   NumberFormat nf = NumberFormat.getIntegerInstance();
	   op += " $" + nf.format(li.getSumIns());
	   return op;
   }

}
