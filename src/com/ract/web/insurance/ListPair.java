package com.ract.web.insurance;
import java.io.*;

public class ListPair implements Serializable{
   /**
	 * 
	 */
   private static final long serialVersionUID = 44543147369880977L;
   private String col1;
   private String col2;
   
   public ListPair(String col1, String col2)
   {
	   this.col1 = col1;
	   this.col2 = col2;
   }
   public String getCol1()
   {
	   return col1;
   }
   public String getCol2()
   {
	   return col2;
   }
   
}
