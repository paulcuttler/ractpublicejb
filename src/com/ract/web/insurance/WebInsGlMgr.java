package com.ract.web.insurance;

import java.util.ArrayList;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import com.ract.common.SystemException;
import com.ract.util.DateTime;

/**
 * WebInsMgr provides access to the Glass's guide table for finding particular
 * vehicles. Most of the methods take a GlVehicle object as the input parameter
 * The lookup procedures use only the relevant fields if they are filled in
 * 
 * @author dgk1
 */

@Stateless
@Remote({ WebInsGlMgrRemote.class })
@Local({ WebInsGlMgrLocal.class })
public class WebInsGlMgr implements WebInsGlMgrRemote {
    private PureLinkAdapter pureLinkAdapter;
    private MotorWebAdapter motorWebAdapter;

    /**
     * Get motor vehicle makes
     * 
     * @param veh
     *            Requires vehicle year to be set
     * @return ArrayList of ListPair items. Column1 = Make, Column2 = Make
     * @throws SystemException
     */
    public ArrayList<ListPair> getMakes(GlVehicle veh) throws SystemException {
        return getPureLinkAdapter().getMakes(veh);
    }

    /**
     * Get a list of models (families) for a given year and make.
     * 
     * @param veh
     *            Expects year and make to be set
     * @return List of Strings.
     * @throws SystemException
     */
    public ArrayList<String> getModels(GlVehicle veh) throws SystemException {
        return getPureLinkAdapter().getModels(veh);
    }

    /**
     * Get motor vehicle body types
     * 
     * @param veh
     *            Expects year, make, model to be set
     * @return ArrayList of ListPair. Column1= Body Type, column2 = body type
     *         code
     * @throws SystemException
     */
    public ArrayList<ListPair> getBodyTypes(GlVehicle veh) throws SystemException {
        return getPureLinkAdapter().getBodyTypes(veh);
    }

    /**
     * Get the transmission type options
     * 
     * @param veh
     *            Expects year, make, model, body type to be set
     * @return List of ListItem. Column1 = transmission, column2 = transmission
     *         code
     * @throws SystemException
     */
    public ArrayList<ListPair> getTransmissionTypes(GlVehicle veh) throws SystemException {
        return getPureLinkAdapter().getTransmissionTypes(veh);
    }

    /**
     * Get the cylinder configurations
     * 
     * @param veh
     *            Expects year, make, model,body type, transmission to be set
     * @return List of ListItem. Column1 = cylinder configuration, column2=
     *         cylinder configuration
     * @throws SystemException
     */
    public ArrayList<String> getCylinders(GlVehicle veh) throws SystemException {
        return getPureLinkAdapter().getCylinders(veh);
    }

    /**
     * Get list of vehicles matching entered criteria. Only adds Nvic,
     * variant,engine capacity and engine type to the data supplied.
     * 
     * @param veh
     *            Requires year and make to be set. Optionally uses model,body
     *            type transmission type and cylinder configuration to reduce
     *            the returned list
     * @return List of GlVehicle objects
     * @throws SystemException
     */
    public ArrayList<GlVehicle> getList(GlVehicle veh, DateTime quoteDate) throws SystemException {
        return getPureLinkAdapter().getMatchingVehicles(veh, quoteDate);
    }

    /**
     * Get value from Glass's data given the vehicle nvic
     * 
     * @param nvic
     *            String
     * @return Integer = vehicle value
     * @throws SystemException
     */
    public Integer getValue(String nvic, DateTime quoteDate) throws SystemException {
        return getPureLinkAdapter().getVehicleValue(nvic, quoteDate);
    }

    public String getShortDescription(String nvic) throws Exception {
        return getVehicle(nvic, new DateTime()).getSeriesDesc();
    }

    public GlVehicle getVehicle(String nvic, DateTime quoteDate) throws Exception {
        return getPureLinkAdapter().getVehicle(nvic, quoteDate);
    }

    /**
     * same as getVehicle - now returns exactly the same data
     * 
     * @param nvic
     * @param quoteDate
     * @return
     * @throws Exception
     */
    public GlVehicle getVehicleCodes(String nvic, DateTime quoteDate) throws Exception {
        return getVehicle(nvic, quoteDate);
    }

    /**
     * Lookup a vehicle NVIC based on registration number and state.
     * 
     * @param String
     *            rego Registration number.
     * @param String
     *            state Australian state.
     */
    public String getNvicByRegistration(String rego, String state) throws Exception {
        return getMotorWebAdapter().getNvicByRegistration(rego, state);
    }

    private PureLinkAdapter getPureLinkAdapter() {
        if (this.pureLinkAdapter == null) {
            pureLinkAdapter = new PureLinkAdapter();
        }
        return pureLinkAdapter;
    }

    private MotorWebAdapter getMotorWebAdapter() {
        if (this.motorWebAdapter == null) {
            motorWebAdapter = new MotorWebAdapter();
        }
        return motorWebAdapter;
    }
}
