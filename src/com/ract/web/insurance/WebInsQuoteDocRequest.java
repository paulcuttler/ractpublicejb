package com.ract.web.insurance;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.ract.common.CommonEJBHelper;
import com.ract.common.CommonMgr;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.reporting.ReportDeliveryFormat;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.common.reporting.XMLReportRequest;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.Interval;
import com.ract.util.XDocument;


public class WebInsQuoteDocRequest extends XMLReportRequest 
{
    private static final long serialVersionUID = 2364839912231897674L;
    private XDocument doc=null;
	private WebInsMgrRemote bean = null;
	private int itemSeq = 0;
	InsAdapter insAdapter = null;
	String prodType = null;
	BigDecimal ap = BigDecimal.ZERO;
	BigDecimal mp = BigDecimal.ZERO;
	ResourceBundle bundle = null;

	public WebInsQuoteDocRequest(Integer quoteNo)throws Exception
	{
		bean = WebInsEJBHelper.getWebInsMgr(); 
		bundle = ResourceBundle.getBundle("noticeOfCover");

        initialSettings();
		Node node = null;
		doc = new XDocument();
		reportXML = (Element)doc.addNode(doc, "webCover");
		node = doc.addNode(reportXML,"details");
		doc.addLeaf(node,"printDate",(new DateTime()).formatShortDate());

		WebInsQuote quote = bean.getQuote(quoteNo);
		String det50Plus = bean.getQuoteDetail(quoteNo,"50+");
        if (det50Plus == null || det50Plus.equals("")) {
            throw new Exception("*** 50+ not set ***");
        }
        if (quote.getCoverType() == null || quote.getCoverType().equals("")) {
            throw new Exception("*** Cover type not set ****");
        }
        if (quote.getStartCover() == null) {
            throw new Exception("*** Cover start date not set ***");
        }
		ArrayList<WebInsClient>cltList = bean.getClients(quoteNo);
        Integer ractClientNo = getRactClientNo(cltList);
		insAdapter = new InsAdapter();
		MessageText messageText;
		addCompanyDetails(node);
		prodType = "NPIP";   // default value
		if(ractClientNo!=null)
		{
		    prodType = insAdapter.getNotionalProdType(ractClientNo,
			  	                                    det50Plus,
				                                    quote.getCoverType(),
				                                    quote.getStartCover());
		}
		else if(det50Plus.equalsIgnoreCase("Yes") || det50Plus.equalsIgnoreCase("true")) {
		    prodType = WebInsQuoteDetail.FIFTY_PLUS;
		}
		ArrayList<MessageText> notices = insAdapter.getNotices("QUOTE", 
				                                              (prodType.equalsIgnoreCase("PIP")?"PIP":"NPIP"), 
				                                              quote.getCoverType(), 
				                                              quote.getStartCover());
		Node noticeNode = doc.addNode(node,"notices");
        for (MessageText notice : notices) {
            doc.addLeaf(noticeNode, "notice", notice.getText());
        }

		String isHPV = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.IS_HPV);
		if (isHPV != null && isHPV.equalsIgnoreCase(Boolean.TRUE.toString())) {
			messageText = insAdapter.getMessageText("WEBINS",
	        "QUOTE",
	        "Wording",
	        "",
	        "HPV300",
	        new DateTime());
			doc.addLeaf(noticeNode,"notice",messageText.getText());
		}		
		
		messageText = insAdapter.getMessageText("WEBINS", 
				                     "QUOTE",
				                     "RISK HEADER",
				                     (prodType.equalsIgnoreCase("NPIP")?"PIP":prodType),
				                     quote.getCoverType(), 
				                     quote.getStartCover());
		doc.addLeaf(node,"riskTypeHeading",messageText.getHeading());
		doc.addLeaf(node,"headerText",messageText.getText());
		
		String payType = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.PAY_METHOD);
		String dd=null;
		if(payType==null || payType.equalsIgnoreCase("annually")) {
		    dd = "NONDD";
		} else { 
		    dd=payType;
		}
		messageText = insAdapter.getMessageText("WEBINS",
				                     "QUOTE",
				                     "PREMIUM MESSAGE",
				                     quote.getCoverType(),
				                     dd,
				                     quote.getStartCover());
		doc.addLeaf(node,"premiumMessage",messageText.getText());
		
		String compDate = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.COMP_DATE);		
		doc.addLeaf(node,"completionDate",compDate.substring(0,16));
		
		String refNo = quote.getSecuredQuoteNo() + "";
		doc.addLeaf(node,"referenceNo",refNo);  
		
		this.addOwner(node, cltList);
		this.addPeriodOfCover(node,
				              quote.getStartCover(),
				              compDate);
		this.addInsured(node,cltList);
		
		String frequency = null;
		if(payType.equalsIgnoreCase("DD")) {
			frequency = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.PAY_FREQ);
			if (frequency == null || frequency.isEmpty()) {
				frequency = WebInsQuoteDetail.PAY_METHOD; // backwards compatibility
			}
		} else { // non-DD
			frequency = WebInsQuoteDetail.PAY_ANNUALLY;
		}

        if (quote.getQuoteType().equalsIgnoreCase("motor")) {
            this.addVehDetails(node, quote, frequency);
        } else if (quote.getQuoteType().equals("home")) {
            String[] rTypeList = null;
            if (quote.getCoverType().equalsIgnoreCase("bldg_cnts")) {
                rTypeList = new String[] { "bldg", "cnts" };
            } else {
                rTypeList = new String[] { quote.getCoverType() };
            }

            situationOfRisk(node, quote.getWebQuoteNo());

            for (String riskType : rTypeList) {
                if (rTypeList.length > 1) {
                    String bName = riskType.toLowerCase() + "Heading";
                    this.addScheduleItem(node, bundle.getString(bName).split(",")[0], "");
                }
                this.addHomeDetails(node, quote, riskType, frequency, prodType);
            }
        }
        
        // this is already stored in the DB, but having presented it to customer, stow it back in the database.
        bean.setQuoteDetail(quoteNo, WebInsQuoteDetail.PREMIUM, ap.toString());
        bean.setQuoteDetail(quoteNo, WebInsQuoteDetail.PAYMENT, mp.toString());

        if (payType.equalsIgnoreCase(WebInsQuoteDetail.PAY_DD) && frequency.equals(WebInsQuoteDetail.PAY_MONTHLY)) {
            doc.addLeaf(node, "payment", CurrencyUtil.formatDollarValue(mp));
        }
        doc.addLeaf(node, "annualTotal", CurrencyUtil.formatDollarValue(ap));

        // System.out.println(doc.toXMLString());
    }

    private void addCompanyDetails(Node parent) throws Exception {
        Company c = insAdapter.getCompanyData();
        doc.addLeaf(parent, "companyName", c.getCompanyName());
        String abnStr = "ABN " + c.getAbn();
        doc.addLeaf(parent, "abn", abnStr);
        doc.addLeaf(parent, "afs", "AFS Licence No. " + c.getAFSNumber());
        doc.addLeaf(parent, "contactPhone", c.getWebPhoneNo());
        doc.addLeaf(parent, "address1", c.getAddress1());
        doc.addLeaf(parent, "address2", c.getAddress2());
        doc.addLeaf(parent, "postalAddress", c.getPostalAddress());
    }

    private void addInsured(Node parent, ArrayList<WebInsClient> clientList) {
        StringBuilder assName = new StringBuilder();
        for (WebInsClient client : clientList) {
            if (client.isOwner()) {
                if (!(assName.length() == 0)) {
                    assName.append("\n");
                }
                assName.append(client.getSelectionDescription());
            }
        }
        this.addScheduleItem(parent, "   ", "   ");
        this.addScheduleItem(parent, "The Insured:", assName.toString());
    }

	private void addPeriodOfCover(Node parent,
			                      DateTime coverStart,
			                      String compDate)throws Exception
	{
		SimpleDateFormat tf = new SimpleDateFormat("hh:mma");
        DateTime dt = new DateTime(DateUtil.parseDate(compDate));
        
        String startTime = tf.format(dt);
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String startDay = df.format(coverStart);
		DateTime now = new DateTime();
        DateTime sd = now.getDateOnly();
		Interval oneDay = new Interval();
		oneDay = oneDay.addDays(1);
		sd = sd.add(oneDay);
		/* in the future:  after midnight tonight */
        if(coverStart.onOrAfterDay(sd))
        {
        	startTime = "0:01AM";
        }

                
        Interval year = new Interval();
        year = year.addYears(1);
		DateTime endDate = coverStart.add(year);
		String endDateStr = df.format(endDate);
		this.addScheduleItem(parent,"Period of Insurance:",
				    startTime + " on " + startDay + " to"
                    + "\n 4.00PM on " + endDateStr);
	}
	
    private void addVehDetails(Node parent, WebInsQuote quote, String frequency) throws Exception {
        Integer quoteNo = quote.getWebQuoteNo();
        String det = null;

        String nvic = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.NVIC);
        WebInsGlMgrRemote glMgr = WebInsEJBHelper.getWebGlMgr();
        GlVehicle vehicle = glMgr.getVehicle(nvic, quote.getQuoteDate());
        String vehDesc = vehicle.getVehYear().toString() + " " + vehicle.getMake() + " " + vehicle.getModel();

        String regNo = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.REG_NO);
        addScheduleItem(parent, "    ", "    ");
        addScheduleItem(parent, "Vehicle:", vehDesc + "\n" + regNo.toUpperCase());
        String financier = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.FINANCIER);
        if (financier == null || financier.isEmpty()) {
            financier = "Not under finance";
        } else {
            List<InRfDet> financierList = insAdapter.getLookupData(null, null, WebInsQuoteDetail.FINANCIER, quote.getQuoteDate());
            for (InRfDet detail : financierList) {
                if (detail.getrClass().equals(financier)) {
                    financier = detail.getrDescription();
                }
            }
        }
        addScheduleItem(parent, "    ", "    ");
        addScheduleItem(parent, "Financier:", financier);
        addOptions(parent, quote, quote.getCoverType());
        String vHeading = "Excesses:";
        if (quote.getCoverType().trim().equals(WebInsQuote.COVER_COMP)) {
            det = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.AGREED_VALUE);
            addScheduleItem(parent, "    ", "    ");
            addScheduleItem(parent, "Agreed Value:", "$" + det);
            det = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.EXCESS);
            det = formatExcess(det, WebInsQuote.COVER_COMP, quote.getQuoteDate());
            addScheduleItem(parent, "    ", "    ");
            add2LineItem(parent, vHeading, "", "Excess/Optional Excess:", det);
            vHeading = "";
        } else {
            // Basic excess for TP is fixed and is supplied by the following code, so this bit is not needed
        }

        ArrayList<ListPair> xsList = insAdapter.getFixedExcesses(prodType, quote.getCoverType(), quote.getStartCover());

        for (ListPair fixedExcessItem : xsList) {
            this.add2LineItem(parent, vHeading, "", fixedExcessItem.getCol1() + ":", fixedExcessItem.getCol2());
            vHeading = "";
        }

        String totalPremium;
        String instalmentAmount;
        if (WebInsQuoteDetail.PAY_ANNUALLY.equals(frequency)) {
            totalPremium = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.ANNUAL_PAYMENT_TOTAL_ANNUAL_PREMIUM);
            instalmentAmount = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.ANNUAL_PAYMENT_INSTALMENT_AMOUNT);
        } else {
            totalPremium = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.MONTHLY_PAYMENT_TOTAL_ANNUAL_PREMIUM);
            instalmentAmount = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.MONTHLY_PAYMENT_INSTALMENT_AMOUNT);
        }
        ap = convertStringToBigDecimal(totalPremium);
        mp = convertStringToBigDecimal(instalmentAmount);
    }

    private BigDecimal convertStringToBigDecimal(String value) throws SystemException {
        DecimalFormat format = new DecimalFormat();
        format.setParseBigDecimal(true);
        BigDecimal result;
        try {
            result = (BigDecimal) format.parse(value);
        } catch (ParseException e) {
            throw new SystemException("Failed to convert string to decimal", e);
        }
        return result;
    }

    private void addHomeDetails(Node parent, WebInsQuote quote, String riskType, String frequency, String prodType) throws Exception {
        Integer quoteNo = quote.getWebQuoteNo();
        String fList = bundle.getString(riskType.toLowerCase());

        String[] fNameArray = fList.split(",");
        for (String fName : fNameArray) {
            if (fName.equalsIgnoreCase("options")) {
                addOptions(parent, quote, riskType);
            } else if (fName.equalsIgnoreCase("specifiedItems")) {
                addSpecifiedItems(parent, quote);
            } else {
                String fieldValue = "";
                String[] aa = bundle.getString(fName).split(",");
                WebInsQuoteDetail qDet = bean.getDetail(quoteNo, fName);

                if (qDet != null) {
                    fieldValue = qDet.getFieldValue();
                }
                String fDesc = "";
                if (aa[0].equals("Excess/Optional Excess")) {
                    fDesc = formatExcess(fieldValue, riskType, quote.getQuoteDate());
                    this.add2LineItem(parent, "", "", aa[0] + ":", fDesc);
                } else if (aa[1].equals("NoProcess")) {
                    // no translation needed
                    this.addScheduleItem(parent, aa[0] + ":", fieldValue);
                } else if (aa[1].equals("t/f")) {
                    // is t/f --> Yes/No
                    if (fieldValue.equalsIgnoreCase("true") || fieldValue.equalsIgnoreCase("yes"))
                        fDesc = "Yes";
                    else if (fieldValue.equalsIgnoreCase("false") || fieldValue.equalsIgnoreCase("no"))
                        fDesc = "No";
                } else if (aa[1].equals("Dollars")) {
                    fDesc = formatSumIns(fieldValue);
                    this.addScheduleItem(parent, aa[0] + ":", fDesc);
                } else if (aa[1].equals("Heading")) {
                    this.addScheduleItem(parent, aa[0] + ":", "");
                } else if (aa[1].equalsIgnoreCase("lookup")) {
                    if (qDet != null) {
                        fDesc = "";
                        List<InRfDet> lookupDataList = insAdapter.getLookupData(prodType, null, qDet.getFieldName(), quote.getQuoteDate());
                        for (InRfDet detail : lookupDataList) {
                            if (detail.getrClass().equals(qDet.getFieldValue())) {
                                fDesc = detail.getrDescription();
                                break;
                            }
                        }
                        if (fDesc.equals("")) {
                            fDesc = qDet.getFieldValue();
                        }
                        this.add2LineItem(parent, "", "", aa[0] + ":", fDesc);
                    } else if (aa[0].equalsIgnoreCase("fixedExcess")) {
                        ArrayList<ListPair> xsList = insAdapter.getFixedExcesses(prodType, riskType, quote.getStartCover());
                        for (ListPair excess : xsList) {
                            this.add2LineItem(parent, "", "", excess.getCol1() + ":", excess.getCol2());
                        }
                    }
                }
            }
        }

        String totalPremium;
        String instalmentAmount;
        if (WebInsQuoteDetail.PAY_ANNUALLY.equals(frequency)) {
            totalPremium = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.ANNUAL_PAYMENT_TOTAL_ANNUAL_PREMIUM);
            instalmentAmount = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.ANNUAL_PAYMENT_INSTALMENT_AMOUNT);
        } else {
            totalPremium = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.MONTHLY_PAYMENT_TOTAL_ANNUAL_PREMIUM);
            instalmentAmount = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.MONTHLY_PAYMENT_INSTALMENT_AMOUNT);
        }
        ap = convertStringToBigDecimal(totalPremium);
        mp = convertStringToBigDecimal(instalmentAmount);
    }

	private String formatSumIns(String sumIns)
	{
		String op = "";
		NumberFormat nf = NumberFormat.getIntegerInstance();
		op = "$" + nf.format(new Integer(sumIns));
		return op;
	}

    private String formatExcess(String excessCode, String riskType, DateTime effDate) throws Exception {
        List<InRfDet> excessList = insAdapter.getLookupData(prodType, riskType, WebInsQuoteDetail.EXCESS_OPTIONS, effDate);

        if (null == excessList) {
            throw new SystemException("Failed to extract excessList for code: " + excessCode);
        }
        String output = "";
        for (InRfDet excess : excessList) {
            if (excess.getrClass().equals(excessCode)) {
                output = "$" + excess.getrDescription();
            }
        }
        return output;
    }

    private void situationOfRisk(Node parent, Integer quoteNo) {
        StringBuilder address = new StringBuilder();

        WebInsQuoteDetail dt = bean.getDetail(quoteNo, WebInsQuoteDetail.SITUATION_NO);
        if (dt != null) {
            address.append(dt.getFieldValue());
        }

        dt = bean.getDetail(quoteNo, WebInsQuoteDetail.SITUATION_STREET);
        if (dt != null) {
            if (!(address.length() == 0)) {
                address.append(" ");
            }
            address.append(dt.getFieldValue());
        }

        dt = bean.getDetail(quoteNo, WebInsQuoteDetail.SITUATION_SUBURB);
        if (dt != null) {
            if (!(address.length() == 0)) {
                address.append("\n");
            }
            address.append(dt.getFieldValue());
        }

        dt = bean.getDetail(quoteNo, WebInsQuoteDetail.SITUATION_POSTCODE);
        if (dt != null) {
            if (!(address.length() == 0)) {
                address.append(" ");
            }

            address.append(dt.getFieldValue());
        }

        this.addScheduleItem(parent, "Situation of Risk:", address.toString());
    }

    private void addOptions(Node parent, WebInsQuote quote, String riskType) throws Exception {
        Integer quoteNo = quote.getWebQuoteNo();
        String fName = "";
        String optValue = "No";
        String optDesc = "";
        String hasOption = "No";

        if (quote.getCoverType().trim().equals(WebInsQuote.COVER_COMP)) {
            String hireCar = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.HIRE_CAR);
            if (hireCar != null) {
                if (hireCar.trim().equals(WebInsQuoteDetail.TRUE)) {
                    hasOption = "Yes";
                } else {
                    hasOption = "No";
                }
            }
            this.add2LineItem(parent, "Options:", "", "Hire Car", hasOption);

            String ws = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.WINDSCREEN);
            if (ws != null) {
                if (ws.trim().equals(WebInsQuoteDetail.TRUE)) {
                    hasOption = "Yes";
                } else {
                    hasOption = "No";
                }
            }
            this.add2LineItem(parent, "", "", "Windscreen excess deletion", hasOption);

        } else if (quote.getCoverType().trim().equals(WebInsQuote.COVER_TP)) {
            String tpft = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.FIRE_THEFT);
            if (tpft != null && tpft.trim().equals(WebInsQuoteDetail.TRUE)) {
                hasOption = "Yes";
            } else {
                hasOption = "No";
            }
            this.add2LineItem(parent, "Options:", "", "Fire and Theft", hasOption);
        } else if (WebInsQuote.TYPE_HOME.equals(quote.getQuoteType())) {
            ArrayList<InRfDet> optionsList = insAdapter.getLookupData("", riskType, WebInsQuoteDetail.OPTIONS, quote.getQuoteDate());
            if (optionsList.size() > 0) {
                String heading = bundle.getString("options").split(",")[0];
                this.addScheduleItem(parent, heading, "");
            }

            for (InRfDet option : optionsList) {
                if (!option.getrDescription().equalsIgnoreCase("Unspecified Personal Effects"))
                // personal effects is handled elsewhere
                {
                    fName = option.getrClass().trim();
                    WebInsQuoteDetail det = bean.getDetail(quoteNo, fName);
                    optDesc = option.getrDescription().trim();
                    if (det != null) {
                        if (det.getFieldValue().equalsIgnoreCase("true")) {
                            optValue = "Yes";
                        } else {
                            optValue = "No";
                        }
                        this.add2LineItem(parent, "", "", optDesc, optValue);
                    }
                }
            }

            if (WebInsQuote.COVER_CNTS.equals(riskType)) {
                WebInsQuoteDetail peff = bean.getDetail(quoteNo, "CNTS_PersonalEffects");
                if (peff != null && peff.getFieldValue().equals("true")) {
                    optValue = "Yes";
                } else {
                    optValue = "No";
                }
                this.add2LineItem(parent, "", "", "Unspecified Personal Effects", optValue);
            }
        }
    }
	
	private void addSpecifiedItems(Node parent,WebInsQuote quote)throws Exception
	{
		String specifiedItems = "";
		ArrayList<InRiskSi>siList = bean.getSpecifiedItems(quote.getWebQuoteNo());
		if(siList != null
		   && siList.size() > 0)
		{	
		   addScheduleItem(parent,"Specified Items:","");	
		   for(int xx = 0; xx< siList.size(); xx++)
		   {
		      InRiskSi si = siList.get(xx); 
		      for(int yy = 0; yy < si.getItemList().size();yy++)
		      {
		    	  int siAmount = si.getSumIns(0).intValue();
		    	  this.add2LineItem(parent, "", "", si.getDescription(0), this.formatSumIns(""+siAmount));
		      }   
		   }
		}
		//now add		
		if(!specifiedItems.equals(""))
		{
			addScheduleItem(parent,"Specified Items:",specifiedItems);
		}

	}

	
	private void addScheduleItem(Node parent,
			                     String heading,
			                     String value)
	{
		Node iNode = doc.addNode(parent,"item");
		doc.addLeaf(iNode,"sequence",++itemSeq + "");
		doc.addLeaf(iNode,"heading",heading);
		doc.addLeaf(iNode,"value",value);
	}
	
	private void add2LineItem(Node parent,
			                 String heading,
			                 String value,
			                 String heading2,
			                 String value2)
	{
		Node iNode = doc.addNode(parent,"item");
		doc.addLeaf(iNode,"sequence",++itemSeq + "");
		doc.addLeaf(iNode,"heading",heading);
		doc.addLeaf(iNode,"value",value);
		doc.addLeaf(iNode,"heading2",heading2);
		doc.addLeaf(iNode,"value2",value2);
	}
	
    private Integer getRactClientNo(ArrayList<WebInsClient> list) {
        Integer ractCltNo = null;
        for (WebInsClient clt : list) {
            if (clt.getRactClientNo() != null && !clt.getRactClientNo().equals(new Integer(0))) {
                ractCltNo = clt.getRactClientNo();
            }
        }
        return ractCltNo;
    }

    private void initialSettings() throws RemoteException {
        this.printerGroup = "it-dev";
        this.setFormName("email");
        this.setDeliveryFormat(ReportDeliveryFormat.getReportDeliveryFormat(ReportRequestBase.REPORT_DELIVERY_FORMAT_PDF));
        this.setMIMEType(ReportRequestBase.MIME_TYPE_PDF);
        this.setReportName("WebCoverDoc");
        this.setReportTemplate("/Ract", "webIns/WebCoverDoc");
        try {
            this.constructURL();
        } catch (Exception e) {
            throw new RemoteException(e.getMessage());
        }
        this.addParameter("URL", this.getReportDataURL());

        /* write the file to the report cache directory */
        try {
            CommonMgr comMgr = CommonEJBHelper.getCommonMgr();
            String reportCache = comMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.DIRECTORY_REPORT_CACHE);
            String fileName = reportName + (new java.util.Date().getTime() + "").substring(0, 8) + ".pdf";
            this.setDestinationFileName(reportCache + "/" + fileName);
        } catch (Exception rex) {
            rex.printStackTrace();
            throw new RemoteException("" + rex);
        }
    }

    public String toXMLString() throws Exception {
        return doc.toXMLString();
    }

    private void addOwner(Node parent, List<WebInsClient> clientList) {
        WebInsClient addressOwner = null;
        WebInsClient owner = null;
        String ownerName = "";
        int ownerCount = 0;

        for (WebInsClient client : clientList) {
            if (client.owner) {
                owner = client;
                if (client.preferredAddress) {
                    addressOwner = client;
                }
                ownerCount++;
            }
        }

        if (ownerCount == 1) {
            ownerName = owner.getSelectionDescription();
        } else {
            ownerName = assembleGroupName(clientList);
        }
        doc.addLeaf(parent, "ownerName", ownerName);

        if (addressOwner.getPostAddress() != null) {
            doc.addLeaf(parent, "ownerAddress1", addressOwner.getPostAddress());
        } else if (addressOwner.getPostProperty() != null) {
            try {
                // The following integer instantiation is being used to check that PostProperty is non-numeric - will throw exception and omit PO BOX if so
                Integer poBox = new Integer(addressOwner.getPostProperty());
                doc.addLeaf(parent, "ownerAddress1", "PO BOX " + poBox);
            } catch (Exception ep) {
                doc.addLeaf(parent, "ownerAddress1", addressOwner.getPostProperty().toUpperCase());
            }
        }

        if (addressOwner.getPostStreetChar() != null) {
            String streetAdd = "";
            if (addressOwner.getPostStreetChar() != null) {
                streetAdd = addressOwner.getPostStreetChar() + " ";
            }
            if (addressOwner.getPostStreet() != null) {
                streetAdd += addressOwner.getPostStreet();
            }
            doc.addLeaf(parent, "ownerAddress2", streetAdd.trim());
        }

        doc.addLeaf(parent, "ownerAddress3", addressOwner.getPostSuburb());
        doc.addLeaf(parent, "ownerAddressState", addressOwner.getPostState());
        doc.addLeaf(parent, "ownerAddressPostcode", addressOwner.getPostPostcode());
    }

    private String assembleGroupName(List<WebInsClient> clientList) {
        StringBuilder postalName = new StringBuilder();
        for (WebInsClient client : clientList) {
            if (client.isOwner() && !client.isCltGroup()) {
                if (!(postalName.length() == 0)) {
                    postalName.append("& ");
                }
                postalName.append(client.getSelectionDescription());
            }
        }
        return postalName.toString();
    }
}
