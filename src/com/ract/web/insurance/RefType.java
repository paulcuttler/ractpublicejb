package com.ract.web.insurance;
import java.io.*;

public class RefType implements Serializable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7652573123268504876L;
	String typeCode;
	String tDescription;
	String tText2;
	String tText3;
	
	public RefType(String tc,
			       String td,
			       String t2,
			       String t3)
	{
		this.typeCode = tc;
		this.tDescription = td;
		this.tText2 = t2;
		this.tText3 = t3;
	}
	
	public RefType(){}
	
	public String getTypeCode() {
		return typeCode;
	}
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}
	public String getTDescription() {
		return tDescription;
	}
	public void setTDescription(String description) {
		tDescription = description;
	}
	public String getTText2() {
		return tText2;
	}
	public void setTText2(String text2) {
		tText2 = text2;
	}
	public String getTText3() {
		return tText3;
	}
	public void setTText3(String text3) {
		tText3 = text3;
	}
}
