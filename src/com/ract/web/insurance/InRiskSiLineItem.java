package com.ract.web.insurance;
import java.io.*;
import java.math.BigDecimal;

import javax.persistence.*;

@Entity
public class InRiskSiLineItem implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6596905494931728671L;
	@Id
	protected InRiskSiLineItemPk pk;
	protected String description;
	protected String serialNo;
	protected BigDecimal sumIns;

	public InRiskSiLineItem(){}
	
	public InRiskSiLineItem(Integer quoteNo,
			                Integer seqNo,
			                Integer lineNo,
			                String description,
			                String serialNo,
			                BigDecimal sumIns)
	{
		pk = new InRiskSiLineItemPk(quoteNo,
				                    seqNo,
				                    lineNo);
		this.description = description;
		this.serialNo    = serialNo;
		this.sumIns      = sumIns;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public BigDecimal getSumIns() {
		return sumIns;
	}

	public void setSumIns(BigDecimal sumIns) {
		this.sumIns = sumIns;
	}

	public InRiskSiLineItemPk getPk() {
		return pk;
	}

	public void setPk(InRiskSiLineItemPk pk) {
		this.pk = pk;
	}

	public void setWebQuoteNo(Integer qNo)
	{
		this.pk.webQuoteNo = qNo;
	}
	public void setSiSeqNo(Integer seqNo)
	{
		this.pk.siSeqNo = seqNo;
	}
	public void setSiLineNo(Integer lineNo)
	{
		this.pk.siLineNo = lineNo;
	}
	public Integer getWebQuoteNo()
	{
		return pk.webQuoteNo;
	}
	public Integer getSiSeqNo()
	{
		return pk.getSiSeqNo();
	}
	public Integer getSiLineNo()
	{
		return pk.getSiLineNo();
	}
}
