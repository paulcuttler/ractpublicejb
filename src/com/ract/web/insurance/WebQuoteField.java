package com.ract.web.insurance;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public 	class 		WebQuoteField 
{
	
	@Id
	private WebQuoteFieldPK	pk;
	
	
	public WebQuoteField() 
	{
		// TODO Auto-generated constructor stub
	}
	public WebQuoteField	(	String	quoteType,
								String  quoteSubType,
								String	fieldName
						    ) 
	{
		pk	=	new	WebQuoteFieldPK ();
		
		pk.quoteType	=	quoteType;
		pk.quoteSubType	=	quoteSubType;
		pk.fieldName	=	fieldName;
		
	}

	public String getQuoteType() 
	{
		return pk.quoteType;
	}

	public void setQuoteType(String quoteType) 
	{
		pk.quoteType = quoteType;
	}

	public String getQuoteSubType() 
	{
		return pk.quoteSubType;
	}

	public void setQuoteSubType(String quoteSubType) 
	{
		pk.quoteSubType = quoteSubType;
	}

	public String getFieldName() 
	{
		return pk.fieldName;
	}

	public void setFieldName(String fieldName) 
	{
		pk.fieldName = fieldName;
	}

}
