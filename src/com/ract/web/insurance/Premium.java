package com.ract.web.insurance;
import java.math.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.io.*;

import com.ract.util.LogUtil;
/**
 * Premium
 * @author dgk1 11/11/2008
 * The premium class provides a vehicle to return premium values
 * from the the premium calculation engine.
 */
public class Premium implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6301307179541040614L;
	/**
	 * annualPremium
	 * The annual premium
	 */
	private BigDecimal annualPremium;
	/**
	 * payment
	 * Payment amount of a single payment
	 */
	private BigDecimal payment;
	
	private BigDecimal baseAnnualPremium;
	private BigDecimal gst;
	private BigDecimal stampDuty;
	
	@Override
	public String toString()
	{
		return "Premium [annualPremium=" + annualPremium + ", payment=" + payment + ", premiumDiscounts=" + premiumDiscounts + "]";
	}
	private Collection<PremiumDiscount> premiumDiscounts;
	
	public PremiumDiscount getPremiumDiscount(String name)
	{
		LogUtil.log(this.getClass(), "name="+name);
		PremiumDiscount discount = null;
		PremiumDiscount tmpDiscount = null;
		Collection<PremiumDiscount> discounts = getPremiumDiscounts();
		for (Iterator<PremiumDiscount> i = discounts.iterator(); i.hasNext() && discount == null;)
		{
			tmpDiscount = i.next();
			if (tmpDiscount.getDiscountName().equals(name))
			{
				discount = tmpDiscount;
			}
		}
		LogUtil.log(this.getClass(), "discount="+discount);		
		return discount;
	}
	
	public void addPremiumDiscount(PremiumDiscount discount)
	{
		if (premiumDiscounts == null)
		{
			premiumDiscounts = new ArrayList<PremiumDiscount>();
		}
		premiumDiscounts.add(discount);
	}
	
	public Collection<PremiumDiscount> getPremiumDiscounts()
	{
		return premiumDiscounts;
	}
	public void setPremiumDiscounts(Collection<PremiumDiscount> premiumDiscounts)
	{
		this.premiumDiscounts = premiumDiscounts;
	}
	/**
	 * Premium
	 * Constructor
	 * @param annualPremium
	 * @param payment
	 */
	public Premium(BigDecimal annualPremium, 
			       BigDecimal payment) {
		this.annualPremium = annualPremium;
		this.payment = payment;
		this.premiumDiscounts = new ArrayList<PremiumDiscount>();
	}
	/**
	 * getAnnualPremium
	 * @return BigDecimal
	 */
	public BigDecimal getAnnualPremium() {
		return annualPremium;
	}
	
	/**
	 * setAnnualPremium
	 * @param annualPremium BigDecimal
	 */
	public void setAnnualPremium(BigDecimal annualPremium) {
		this.annualPremium = annualPremium;
	}
	
	/**
	 * getPayment
	 * @return BigDecimal
	 */
	public BigDecimal getPayment() {
		return payment;
	}
	/**
	 * setPayment
	 * @param payment BigDecimal
	 */
	public void setPayment(BigDecimal payment) {
		this.payment = payment;
	}

    
	public BigDecimal getBaseAnnualPremium() {
        return baseAnnualPremium;
    }

    
	public void setBaseAnnualPremium(BigDecimal baseAnnualPremium) {
        this.baseAnnualPremium = baseAnnualPremium;
    }

    public BigDecimal getGst() {
        return gst;
    }

    public void setGst(BigDecimal gst) {
        this.gst = gst;
    }

    public BigDecimal getStampDuty() {
        return stampDuty;
    }

    public void setStampDuty(BigDecimal stampDuty) {
        this.stampDuty = stampDuty;
    }

}
