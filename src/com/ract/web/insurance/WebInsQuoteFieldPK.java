package com.ract.web.insurance;

import javax.persistence.*;
import java.io.*;
@Embeddable
public	class 		WebInsQuoteFieldPK 
		implements 	Serializable
{
    private static final long serialVersionUID = 8926230424271748500L;
	private	String		quoteType;
	private	String		quoteSubType;
	private	String		fieldName;
	
	public WebInsQuoteFieldPK()
	{
		
	}
	

	
	public WebInsQuoteFieldPK	(	String 	quoteType, 
									String	quoteSubType,
									String	fieldName) 
	{
		this.quoteType		=	quoteType;
		this.fieldName 		= 	fieldName;
		this.quoteSubType	=	quoteSubType;
	}



	public String getQuoteType() 
	{
		return quoteType;
	}
	public void setQuoteType(String quoteType) 
	{
		this.quoteType = quoteType;
	}



	public String getQuoteSubType() 
	{
		return quoteSubType;
	}
	public void setQuoteSubType(String quoteSubType) 
	{
		this.quoteSubType = quoteSubType;
	}



	public String getFieldName() 
	{
		return fieldName;
	}
	public void setFieldName(String fieldName) 
	{
		this.fieldName = fieldName;
	}



	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fieldName == null) ? 0 : fieldName.hashCode());
		result = prime * result
				+ ((quoteSubType == null) ? 0 : quoteSubType.hashCode());
		result = prime * result
				+ ((quoteType == null) ? 0 : quoteType.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj) 
		{
			return true;
		}
		
		if (obj == null) 
		{
			return false;
		}
		
		if (getClass() != obj.getClass()) 
		{
			return false;
		}
		
		WebInsQuoteFieldPK other = (WebInsQuoteFieldPK) obj;
		if (fieldName == null) 
		{
			if (other.fieldName != null) 
			{
				return false;
			}
		} else if (!fieldName.equals(other.fieldName)) 
		{
			return false;
		}
		
		if (quoteSubType == null) 
		{
			if (other.quoteSubType != null) 
			{
				return false;
			}
		} else if (!quoteSubType.equals(other.quoteSubType)) 
		{
			return false;
		}
		
		if (quoteType == null) 
		{
			if (other.quoteType != null) 
			{
				return false;
			}
		} else if (!quoteType.equals(other.quoteType)) 
		{
			return false;
		}
		
		return true;
	}


}
