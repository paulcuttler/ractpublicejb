package com.ract.web.insurance;

import java.io.Serializable;
import javax.persistence.*;

@Embeddable
public class InRiskSiPk implements Serializable 
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -2168271725211353040L;
	public Integer webQuoteNo;
    public Integer siSeqNo;
   
    public InRiskSiPk(Integer webQuoteNo, Integer siSeqNo)
    {
	
	  this.webQuoteNo = webQuoteNo;
	  this.siSeqNo = siSeqNo;
    }
    public InRiskSiPk(){}
	
    public Integer getWebQuoteNo() {
		return webQuoteNo;
	}
	public void setWebQuoteNo(Integer webQuoteNo) {
		this.webQuoteNo = webQuoteNo;
	}
	public Integer getSiSeqNo() {
		return siSeqNo;
	}
	public void setSiSeqNo(Integer siSeqNo) {
		this.siSeqNo = siSeqNo;
	}
	
	public boolean equals(InRiskSiPk other)
	{
		return webQuoteNo.equals(other.webQuoteNo)
		      && siSeqNo.equals(other.siSeqNo);
	}
	

   
}
