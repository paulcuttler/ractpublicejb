package com.ract.web.insurance;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;

import org.hibernate.Session;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.CommonEJBHelper;
import com.ract.common.GenericException;
import com.ract.common.MailMgr;
import com.ract.common.PostalAddressVO;
import com.ract.common.ResidentialAddressVO;
import com.ract.common.SequenceMgr;
import com.ract.common.ServiceLocator;
import com.ract.common.SourceSystem;
import com.ract.common.SystemDataException;
import com.ract.common.mail.MailMessage;
import com.ract.common.reporting.ReportGenerator;
import com.ract.common.reporting.ReportRegister;
import com.ract.common.reporting.ReportRequestBase;
import com.ract.insurance.Policy;
import com.ract.payment.receipting.OCRAdapter;
import com.ract.payment.receipting.Payable;
import com.ract.util.ConnectionUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.web.agent.WebAgentStatus;
import com.ract.web.client.CustomerMgrLocal;
import com.ract.web.client.WebClient;
import com.ract.web.common.GnSetting;
import com.ract.web.common.WebPayable;


/**
 * Remote interface to WebInsMgr session bean.
 * This is the main interface which provides methods for setting and retrieving Insurance
 * data for Web Insurance quotes
 * @author dgk1
 * 12/11/2008
 */

@Stateless
@Remote({WebInsMgrRemote.class})
@Local({WebInsMgrLocal.class})
public class WebInsMgr implements WebInsMgrRemote {
	
	@PersistenceContext
	EntityManager em;
	
	@PersistenceContext(unitName="ractPublicEJB")
	Session hsession;

	@Resource(mappedName="java:/WebDS")
  private DataSource webDataSource; 
	
	final Boolean TRUE = new Boolean(true);
	final Boolean FALSE = new Boolean(false);
	
	private InsAdapter insAdapter = new InsAdapter();

	/**
	 * Create a new quote object and writes it to the database.  
	 * @param quoteDate  Date cover is to start
	 * @param quoteType  Type of quote.  Initially only "motor" will be accepted
	 * @return quote number of the newly created quote
	 */
	public Integer startQuote(DateTime quoteDate,
			                  String quoteType)throws Exception
	{
        ServiceLocator serviceLocator = ServiceLocator.getInstance();
        SequenceMgr mgr = CommonEJBHelper.getSequenceMgr();
		WebInsQuote wQuote = new WebInsQuote();		
		Integer quoteNo = mgr.getNextID(WebInsQuote.SEQUENCE_QUOTE);
		wQuote.setWebQuoteNo(quoteNo);
		wQuote.setQuoteDate(quoteDate);
		wQuote.setCreateDate(new DateTime());
		wQuote.setQuoteType(quoteType);
		wQuote.setQuoteStatus("new");
	    em.persist(wQuote);
        return quoteNo;
	}
	
	/**
	 * Returns WebInsQuote object with the given quote number
	 * populated with whatever data has been saved.
	 * 
	 * @param quoteNo
	 * @return WebInsQuote
	 */	
	public WebInsQuote getQuote(Integer quoteNo)
	{
        WebInsQuote quote = em.find(WebInsQuote.class, quoteNo);
		return quote;
	}
	
	public WebInsQuoteDetail getDetail(Integer quoteNo,
			                           String fieldName)
	{
//		System.out.println("WebInsMgr.getDetail: " + quoteNo + " " + fieldName );
		WebInsQuoteDetailPK quoteDetPK = new WebInsQuoteDetailPK(quoteNo, fieldName);
	   	WebInsQuoteDetail det = em.find(WebInsQuoteDetail.class, quoteDetPK);
	   	return det;
	}	
	
	/**
	 * Sets the status of the quote.  
	 * Legal values should be selected from static variable in WebInsQuote: 
	 *    NEW    Set when a quote is created        
     *    QUOTE  Change status to quote once the premium has been displayed      
     *    COMPLETE Change status to complete when the final scripting is displayed   
     *    COVER_ISSUED Indicates that the "Finalise" button has been clicked
     *                 and the startCover method has been run successfully.
     *    CONVERTED Will indicate that a policy has been created in the 
     *               insurance system   
	 * @param quoteNo
	 * @param status
	 */	
	public void setQuoteStatus(Integer quoteNo,String status)
	{
		WebInsQuote quote = getQuote(quoteNo);
		quote.setQuoteStatus(status);
		em.merge(quote);
	}
	
  /**
   * Takes the quote number of an existing WebInsQuote and sets the 'quote date' to the
   * supplied date
   * @param quoteNo Integer
   * @param qDate   DateTime
   * @throws Exception
   */	
	public void setQuoteDate(Integer quoteNo, DateTime qDate)
	{
		WebInsQuote quote = getQuote(quoteNo);
		quote.setQuoteDate(qDate);
		em.merge(quote);
	}
	
	public void setCoverType(Integer quoteNo, String coverType)
	{
		WebInsQuote quote = getQuote(quoteNo);
		quote.setCoverType(coverType);
		em.merge(quote);
	}
	
	public void setStartCoverDate(Integer quoteNo, DateTime coverDate)throws Exception
	{
		WebInsQuote quote = getQuote(quoteNo);
		quote.setStartCover(coverDate);
		em.merge(quote);
	}

	public void setQuote(WebInsQuote quote)
	{
		em.merge(quote);
	}

	/**
	 * Set the value of the field to the quote/database.
	 * @param quoteNo Integer
	 * @param fieldName String
	 * @param fieldValue
	 */
	public void setQuoteDetail(Integer quoteNo,
			                 String fieldName,
			                 String fieldValue)
	{
		WebInsQuoteDetail det = getDetail(quoteNo,fieldName);
	   	if(det==null)
	   	{
	   		det = new WebInsQuoteDetail(quoteNo,
	   				                    fieldName,
	   				                    fieldValue);
	   	    em.persist(det);	   	
	   	}
	   	else
	   	{
	   		det.setFieldValue(fieldValue);
	   		em.merge(det);
	   	}
	}
	
	public void setQuoteDetail(WebInsQuoteDetail det)throws Exception
	{
		WebInsQuoteDetail check = getDetail(det.getWebQuoteNo(),
				                          det.getFieldName());
		if(check==null)
		{
			em.persist(det);
		}
		else
		{
			em.merge(det);
		}

	}
	
	/**
	 * Returns the value of a quote detail field 
	 * @param quoteNo
	 * @param fieldName
	 * @return String
	 */	
	public String getQuoteDetail(Integer quoteNo,
			                   String fieldName)
	{
		WebInsQuoteDetailPK quoteDetPK = new WebInsQuoteDetailPK(quoteNo, fieldName);
	   	WebInsQuoteDetail det = em.find(WebInsQuoteDetail.class, quoteDetPK);
		if(det != null && det.getFieldValue() != null)
		{
			return det.getFieldValue().trim();
		}
		else
		{
			return null;
		}
	}

	/**
	 * Validates card number.  If found creates new webClient record for
	 * this quote using the existing client in the customer database.
	 * @param quoteNo
	 * @param cardNo
	 * @param birthDate
	 */
	public WebInsClient setFromRACTClient(Integer quoteNo,
			                              String cardNo,
			                              DateTime birthDate)
	{
		WebInsClient webClt = null;
		try
		{
		   ServiceLocator slo = ServiceLocator.getInstance();
           ClientMgr mgr= ClientEJBHelper.getClientMgr();
           ClientVO clt = mgr.getClientByMembershipCardNumber(cardNo);
           if(clt==null || !clt.getBirthDate().formatShortDate().equals(birthDate.formatShortDate()))
           {
        	   return null;
           }
           else
           { 
        	   WebInsClient rClt = this.getClientByRACTClientNo(quoteNo, 
        			                                           clt.getClientNumber());
        	   if(rClt != null)
        	   {
                 return rClt;
               }
        	   else
        	   {
       		       webClt = this.createInsuranceClient(quoteNo);
        		   webClt.setClientType(WebInsClient.CLIENT_TYPE_INSURANCE);
        		   webClt.setBirthDate(birthDate);
        		   webClt.setOwner(false);
        		   webClt.setEmailAddress(clt.getEmailAddress());
        		   webClt.setGender(clt.getSex() ? WebClient.GENDER_MALE : WebClient.GENDER_FEMALE);
        		   webClt.setGivenNames(clt.getGivenNames());
        		   webClt.setSurname(clt.getSurname());
        		   webClt.setTitle(clt.getTitle());
        		   webClt.setCltGroup(false);
        		   webClt.setHomePhone(clt.getHomePhone());
        		   webClt.setMobilePhone(clt.getMobilePhone());
        		   webClt.setWorkPhone(clt.getWorkPhone());
        		   
        		   PostalAddressVO pa = clt.getPostalAddress();
        		   webClt.setPostPostcode(pa.getPostcode());
        		   webClt.setPostCountry(pa.getCountry());
        		   webClt.setPostState(pa.getState());
        		   webClt.setPostProperty(pa.getProperty());
        		   webClt.setPostStreetChar(pa.getPropertyQualifier());
        		   webClt.setPostStreet(pa.getStreet());
        		   webClt.setPostStsubid(pa.getStreetSuburbID());
        		   webClt.setPostSuburb(pa.getStreetSuburb().getSuburb());
        		   webClt.setPostDpid(pa.getDpid());
        		   webClt.setPostLatitude(pa.getLatitude());
        		   webClt.setPostLongitude(pa.getLongitude());
        		   webClt.setPostGnaf(pa.getGnaf());
        		   webClt.setPostAusbar(pa.getAusbar());
        		   
        		   ResidentialAddressVO ra = clt.getResidentialAddress();
        		   webClt.setResiPostcode(ra.getPostcode());
        		   webClt.setResiCountry(ra.getCountry());
        		   webClt.setResiState(ra.getState());
        		   webClt.setResiProperty(ra.getProperty());
        		   webClt.setResiStreet(ra.getStreet());
        		   webClt.setResiStreetChar(ra.getPropertyQualifier());
        		   webClt.setResiStsubid(ra.getStreetSuburbID());
        		   webClt.setResiSuburb(ra.getStreetSuburb().getSuburb()); 
        		   webClt.setResiDpid(ra.getDpid());
        		   webClt.setResiLatitude(ra.getLatitude());
        		   webClt.setResiLongitude(ra.getLongitude());
        		   webClt.setResiGnaf(ra.getGnaf());
        		   webClt.setResiAusbar(ra.getAusbar());
        		   
        		   webClt.setRactClientNo(clt.getClientNumber());
        	   }
           }
 		}
		catch(Exception ex)
		{
            ex.printStackTrace();			
			return null;
		}
		return webClt;
	}
	
	/**
	 * @param Integer webQuoteNo.
	 * @return Integer Vehicle value in $
	 */
	public Integer getGlVehicleValue(Integer quoteNo)
	{
		Integer value = null;
		WebInsQuote qt = this.getQuote(quoteNo);
		String nvic = getQuoteDetail(quoteNo, "Nvic");
		if(nvic!=null)
		{
            try
            {
               WebInsGlMgrRemote glMgr = (WebInsGlMgr)ServiceLocator.getInstance().getObject("WebInsGlMgr/remote");	
        	   value = glMgr.getValue(nvic,qt.getQuoteDate());
            }
            catch(Exception ex)
            {
            	value=null;
            }
		}
		return value;
	}
	
	@EJB
	CustomerMgrLocal customerMgr;
	
	/**
	 * Create new empty WebInsClient record(s) with a new WebClientNo
	 * @param Integer webQuoteNo 
	 * @return WebInsClient
	 */
	public WebInsClient createInsuranceClient(Integer webQuoteNo)throws Exception
	{
		WebInsClient clt = new WebInsClient();
		clt.setWebQuoteNo(webQuoteNo);
		clt = (WebInsClient)customerMgr.createWebClient(clt);
    return clt;
	}

	/**
	 * Retrieve existing WebInsClient record from the database
	 * @param webClientNo
	 * @return
	 * @throws Exception
	 */
	public WebInsClient getWebInsClient(Integer webClientNo)throws Exception
	{
		WebInsClient clt = em.find(WebInsClient.class, webClientNo);
		return clt;
	}
	
	/**
	 * Retrieve a list of all the detail records associated with the specified client
	 * @param clientNo Integer - the Web Client for which the details are required
	 * @return ArrayList of WebInsClientDetail
	 * @throws Exception
	 */	
    public ArrayList<WebInsClientDetail> getClientDetails(Integer clientNo) throws Exception {
        ArrayList<WebInsClientDetail> list = new ArrayList<WebInsClientDetail>();
        Query query = em.createQuery("select e from WebInsClientDetail e where e.pk.webClientNo = ?1");
        query.setParameter(1, clientNo);
        List rs = query.getResultList();

        if (null != rs) {
            for (Object clientDetail : rs) {
                WebInsClientDetail det = (WebInsClientDetail) clientDetail;
                list.add(det);
            }
        }
        return list;
    }
	
	/**
	 * Stores the client record together with any client detail records
	 * @param clt
	 * @throws Exception
	 */
	public void setClient(WebInsClient clt)throws Exception
	{
		
		if(clt.getWebClientNo()==null)
		{
			throw new Exception("Cannot use setClient on a client without a web client no");
		}
		if(clt.getClientType()==null || !clt.getClientType().equals(WebClient.CLIENT_TYPE_INSURANCE))
		{
			throw new Exception("Null or invalid client type (must be \"insurance\"");
		}
        em.merge(clt);
	}
	
	/**
	 * Sets a single client detail record to the database. 
	 * @param details WebInsClientDetail
	 * @param clt
	 * @throws Exception
	 */
	public void setClientDetail(WebInsClientDetail det)throws Exception
	{
		/* if a detail object has been retrieved from the database
		 * it will have sequence.  If not, one will have to be provided
		 * Do a pass through the list to ascertain the highest number used, then
		 * increment from there.
		 */
		if(det.pk.getDetailSeq()==null)
		{
			det.pk.setDetailSeq(this.getNextDetSeq(det.pk.webClientNo));
			em.persist(det);
		}
		else
		{
			em.merge(det);
		}
	}

	/**
	 * Retrieves a list of WebInsClient records attached to a given quote.
	 * @param webQuoteNo Integer - the web quote number for which the clients are to be retrieved.
	 * @return ArrayList of WebInsClient objects
	 * @throws Exception
	 */	
    public ArrayList<WebInsClient> getClients(Integer webQuoteNo) throws Exception {
        ArrayList<WebInsClient> clients = null;
        Query query = em.createQuery("select e from WebInsClient e where e.webQuoteNo = ?1");
        query.setParameter(1, webQuoteNo);
        List rs = query.getResultList();

        if (null != rs && rs.size() > 0) {
            clients = new ArrayList<WebInsClient>();
            for (Object client : rs) {
                WebInsClient clt = (WebInsClient) client;
                clients.add(clt);
            }
        }

        return clients;
    }
	
	/**
	 * Removes a single client detail record given the Web Client Number and detail sequence
	 * @param clientNo  Integer the web client number
	 * @param detailSeq Integer the sequence of the particular record
	 * @throws Exception
	 */	
	public void removeClientDetail(Integer clientNo, Integer detailSeq)throws Exception
	{
		WebInsClientDetail thisDet = getClientDetail(clientNo,
				                               detailSeq);
		if(thisDet!=null)
		{
		   em.remove(thisDet);
		}
	}
	
	/**
	 * Removes a single WebInsClientDetail record from the database
	 * @param det WebInsClientDetail - the object to be removed.  Requires web client number and
	 * detail sequence to be set
	 * @throws Exception
	 */	
	public void removeClientDetail(WebInsClientDetail det)throws Exception
	{
		this.removeClientDetail(det.pk.webClientNo,det.pk.detailSeq);
	}
	
	public WebInsClientDetail getClientDetail(Integer clientNo,Integer detailSeq)throws Exception
	{
		WebInsClientDetailPK pk = new WebInsClientDetailPK();
		pk.setDetailSeq(detailSeq);
		pk.setWebClientNo(clientNo);
		WebInsClientDetail det = em.find(WebInsClientDetail.class, pk);
		return det;
	}
	
	/**
	 * Get a list of client policies including risks.
	 * @param clientNumber
	 * @return
	 * @throws GenericException
	 */
	@Deprecated
	public Collection<Policy> getClientPolicies(Integer clientNumber, boolean loadRisks, boolean includeCancelled, boolean includeRenewal) throws GenericException
	{
	  insAdapter = new InsAdapter();
	  return insAdapter.getClientPolicies(clientNumber, loadRisks, includeCancelled, includeRenewal);
	}
	
	/**
	 * Get a list of reference values
	 * @param refType
	 * @param effDate
	 * @return
	 * @throws GenericException
	 */
	public ArrayList<RefType> getRefList(String refType, DateTime effDate)throws Exception
	{
		insAdapter = new InsAdapter();
		return insAdapter.getTypeList(refType, effDate);
	}
	
	public GnSetting getGnSetting(String tableName,String tCode )throws Exception
	{
		insAdapter = new InsAdapter();
		return insAdapter.getSysSetting(tableName,tCode);
	}
	
  /**
   * Removes the specified WebClient from the quote.
   * @param clientNo Integer - the web client no.
   * @throws Exception
   */	
	public void removeClient(Integer clientNo)throws Exception
	{
		WebInsClient thisClt = this.getWebInsClient(clientNo);
		if(thisClt!=null)
		{
			em.remove(thisClt);
			this.removeAllClientDetails(clientNo);
		}
	}
	
	/**
	 * Removes a quote detail record from the specified quote number
	 * @param quoteNo Integer - the web Quote Number from which the entry is to be removed
	 * @param fieldName String - the name of the detail record to be removed.
	 * @throws Exception
	 */	
	public void removeQuoteDetail(Integer quoteNo, String fName)throws Exception
	{
		WebInsQuoteDetailPK pk = new WebInsQuoteDetailPK();
		pk.setWebQuoteNo(quoteNo);
		pk.setFieldName(fName);
		WebInsQuoteDetail thisDet = em.find(WebInsQuoteDetail.class,pk);
		if(thisDet!=null)
		{
			em.remove(thisDet);
		}
	}

    /**
     * Do not use.
     * This routine is called by WebInsMgr when 'details' are
     * written to the database.  It is at this point that the detail seq
     * is allocated.  The existence of a non null detailSeq field in a WebInsClient
     * entity indicates that it has been written to the database (or retrieved from 
     * the database
     * @return Integer - the next available sequence for this client
     */
    private Integer getNextDetSeq(Integer clientNo) {
        Query q = em.createQuery("Select e from WebInsClientDetail e where e.pk.webClientNo = ?1");
        q.setParameter(1, clientNo);
        List list = q.getResultList();
        int lastUsedDetSeq = 0;
        WebInsClientDetail det = null;
        if (null != list) {
            for (Object clientDetail : list) {
                det = (WebInsClientDetail) clientDetail;
                if (det.pk.getDetailSeq().intValue() > lastUsedDetSeq) {
                    lastUsedDetSeq = det.pk.getDetailSeq().intValue();
                }
            }
        }

        lastUsedDetSeq++;
        return new Integer(lastUsedDetSeq);
    }
    
  	/**
  	 * A utility routine which lists all client attributes along with the client detail records
  	 * into a printable string
  	 * @param clientNo Integer
  	 * @return
  	 */    
    public String listClientDetails(Integer clientNo)
    {
    	String oStr = "";
    	try 
    	{
			ArrayList<WebInsClientDetail> list = this.getClientDetails(clientNo);
			if(list!=null)
			{
				for(int xx=0;xx<list.size();xx++)
				{
					WebInsClientDetail det  = list.get(xx);
					oStr = oStr 
					     + "\n" + det.pk.detailSeq
					     + "  " + det.getDetMonth() + "/" + det.getDetYear()
					     + "  " + det.getDetail();
				}
			}
		} 
    	catch (Exception e) 
    	{
			e.printStackTrace();
		}
    	return oStr;
    }
    
  	/**
  	 * Remove all WebInsClientDetail records.  Does not remove the WebInsClient record itself
  	 * @param clientNo Integer - the web client number
  	 * @throws Exception
  	 */    
    public void removeAllClientDetails(Integer clientNo)throws Exception
    {
    	ArrayList<WebInsClientDetail> list = this.getClientDetails(clientNo);
    	if(list==null)return;
    	for(int xx = 0;xx<list.size();xx++)
    	{
    		WebInsClientDetail det = list.get(xx);
    		this.removeClientDetail(det);
    	}
    }

    private WebInsClient getClientByRACTClientNo(Integer quoteNo,
    		                                     Integer ractClientNo)throws Exception
    {
    	WebInsClient clt = null;
    	Query q = em.createQuery("Select e from WebInsClient e where e.ractClientNo = ?1"
    			                + " and e.webQuoteNo = ?2");
    	q.setParameter(1, ractClientNo);
    	q.setParameter(2,quoteNo);
    	List ls = q.getResultList();
    	if(ls==null) {
    	    return null;
    	}
    	if(ls.size() > 0)
    	{
    		clt = (WebInsClient)ls.get(0);
    	}
        return clt;	
    }   		      
    
  	/**
  	 * Retrieve a list of all details associated with a particular quote number
  	 * @param quoteNo Integer
  	 * @return ArrayList of WebInsQuoteDetail
  	 * @throws Exception
  	 */    
    public ArrayList<WebInsQuoteDetail> getAllQuoteDetails(Integer quoteNo) throws Exception {
        ArrayList<WebInsQuoteDetail> aList = null;
        Query q = em.createQuery("Select e from WebInsQuoteDetail e" + " where e.pk.webQuoteNo = ?1");

        q.setParameter(1, quoteNo);
        List l = q.getResultList();

        if (null != l && l.size() > 0) {
            aList = new ArrayList<WebInsQuoteDetail>();

            for (Object quoteDetail : l) {
                WebInsQuoteDetail det = (WebInsQuoteDetail) quoteDetail;
                aList.add(det);
            }
        }
        return aList;
    }

    public ArrayList<String> getFieldsForQuoteType(String quoteType, String quoteSubType) throws Exception {
        ArrayList<String> fldNames = new ArrayList<String>();

        Query q = em.createQuery("select e from WebQuoteField e where e.pk.quoteType = ?1 and e.pk.quoteSubType = ?2 ");
        q.setParameter(1, quoteType);
        q.setParameter(2, quoteSubType);

        List l = q.getResultList();

        if (null != l) {
            for (Object webQuote : l) {
                fldNames.add(((WebQuoteField) webQuote).getFieldName());
            }
        }

        return fldNames;
    }
	
	/**
	 * Takes secured quote number and checks it's validity.  If valid
	 * checks for the existence of the quote with that quote number.  
	 * If found returns the quote number, otherwise returns null.
	 * @param securedQuoteNo
	 * @return
	 * @throws Exception
	 */	
	public Integer validateSecuredQuoteNo(String securedQuoteNo)throws Exception
    {
    	Integer quoteNo = null;
    	String quoteNoStr = securedQuoteNo.substring(1);
    	String firstDigit = securedQuoteNo.substring(0,1);
    	String cd = WebInsQuote.checkDigit(quoteNoStr,7);
    	if(!cd.equals(firstDigit))
    	{
    		quoteNo =  null;
    	}
    	else 
    	{
    		WebInsQuote qt = this.getQuote(new Integer(quoteNoStr));
    		if(qt==null)quoteNo = null;
    		else quoteNo = qt.getWebQuoteNo();
    	}
    	return quoteNo;
    }
    

	/**
	 * Calls the premium calculation engine on the web quote.  Requires 
	 * all relevant quote details to be in place.
	 * @param webQuoteNo Integer - the quote number
	 * @param paymentFrequency - "annual" or "monthly" 
	 * @param coverType - "car" or "tp".  Other cover types not implemented at this point
	 * @return Premium 
	 * The return value holds the total annual premium including gst and stamp duty,
	 * as well as the payment size for the selected payment frequency.
	 * @throws Exception
	 */
    public Premium getPremium(Integer webQuoteNo,
    		                  String paymentFrequency,
    		                  String coverType)throws Exception
    {
    	ArrayList<WebInsQuoteDetail> list = this.getAllQuoteDetails(webQuoteNo);
    	ArrayList<InRiskSi> siList = this.getSpecifiedItems(webQuoteNo);
    	WebInsQuote quote = this.getQuote(webQuoteNo);
        if(coverType.equals(WebInsQuote.COVER_COMP)
        	|| coverType.equals(WebInsQuote.COVER_TP))
        {	
    	   addGender(list,webQuoteNo);
        }
        insAdapter = new InsAdapter();
    	Integer ractClientNo = this.getRactClientNo(webQuoteNo);
    	if(ractClientNo == null)ractClientNo = new Integer(-1);
    	
    	List<WebInsClient> clientList = getClients(webQuoteNo);
    	
    	StringBuffer discountTypeList = new StringBuffer();
    	StringBuffer discountList = new StringBuffer();
    	Premium p = insAdapter.calculatePremium(quote,
    			                        ractClientNo, 
    			                        paymentFrequency, 
    			                        list, 
    			                        siList,
    			                        clientList,
    			                        coverType,
    			                        discountTypeList,
    			                        discountList);
    	//a bit out of place, but I can put it here:
    	if(quote.getQuoteStatus().equals(WebInsQuote.NEW))
    	{
    		quote.setQuoteStatus(WebInsQuote.QUOTE);
    		this.setQuote(quote);
    	}
    	
    	String dTypeList = discountTypeList.toString();
    	LogUtil.log(this.getClass(), "dTypeList="+dTypeList);
    	String[] dTypeArray = null;
    	if (dTypeList.indexOf(",") != -1)
    	{
        dTypeArray = dTypeList.split(",");
    	}    	
    	String[] dArray =  null;
    	String dList = discountList.toString();
    	if (dList.indexOf(",") != -1)
    	{
      	dArray = dList.split(",");    		
    	}
    	LogUtil.log(this.getClass(), "dList="+dList);

    	PremiumDiscount pd = null;
    	if (dArray != null)
    	{
      	LogUtil.log(this.getClass(), "many dTypeList="+dTypeList);    		
	    	for(int xx = 0; xx<dArray.length; xx++)
	    	{
	    		pd = new PremiumDiscount();
	    		pd.setDiscountName(dTypeArray[xx]);
	    		pd.setDiscountAmount(new BigDecimal(dArray[xx]));
	    		p.addPremiumDiscount(pd);
	    	}
    	}
    	else
    	{
    		if (dList != null &&
    				dList.trim().length() > 0)
    		{
    			
        	LogUtil.log(this.getClass(), "one dTypeList="+dTypeList);
	    		pd = new PremiumDiscount();
	    		pd.setDiscountName(dTypeList);
	    		pd.setDiscountAmount(new BigDecimal(dList));
	    		p.addPremiumDiscount(pd);
    		}
    	}
    	
    	LogUtil.log(this.getClass(), "Premium="+p);
    	return p;
    }
    
	private void addGender(ArrayList<WebInsQuoteDetail> list,Integer quoteNo)throws Exception
	{
		WebInsClient clt = getRatingDriver(quoteNo);
		if(clt!=null)
		{	
		   WebInsQuoteDetail det = new WebInsQuoteDetail(quoteNo,
				                                      "driverGender",
			 	                                      clt.getGender());
//		det.setFieldName("driverGender");
//		det.setFieldValue(clt.getGender());
		   if(det!=null)
		   {
	          list.add(det);
		   }
		}
	}
    
    public Integer getRactClientNo(Integer webQuoteNo)throws Exception
    {
    	ArrayList<WebInsClient> clients = this.getClients(webQuoteNo);
    	Integer ractClientNo = null;
    	WebInsClient clt = null;
    	if(clients!=null)
    	{	
    		for(int xx = 0; xx<clients.size();xx++)
    		{
    			clt = clients.get(xx);
    			if(clt.getRactClientNo()!=null)ractClientNo = clt.getRactClientNo();
    		}
    	}
    	return ractClientNo;
    }

  	/**
  	 * Applies the rule to determine which of the listed clients who are drivers will
  	 * be used to determine the 'no claim discount' rating.
  	 * @param quoteNo Integer
  	 * @return WebInsClient - the rating driver
  	 * @throws Exception
  	 */
    public WebInsClient getRatingDriver(Integer quoteNo)throws Exception
    {
    	WebInsClient clt = null;
    	ArrayList<WebInsClient> list = this.getClients(quoteNo);
    	ArrayList<WebInsClient> drivers = new ArrayList<WebInsClient>();
    	for(int xx=0;xx<list.size();xx++)
    	{
    		clt = list.get(xx);
    		if(clt.isDriver())
    		{
   				drivers.add(list.get(xx));
     		}
    	}
    	if(drivers.size()>0)
    	{
    	   Collections.sort(drivers, new CompareDriver());
    	   clt = drivers.get(0);
    	}
       	return clt;
    }
    private class CompareDriver implements java.util.Comparator<WebInsClient>
    {
    	public int compare(WebInsClient d1, WebInsClient d2)
    	{
    		int f1 = this.transFreq(d1.getDrivingFrequency());
    		int f2 = this.transFreq(d2.getDrivingFrequency());
    		if(f1 != f2) return f1 - f2;
    		else
    		{  		
    		   //same driving frequency, use birthdate
    			if(d1.getBirthDate().after(d2.getBirthDate()))return -1;
    			else if(d1.getBirthDate().before(d2.getBirthDate()))return 1;
    			else return 0;
    		}
    	}
	    
	    private int transFreq(String freq)
	    {
	    	int x;
	    	if(freq==null)
	    	{
	    		x = 5;
	    	}
	    	else
	    	{
		    	if(freq.trim().equalsIgnoreCase(WebInsClient.DRIVE_DAILY))x = 1;
		    	else if(freq.trim().equalsIgnoreCase(WebInsClient.DRIVE_3_4))x = 2;
		    	else if(freq.trim().equalsIgnoreCase(WebInsClient.DRIVE_1_2))x = 3;
		    	else x = 4;
	    	}
	    	return x;
	    }
    }
    /**
     * Calculates the applicable No claim discount based on the 'rating driver' 
     * @param clt WebInsClient - the 'rating driver' 
     * @return Integer - the No claim discount(%)
     * @throws Exception
     */
    public Integer calculateNCD(WebInsClient clt)throws Exception
    {
    	ArrayList<WebInsClientDetail> accidentList = this.getClientDetails(clt.getWebClientNo());
    	insAdapter = new InsAdapter();
    	Integer currentNcd = insAdapter.getNcd(accidentList,
    			                       clt.getNumberOfAccidents(),
    			                       clt.getYearCommencedDriving());
        return currentNcd;  // could be null;
    }
    
    /**
     * Looks through all the owners attached to the quote.  Returns
     * true if at least one is aged over 50 as at the quote date
     * @param quoteNo
     * @return Boolean - true of there is an over 50, otherwise false
     * @throws Exception
     */
    @Deprecated
    public Boolean checkFiftyPlus(Integer quoteNo)throws Exception
    {
    	Boolean ok = new Boolean(false);
    	ArrayList<WebInsClient> list = this.getClients(quoteNo);
    	WebInsQuote qt = this.getQuote(quoteNo);
    	WebInsClient clt = null;
    	int age;
	
    	if(list==null || list.size()==0)ok = null;
    	else
    	{
    		for(int xx = 0; xx < list.size(); xx++ )
    		{
    			clt = list.get(xx);
    			if(clt.isOwner()) 
    			{
//    				if(clt.cltGroup)
//    				{
//    					insAdapter = new InsAdapter();
//                        ArrayList<Integer>ractList = insAdapter.getGroupMembers(clt.getRactClientNo());                        
//    					for(int yy=0;yy<ractList.size();yy++)
//    					{
//    					   Integer ractClientNo = new Integer(ractList.get(yy));    					 
//    					   ClientMgr cltMgr =  ClientEJBHelper.getClientMgr();
//    					   ClientVO rClt = cltMgr.getClient(ractClientNo);    					   
//    					   if(rClt.getBirthDate()!= null)
//    					   {
//    						   age = DateUtil.getYears(qt.getQuoteDate(), rClt.getBirthDate());
//    						   if(age >= 50) ok = TRUE;
//    					   }
//    					}
//    				}
//    				else 
				    if(clt.getBirthDate()!=null)
    				{   
//System.out.println("Get birthdate for individual " + clt.getWebClientNo());    					
    					age = DateUtil.getYears(qt.getQuoteDate(),clt.getBirthDate());
    			        if(age >= 50)ok = TRUE;
    				}
    			}
    		}
    	}    	
    	return ok;
    }
    
    /**
     * Called after acceptance.  Requires quote status to be "accepted".
     * Generates temporary cover document
     * @param quoteNo Integer - the quote number for which the document
     * is to be generated
     * @throws Exception
     */
    public void printCoverDoc(Integer quoteNo)throws Exception
    {
    	ReportRequestBase rep = null;
    	try
    	{
    		rep = new WebInsQuoteDocRequest(quoteNo);
    		String reportKeyName = ReportRegister.addReport(rep);
    		rep.setReportKeyName(reportKeyName);
    		rep.constructURL();
    		rep.addParameter("URL", rep.getReportDataURL());
    		OutputStream fStream = FileUtil.openStreamForWriting(rep.getDestinationFileName(), false);
    		rep.setOutputStream(fStream);

    		new ReportGenerator().runReport(rep);

    		ReportRegister.removeReport(reportKeyName);
    		String emailTo = getQuoteDetail(quoteNo,"confirmationEmailAddress");
    		if(emailTo==null || emailTo.equals(""))
    		{
    			throw new Exception("WebQuoteNo " + quoteNo + " Email address (confirmationEmailAddress) not set");
    		}
    		// @todo replace String references with Constants
    		String ccAddress = this.getGnSetting("IWS","CCADD").getDescription();
    		String fromAddress = this.getGnSetting("IWS","FromAdd").getDescription();
    		MailMessage message= new MailMessage();
    		message.setSubject("RACT Insurance: Notice of Cover");
    		message.setRecipient(emailTo);
    		message.setMessage("Thank you for your query regarding RACT Insurance, please find attached your Notice of Cover.\n\nYour insurance policy has been finalised and your cover will commence on the selected cover start date.\n\nIf you have any queries please contact RACT on 13 27 22.");
    		message.setBcc(ccAddress);
    		message.setFrom(fromAddress);

    		Hashtable<String,String> files = new Hashtable<String, String>();
    		files.put("NoticeOfCover.pdf", rep.getDestinationFileName());
    		message.setFiles(files);	

    		MailMgr mailMgr = CommonEJBHelper.getMailMgr();
    		mailMgr.sendMail(message);
    	}
    	catch(Exception ex)
    	{
    		System.err.println("Unable to send Notice of Cover for quoteNo: " + quoteNo);
    		ex.printStackTrace();
    		//  System.out.println(((WebInsQuoteDocRequest)rep).toXMLString());
    	}    	
    }
        
//    public void emailReport(String subject,
//    		String reportName,
//    		String fileNameToEmail,
//    		String toAddress,
//    		String bccAddress,
//    		String fromAddress,
//    		String messageBody,
//    		boolean specifyEnvironment) throws ReportException
//    {
//    	String messageSubject = (subject == null ? reportName : subject);
// /*   	System.out.println("\n---------------------------------------"
//    			+ "\nsubject = " + subject
//    			+ "\nreportName = " + reportName
//    			+ "\nfileNameToEmail = " + fileNameToEmail
//    			+ "\ntoAddress = " + toAddress
//    			+ "\nbccAddress = " + bccAddress
//    			+ "\nfromAddress = " + fromAddress
//    			+ "\nmessageBody = " + messageBody
//    			+ "\nspecifyEnvironment = " + specifyEnvironment);
//*/
//    	try
//    	{
//    		Hashtable files = new Hashtable();
//    		files.put(reportName, fileNameToEmail);
//    		MailMgr mailMgr = CommonEJBHelper.getMailMgr();
//    		mailMgr.sendMail(toAddress,
//    				bccAddress,
//    				messageSubject,
//    				messageBody,
//    				fromAddress,
//    				files,
//    				specifyEnvironment,false);
//    	}
//    	catch(IOException ex)
//    	{
//    		System.out.println("\n---------------------------------------"
//    				+ "\nsubject = " + subject
//    				+ "\nreportName = " + reportName
//    				+ "\nfileNameToEmail = " + fileNameToEmail
//    				+ "\ntoAddress = " + toAddress
//    				+ "\nbccAddress = " + bccAddress
//    				+ "\nfromAddress = " + fromAddress
//    				+ "\nmessageBody = " + messageBody
//    				+ "\nspecifyEnvironment = " + specifyEnvironment);
//    		throw new ReportException(ex);
//    	}
//    	//      	Clean up the temporary file once the email is on its way
//    	FileUtil.deleteFile(fileNameToEmail);
//    }

      /**
       * Look up default excess setting for the specified cover (risk) type
       * and date.
       * @param coverType String (bldg,boat,car,cc,cnts,cvan,inv,peff,tp)
       * @param cDate DateTime - Date of enquiry
       * @return Integer - the excess amount
       * @throws Exception
       */ 
    @Deprecated
    public Integer getBasicXs(String coverType, DateTime cDate)throws Exception
    {
    	Integer xs = null;
    	try
    	{
    	   insAdapter = new InsAdapter();
    	   xs = insAdapter.getBasicXs(coverType, cDate);
    	}
    	catch(Exception ex)
    	{
    		throw ex;
    	}
    	return xs;   	
    }
    
    /**
     * Run this routine when the client finally accepts the cover.  All other
     * mandatory questions must already be answered and stored away.
     * startCover sets the quote status to COVER_ISSUED; sets the coverStart date to the 
     * required date and time; enters the COMP_DATE
     * 
     * @param quoteNo  Integer
     * @param cardType  String
     * @param bsb  String
     * @param accountNumber  String
     * @param cardExpiry  String
     * @param accountName  String
     * @param paymentDay  String
     * @param paymentFrequency  String
     * @throws Exception
     */
    @Override
    public void startCover(Integer quoteNo, String cardType, String bsb, String accountNumber, String cardExpiry, String accountName, String paymentDay, String paymentFrequency) throws Exception {
        WebInsQuote quote = this.getQuote(quoteNo);
        checkQuoteDetails(quoteNo, quote.getCoverType());
        List<WebInsClient> clientList = checkClients(quoteNo, quote.getCoverType());

        DateTime compDate = new DateTime();
        String formattedDate = compDate.formatLongDate();
        this.setQuoteDetail(quoteNo, WebInsQuoteDetail.COMP_DATE, formattedDate);

        this.setQuoteDetail(quoteNo, WebInsQuoteDetail.PAY_FREQ, paymentFrequency);
        //quote.setQuoteStatus(WebInsQuote.COVER_ISSUED);
        this.setQuoteStatus(quoteNo, WebInsQuote.COVER_ISSUED);
        
        List<WebInsQuoteDetail> quoteDetailsList = getAllQuoteDetails(quoteNo);
        List<InRiskSi> specifiedItemsList = getSpecifiedItems(quoteNo);
        insAdapter = new InsAdapter();
        String policyReference = insAdapter.savePolicyToPure(quote, quoteDetailsList, specifiedItemsList, clientList, cardType, bsb, accountNumber, cardExpiry, accountName, paymentDay, paymentFrequency);

        this.setQuoteDetail(quoteNo, WebInsQuoteDetail.PURE_POLICY_REFERENCE, policyReference);
        //quote.setQuoteStatus(WebInsQuote.CONVERTED);
        this.setQuoteStatus(quoteNo, WebInsQuote.CONVERTED);
    }
   
    /**
    * checkClients
    * Checks that mandatory data has been supplied and is consistent.
    * Consistency checks:
    *    - if 50+ then there must be at least one policy owner over 50
    *    - There must be exactly one client marked as preferred address
    *    - Group address must be preferred address (nb group record only required
    *      if the group address is not the same as any owner address
    *    - Group must be an owner before checking group address   
    * Motor only:   
    *    - There must be at least one listed driver
    *    - If there is more than one owner there must be at least one client
    *      marked as group address (need not be an owner ie, there can be
    *      a client added just to carry the group address)
    *    - preferred address must be an owner or group.
    *    - Group must be an owner before checking group address
    *    
    * Every client except a "group" must have given names, surname birthdate, 
    * residential address.  A "group" record must have at least given names, surname
    * and postal address   
    */
    public List<WebInsClient> checkClients(Integer quoteNo, String coverType) throws Exception {
        List<WebInsClient> list = this.getClients(quoteNo);
        if (list.isEmpty()) {
            throw new Exception("No attached clients");
        }

        String plus50 = this.getQuoteDetail(quoteNo, WebInsQuoteDetail.FIFTY_PLUS);
        if (plus50.equals(WebInsQuoteDetail.TRUE)) {
            if (!this.checkFiftyPlus(quoteNo).booleanValue())
                throw new Exception("50+ without over fifty client");
        }

        int prefAddressCount = 0;
        int groupCount = 0;
        String quoteType = this.getQuote(quoteNo).getQuoteType();

        // Motor specific
        if (quoteType.equalsIgnoreCase(WebInsQuote.TYPE_MOTOR)) {
            boolean hasDriver = false;
            int ownerCount = 0;

            for (WebInsClient clt : list) {
                if (clt.isOwner())
                    ownerCount++;
                if (clt.isPreferredAddress()) {
                    prefAddressCount++;
                    if (!(clt.isOwner() || clt.isCltGroup()))
                        throw new Exception("Preferred address must be an owner or group");
                }
                if (clt.isCltGroup() && clt.isOwner()) {
                    groupCount++;
                    if (!clt.isPreferredAddress())
                        throw new Exception("Group address is not (but should be) preferred address");

                }
                if (clt.isDriver())
                    hasDriver = true;
            }
            if (!hasDriver && (coverType.equals(WebInsQuote.COVER_COMP) || coverType.equals(WebInsQuote.COVER_TP)))
                throw new Exception("No drivers attached to this quote");
            if (ownerCount == 0)
                throw new Exception("No owners listed");

        } else if (quoteType.equalsIgnoreCase(WebInsQuote.TYPE_HOME)) {
            // Home specific
            for (WebInsClient clt : list) {
                if (clt.isPreferredAddress()) {
                    prefAddressCount++;
                }
                if (clt.isCltGroup() && clt.isOwner()) {
                    groupCount++;
                    if (!clt.isPreferredAddress()) {
                        throw new Exception("Group address is not (but should be) preferred address");
                    }
                }
            }
        }

        // Common tests
        if (prefAddressCount == 0)
        {
            throw new Exception("Preferred address is not set");
        }
        
        return list;
    }
   
    /**    
    * Required detail entries for motor vehicles:
    *   50+                    yes/no             
	*	agreedValue            integer value     
	*	driverDob              dd/mm/yyyy date of birth of rating driver
	*	engineImobiliser       yes/no
	*	excess                 integer value
	*	finType                yes/no
	*	garagedAt              integer -street suburb id
	*	grossPremium           float value
	*	ncd                    integer (% no claim discount)
	*	nvic                   string - motor vehicle nvic
	*	payment                integer value - payment amount for monthly payments
	*	primaryUse             string value
	*	regNo                  motor vehicle registration number
    *   payFreq            monthly/annual
    *   payMethod          dd/cash
	*	windscreen             yes/no (yes => windscreen exces = $0)
 	*	hireCar                yes/no
    *   fireAndTheft           yes/no
    *   
    *   If payment is by direct debit, then account details will also be required:
    *       accountType        cc/debit
    *   	accountName        String - name of account
	*       bsbNo              String of digits (if applicable)
	*       accountNo          String of digits
	*       accExpiry          mm/yy for credit accounts only
	*       ddDay              integer - debit day
	*       
	*   cover start date must be non null, and within 14 days of the quote date.    
    * @param quoteNo
    * @throws Exception
    */
   private void checkQuoteDetails(Integer quoteNo,
		                          String coverType)
   			throws Exception
   {
	   /*
	    * this list would be better held in a table or xml document
	    * somewhere together with a way to discriminate between
	    * risk types.
	    * For now, the only risk types are CAR and TP, and this will suffice
	    */
	   ArrayList <String> fieldNames = null;
	   String quoteSubType;
	   
	   String quoteType = this.getQuote(quoteNo).getQuoteType();
	   ArrayList<WebInsQuoteDetail> list = this.getAllQuoteDetails(quoteNo);
	   Hashtable<String,WebInsQuoteDetail> ht = new Hashtable<String,WebInsQuoteDetail>();
	   
	   WebInsQuoteDetail det = null;
	   
	   int xx = 0;
	   for(xx = 0; xx < list.size(); xx++ )
	   {
		   det = list.get(xx);
		   ht.put(det.getFieldName(), det);
	   }
	   

	   try 
	   {
		   quoteSubType = ht.get("quoteSubType").getFieldValue();
	   } catch (Exception e1) 
	   {
		   quoteSubType = null;
	   }
	   
	   fieldNames	=	this.getFieldsForQuoteType(quoteType, quoteSubType);
  
	   for(	xx=0 ;	xx < fieldNames.size(); xx++ )
	   {
		   String fName = fieldNames.get(xx);
		   
		   det = ht.get(fName);
		   
		   if(		det==null
		      || 	det.getFieldValue()==null
		      || 	det.getFieldValue().equals("")
		      )
		   {
			   throw new Exception("No value entered for " + fName);				   
		   }
	   
	   }
	   

	   try 
	   {
		   ht.get(WebInsQuoteDetail.PAY_METHOD).getFieldValue();
	   } 
	   catch (Exception e) 
	   {
		   throw new Exception ("Quote " + quoteNo.toString() + " doesn't have " + WebInsQuoteDetail.PAY_METHOD);
	   }
   }

   /**
    * @todo
    * Notify users that a new policy has been created through the web interface
    * @param quoteNo
    * @throws Exception
    */
   public void notifyNewPolicy(Integer quoteNo)throws Exception
   {
	   throw new Exception("Notify is not yet implemented");
   }
   
 	/**
 	 * Looks up all groups to which this client belongs and returns a list 
 	 * NOTE that the existence in this list does not imply that a WebInsClient
 	 * exists in the database.
 	 * @param Integer quoteNo
 	 * @return ArrayList<WebInsClient>
 	 */   
   public ArrayList<WebInsClient> getClientGroups(Integer quoteNo)throws Exception
   {
       insAdapter = new InsAdapter();
       Integer ractClientNo = null;
       ArrayList<WebInsClient> list = this.getClients(quoteNo);
       if(list==null)return null;
       for(int xx = 0; xx<list.size();xx++)
       {
           WebInsClient clt = list.get(xx);
           if(clt.getRactClientNo()!=null)
               ractClientNo = clt.getRactClientNo();
       }
       if(ractClientNo!=null) {
           return insAdapter.getAssociatedGroups(ractClientNo);
       } else {
           return null;
       }
   }
   
  /**
   * Creates (persistent) WebInsClient.  Sets preferredAddress to true
   * @param quoteNo
   * @param clt
   * @throws Exception
   */
	public void useExistingGroup(Integer quoteNo, Integer ractClientNo)throws Exception
	{
		ClientMgr mgr = WebInsEJBHelper.getClientMgr();
		ClientVO clt = mgr.getClient(ractClientNo);
		WebInsClient webClt = makeClientFromRactClient(quoteNo,clt);
		webClt.setOwner(true);
		webClt.setCltGroup(true);
		this.setClient(webClt);
	}
	
	private WebInsClient makeClientFromRactClient(Integer quoteNo,ClientVO clt)throws Exception
	{
	    LogUtil.warn(this.getClass(), "*** makeClientFromRactClient");
		WebInsClient webClt = null;
		webClt = this.createInsuranceClient(quoteNo);
		webClt.setClientType(WebInsClient.CLIENT_TYPE_INSURANCE);
		webClt.setBirthDate(clt.getBirthDate());
		webClt.setEmailAddress(clt.getEmailAddress());
		webClt.setGender(clt.getSex()?WebClient.GENDER_MALE:WebClient.GENDER_FEMALE);//?
		webClt.setGivenNames(clt.getGivenNames());
		webClt.setSurname(clt.getSurname());
		webClt.setTitle(clt.getTitle());
		webClt.setHomePhone(clt.getHomePhone());
		webClt.setMobilePhone(clt.getMobilePhone());
		webClt.setWorkPhone(clt.getWorkPhone());
		PostalAddressVO pa = clt.getPostalAddress();
		webClt.setPostPostcode(pa.getPostcode());
		webClt.setPostCountry(pa.getCountry());
		webClt.setPostState(pa.getState());
		webClt.setPostAddress(pa.getProperty());
		webClt.setPostStreetChar(pa.getAddressLine1());
		webClt.setPostStreet(pa.getStreet());
		webClt.setPostStsubid(pa.getStreetSuburbID());
		webClt.setPostSuburb(pa.getStreetSuburb().getSuburb());
		ResidentialAddressVO ra = clt.getResidentialAddress();
		//TODO other residential fields
		webClt.setResiStreet(ra.getStreet());
		webClt.setResiStreetChar(ra.getPropertyQualifier());
		webClt.setResiStsubid(pa.getStreetSuburbID());
		webClt.setResiSuburb(ra.getStreetSuburb().getSuburb()); 
		webClt.setRactClientNo(clt.getClientNumber());
		return webClt;
	}
	
	/**
	 * Find all quotes having a particular status
	 * @param status String
	 * @return ArrayList<WebInsQuote>
	 * @throws Exception
	 */
    public ArrayList<WebInsQuote> findQuotesByStatus(String status) {
        ArrayList<WebInsQuote> list = new ArrayList<WebInsQuote>();
        Query query = em.createQuery("from WebInsQuote e where e.quoteStatus = ?1 ");
        query.setParameter(1, status);
        List rs = query.getResultList();
        if (null != rs) {
            for (Object quote : rs) {
                WebInsQuote qt = (WebInsQuote) quote;
                list.add(qt);
            }
        }

        return list;
    }

	/**
	 * Find all quotes by status for a range of status values
	 * @param statusList ArrayList of String values representing the required set of statuses
	 * @return ArrayList<WebInsQuote>
	 */	
    public ArrayList<WebInsQuote> findQuotesByStatus(String[] statusList) throws Exception {
        ArrayList<WebInsQuote> list = new ArrayList<WebInsQuote>();
        String statusSet = "";
        for (int xx = 0; xx < statusList.length; xx++) {
            if (!statusSet.equals(""))
                statusSet += ",";
            statusSet += "'" + statusList[xx] + "'";
        }
        Query query = em.createQuery("from WebInsQuote e where e.quoteStatus in(" + statusSet + ")");
        List rs = query.getResultList();
        if (null != rs) {
            for (Object quote : rs) {
                WebInsQuote qt = (WebInsQuote) quote;
                this.setSalesBranch(qt.getWebQuoteNo());
                list.add(qt);
            }
        }
        return list;
    }
	
    /**
     * Return a sales branch code according to the sales branch
     * allocation table (client.gn-table.tab-name = "IWB")
     */
	@Deprecated
	public String allocateSalesBranch() throws Exception 
	{
       return allocateSalesBranch(null);
	}
	
	@Deprecated
	public String allocateSalesBranch(String agentCode) throws Exception 
	{
       insAdapter = new InsAdapter();
       return insAdapter.allocateSalesBranch(agentCode);
	}

	@Deprecated
	private void setSalesBranch(Integer quoteNo)throws Exception
	{
		String salesBranch = this.getQuoteDetail(quoteNo, WebInsQuoteDetail.SALES_BRANCH);		
		String agentCode = this.getQuoteDetail(quoteNo, WebInsQuoteDetail.AGENT_CODE);
		
		if(salesBranch==null)
		{
			salesBranch = this.allocateSalesBranch(agentCode);
			this.setQuoteDetail(quoteNo,WebInsQuoteDetail.SALES_BRANCH, salesBranch);
		}
	}
	
    public ArrayList<WebInsQuote> findQuotesBySalesBranch(String salesBranch) throws Exception {
        ArrayList<WebInsQuote> list = new ArrayList<WebInsQuote>();
        try {
            String qString = "from WebInsQuote e where e.quoteStatus = ?1";
            Query query = em.createQuery(qString);
            query.setParameter(1, WebInsQuote.COVER_ISSUED);
            List rs = query.getResultList();
            if (null != rs) {
                for (Object quote : rs) {
                    WebInsQuote qt = (WebInsQuote) quote;
                    qString = "from WebInsQuoteDetail w" + " where w.pk.webQuoteNo = ?1" + " and w.pk.fieldName = ?2";
                    Query dq = em.createQuery(qString);
                    dq.setParameter(1, qt.getWebQuoteNo());
                    dq.setParameter(2, WebInsQuoteDetail.SALES_BRANCH);
                    List ds = dq.getResultList();
                    if (ds.size() == 0) {
                        String agentCode = this.getQuoteDetail(qt.getWebQuoteNo(), WebInsQuoteDetail.AGENT_CODE);
                        String br = this.allocateSalesBranch(agentCode);

                        this.setQuoteDetail(qt.getWebQuoteNo(), WebInsQuoteDetail.SALES_BRANCH, br);
                        if (br.equals(salesBranch)) {
                            list.add(qt);
                        }
                    } else /* should only be one, just take the first */
                    {
                        WebInsQuoteDetail det = (WebInsQuoteDetail) ds.get(0);

                        if (det.getFieldValue() != null && det.getFieldValue().equals(salesBranch)) {
                            list.add(qt);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
        return list;
    }
	
	/**
	 * getSpecifiedItems
	 * @param webQuoteNo
	 * @return ArrayList<InRiskSi>
	 * @throws Exception
	 * Gets all specified items for the given quote number.
	 * Each InRiskSi object returned will also have the relevant inRiskSiLineItems loaded
	 * (if there are any)
	 */
    public ArrayList<InRiskSi> getSpecifiedItems(Integer webQuoteNo) throws Exception {
        ArrayList<InRiskSi> siList = null;
        Query q = em.createQuery("Select e from InRiskSi e" + " where e.pk.webQuoteNo = ?1");

        q.setParameter(1, webQuoteNo);
        List l = q.getResultList();

        if (null != l && l.size() > 0) {
            siList = new ArrayList<InRiskSi>();

            for (Object specifiedItem : l) {
                InRiskSi det = (InRiskSi) specifiedItem;
                det.setItemList(getAllSiLineItems(webQuoteNo, det.getSiSeqNo()));
                siList.add(det);
            }
        }
        return siList;
    }
	
	/**
	 * getSiLineItem
	 * @param quoteNo
	 * @param seqNo
	 * @param lineNo
	 * @return InRiskSiLineItem
	 * @throws Exception
	 * Retrieves as single inRiskSiLineItem from the database
	 */
    public InRiskSiLineItem getSiLineItem(Integer quoteNo, Integer seqNo, Integer lineNo) throws Exception {
        InRiskSiLineItem item = null;
        Query q = em.createQuery("select e from InRiskSiLineItem e " + " where e.pk.webQuoteNo = ?1" + " and   e.pk.siSeqNo    = ?2" + " and   e.pk.siLineNo   = ?3");
        q.setParameter(1, quoteNo);
        q.setParameter(2, seqNo);
        q.setParameter(3, lineNo);
        List l = q.getResultList();
        if (null != l && l.size() > 0) {
            item = (InRiskSiLineItem) l.get(0);
        }
        return item;
    }
	
	/**
	 * getAllSiLineItems
	 * @param quoteNo
	 * @param seqNo
	 * @return ArrayList of InRiskSiLineItems
	 * @throws Exception
	 * Retrieves all InRiskSiLineItem records for the Specified Item
	 * with the given quoteNo and siSequenceNo
	 */
    public ArrayList<InRiskSiLineItem> getAllSiLineItems(Integer quoteNo, Integer seqNo) throws Exception {
        ArrayList<InRiskSiLineItem> list = null;
        Query q = em.createQuery("select e from InRiskSiLineItem e" + " where e.pk.webQuoteNo = ?1" + " and e.pk.siSeqNo = ?2");
        q.setParameter(1, quoteNo);
        q.setParameter(2, seqNo);
        List l = q.getResultList();
        if (null != l && l.size() > 0) {
            list = new ArrayList<InRiskSiLineItem>();
            for (Object riskItem : l) {
                InRiskSiLineItem item = (InRiskSiLineItem) riskItem;
                list.add(item);
            }
        }
        return list;
    }
	
	/**
	 * getSpecifiedItem
	 * @param Integer webQuoteNo
	 * @param Integer siSeqNo
	 * @return InRiskSi
	 * @throws Exception
	 * Retrieves a single InRiskSi record using the quoteNo and seqNo provided.
	 * The returned InRiskSi instance is populated with whatever line items belong.
	 */
    public InRiskSi getSpecifiedItem(Integer webQuoteNo, Integer siSeqNo) throws Exception {
        InRiskSi si = null;
        Query q = em.createQuery("Select e from InRiskSi e" + " where e.pk.webQuoteNo = ?1" + " and e.pk.siSeqNo = ?2");
        q.setParameter(1, webQuoteNo);
        q.setParameter(2, siSeqNo);
        List l = q.getResultList();
        if (null != l && l.size() > 0) {
            si = (InRiskSi) l.get(0);
            si.setItemList(getAllSiLineItems(webQuoteNo, siSeqNo));
        }
        return si;
    }
	
	/**
	 * setSpecifiedItem
	 * @param InRiskSi item
	 * @throws Exception
	 * Writes the given InRiskSi object and any contained InRiskSiLineItem objects
	 * to the database
	 */
	public void setSpecifiedItem(InRiskSi item)throws Exception
	{
		InRiskSi anItem = this.getSpecifiedItem(item.getWebQuoteNo(),item.getSiSeqNo());
		if(anItem==null)
		{  
			em.persist(item);
		}
		else
		{
			em.merge(item);
		}
	    setAllSiLineItems(item);	
	}
	
	/**
	 * setAllSiLineItems
	 * @param InRiskSi item
	 * @throws Exception
	 * Writes all InRiskSiLineItem objects contained in the InRiskSi provided as a parameter,
	 * to the database.
	 */
	public void setAllSiLineItems(InRiskSi item)throws Exception
	{
		InRiskSiLineItem lineItem = null;
		/*need to make sure any removed, are removed from the database*/
		this.removeAllSiLineItems(item);
		for(int xx = 0;xx<item.getItemList().size();xx++)
		{
			lineItem = item.getItemList().get(xx);
			setSiLineItem(lineItem);
		}
	}
	
	/**
	 * setSiLineItem
	 * @param InRiskSiLineItem lineItem
	 * @throws Exception
	 * Writes a single InRiskSiLineItem to the database.
	 */
	public void setSiLineItem(InRiskSiLineItem lineItem)throws Exception
	{
		InRiskSiLineItem al = this.getSiLineItem(lineItem.getWebQuoteNo(),
				                                 lineItem.getSiSeqNo(),
				                                 lineItem.getSiLineNo());
		if(al==null)
		{
			em.persist(lineItem);
		}
		else
		{
			em.merge(lineItem);
		}
	}
	
	/**
	 * setSpecifiedItems
	 * @param ArrayList<InRiskSi> itemList
	 * @throws Exception
	 * Writes an entire array list of InRiskSi object, together with all contained
	 * InRiskSiLineItem objects, to the database.
	 * 
	 */
	public void setSpecifiedItems(ArrayList<InRiskSi> itemList)throws Exception
	{       		
		InRiskSi det = null;
		for(int xx = 0;xx<itemList.size();xx++)
		{
			det = itemList.get(xx);
			this.setSpecifiedItem(det);
	   	}
	}
	
	/**
	 * removeAllSpecifiedItems(Integer quoteNo)
	 *                         
	 */
	public void removeAllSpecifiedItems(Integer quoteNo)throws Exception
	{
		ArrayList<InRiskSi> siList = this.getSpecifiedItems(quoteNo);
		if(siList != null)
		{	
			for(int xx = 0; xx< siList.size();xx++)
			{
				InRiskSi item = siList.get(xx);				
				for(int yy = 0; yy<item.getLineCount();yy++)
		        {
		           InRiskSiLineItem li = item.getLineItem(yy);
		           if(li != null) em.remove(li);
		        }
		        em.remove(item);
			}
		}
	}
	
	/**
	 * removeSpecifiedItem
	 * @param Integer webQuoteNo
	 * @param Integer siSeqNo
	 * @throws Exception
	 * Removes the InRiskSi record and all its contained line items
	 * from the database.
	 */
	public void removeSpecifiedItem(Integer webQuoteNo,
			                        Integer siSeqNo)throws Exception
    {			                        
	    InRiskSi item = this.getSpecifiedItem(webQuoteNo, siSeqNo);
	    if(item != null)
	    {
	    	em.remove(item);
	        for(int xx = 0; xx<item.getItemList().size();xx++)
	        {
	           InRiskSiLineItem li = item.getLineItem(xx);
	           if(li != null) em.remove(li);
	        }
	    }
    }
	
	public void removeAllSiLineItems(InRiskSi si)
	{
		for(int xx = 0; xx< si.getItemList().size();xx++)
		{
			InRiskSiLineItem li = si.getItemList().get(xx);
			em.remove(li);
		}
	}
	
	/**
	 * Retrieves a collection of lookup meta data
	 * @param String Product name (PIP,50+,TSP)
	 * @param String Risk type (car,bldg,cnts)
	 * @param String Descriptive name of the particular data required
	 * @return Collection of <InRFDet>
	 * 
	 * Recognised names are:
	 * BUILDINGTYPE
	 * BUILDINGWORK
	 * BUSINESSUSE
	 * CLAIMS
	 * CONSTRUCTION
	 * CRIMINALCONV
	 * EXCESS_OPT
	 * FINANCIER
	 * FINTYPE
	 * MANAGER
	 * NOCONVERT
	 * OCCUPANCY
	 * OPTIONS
	 * ROOF
	 * SECURITY TYP  )
	 * SECURITYTYPE ) these two are redundant, but I don�t know which is used.
	 * SPECCNTS
	 * SPECPEFF
	 * UNRELATEDPER
	 */
	public ArrayList<InRfDet> getLookupData(String product,
			                                String riskType,
			                                String fieldName,
			                                DateTime effDate)throws Exception
	{		
		insAdapter = new InsAdapter();
		return insAdapter.getLookupData(product, riskType, fieldName, effDate);
	}
	
	/**
	 * getInsSetting
	 * Single entry point for insurance setting lookup for the Web system
	 * @param String Table name to lookup
	 * @param String Key in the table
	 * @param DateTime  Effective date for selection of the correct version(where applicable)
	 * @return String The string representation of the setting requested.
	 *
	 * Valid table names are:
   * UnacceptableSuburb (key = suburb name)
	 * MaxSumInsured         (key = risk type)
	 * MinSumInsured          (key = risk type)
	 */
	public String getInsSetting(String tName,
			                    String tKey,
			                    DateTime effDate)throws Exception
	{
	   insAdapter = new InsAdapter();
	   return insAdapter.getInsSetting(tName, tKey, effDate);
	}
	
	public	WebAgentStatus	validateAgent	(	String	agentCode,
												String	agentPassword
											)
			throws	Exception
	{
		WebAgentStatus was;
		
		insAdapter = new InsAdapter();
		
		was	=	insAdapter.validateAgent(agentCode, agentPassword);
		
		return	was;
	}
	
	public ArrayList<ListPair> getFixedExcesses(String prodType,
                                                String riskType,
                                                DateTime effDate)throws Exception
    {
	   insAdapter = new InsAdapter();
	   return insAdapter.getFixedExcesses(prodType, riskType, effDate);
    }
	
	public String getNotionalProdType(Integer clientNo,
			                          String fiftyPlus,
			                          String  riskType,
			                          DateTime effDate)throws Exception
	{
	   return insAdapter.getNotionalProdType(clientNo, fiftyPlus, riskType, effDate);
	}

	public String getMessageText(String category, String type, String subType, String prodType, String coverType, DateTime startDate) throws Exception {
	  MessageText mt = insAdapter.getMessageText(category, 
	  		type,
	  		subType,
        (prodType.equalsIgnoreCase("NPIP")?"PIP":prodType),
        coverType, 
        startDate);
		
		return mt.getText();
	}
	
	public Collection<WebPayable> getConvertedTransactions(DateTime since) throws RemoteException {
		Collection<WebPayable> resultList = new ArrayList<WebPayable>();
		Connection connection = null;
    PreparedStatement statement = null;
    
    final String SQL_INS_CONVERTED = 
		"SELECT webQuoteNo, oiSeqNo, receiptReference, clientNo, amountPayable, conversionDate FROM\n" +
    "(" + 
	    "SELECT dbo.WebInsQuote.webQuoteNo, oiSeqNo, receiptReference, clientNo, amountPayable, conversionDate FROM\n" +
			"(\n" +
			"  SELECT [dbo].[WebInsQuoteDetail].[webQuoteNo]\n" +
			"      ,fieldValue AS oiSeqNo\n" +
			"      ,tStamp AS conversionDate\n" +
			"  FROM [dbo].[WebInsQuoteDetail]\n" +
			"  WHERE fieldName IN ('OiSeqNo')\n" +
			"  AND tStamp >= ?\n" +
			") AS oiRef\n" +
			"INNER join\n" +
			"(\n" +
			"  SELECT [dbo].[WebInsQuoteDetail].[webQuoteNo]\n" +
			"      ,fieldValue AS receiptReference\n" +
			"  FROM [dbo].[WebInsQuoteDetail]\n" +
			"  WHERE fieldName IN ('receiptReference')\n" +
			") AS recRef\n" +
			"ON oiRef.webQuoteNo = recRef.webQuoteNo\n" +
			"INNER join\n" +
			"(\n" +
			"  SELECT [dbo].[WebInsQuoteDetail].[webQuoteNo]\n" +
			"      ,fieldValue AS amountPayable\n" +
			"  FROM [dbo].[WebInsQuoteDetail]\n" +
			"  WHERE fieldName IN ('payableAmount')\n" +
			") AS amtRef\n" +
			"ON recRef.webQuoteNo = amtRef.webQuoteNo\n" +
			"INNER join\n" +
			"(\n" +
			"  SELECT [dbo].[WebInsQuoteDetail].[webQuoteNo]\n" +
			"	     ,fieldValue AS clientNo\n" +
			"  FROM [dbo].[WebInsQuoteDetail]\n" +
			"  WHERE fieldName IN ('clientNo')\n" +
			") AS cltRef\n" +
			"ON amtRef.webQuoteNo = cltRef.webQuoteNo\n" +			
			"INNER JOIN dbo.WebInsClient ON recRef.webQuoteNo = dbo.WebInsClient.webQuoteNo\n" +
			"INNER JOIN dbo.WebClient ON dbo.WebInsClient.webClientNo = dbo.WebClient.webClientNo\n" +
			"INNER JOIN dbo.WebInsQuote ON recRef.webQuoteNo = dbo.WebInsQuote.webQuoteNo\n" +
			"WHERE dbo.WebInsQuote.quoteStatus = ?\n" +
			"AND preferredAddress = 1\n" +
			"AND ractClientNo IS NOT NULL\n" + 
		") AS tmp\n" +
		"GROUP BY webQuoteNo, oiSeqNo, receiptReference, clientNo, amountPayable, conversionDate";
    
		try
	  {
	    connection = webDataSource.getConnection();
	    statement = connection.prepareStatement(SQL_INS_CONVERTED);
	    statement.setDate(1, since.toSQLDate());
	    statement.setString(2, WebInsQuote.CONVERTED);
	    
	    ResultSet resultSet = statement.executeQuery();
	
	    while(resultSet.next())
	    {
	      Integer webQuoteNo = resultSet.getInt(1);
	      Integer oiSeqNo = resultSet.getInt(2);
	      //String receiptReference = resultSet.getString(3);
	      Integer ractClientNo = resultSet.getInt(4);
	      BigDecimal amountPayable = resultSet.getBigDecimal(5);
	      Date quoteDate = resultSet.getDate(6);
	      
	      WebPayable result = new WebPayable();
	      result.setSourceReference(webQuoteNo);
	      result.setPayRefNo(oiSeqNo);
	      result.setClientNo(ractClientNo);
	      result.setAmountPaid(amountPayable);
	      result.setSourceSystem(SourceSystem.INSURANCE.getAbbreviation());
	      result.setCreateDate(new DateTime(quoteDate));
	      
	      resultList.add(result);
	    }
	  }
	  catch(SQLException e)
	  {
	    throw new RemoteException("Error executing SQL " + SQL_INS_CONVERTED + " : " + e.toString());
	  }
	  finally
	  {
	    ConnectionUtil.closeConnection(connection, statement);
	  }
		
		return resultList;
	}
	
	public Collection<WebPayable> getUnreceiptedPayables(DateTime since) throws RemoteException, SystemDataException {
		Collection<WebPayable> resultList = new ArrayList<WebPayable>();
		Collection<WebPayable> txnList = getConvertedTransactions(since);
		OCRAdapter ocrAdapter = new OCRAdapter();
				
		for (WebPayable txn : txnList) {
/*
			List<Integer> clientList = new ArrayList<Integer>();
			
			// client - get group client
			ClientAdapter clientAdapter = ClientFactory.getClientAdapter();
			Collection<Integer> ownedGroups = clientAdapter.getOwnedGroups(txn.getClientNo());
			
			//System.out.println("ownedGroups: " + ownedGroups);
			
			if (ownedGroups == null || ownedGroups.isEmpty()) {
				clientList.add(txn.getClientNo());
			} else {
				clientList.addAll(ownedGroups);
			}
			
			//System.out.println("clientList: " + clientList);
			
			for (Integer c : clientList) {*/
				String uniqueID = txn.getClientNo() + "/" + txn.getPayRefNo();
				
				Payable payable = null;
				try {
					payable = ocrAdapter.getPayable(uniqueID, SourceSystem.INSURANCE.getAbbreviation());
				} catch (Exception e) {
					LogUtil.warn(getClass(), "Could not find oi-payable: " + uniqueID);
					//e.printStackTrace();
					continue;
				}
						
				if (payable.getAmountOutstanding().compareTo(BigDecimal.ZERO) == -1 || payable.getAmountOutstanding().compareTo(BigDecimal.ZERO) == 0) { // <= 0
					continue;
				}
				if (payable.getReceiptNo() != null && !payable.getReceiptNo().isEmpty()) {
					continue;
				}
				
				resultList.add(txn);
			//}
		}
		
		return resultList;
	}

    @Override
    public Float getMinVehicleValuePercentage(DateTime queryDate, int vehicleAge) throws RemoteException {
        insAdapter = new InsAdapter();
        RefType minMaxValueData = insAdapter.getVehicleMinMaxValueData(queryDate, vehicleAge);
        return Float.parseFloat(minMaxValueData.getTText2());
    }

    @Override
    public Float getMaxVehicleValuePercentage(DateTime queryDate, int vehicleAge) throws RemoteException {
        insAdapter = new InsAdapter();
        RefType minMaxValueData = insAdapter.getVehicleMinMaxValueData(queryDate, vehicleAge);
        return Float.parseFloat(minMaxValueData.getTText3());
    }
}

