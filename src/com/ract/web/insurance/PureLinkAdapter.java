package com.ract.web.insurance;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import com.ract.common.SystemException;
import com.ract.purelink.webservice.*;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.web.agent.WebAgentStatus;

public class PureLinkAdapter {
    private static ObjectFactory factory = new ObjectFactory();
    private static String ractPureLinkEndpoint = "http://localhost/RactPureLinkWS/RactPureLinkPortBindingService.svc";
    private static RactPureLinkPortBindingService service;

    static {
        try {
            ractPureLinkEndpoint = FileUtil.getProperty("master", "ractpurelink.server.url");
            LogUtil.warn(PureLinkAdapter.class, "Loaded RactPureLink endpoint from configuration: " + ractPureLinkEndpoint);
        } catch (Exception e1) {
            LogUtil.warn(PureLinkAdapter.class, "*******************************************************************************************");
            LogUtil.warn(PureLinkAdapter.class, e1);
            LogUtil.warn(PureLinkAdapter.class, "Could not determine ractpurelink.server.url, defaulting to: " + ractPureLinkEndpoint);
        }

        String wsdl_location = ractPureLinkEndpoint + "?singleWsdl";
        LogUtil.warn(PureLinkAdapter.class, "Generated wsdl location based on endpoint: " + wsdl_location);

        try {
            service = new RactPureLinkPortBindingService(new URL(wsdl_location), new QName("http://www.ract.com.au/RactPureLink", "RactPureLinkPortBindingService"));
        } catch (MalformedURLException e) {
            LogUtil.fatal(PureLinkAdapter.class, e);
            LogUtil.warn(PureLinkAdapter.class, "Failed to create URL for the wsdl Location: '" + wsdl_location + "', retrying using default configuration");
            LogUtil.warn(PureLinkAdapter.class, e);
            service = new RactPureLinkPortBindingService();
        }
    }

    public PureLinkAdapter() {
    }

    private IRactPureLinkServiceContract getPort() {
        IRactPureLinkServiceContract port = service.getBasicHttpBindingIRactPureLinkServiceContract();
        BindingProvider bindingProvider = (BindingProvider) port;
        bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, ractPureLinkEndpoint);
        return port;
    }

    public ArrayList<ListPair> getMakes(GlVehicle veh) throws SystemException {
        ArrayList<ListPair> pureMakes = new ArrayList<ListPair>();

        GetMakesRequest request = new GetMakesRequest();
        request.setYear(veh.getVehYear().toString());

        GetMakesResponse response = null;
        try {
            response = getPort().getMakesInput(request);
        } catch (IRactPureLinkServiceContractGetMakesInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting Make data\n" + e);
            throw new SystemException("Error occured extracting Make data", e);
        }

        if (null != response) {
            for (String make : response.getMake()) {
                ListPair pair = new ListPair(make, make);
                pureMakes.add(pair);
            }
        }
        return pureMakes;
    }

    public ArrayList<String> getModels(GlVehicle veh) throws SystemException {
        ArrayList<String> models = new ArrayList<String>();

        GetFamiliesRequest request = new GetFamiliesRequest();
        request.setYear(veh.getVehYear().toString());
        request.setMake(veh.getMake());

        GetFamiliesResponse response = null;
        try {
            response = getPort().getFamiliesInput(request);
        } catch (IRactPureLinkServiceContractGetFamiliesInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting Model data\n" + e);
            throw new SystemException("Error occured extracting Model data", e);
        }

        if (null != response) {
            for (String family : response.getFamily()) {
                models.add(family);
            }
        }
        return models;
    }

    public ArrayList<ListPair> getBodyTypes(GlVehicle veh) throws SystemException {
        ArrayList<ListPair> pureStyles = new ArrayList<ListPair>();

        GetStylesRequest request = new GetStylesRequest();
        request.setYear(veh.getVehYear().toString());
        request.setMake(veh.getMake());
        request.setFamily(veh.getModel());

        GetStylesResponse response = null;
        try {
            response = getPort().getStylesInput(request);
        } catch (IRactPureLinkServiceContractGetStylesInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting BodyType data\n" + e);
            throw new SystemException("Error occured extracting BodyType data", e);
        }

        if (null != response) {
            for (String style : response.getStyle()) {
                pureStyles.add(new ListPair(style, style));
            }
        }
        return pureStyles;
    }

    public ArrayList<ListPair> getTransmissionTypes(GlVehicle veh) throws SystemException {
        ArrayList<ListPair> pureTransmissions = new ArrayList<ListPair>();

        GetTransmissionsRequest request = new GetTransmissionsRequest();
        request.setYear(veh.getVehYear().toString());
        request.setMake(veh.getMake());
        request.setFamily(veh.getModel());
        request.setStyle(veh.getBodyType());

        GetTransmissionsResponse response = null;
        try {
            response = getPort().getTransmissionsInput(request);
        } catch (IRactPureLinkServiceContractGetTransmissionsInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting Transmission data\n" + e);
            throw new SystemException("Error occured extracting Transmission data", e);
        }

        if (null != response) {
            for (String transmission : response.getTransmission()) {
                pureTransmissions.add(new ListPair(transmission, transmission));
            }
        }
        return pureTransmissions;
    }

    public ArrayList<String> getCylinders(GlVehicle veh) throws SystemException {
        ArrayList<String> cylinders = new ArrayList<String>();

        GetCylindersRequest request = new GetCylindersRequest();
        request.setYear(veh.getVehYear().toString());
        request.setMake(veh.getMake());
        request.setFamily(veh.getModel());
        request.setStyle(veh.getBodyType());
        request.setTransmission(veh.getTransmission());

        GetCylindersResponse response = null;
        try {
            response = getPort().getCylindersInput(request);
        } catch (IRactPureLinkServiceContractGetCylindersInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting Cylinder data\n" + e);
            throw new SystemException("Error occured extracting Cylinder data", e);
        }
        if (null != response) {
            for (String cylinder : response.getCylinder()) {
                cylinders.add(cylinder);
            }
        }
        return cylinders;
    }

    public ArrayList<GlVehicle> getMatchingVehicles(GlVehicle veh, DateTime quoteDate) throws SystemException {
        ArrayList<GlVehicle> vehicles = new ArrayList<GlVehicle>();

        GetMatchingVehiclesRequest request = new GetMatchingVehiclesRequest();
        request.setYear(veh.getVehYear().toString());
        request.setMake(veh.getMake());
        request.setFamily(factory.createGetMatchingVehiclesRequestFamily(veh.getModel()));
        request.setStyle(factory.createGetMatchingVehiclesRequestStyle(veh.getBodyType()));
        request.setTransmission(factory.createGetMatchingVehiclesRequestTransmission(veh.getTransmission()));
        request.setCylinder(factory.createGetMatchingVehiclesRequestCylinder(veh.getCylinders()));

        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));

        GetMatchingVehiclesResponse response = null;
        try {
            response = getPort().getMatchingVehiclesInput(request);
        } catch (IRactPureLinkServiceContractGetMatchingVehiclesInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting vehicle list data\n" + e);
            throw new SystemException("Error occured extracting vehicle list data", e);
        }

        if (null != response) {
            for (GlassesVehicle vehicle : response.getVehicle()) {
                vehicles.add(createGlVehicle(vehicle));
            }
        }
        return vehicles;
    }

    public Integer getVehicleValue(String nvic, DateTime quoteDate) throws SystemException {
        GetVehicleValueRequest request = new GetVehicleValueRequest();
        request.setNvic(nvic);
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));

        GetVehicleValueResponse response = null;
        try {
            response = getPort().getVehicleValueInput(request);
        } catch (IRactPureLinkServiceContractGetVehicleValueInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting vehicle value data\n" + e);
            throw new SystemException("Error occured extracting vehicle value data", e);
        }

        if (null == response) {
            LogUtil.warn(getClass(), "Null response received extracting vehicle value data");
            throw new SystemException("Null response received extracting vehicle value data");
        } else {
            return response.getVehicleValue();
        }
    }

    public GlVehicle getVehicle(String nvic, DateTime quoteDate) throws Exception {
        GlassesVehicle vehicle = getGlassesVehicle(nvic, quoteDate);
        if (null == vehicle) {
            return null;
        }
        return createGlVehicle(vehicle);
    }

    private GlassesVehicle getGlassesVehicle(String nvic, DateTime quoteDate) throws Exception {
        if (null == nvic) {
            return null;
        }

        GetVehicleByNvicRequest request = new GetVehicleByNvicRequest();
        request.setNvic(nvic);
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));

        GetVehicleByNvicResponse response = null;

        try {
            response = getPort().getVehicleByNvicInput(request);
        } catch (IRactPureLinkServiceContractGetVehicleByNvicInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting vehicle data by nvic\n" + e);
            throw new SystemException("Error occured extracting vehicle data by nvic", e);
        }

        if (null == response.getVehicle()) {
            return null;
        }

        return response.getVehicle().getValue();
    }

    public ArrayList<InRfDet> getFinanceTypes(DateTime quoteDate) throws SystemException {
        ArrayList<InRfDet> financeTypeList = new ArrayList<InRfDet>();

        GetFinanceTypeRequest request = new GetFinanceTypeRequest();
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));

        GetFinanceTypeResponse response = null;
        try {
            response = getPort().getFinanceTypeInput(request);
        } catch (IRactPureLinkServiceContractGetFinanceTypeInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting finance type data\n" + e);
            throw new SystemException("Error occured extracting finance type data", e);
        }

        if (null != response) {
            for (UdlRecord record : response.getFinanceType()) {
                financeTypeList.add(createInRfDet(record, "WFT"));
            }
        }
        return financeTypeList;
    }

    public ArrayList<InRfDet> getFinanciers(DateTime quoteDate) throws SystemException {
        ArrayList<InRfDet> financierList = new ArrayList<InRfDet>();

        GetFinancierRequest request = new GetFinancierRequest();
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));

        GetFinancierResponse response = null;
        try {
            response = getPort().getFinancierInput(request);
        } catch (IRactPureLinkServiceContractGetFinancierInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting financier data\n" + e);
            throw new SystemException("Error occured extracting financier data", e);
        }

        if (null != response) {
            for (UdlRecord record : response.getFinancier()) {
                financierList.add(createInRfDet(record, "WFI"));
            }
        }
        return financierList;
    }

    public ArrayList<InRfDet> getBusinessUsages(DateTime quoteDate) throws SystemException {
        ArrayList<InRfDet> businessUsagesList = new ArrayList<InRfDet>();

        GetBusinessUsageRequest request = new GetBusinessUsageRequest();
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));

        GetBusinessUsageResponse response = null;
        try {
            response = getPort().getBusinessUsageInput(request);
        } catch (IRactPureLinkServiceContractGetBusinessUsageInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting business usage data\n" + e);
            throw new SystemException("Error occured extracting business usage data", e);
        }

        if (null != response) {
            for (UdlRecord record : response.getBusinessUsage()) {
                businessUsagesList.add(createInRfDet(record, "IBU"));
            }
        }
        return businessUsagesList;
    }

    public ArrayList<InRfDet> getBuildingLayout(DateTime quoteDate) throws SystemException {
        ArrayList<InRfDet> buildingLayoutList = new ArrayList<InRfDet>();

        GetBuildingLayoutRequest request = new GetBuildingLayoutRequest();
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));

        GetBuildingLayoutResponse response = null;
        try {
            response = getPort().getBuildingLayoutInput(request);
        } catch (IRactPureLinkServiceContractGetBuildingLayoutInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting building layout data\n" + e);
            throw new SystemException("Error occured extracting building layout data", e);
        }

        if (null != response) {
            for (UdlRecord record : response.getBuildingLayout()) {
                buildingLayoutList.add(createInRfDet(record, "WSL"));
            }
        }
        return buildingLayoutList;
    }

    public ArrayList<InRfDet> getWallConstruction(DateTime quoteDate) throws SystemException {
        ArrayList<InRfDet> wallConstructionList = new ArrayList<InRfDet>();

        GetWallConstructionRequest request = new GetWallConstructionRequest();
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));

        GetWallConstructionResponse response = null;
        try {
            response = getPort().getWallConstructionInput(request);
        } catch (IRactPureLinkServiceContractGetWallConstructionInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting wall construction data\n" + e);
            throw new SystemException("Error occured extracting wall construction data", e);
        }

        if (null != response) {
            for (UdlRecord record : response.getWallConstruction()) {
                wallConstructionList.add(createInRfDet(record, "WCN"));
            }
        }
        return wallConstructionList;
    }

    public ArrayList<InRfDet> getRoofConstruction(DateTime quoteDate) throws SystemException {
        ArrayList<InRfDet> roofConstructionList = new ArrayList<InRfDet>();

        GetRoofConstructionRequest request = new GetRoofConstructionRequest();
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));

        GetRoofConstructionResponse response = null;
        try {
            response = getPort().getRoofConstructionInput(request);
        } catch (IRactPureLinkServiceContractGetRoofConstructionInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting roof construction data\n" + e);
            throw new SystemException("Error occured extracting roof construction data", e);
        }

        if (null != response) {
            for (UdlRecord record : response.getRoofConstruction()) {
                roofConstructionList.add(createInRfDet(record, "WSR"));
            }
        }
        return roofConstructionList;
    }

    public ArrayList<InRfDet> getHomeLocks(DateTime quoteDate) throws SystemException {
        ArrayList<InRfDet> homeLockList = new ArrayList<InRfDet>();

        GetHomeLocksRequest request = new GetHomeLocksRequest();
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));

        GetHomeLocksResponse response = null;
        try {
            response = getPort().getHomeLocksInput(request);
        } catch (IRactPureLinkServiceContractGetHomeLocksInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting home locks data\n" + e);
            throw new SystemException("Error occured extracting home locks data", e);
        }

        if (null != response) {
            for (UdlRecord record : response.getHomeLock()) {
                homeLockList.add(createInRfDet(record, "WSR"));
            }
        }
        return homeLockList;
    }

    public ArrayList<InRfDet> getHomeAlarms(DateTime quoteDate) throws SystemException {
        ArrayList<InRfDet> homeAlarmList = new ArrayList<InRfDet>();

        GetHomeAlarmsRequest request = new GetHomeAlarmsRequest();
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));

        GetHomeAlarmsResponse response = null;
        try {
            response = getPort().getHomeAlarmsInput(request);
        } catch (IRactPureLinkServiceContractGetHomeAlarmsInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting home alarm data\n" + e);
            throw new SystemException("Error occured extracting home alarm data", e);
        }

        if (null != response) {
            for (UdlRecord record : response.getHomeAlarm()) {
                homeAlarmList.add(createInRfDet(record, "WSR"));
            }
        }
        return homeAlarmList;
    }

    public ArrayList<InRfDet> getOccupancy(DateTime quoteDate, String riskType, boolean suppressRenter) throws SystemException {
        ArrayList<InRfDet> occupancyList = new ArrayList<InRfDet>();

        GetOccupancyRequest request = new GetOccupancyRequest();
        request.setRiskTypeCode(RiskTypeCode.fromValue(riskType));
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));

        GetOccupancyResponse response = null;
        try {
            response = getPort().getOccupancyInput(request);
        } catch (IRactPureLinkServiceContractGetOccupancyInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting occupancy data\n" + e);
            throw new SystemException("Error occured extracting occupancy data", e);
        }

        if (null != response) {
            for (UdlRecord record : response.getOccupancies()) {
                if (suppressRenter && "Renter".equalsIgnoreCase(record.getDescription())) {
                    continue;
                }
                occupancyList.add(createInRfDet(record, "WOC"));
            }
        }
        return occupancyList;
    }

    public ArrayList<InRfDet> getExcess(DateTime quoteDate, String riskType, String coverType, boolean isSilverSaver) throws SystemException {
        ArrayList<InRfDet> excessList = new ArrayList<InRfDet>();

        GetExcessRequest request = new GetExcessRequest();
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));
        request.setRiskTypeCode(RiskTypeCode.fromValue(riskType));
        request.setCoverTypeCode(CoverTypeCode.fromValue(coverType));
        request.setIsSilverSaver(isSilverSaver);

        GetExcessResponse response = null;
        try {
            response = getPort().getExcessInput(request);
        } catch (IRactPureLinkServiceContractGetExcessInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured performing excess data extract\n" + e);
            throw new SystemException("Error occured performing excess data extract", e);
        }

        if (null != response) {
            for (ExcessDetail record : response.getExcess()) {
                excessList.add(createInRfDet(record, null));
            }
        }

        return excessList;
    }

    public ArrayList<InRfDet> getOptions(DateTime quoteDate, String riskType, String coverType) throws SystemException {
        ArrayList<InRfDet> optionList = new ArrayList<InRfDet>();

        GetOptionsRequest request = new GetOptionsRequest();
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));
        request.setRiskTypeCode(RiskTypeCode.fromValue(riskType));
        request.setCoverTypeCode(CoverTypeCode.fromValue(coverType));

        GetOptionsResponse response = null;
        try {
            response = getPort().getOptionsInput(request);
        } catch (IRactPureLinkServiceContractGetOptionsInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured performing options data extract\n" + e);
            throw new SystemException("Error occured performing options data extract", e);
        }

        if (null != response) {
            for (OptionDetail record : response.getOption()) {
                optionList.add(createInRfDet(record, null));
            }
        }

        return optionList;
    }

    public ArrayList<InRfDet> getHomeContentsCategories(DateTime quoteDate) throws SystemException {
        ArrayList<InRfDet> homeContentsCategoryList = new ArrayList<InRfDet>();

        GetHomeContentsCategoriesRequest request = new GetHomeContentsCategoriesRequest();
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));

        GetHomeContentsCategoriesResponse response = null;
        try {
            response = getPort().getHomeContentsCategoriesInput(request);
        } catch (IRactPureLinkServiceContractGetHomeContentsCategoriesInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting home contents category data\n" + e);
            throw new SystemException("Error occured extracting home contents category data", e);
        }

        if (null != response) {
            for (UdlRecord record : response.getHomeContentsCategory()) {
                homeContentsCategoryList.add(createInRfDet(record, "WCL"));
            }
        }
        return homeContentsCategoryList;
    }

    public ArrayList<RefType> getVehicleUsages(DateTime quoteDate) throws SystemException {
        ArrayList<RefType> vehicleUsageList = new ArrayList<RefType>();

        GetVehicleUsageRequest request = new GetVehicleUsageRequest();
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));

        GetVehicleUsageResponse response = null;
        try {
            response = getPort().getVehicleUsageInput(request);
        } catch (IRactPureLinkServiceContractGetVehicleUsageInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting vehicle usage data\n" + e);
            throw new SystemException("Error occured extracting vehicle usage data", e);
        }

        if (null != response) {
            for (UdlRecord record : response.getVehicleUsage()) {
                vehicleUsageList.add(createRefType(record));
            }
        }
        return vehicleUsageList;
    }

    public ArrayList<RefType> getDrivingFrequencies(DateTime quoteDate) throws SystemException {
        ArrayList<RefType> drivingFrequencyList = new ArrayList<RefType>();

        GetDrivingFrequencyRequest request = new GetDrivingFrequencyRequest();
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));

        GetDrivingFrequencyResponse response = null;
        try {
            response = getPort().getDrivingFrequencyInput(request);
        } catch (IRactPureLinkServiceContractGetDrivingFrequencyInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured extracting driving frequency data\n" + e);
            throw new SystemException("Error occured extracting driving frequency data", e);
        }

        if (null != response) {
            for (UdlRecord record : response.getDrivingFrequency()) {
                drivingFrequencyList.add(createRefType(record));
            }
        }
        return drivingFrequencyList;
    }

    public String getSuburbValidation(DateTime quoteDate, String suburb) throws SystemException {
        GetSuburbValidationRequest request = new GetSuburbValidationRequest();
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));
        request.setSuburb(suburb);

        GetSuburbValidationResponse response = null;
        try {
            response = getPort().getSuburbValidationInput(request);
        } catch (IRactPureLinkServiceContractGetSuburbValidationInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured performing suburb validation\n" + e);
            throw new SystemException("Error occured performing suburb validation", e);
        }

        if (null != response) {
            return response.getSuburbValidationResult().toString();
        }
        return "UK";
    }

    public int getMinInsurable(DateTime quoteDate, String riskType, String coverType) throws SystemException {
        GetMinMaxInsurableRequest request = new GetMinMaxInsurableRequest();
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));
        request.setRiskTypeCode(RiskTypeCode.fromValue(riskType));
        request.setCoverTypeCode(CoverTypeCode.fromValue(coverType));

        GetMinMaxInsurableResponse response = null;
        try {
            response = getPort().getMinMaxInsurableInput(request);
        } catch (IRactPureLinkServiceContractGetMinMaxInsurableInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured performing Min/Max insurable extract\n" + e);
            throw new SystemException("Error occured performing Min/Max insurable extract", e);
        }

        return response.getMinValue();
    }

    public int getMaxInsurable(DateTime quoteDate, String riskType, String coverType) throws SystemException {
        GetMinMaxInsurableRequest request = new GetMinMaxInsurableRequest();
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));
        request.setRiskTypeCode(RiskTypeCode.fromValue(riskType));
        request.setCoverTypeCode(CoverTypeCode.fromValue(coverType));

        GetMinMaxInsurableResponse response = null;
        try {
            response = getPort().getMinMaxInsurableInput(request);
        } catch (IRactPureLinkServiceContractGetMinMaxInsurableInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured performing Min/Max insurable extract\n" + e);
            throw new SystemException("Error occured performing Min/Max insurable extract", e);
        }

        return response.getMaxValue();
    }

    public Premium getPremium(WebInsQuote quote, String paymentFrequency, ArrayList<WebInsQuoteDetail> quoteDetailList, ArrayList<InRiskSi> specifiedItemsList, List<WebInsClient> clientList) throws SystemException {
        GetQuoteRequest request = new GetQuoteRequest();
        GetQuoteResponse response = null;

        if ("HOME".equalsIgnoreCase(quote.getQuoteType())) {
            request.setHomeQuoteDetails(factory.createGetQuoteRequestHomeQuoteDetails(getHomeQuoteDetails(quote, paymentFrequency, quoteDetailList, specifiedItemsList)));
        } else {
            // MOTR - COMP or TPPD
            request.setMotorQuoteDetails(factory.createGetQuoteRequestMotorQuoteDetails(getMotorQuoteDetails(quote, paymentFrequency, quoteDetailList, clientList)));
        }

        try {
            response = getPort().getQuoteInput(request);
        } catch (IRactPureLinkServiceContractGetQuoteInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured retrieving quote data\n" + e);
            throw new SystemException("Error occured retrieving quote data", e);
        }

        LogUtil.info(this.getClass(), "extracted total annual premium: " + response.getTotalAnnualPremium());
        LogUtil.info(this.getClass(), "extracted installment amount: " + response.getInstallmentAmount());
        LogUtil.debug(this.getClass(), "DRE response: " + response.getDreResult());

        Premium premium = new Premium(response.getTotalAnnualPremium(), response.getInstallmentAmount());
        premium.setBaseAnnualPremium(response.getBaseAnnualPremium());
        premium.setGst(response.getGst());
        premium.setStampDuty(response.getStampDuty());
        return premium;
    }

    public WebAgentStatus validateAgent(String agentCode, String agentPassword) throws SystemException {
        ValidateAgentRequest request = new ValidateAgentRequest();
        request.setAgentCode(agentCode);
        request.setPassword(agentPassword);

        ValidateAgentResponse validationOutput = null;
        try {
            validationOutput = getPort().validateAgentInput(request);
        } catch (IRactPureLinkServiceContractValidateAgentInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured performing Agent Validation\n" + e);
            throw new SystemException("Error occured performing Agent Validation", e);
        }

        WebAgentStatus response = new WebAgentStatus();
        response.setAgentId(0);
        response.setAgentName(validationOutput.getAgentName());
        response.setAgentStatusMessage(validationOutput.getAgentStatusMessage());
        if ("OK".equals(response.getAgentStatusMessage())) {
            response.setAgentStatusCode(0);
        } else {
            response.setAgentStatusCode(1);
        }

        return response;
    }

    public Integer getNoClaimDiscount(int numberOfAccidents, int yearCommencedDriving) throws SystemException {

        GetNoClaimDiscountRequest request = new GetNoClaimDiscountRequest();
        request.setNumberOfAccidents(numberOfAccidents);
        request.setYearStartedDriving(yearCommencedDriving);
        request.setQueryDate(convertDateTimeToXmlGregorianDate(new DateTime()));

        GetNoClaimDiscountResponse response = null;
        try {
            response = getPort().getNoClaimDiscountInput(request);
        } catch (IRactPureLinkServiceContractGetNoClaimDiscountInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured performing getNoClaimDiscount\n" + e);
            throw new SystemException("Error occured performing getNoClaimDiscount", e);
        }

        return response.getNoClaimBenefit().intValue();
    }

    public List<ListPair> getFixedExcess(DateTime quoteDate, String riskType, String subRiskType, boolean isSilverSaver) throws SystemException {

        GetFixedExcessRequest request = new GetFixedExcessRequest();
        request.setQueryDate(convertDateTimeToXmlGregorianDate(quoteDate));
        request.setRiskTypeCode(RiskTypeCode.fromValue(riskType));
        request.setSubRiskTypeCode(SubRiskTypeCode.fromValue(subRiskType));
        request.setIsSilverSaver(isSilverSaver);

        GetFixedExcessResponse response = null;
        try {
            response = getPort().getFixedExcessInput(request);
        } catch (IRactPureLinkServiceContractGetFixedExcessInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured performing getFixedExcess\n" + e);
            throw new SystemException("Error occured performing getFixedExcess", e);
        }

        List<ListPair> fixedExcessList = new ArrayList<ListPair>();

        if (null != response && null != response.getFixedExcess()) {
            for (FixedExcessDetail fixedExcess : response.getFixedExcess()) {
                ListPair excessPair = new ListPair(fixedExcess.getDescription(), "$" + fixedExcess.getAmount());
                fixedExcessList.add(excessPair);
            }
        }

        return fixedExcessList;
    }

    public String savePolicyToPure(WebInsQuote quote, List<WebInsQuoteDetail> quoteDetailsList, List<InRiskSi> specifiedItemsList, List<WebInsClient> clientList, String cardType, String bsb, String accountNumber, String cardExpiry, String accountName, String paymentDay, String paymentFrequency) throws Exception {
        // LogUtil.warn(this.getClass(), "****************************************************");
        // LogUtil.warn(this.getClass(), "savePolicyToPure invoked with parameters:");
        // LogUtil.warn(this.getClass(), "quoteNumber: " + quote.getWebQuoteNo());
        // LogUtil.warn(this.getClass(), "cardType: " + cardType);
        // LogUtil.warn(this.getClass(), "bsb: " + bsb);
        // LogUtil.warn(this.getClass(), "accountNumber: " + accountNumber);
        // LogUtil.warn(this.getClass(), "cardExpiry: " + cardExpiry);
        // LogUtil.warn(this.getClass(), "accountName: " + accountName);
        // LogUtil.warn(this.getClass(), "paymentDay: " + paymentDay);
        // LogUtil.warn(this.getClass(), "paymentFrequency: " + paymentFrequency);
        // LogUtil.warn(this.getClass(), "coverType: " + quote.getCoverType());
        // LogUtil.warn(this.getClass(), "****************************************************");
        // LogUtil.warn(this.getClass(), "dumping client list:");
        // for (WebInsClient client : clientList) {
        // LogUtil.warn(this.getClass(), "client: " + client.toString());
        // }
        // LogUtil.warn(this.getClass(), "****************************************************");

        PersonInsured personInsured = new PersonInsured();
        List<Integer> secondaryPolicyHolders = new ArrayList<Integer>();
        Address billingAddress = null;

        for (WebInsClient client : clientList) {
            if (client.isOwner()) {
                AddUpdateFindPartyRequest addUpdateFindRequest = new AddUpdateFindPartyRequest();
                addUpdateFindRequest.setSalutation(client.getTitle());
                addUpdateFindRequest.setTitle(client.getTitle());
                addUpdateFindRequest.setForename(nullSafeStringToUpper(client.getGivenNames()));
                addUpdateFindRequest.setSurname(nullSafeStringToUpper(client.getSurname()));
                addUpdateFindRequest.setDateOfBirth(convertDateTimeToXmlGregorianDate(client.getBirthDate()));
                addUpdateFindRequest.setGender(Gender.fromValue(client.getGender()));
                addUpdateFindRequest.setInitials(getInitials(client.getGivenNames()));
                if (null != client.getRactClientNo()) {
                    addUpdateFindRequest.setRactId(factory.createAddUpdateFindPartyRequestRactId(client.getRactClientNo().toString()));
                }
                boolean isPostalAddressDifferent = getBooleanFieldValue("differentPostalAddress", quoteDetailsList);
                addUpdateFindRequest.setCorrespondanceAddress(getCorrespondanceAddress(client, isPostalAddressDifferent));
                addUpdateFindRequest.setResidentialAddress(factory.createAddUpdateFindPartyRequestResidentialAddress(getResidentialAddress(client)));

                ContactList contactList = new ContactList();
                Contact contact = getContact(client.getMobilePhone(), ContactType.MOBILE);
                if (null != contact) {
                    contactList.getContact().add(contact);
                }
                contact = getContact(client.getWorkPhone(), ContactType.WORKPHONE);
                if (null != contact) {
                    contactList.getContact().add(contact);
                }
                contact = getContact(client.getHomePhone(), ContactType.HOMEPHONE);
                if (null != contact) {
                    contactList.getContact().add(contact);
                }
                contact = getContact(client.getFaxNo(), ContactType.FAX);
                if (null != contact) {
                    contactList.getContact().add(contact);
                }
                contact = getContact(client.getEmailAddress(), ContactType.EMAIL);
                if (null != contact) {
                    contactList.getContact().add(contact);
                }
                addUpdateFindRequest.setContact(contactList);

                AddUpdateFindPartyResponse addUpdateFindResponse;
                try {
                    addUpdateFindResponse = getPort().addUpdateFindPartyInput(addUpdateFindRequest);
                } catch (IRactPureLinkServiceContractAddUpdateFindPartyInputRactErrorFaultMessage e) {
                    LogUtil.warn(getClass(), e);
                    throw new Exception("Error occured during party find / create / update");
                }

                if (client.isPreferredAddress()) {
                    // populate PersonInsured details
                    personInsured.setSalutation(client.getTitle());
                    personInsured.setForename(nullSafeStringToUpper(client.getGivenNames()));
                    personInsured.setSurname(nullSafeStringToUpper(client.getSurname()));
                    personInsured.setPartyId(addUpdateFindResponse.getPartyId());
                    personInsured.setRactId(addUpdateFindResponse.getRactId());

                    billingAddress = getCorrespondanceAddress(client, isPostalAddressDifferent);
                } else {
                    // Add to list of associates
                    secondaryPolicyHolders.add(addUpdateFindResponse.getPartyId());
                }

                if (client.isCltGroup()) {
                    secondaryPolicyHolders.addAll(client.getGroupList());
                }
            }
        }

        CreatePolicyRequest createPolicyRequest = new CreatePolicyRequest();

        createPolicyRequest.setPersonInsured(personInsured);
        PartyIdentifierList secondaryPolicyHoldersPartyIdentifierList = new PartyIdentifierList();
        secondaryPolicyHoldersPartyIdentifierList.getPartyIdentifier().addAll(secondaryPolicyHolders);
        createPolicyRequest.setSecondaryPolicyHolders(secondaryPolicyHoldersPartyIdentifierList);
        createPolicyRequest.setSourceSystemPolicyReference(getStringFieldValue("displayQuoteNumber", quoteDetailsList));

        if (WebInsQuote.COVER_COMP.equalsIgnoreCase(quote.getCoverType()) || WebInsQuote.COVER_TP.equalsIgnoreCase(quote.getCoverType())) {
            createPolicyRequest.setMotorQuoteDetails(factory.createCreatePolicyRequestMotorQuoteDetails(getMotorQuoteDetails(quote, paymentFrequency, quoteDetailsList, clientList)));
        } else {
            createPolicyRequest.setHomeQuoteDetails(factory.createCreatePolicyRequestHomeQuoteDetails(getHomeQuoteDetails(quote, paymentFrequency, quoteDetailsList, specifiedItemsList)));
        }

        XMLGregorianCalendar compDate = getDateTimeFieldValue(WebInsQuoteDetail.COMP_DATE, quoteDetailsList);
        XMLGregorianCalendar startDate = convertDateTimeToXmlGregorianDate(quote.getStartCover());

        if (startDate.compare(compDate) == DatatypeConstants.GREATER) {
            startDate.setMinute(1);
            createPolicyRequest.setCoverStartDate(startDate);
        } else {
            createPolicyRequest.setCoverStartDate(compDate);
        }

        Calendar cal = createPolicyRequest.getCoverStartDate().toGregorianCalendar();
        cal.set(Calendar.HOUR_OF_DAY, 16);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.YEAR, 1);
        DateTime calendarEndDate = new DateTime(cal.getTime());
        createPolicyRequest.setCoverEndDate(convertDateTimeToXmlGregorianDate(calendarEndDate));

        String payMethod = getStringFieldValue(WebInsQuoteDetail.PAY_METHOD, quoteDetailsList);
        PaymentMethod paymentMethod = new PaymentMethod();
        if (WebInsQuoteDetail.PAY_CASH.equalsIgnoreCase(payMethod)) {
            // cash payments
            paymentMethod.setAgencyCollection(true);
        }
        if (WebInsQuoteDetail.PAY_DD.equalsIgnoreCase(payMethod)) {
            // credit card or bank details installments
            PayByInstalmentsDetails instalmentDetails = new PayByInstalmentsDetails();

            if ("debit".equalsIgnoreCase(cardType)) {
                // bank details
                BankDetails bankDetails = new BankDetails();
                bankDetails.setAccountName(accountName);
                bankDetails.setAccountNumber(accountNumber);
                bankDetails.setBsb(bsb);
                instalmentDetails.setBankDetails(factory.createPayByInstalmentsDetailsBankDetails(bankDetails));
            } else {
                // credit details
                CreditCardDetails creditCard = new CreditCardDetails();
                creditCard.setCreditCardType(CardType.fromValue(cardType));
                creditCard.setAccountName(accountName);
                creditCard.setAccountNumber(accountNumber);
                String[] tokens = cardExpiry.split("/");
                creditCard.setExpiryMonth(tokens[0]);
                creditCard.setExpiryYear(tokens[1]);
                creditCard.setBillingAddress(billingAddress);
                instalmentDetails.setCreditCardDetails(factory.createPayByInstalmentsDetailsCreditCardDetails(creditCard));
            }
            instalmentDetails.setPaymentDay(Integer.valueOf(paymentDay));
            instalmentDetails.setPaymentFrequency(getPaymentFrequencyFromString(paymentFrequency));

            paymentMethod.setPayByInstalments(factory.createPaymentMethodPayByInstalments(instalmentDetails));

        }
        if (WebInsQuoteDetail.PAY_ANNUALLY.equalsIgnoreCase(payMethod)) {
            // credit card pay now
            CreditCardDetails creditCard = new CreditCardDetails();
            creditCard.setCreditCardType(CardType.fromValue(cardType));
            creditCard.setAccountName(accountName);
            creditCard.setAccountNumber(accountNumber);
            String[] tokens = cardExpiry.split("/");
            creditCard.setExpiryMonth(tokens[0]);
            creditCard.setExpiryYear(tokens[1]);
            creditCard.setBillingAddress(billingAddress);
            paymentMethod.setPayNow(factory.createPaymentMethodPayNow(creditCard));
        }
        createPolicyRequest.setPaymentMethod(paymentMethod);

        try {
            CreatePolicyResponse response = getPort().createPolicyInput(createPolicyRequest);
            return response.getPolicyReference();
        } catch (IRactPureLinkServiceContractCreatePolicyInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Catching RactPureLinkPortTypeCreatePolicyRactErrorFaultMessage");
            LogUtil.warn(getClass(), e);
            throw e;
        } catch (Exception e) {
            LogUtil.warn(getClass(), "Catching Exception");
            LogUtil.warn(getClass(), e);
            throw e;
        }
    }

    public ArrayList<WebInsClient> getPartyAssociations(Integer ractClientNo) throws Exception {
        ArrayList<WebInsClient> groups = null;

        GetPartyAssociationsRequest request = new GetPartyAssociationsRequest();
        request.setRactId(ractClientNo.toString());

        GetPartyAssociationsResponse response = null;
        try {
            response = getPort().getPartyAssociationsInput(request);
        } catch (IRactPureLinkServiceContractGetPartyAssociationsInputRactErrorFaultMessage e) {
            LogUtil.warn(getClass(), "Error occured performing getPartyAssociations\n" + e);
            throw new SystemException("Error occured performing getPartyAssociations", e);
        }

        if (null != response && null != response.getPartyAssociations() && null != response.getPartyAssociations().getPartyAssociation()) {
            groups = new ArrayList<WebInsClient>();

            for (PartyAssociation partyAssociation : response.getPartyAssociations().getPartyAssociation()) {
                StringBuilder descriptionBuilder = new StringBuilder();
                WebInsClient client = new WebInsClient();

                descriptionBuilder.append(partyAssociation.getPrimaryParty().getInitials());
                descriptionBuilder.append(" ");
                descriptionBuilder.append(partyAssociation.getPrimaryParty().getSurname());

                client.setBirthDate(new DateTime(partyAssociation.getPrimaryParty().getDateOfBirth().toGregorianCalendar().getTimeInMillis()));
                client.setClientType(WebInsClient.CLIENT_TYPE_INSURANCE);
                client.setCltGroup(true);

                // client.setCreateDate(createDate);
                client.setCurrentNCD(null);
                client.setDriver(false);
                client.setDrivingFrequency(null);
                client.setEmailAddress(getContactDetail(partyAssociation.getPrimaryParty().getContact(), ContactType.EMAIL));
                client.setFaxNo(getContactDetail(partyAssociation.getPrimaryParty().getContact(), ContactType.FAX));
                client.setGender(partyAssociation.getPrimaryParty().getGender().value());
                client.setGivenNames(partyAssociation.getPrimaryParty().getForename());
                // client.setGroupList(groupListString);
                // client.setGroupListString(groupListString);
                client.setHomePhone(getContactDetail(partyAssociation.getPrimaryParty().getContact(), ContactType.HOMEPHONE));
                client.setMobilePhone(getContactDetail(partyAssociation.getPrimaryParty().getContact(), ContactType.MOBILE));
                client.setNumberOfAccidents(0);
                client.setOwner(false);

                if (null != partyAssociation.getPrimaryParty().getCorrespondanceAddress()) {
                    // client.setPostAddress(postAddress);
                    // - MARKED TRANSIENT - // client.setPostalAddress(postalAddress);
                    client.setPostAusbar(partyAssociation.getPrimaryParty().getCorrespondanceAddress().getAusbar().getValue());
                    client.setPostCountry(partyAssociation.getPrimaryParty().getCorrespondanceAddress().getCountry());
                    client.setPostDpid(partyAssociation.getPrimaryParty().getCorrespondanceAddress().getDpid().getValue());
                    client.setPostGnaf(partyAssociation.getPrimaryParty().getCorrespondanceAddress().getGnafId().getValue());
                    client.setPostLatitude(partyAssociation.getPrimaryParty().getCorrespondanceAddress().getLatitude().getValue());
                    client.setPostLongitude(partyAssociation.getPrimaryParty().getCorrespondanceAddress().getLongitude().getValue());
                    client.setPostPostcode(partyAssociation.getPrimaryParty().getCorrespondanceAddress().getPostcode());
                    if (null != partyAssociation.getPrimaryParty().getCorrespondanceAddress().getPropertyName() && !partyAssociation.getPrimaryParty().getCorrespondanceAddress().getPropertyName().getValue().isEmpty()) {
                        client.setPostProperty(partyAssociation.getPrimaryParty().getCorrespondanceAddress().getPropertyName().getValue());
                    }
                    client.setPostState(partyAssociation.getPrimaryParty().getCorrespondanceAddress().getState().toString());
                    client.setPostStreet(partyAssociation.getPrimaryParty().getCorrespondanceAddress().getStreetName());
                    client.setPostStreetChar(partyAssociation.getPrimaryParty().getCorrespondanceAddress().getStreetNumber());
                    // client.setPostStsubid(postStsubid);
                    client.setPostSuburb(partyAssociation.getPrimaryParty().getCorrespondanceAddress().getSuburb());
                }

                client.setPreferredAddress(false);
                client.setPreferredContactMethod(null);

                client.setRactClientNo(Integer.valueOf(partyAssociation.getPrimaryParty().getRactId()));

                if (null != partyAssociation.getPrimaryParty().getResidentialAddress()) {
                    client.setResiAusbar(partyAssociation.getPrimaryParty().getResidentialAddress().getValue().getAusbar().getValue());
                    client.setResiCountry(partyAssociation.getPrimaryParty().getResidentialAddress().getValue().getCountry());
                    // - MARKED TRANSIENT - //client.setResidentialAddress(residentialAddress);
                    client.setResiDpid(partyAssociation.getPrimaryParty().getResidentialAddress().getValue().getDpid().getValue());
                    client.setResiGnaf(partyAssociation.getPrimaryParty().getResidentialAddress().getValue().getGnafId().getValue());
                    client.setResiLatitude(partyAssociation.getPrimaryParty().getResidentialAddress().getValue().getLatitude().getValue());
                    client.setResiLongitude(partyAssociation.getPrimaryParty().getResidentialAddress().getValue().getLongitude().getValue());
                    client.setResiPostcode(partyAssociation.getPrimaryParty().getResidentialAddress().getValue().getPostcode());
                    if (null != partyAssociation.getPrimaryParty().getResidentialAddress().getValue().getPropertyName() && !partyAssociation.getPrimaryParty().getResidentialAddress().getValue().getPropertyName().getValue().isEmpty()) {
                        client.setResiProperty(partyAssociation.getPrimaryParty().getResidentialAddress().getValue().getPropertyName().getValue());
                    }
                    client.setResiState(partyAssociation.getPrimaryParty().getResidentialAddress().getValue().getState().toString());
                    client.setResiStreet(partyAssociation.getPrimaryParty().getResidentialAddress().getValue().getStreetName());
                    client.setResiStreetChar(partyAssociation.getPrimaryParty().getResidentialAddress().getValue().getStreetNumber());
                    // client.setResiStsubid(resiStsubid);
                    client.setResiSuburb(partyAssociation.getPrimaryParty().getResidentialAddress().getValue().getSuburb());
                }

                client.setSurname(partyAssociation.getPrimaryParty().getSurname());
                // client.setTData(data);
                client.setTitle(partyAssociation.getPrimaryParty().getTitle());
                // client.setWebClientNo(webClientNo);
                // client.setWebQuoteNo(webQuoteNo);
                client.setWorkPhone(getContactDetail(partyAssociation.getPrimaryParty().getContact(), ContactType.WORKPHONE));
                client.setYearCommencedDriving(null);

                if (null != partyAssociation.getSecondaryParties() && null != partyAssociation.getSecondaryParties().getParty()) {
                    StringBuilder sb = new StringBuilder();
                    for (Party associate : partyAssociation.getSecondaryParties().getParty()) {
                        descriptionBuilder.append(", ");
                        descriptionBuilder.append(associate.getInitials());
                        descriptionBuilder.append(" ");
                        descriptionBuilder.append(associate.getSurname());

                        sb.append(associate.getPartyId());
                        sb.append(',');
                    }
                    sb.deleteCharAt(sb.lastIndexOf(","));
                    client.setGroupListString(sb.toString());
                }
                client.setGroupDescription(descriptionBuilder.toString());

                LogUtil.warn(this.getClass(), "************************ adding new client to associated list: ");
                LogUtil.warn(this.getClass(), client.toString());

                groups.add(client);
            }
        }

        return groups;
    }

    public RefType getVehicleMinMaxValueData(DateTime queryDate, int vehicleAge) throws RemoteException {
        RefType output = null;

        if (null != queryDate) {
            GetVehicleMinMaxValueDataRequest request = new GetVehicleMinMaxValueDataRequest();
            GetVehicleMinMaxValueDataResponse response = null;
            request.setQueryDate(convertDateTimeToXmlGregorianDate(queryDate));
            request.setVehicleAge(vehicleAge);
            try {
                response = getPort().getVehicleMinMaxValueDataInput(request);
            } catch (IRactPureLinkServiceContractGetVehicleMinMaxValueDataInputRactErrorFaultMessage e) {
                LogUtil.warn(getClass(), "Error occured performing getVehicleMinMaxValueDataInput\n" + e);
                throw new SystemException("Error occured performing getVehicleMinMaxValueDataInput", e);
            }
            output = new RefType();
            output.setTypeCode(response.getVehicleValueCode());
            output.setTText2(response.getMinValueLimitPercent().divide(BigDecimal.valueOf(100)).toString());
            output.setTText3(response.getMaxValueLimitPercent().divide(BigDecimal.valueOf(100)).toString());
            output.setTDescription("VehicleMinMaxValueData");
        }

        return output;
    }

    private String getContactDetail(ContactList contactList, ContactType contactType) {
        if (null != contactList && null != contactList.getContact()) {
            for (Contact contact : contactList.getContact()) {
                if (contactType.equals(contact.getContactType())) {
                    return contact.getContactDetail();
                }
            }
        }
        return null;
    }

    private Contact getContact(String contactDetail, ContactType type) {
        if (null == contactDetail || contactDetail.isEmpty()) {
            return null;
        }
        Contact response = new Contact();
        response.setContactDetail(contactDetail);
        response.setContactType(type);
        return response;
    }

    private Address getResidentialAddress(WebInsClient client) {
        Address address = new Address();
        address.setGnafId(factory.createAddressGnafId(client.getResiGnaf()));
        address.setLatitude(factory.createAddressLatitude(client.getResiLatitude()));
        address.setLongitude(factory.createAddressLongitude(client.getResiLongitude()));
        address.setPostcode(client.getResiPostcode());
        if (null == client.getResiState() || client.getResiState().isEmpty()) {
            address.setState(State.TAS);
        } else {
            address.setState(State.fromValue(client.getResiState()));
        }
        address.setPostcodeIsTasmanian(State.TAS.equals(address.getState()));
        address.setStreetName(nullSafeStringToUpper(client.getResiStreet()));
        address.setStreetNumber(nullSafeStringToUpper(client.getResiStreetChar()));
        address.setSuburb(nullSafeStringToUpper(client.getResiSuburb()));
        address.setAusbar(factory.createAddressAusbar(client.getResiAusbar()));
        address.setDpid(factory.createAddressDpid(client.getResiDpid()));
        address.setPropertyName(factory.createAddressPropertyName(nullSafeStringToUpper(client.getResiProperty())));
        if (null == client.getResiCountry() || client.getResiCountry().isEmpty()) {
            address.setCountry("Australia");
        } else {
            address.setCountry(client.getResiCountry());
        }

        return address;
    }

    private Address getCorrespondanceAddress(WebInsClient client, boolean isPostalAddressDifferent) {
        Address address = new Address();
        address.setGnafId(factory.createAddressGnafId(client.getPostGnaf()));
        address.setLatitude(factory.createAddressLatitude(client.getPostLatitude()));
        address.setLongitude(factory.createAddressLongitude(client.getPostLongitude()));
        address.setPostcode(client.getPostPostcode());
        if (null == client.getPostState() || client.getPostState().isEmpty()) {
            address.setState(State.TAS);
        } else {
            address.setState(State.fromValue(client.getPostState()));
        }
        address.setPostcodeIsTasmanian(State.TAS.equals(address.getState()));
        if (isPostalAddressDifferent) {
            // We need to do a dodgy POBox prefix. We need to check if the post property is numeric (prefix) or not (don't prefix) here
            try {
                // The following integer instantiation is being used to check that PostProperty is non-numeric
                // If it is numeric, prefix with PO Box. If not, it will throw exception and omit PO BOX
                Integer poBox = new Integer(client.getPostProperty());
                address.setStreetNumber("PO BOX " + poBox);
            } catch (Exception ep) {
                address.setStreetNumber(nullSafeStringToUpper(client.getPostProperty()));
            }
        } else {
            address.setStreetNumber(nullSafeStringToUpper(client.getPostStreetChar()));
            address.setStreetName(nullSafeStringToUpper(client.getPostStreet()));
            address.setPropertyName(factory.createAddressPropertyName(nullSafeStringToUpper(client.getPostProperty())));
        }
        address.setSuburb(nullSafeStringToUpper(client.getPostSuburb()));
        address.setAusbar(factory.createAddressAusbar(client.getPostAusbar()));
        address.setDpid(factory.createAddressDpid(client.getPostDpid()));
        if (null == client.getPostCountry() || client.getPostCountry().isEmpty()) {
            address.setCountry("Australia");
        } else {
            address.setCountry(client.getPostCountry());
        }
        return address;
    }

    private HomeQuoteDetails getHomeQuoteDetails(WebInsQuote quote, String paymentFrequency, List<WebInsQuoteDetail> quoteDetailsList, List<InRiskSi> specifiedItemsList) throws SystemException {
        HomeQuoteDetails homeQuoteDetail = new HomeQuoteDetails();

        String channelCode = getStringFieldValue(WebInsQuoteDetail.AGENT_CODE, quoteDetailsList);
        if (null == channelCode) {
            channelCode = "WEB";
        }
        homeQuoteDetail.setChannelCode(channelCode);

        homeQuoteDetail.setRiskType(RiskTypeCode.HOME);
        homeQuoteDetail.setRatingDate(convertDateTimeToXmlGregorianDate(quote.getQuoteDate()));
        homeQuoteDetail.setBuildingOnly(false);
        homeQuoteDetail.setContentsOnly(false);
        homeQuoteDetail.setBuildingAndContents(false);

        if (WebInsQuote.COVER_INV.equalsIgnoreCase(quote.getCoverType())) {
            homeQuoteDetail.setRiskType(RiskTypeCode.INVS);
            // homeQuoteDetail.setBuildingAndContents(true);
            homeQuoteDetail.setBuildingOnly(true);
        }
        if (WebInsQuote.COVER_BLDG.equalsIgnoreCase(quote.getCoverType())) {
            homeQuoteDetail.setBuildingOnly(true);
        }
        if (WebInsQuote.COVER_CNTS.equalsIgnoreCase(quote.getCoverType())) {
            homeQuoteDetail.setContentsOnly(true);
        }
        if (WebInsQuote.COVER_BLDG_AND_CNTS.equalsIgnoreCase(quote.getCoverType())) {
            homeQuoteDetail.setBuildingAndContents(true);
        }

        homeQuoteDetail.setInsuredAddress(getAddress(quoteDetailsList));

        homeQuoteDetail.setOccupancyCode(getStringFieldValue(WebInsQuoteDetail.OCCUPANCY, quoteDetailsList));
        homeQuoteDetail.setYearConstructed(getIntegerFieldValue(WebInsQuoteDetail.YEAR_CONST, quoteDetailsList));
        homeQuoteDetail.setBuildingLayoutCode(getStringFieldValue(WebInsQuoteDetail.BUILDING_TYPE, quoteDetailsList));
        homeQuoteDetail.setWallConstructionCode(getStringFieldValue(WebInsQuoteDetail.CONSTRUCTION, quoteDetailsList));
        homeQuoteDetail.setRoofConstructionCode(getStringFieldValue(WebInsQuoteDetail.ROOF, quoteDetailsList));
        homeQuoteDetail.setBusinessUsageCode(getStringFieldValue(WebInsQuoteDetail.BUSINESS_USE, quoteDetailsList));
        homeQuoteDetail.setSecurityAlarmTypeCode(getStringFieldValue(WebInsQuoteDetail.SECURITY_ALARM, quoteDetailsList));
        homeQuoteDetail.setDeadLocksWindowLocksCode(getStringFieldValue(WebInsQuoteDetail.SECURITY_LOCK, quoteDetailsList));

        if (getBooleanFieldValue(WebInsQuoteDetail.WORK_UNDERTAKEN, quoteDetailsList)) {
            if (getBooleanFieldValue(WebInsQuoteDetail.EXCEED_50K, quoteDetailsList)) {
                homeQuoteDetail.setBuildingWorksCode("G");
            } else {
                homeQuoteDetail.setBuildingWorksCode("L");
            }
        } else {
            homeQuoteDetail.setBuildingWorksCode("N");
        }

        homeQuoteDetail.setGoodOrderAndRepair(getBooleanFieldValue("goodOrder", quoteDetailsList));
        homeQuoteDetail.setHardwiredSmokeAlarm(getBooleanFieldValue(WebInsQuoteDetail.SMOKE_ALARM, quoteDetailsList));
        homeQuoteDetail.setOldestResidentDob(getDateFieldValue("oldestDateOfBirth", quoteDetailsList));

        if (getBooleanFieldValue("underFinance", quoteDetailsList)) {
            // homeQuoteDetail.getMortgagee().add(getStringFieldValue(WebInsQuoteDetail.FINANCIER, quoteDetailsList));
            MortgageeList mortgageeArray = new MortgageeList();
            mortgageeArray.getMortgagee().add(getStringFieldValue(WebInsQuoteDetail.FINANCIER, quoteDetailsList));
            homeQuoteDetail.setMortgagee(mortgageeArray);
        }

        homeQuoteDetail.setPersonalEffectsRequired(getBooleanFieldValue("CNTS_PersonalEffects", quoteDetailsList));
        if (getBooleanFieldValue("CNTS_PersonalEffects", quoteDetailsList)) {
            homeQuoteDetail.setUnspecifiedPersonalEffectsCode("UN1500");
            // } else {
            // homeQuoteDetail.setUnspecifiedPersonalEffectsCode("UN0000");
        }
        homeQuoteDetail.setSpecifiedPersonalEffectsSumInsured(0);

        homeQuoteDetail.setBuildingIndexed(true);
        homeQuoteDetail.setContentsIndexed(true);

        if (WebInsQuote.COVER_INV.equalsIgnoreCase(quote.getCoverType())) {
            homeQuoteDetail.setTotalBuildingSumInsured(getIntegerFieldValue(WebInsQuoteDetail.INV_SUM_INSURED, quoteDetailsList));

            // homeQuoteDetail.setTotalContentsSumInsured(10000);
            homeQuoteDetail.setTotalContentsSumInsured(0);
            if (getBooleanFieldValue("INV_ContentsCover30", quoteDetailsList)) {
                // homeQuoteDetail.setTotalContentsSumInsured(30000);
                homeQuoteDetail.setTotalContentsSumInsured(20000);
                homeQuoteDetail.setBuildingOnly(false);
                homeQuoteDetail.setBuildingAndContents(true);
            }
            if (getBooleanFieldValue("INV_ContentsCover50", quoteDetailsList)) {
                // homeQuoteDetail.setTotalContentsSumInsured(50000);
                homeQuoteDetail.setTotalContentsSumInsured(40000);
                homeQuoteDetail.setBuildingOnly(false);
                homeQuoteDetail.setBuildingAndContents(true);
            }
            homeQuoteDetail.setUnspecifiedContentsSumInsured(homeQuoteDetail.getTotalContentsSumInsured());
            homeQuoteDetail.setSpecifiedContentsSumInsured(0);
        } else {
            homeQuoteDetail.setTotalBuildingSumInsured(getIntegerFieldValue(WebInsQuoteDetail.BLDG_SUM_INSURED, quoteDetailsList));
            homeQuoteDetail.setTotalContentsSumInsured(getIntegerFieldValue(WebInsQuoteDetail.CNTS_SUM_INSURED, quoteDetailsList));

            SpecifiedItemList specifiedContentsArray = new SpecifiedItemList();
            List<SpecifiedItem> specifiedContents = specifiedContentsArray.getSpecifiedItem();
            specifiedContents.addAll(getSpecifiedContents(specifiedItemsList));
            homeQuoteDetail.setSpecifiedContents(specifiedContentsArray);

            int specifiedContentsSumInsured = calculateSpecifiedContentsSumInsured(specifiedContents);
            int unspecifiedContentsSumInsured = homeQuoteDetail.getTotalContentsSumInsured() - specifiedContentsSumInsured;
            if (unspecifiedContentsSumInsured < 0) {
                unspecifiedContentsSumInsured = 0;
            }

            homeQuoteDetail.setSpecifiedContentsSumInsured(specifiedContentsSumInsured);
            homeQuoteDetail.setUnspecifiedContentsSumInsured(unspecifiedContentsSumInsured);
        }

        homeQuoteDetail.setPaymentFrequency(getPaymentFrequencyFromString(paymentFrequency));
        if (WebInsQuote.COVER_INV.equalsIgnoreCase(quote.getCoverType())) {
            homeQuoteDetail.setSubRiskType(SubRiskTypeCode.PINV);
        } else {
            homeQuoteDetail.setSubRiskType(SubRiskTypeCode.HOME);
        }

        homeQuoteDetail.setAccidentalDamageBuilding(getBooleanFieldValue("BLDGAD", quoteDetailsList));
        homeQuoteDetail.setFusionBuilding(getBooleanFieldValue("BLDGFUSION", quoteDetailsList));
        homeQuoteDetail.setStormGatesAndFenceBuilding(getBooleanFieldValue("BLDGSTORM", quoteDetailsList));
        homeQuoteDetail.setDomesticWorkersCompBuilding(false);

        homeQuoteDetail.setAccidentalDamageContents(getBooleanFieldValue("CNTSAD", quoteDetailsList));
        homeQuoteDetail.setFusionContents(getBooleanFieldValue("CNTSFUSION", quoteDetailsList));
        homeQuoteDetail.setDomesticWorkersCompContents(getBooleanFieldValue("CNTSDWC", quoteDetailsList));

        homeQuoteDetail.setFusionInvestorBuilding(getBooleanFieldValue("PINVBLDFSN", quoteDetailsList));
        homeQuoteDetail.setFusionInvestorContents(getBooleanFieldValue("PINVCNTFSN", quoteDetailsList));
        homeQuoteDetail.setStormGatesAndFenceInvestorBuilding(getBooleanFieldValue("PINVSTORM", quoteDetailsList));

        homeQuoteDetail.setPipDiscount(getBooleanFieldValue(WebInsQuoteDetail.PIP_DISCOUNT, quoteDetailsList));
        homeQuoteDetail.setStaffIndicator(false);
        homeQuoteDetail.setStaffDiscountCode("");
        homeQuoteDetail.setDiscretionaryDiscountCode("");
        homeQuoteDetail.setSilverSaverDiscount(getBooleanFieldValue(WebInsQuoteDetail.FIFTY_PLUS, quoteDetailsList));
        homeQuoteDetail.setCampaignDiscountCode("");

        if (WebInsQuote.COVER_INV.equalsIgnoreCase(quote.getCoverType())) {
            homeQuoteDetail.setInvestorBuildingExcessCode(getStringFieldValue(WebInsQuoteDetail.INV_EXCESS, quoteDetailsList));
            // TODO: dirty hack to populate contents excess for investor - could be cleaned up to pull data from UDL?
            if (null == homeQuoteDetail.getInvestorBuildingExcessCode()) {
                homeQuoteDetail.setInvestorContentsExcessCode(null);
            } else {
                String investorContentsExcess = homeQuoteDetail.getInvestorBuildingExcessCode().replace("BLDG", "CNTS");
                homeQuoteDetail.setInvestorContentsExcessCode(investorContentsExcess);
            }
        } else {
            homeQuoteDetail.setBuildingExcessCode(getStringFieldValue(WebInsQuoteDetail.BLDG_EXCESS, quoteDetailsList));
            homeQuoteDetail.setContentsExcessCode(getStringFieldValue(WebInsQuoteDetail.CNTS_EXCESS, quoteDetailsList));
        }

        if (WebInsQuoteDetail.PAY_ANNUALLY.equalsIgnoreCase(paymentFrequency)) {
            homeQuoteDetail.setBaseAnnualPremium(getDecimalFieldValue(WebInsQuoteDetail.ANNUAL_PAYMENT_BASE_ANNUAL_PERMIUM, quoteDetailsList));
            homeQuoteDetail.setGst(getDecimalFieldValue(WebInsQuoteDetail.ANNUAL_PAYMENT_GST, quoteDetailsList));
            homeQuoteDetail.setStampDuty(getDecimalFieldValue(WebInsQuoteDetail.ANNUAL_PAYMENT_STAMP_DUTY, quoteDetailsList));
            homeQuoteDetail.setTotalAnnualPremium(getDecimalFieldValue(WebInsQuoteDetail.ANNUAL_PAYMENT_TOTAL_ANNUAL_PREMIUM, quoteDetailsList));
            homeQuoteDetail.setInstallmentAmount(getDecimalFieldValue(WebInsQuoteDetail.ANNUAL_PAYMENT_INSTALMENT_AMOUNT, quoteDetailsList));
        } else {
            homeQuoteDetail.setBaseAnnualPremium(getDecimalFieldValue(WebInsQuoteDetail.MONTHLY_PAYMENT_BASE_ANNUAL_PERMIUM, quoteDetailsList));
            homeQuoteDetail.setGst(getDecimalFieldValue(WebInsQuoteDetail.MONTHLY_PAYMENT_GST, quoteDetailsList));
            homeQuoteDetail.setStampDuty(getDecimalFieldValue(WebInsQuoteDetail.MONTHLY_PAYMENT_STAMP_DUTY, quoteDetailsList));
            homeQuoteDetail.setTotalAnnualPremium(getDecimalFieldValue(WebInsQuoteDetail.MONTHLY_PAYMENT_TOTAL_ANNUAL_PREMIUM, quoteDetailsList));
            homeQuoteDetail.setInstallmentAmount(getDecimalFieldValue(WebInsQuoteDetail.MONTHLY_PAYMENT_INSTALMENT_AMOUNT, quoteDetailsList));
        }

        homeQuoteDetail.setClaimHistory(getIntegerFieldValue(WebInsQuoteDetail.CLAIM, quoteDetailsList));
        homeQuoteDetail.setLiabilityClaimHistory(0);
        homeQuoteDetail.setClaimRefused(false);
        homeQuoteDetail.setInsuranceRefused(getBooleanFieldValue("insuranceRefused", quoteDetailsList));
        homeQuoteDetail.setCriminalOffences(getBooleanFieldValue(WebInsQuoteDetail.CRIMINAL_CONV, quoteDetailsList));
        homeQuoteDetail.setCriminalOffenceDetails(null);

        return homeQuoteDetail;
    }

    private MotorQuoteDetails getMotorQuoteDetails(WebInsQuote quote, String paymentFrequency, List<WebInsQuoteDetail> quoteDetailsList, List<WebInsClient> clientList) throws SystemException {
        MotorQuoteDetails motorQuoteDetail = new MotorQuoteDetails();

        String channelCode = getStringFieldValue(WebInsQuoteDetail.AGENT_CODE, quoteDetailsList);
        if (null == channelCode) {
            channelCode = "WEB";
        }
        motorQuoteDetail.setChannelCode(channelCode);

        motorQuoteDetail.setRiskType(RiskTypeCode.MOTR);
        motorQuoteDetail.setRatingDate(convertDateTimeToXmlGregorianDate(quote.getQuoteDate()));

        motorQuoteDetail.setEngineImmobiliser(getBooleanFieldValue(WebInsQuoteDetail.IMOBILISER, quoteDetailsList));
        motorQuoteDetail.setGoodOrderAndRepair(getBooleanFieldValue("nonExistingDamage", quoteDetailsList));

        String lookupOption = getStringFieldValue("vehicleLookupOption", quoteDetailsList);
        motorQuoteDetail.setBypassRegoLookup("lookup".equalsIgnoreCase(lookupOption));
        // motorQuoteDetail.setBypassGlasses("rego".equalsIgnoreCase(lookupOption));
        // BypassGlasses is always 0 - in the portal, you can:
        // 1. Enter Rego
        // 2. Bypass Rego, and enter glasses details
        // 3. Bypass Rego & Glasses, and enter freeform text
        // B2C does not support option3, so bypassGlasses is ALWAYS false
        motorQuoteDetail.setBypassGlasses(false);

        String nvic = getStringFieldValue("nvic", quoteDetailsList);
        if (null != nvic && !"".equals(nvic)) {
            GlassesVehicle vehicle;
            try {
                vehicle = getGlassesVehicle(nvic, quote.getQuoteDate());
            } catch (Exception e) {
                throw new SystemException(e);
            }

            motorQuoteDetail.setGlassesData(vehicle);
        }
        motorQuoteDetail.setRegistrationState(State.TAS);
        motorQuoteDetail.setRegistrationNumber(nullSafeStringToUpper(getStringFieldValue("regNo", quoteDetailsList)));

        motorQuoteDetail.setFirstOwner(false);

        motorQuoteDetail.setUsageCode(getStringFieldValue(WebInsQuoteDetail.USEAGE, quoteDetailsList));
        // acceptableGoodOrderAndRepairCode - leave blank if no damage

        String financier = getStringFieldValue(WebInsQuoteDetail.FINANCIER, quoteDetailsList);
        if (null != financier && !"".equals(financier)) {
            motorQuoteDetail.setFinancier(factory.createMotorQuoteDetailsFinancier(financier));
        }

        // Driver details!
        int ratingDriverId = getIntegerFieldValue("rating driver", quoteDetailsList);
        motorQuoteDetail.setListedDrivers(new DriverDetailsList());
        for (WebInsClient client : clientList) {
            if (ratingDriverId == client.getWebClientNo()) {
                // Rating Driver
                motorQuoteDetail.setRatingDriver(getDriverDetails(client, getDecimalFieldValue(WebInsQuoteDetail.NO_CLAIM_DISCOUNT, quoteDetailsList)));
            }
            if (client.isDriver()) {
                // Listed Drivers
                motorQuoteDetail.getListedDrivers().getDriver().add(getDriverDetails(client, BigDecimal.ZERO));
            }
        }

        motorQuoteDetail.setGarageAddress(getAddress(quoteDetailsList));

        motorQuoteDetail.setCompCar(WebInsQuote.COVER_COMP.equalsIgnoreCase(quote.getCoverType()));
        motorQuoteDetail.setTpCar(WebInsQuote.COVER_TP.equalsIgnoreCase(quote.getCoverType()));
        motorQuoteDetail.setMinValue(getIntegerFieldValue(WebInsQuoteDetail.AGREED_VALUE_LOWER, quoteDetailsList));
        motorQuoteDetail.setAverageValue(getIntegerFieldValue(WebInsQuoteDetail.AGREED_VALUE_DEFAULT, quoteDetailsList));
        motorQuoteDetail.setMaxValue(getIntegerFieldValue(WebInsQuoteDetail.AGREED_VALUE_UPPER, quoteDetailsList));
        motorQuoteDetail.setAgreedValue(getIntegerFieldValue(WebInsQuoteDetail.AGREED_VALUE, quoteDetailsList));

        motorQuoteDetail.setWindscreenDeletionOption(getBooleanFieldValue(WebInsQuoteDetail.WINDSCREEN, quoteDetailsList));
        motorQuoteDetail.setHireCarOption(getBooleanFieldValue(WebInsQuoteDetail.HIRE_CAR, quoteDetailsList));
        motorQuoteDetail.setFireTheftOption(getBooleanFieldValue(WebInsQuoteDetail.FIRE_THEFT, quoteDetailsList));
        motorQuoteDetail.setPipDiscount(getBooleanFieldValue(WebInsQuoteDetail.PIP_DISCOUNT, quoteDetailsList));
        motorQuoteDetail.setSilverSaverDiscount(getBooleanFieldValue(WebInsQuoteDetail.FIFTY_PLUS, quoteDetailsList));
        if (WebInsQuote.COVER_COMP.equalsIgnoreCase(quote.getCoverType())) {
            motorQuoteDetail.setSubRiskType(SubRiskTypeCode.COMC);
        } else {
            motorQuoteDetail.setSubRiskType(SubRiskTypeCode.TPPD);
        }

        try {
            Calendar cal = Calendar.getInstance();
            cal.setTime(quote.getQuoteDate());
            int vehicleAge = 0;
            if (null != motorQuoteDetail.getGlassesData() && null != motorQuoteDetail.getGlassesData().getYear()) {
                vehicleAge = cal.get(Calendar.YEAR) - Integer.valueOf(motorQuoteDetail.getGlassesData().getYear());
            }

            String maxValueCode = getVehicleMinMaxValueData(quote.getQuoteDate(), vehicleAge).getTypeCode();
            motorQuoteDetail.setMaxValueCode(maxValueCode);
        } catch (RemoteException e) {
            throw new SystemException("Error encountered during call of getVehicleMinMaxValueData", e);
        }

        motorQuoteDetail.setBasicExcessCode(getStringFieldValue(WebInsQuoteDetail.EXCESS, quoteDetailsList));

        if (WebInsQuoteDetail.PAY_ANNUALLY.equalsIgnoreCase(paymentFrequency)) {
            motorQuoteDetail.setBaseAnnualPremium(getDecimalFieldValue(WebInsQuoteDetail.ANNUAL_PAYMENT_BASE_ANNUAL_PERMIUM, quoteDetailsList));
            motorQuoteDetail.setGst(getDecimalFieldValue(WebInsQuoteDetail.ANNUAL_PAYMENT_GST, quoteDetailsList));
            motorQuoteDetail.setStampDuty(getDecimalFieldValue(WebInsQuoteDetail.ANNUAL_PAYMENT_STAMP_DUTY, quoteDetailsList));
            motorQuoteDetail.setTotalAnnualPremium(getDecimalFieldValue(WebInsQuoteDetail.ANNUAL_PAYMENT_TOTAL_ANNUAL_PREMIUM, quoteDetailsList));
            motorQuoteDetail.setInstallmentAmount(getDecimalFieldValue(WebInsQuoteDetail.ANNUAL_PAYMENT_INSTALMENT_AMOUNT, quoteDetailsList));
        } else {
            motorQuoteDetail.setBaseAnnualPremium(getDecimalFieldValue(WebInsQuoteDetail.MONTHLY_PAYMENT_BASE_ANNUAL_PERMIUM, quoteDetailsList));
            motorQuoteDetail.setGst(getDecimalFieldValue(WebInsQuoteDetail.MONTHLY_PAYMENT_GST, quoteDetailsList));
            motorQuoteDetail.setStampDuty(getDecimalFieldValue(WebInsQuoteDetail.MONTHLY_PAYMENT_STAMP_DUTY, quoteDetailsList));
            motorQuoteDetail.setTotalAnnualPremium(getDecimalFieldValue(WebInsQuoteDetail.MONTHLY_PAYMENT_TOTAL_ANNUAL_PREMIUM, quoteDetailsList));
            motorQuoteDetail.setInstallmentAmount(getDecimalFieldValue(WebInsQuoteDetail.MONTHLY_PAYMENT_INSTALMENT_AMOUNT, quoteDetailsList));
        }
        motorQuoteDetail.setPaymentFrequency(getPaymentFrequencyFromString(paymentFrequency));

        // motorQuoteDetail.setClaimHistory(getIntegerFieldValue(WebInsQuoteDetail.CLAIM, list));
        // motorQuoteDetail.setLiabilityClaimHistory(0);
        motorQuoteDetail.setClaimRefused(false);
        motorQuoteDetail.setInsuranceRefused(false);
        motorQuoteDetail.setCriminalOffences(getBooleanFieldValue("criminalConvicted", quoteDetailsList));
        // motorQuoteDetail.setCriminalOffenceDetails(null);

        return motorQuoteDetail;
    }

    private DriverDetails getDriverDetails(WebInsClient client, BigDecimal ncd) throws SystemException {
        DriverDetails response = new DriverDetails();
        response.setAtFaultClaims(0);
        response.setDateOfBirth(convertDateTimeToXmlGregorianDate(client.getBirthDate()));
        response.setDrivingFrequencyCode(client.getDrivingFrequency());
        response.setGender(Gender.fromValue(client.getGender()));
        response.setGivenName(nullSafeStringToUpper(client.getGivenNames()));
        response.setLossOfLicenceOffences(0);
        response.setMaliciousAndTheftClaims(0);
        response.setNcbAmount(ncd);
        if (null == client.getNumberOfAccidents()) {
            response.setNumberOfAccidents(0);
        } else {
            response.setNumberOfAccidents(client.getNumberOfAccidents());
        }
        response.setSurname(nullSafeStringToUpper(client.getSurname()));
        if (null != client.getYearCommencedDriving()) {
            response.setYearCommencedDriving(client.getYearCommencedDriving());
        }

        return response;
    }

    private Address getAddress(List<WebInsQuoteDetail> list) {
        // Ausbar, Dpid, PropertyName not stored in DB for situation address
        Address address = new Address();
        // address.setAusbar(value)
        address.setCountry("Australia");
        // address.setDpid(value)
        address.setGnafId(factory.createAddressGnafId(getStringFieldValue(WebInsQuoteDetail.SITUATION_GNAF, list)));
        address.setLatitude(factory.createAddressLatitude(getStringFieldValue(WebInsQuoteDetail.SITUATION_LATITUDE, list)));
        address.setLongitude(factory.createAddressLongitude(getStringFieldValue(WebInsQuoteDetail.SITUATION_LONGITUDE, list)));
        address.setPostcode(getStringFieldValue(WebInsQuoteDetail.SITUATION_POSTCODE, list));
        address.setPostcodeIsTasmanian(true);
        // address.setPropertyName(value)
        address.setState(State.TAS);
        address.setStreetName(nullSafeStringToUpper(getStringFieldValue(WebInsQuoteDetail.SITUATION_STREET, list)));
        address.setStreetNumber(nullSafeStringToUpper(getStringFieldValue(WebInsQuoteDetail.SITUATION_NO, list)));
        address.setSuburb(nullSafeStringToUpper(getStringFieldValue(WebInsQuoteDetail.SITUATION_SUBURB, list)));
        return address;
    }

    private int calculateSpecifiedContentsSumInsured(List<SpecifiedItem> specifiedContents) {
        int totalInsured = 0;
        if (null == specifiedContents || specifiedContents.isEmpty()) {
            return totalInsured;
        }

        for (SpecifiedItem item : specifiedContents) {
            totalInsured += item.getSumInsured();
        }

        return totalInsured;
    }

    private List<SpecifiedItem> getSpecifiedContents(List<InRiskSi> siList) {
        List<SpecifiedItem> response = new ArrayList<SpecifiedItem>();

        if (null == siList) {
            return response;
        }

        for (InRiskSi inRiskSi : siList) {
            LogUtil.warn(this.getClass(), "Class: " + inRiskSi.getSiClass());
            if (null != inRiskSi.getItemList()) {
                for (InRiskSiLineItem inRiskSiLineItem : inRiskSi.getItemList()) {
                    LogUtil.warn(this.getClass(), "        - Item detail: " + inRiskSiLineItem.getDescription() + " - " + inRiskSiLineItem.getSumIns());

                    SpecifiedItem item = new SpecifiedItem();
                    item.setCategoryCode(inRiskSi.getSiClass().trim());
                    item.setSubCategoryCode(inRiskSi.getSiClass().trim() + "OTH");
                    item.setDescription(inRiskSiLineItem.getDescription());
                    item.setSerialNumber(factory.createSpecifiedItemSerialNumber(inRiskSiLineItem.getSerialNo().trim()));
                    item.setSumInsured(inRiskSiLineItem.getSumIns().intValue());
                    response.add(item);
                }
            } else {
                LogUtil.warn(this.getClass(), "        - No items in list for " + inRiskSi.getSiClass());
            }
        }

        return response;
    }

    private PaymentFrequency getPaymentFrequencyFromString(String frequency) {
        if (WebInsQuoteDetail.PAY_ANNUALLY.equalsIgnoreCase(frequency)) {
            return PaymentFrequency.A;
        }
        if (WebInsQuoteDetail.PAY_MONTHLY.equalsIgnoreCase(frequency)) {
            return PaymentFrequency.M;
        }
        if ("quarterly".equalsIgnoreCase(frequency)) {
            return PaymentFrequency.Q;
        }
        if ("half yearly".equalsIgnoreCase(frequency)) {
            return PaymentFrequency.H;
        }
        LogUtil.warn(this.getClass(), "******************************************* Applying default PaymentFrequency.A for supplied frequency: " + frequency);
        return PaymentFrequency.A;
    }

    private XMLGregorianCalendar getDateFieldValue(String fieldName, List<WebInsQuoteDetail> detailList) throws SystemException {
        WebInsQuoteDetail field = getDetailByFieldName(fieldName, detailList);
        if (null == field || field.getFieldValue().isEmpty()) {
            return null;
        }

        XMLGregorianCalendar result = null;
        Date date;
        SimpleDateFormat simpleDateFormat;
        GregorianCalendar gregorianCalendar;

        try {
            simpleDateFormat = new SimpleDateFormat("dd/mm/yyyy");
            date = simpleDateFormat.parse(field.getFieldValue());
            gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
            gregorianCalendar.setTime(date);
            result = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        } catch (Exception e) {
            throw new SystemException("Failed to parse XMLGregorianCalendar from string", e);
        }
        return result;
    }

    private XMLGregorianCalendar getDateTimeFieldValue(String fieldName, List<WebInsQuoteDetail> detailList) throws SystemException {
        WebInsQuoteDetail field = getDetailByFieldName(fieldName, detailList);
        if (null == field || field.getFieldValue().isEmpty()) {
            return null;
        }

        XMLGregorianCalendar result = null;
        Date date;
        GregorianCalendar gregorianCalendar;

        try {
            date = DateUtil.parseDate(field.getFieldValue());
            gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
            gregorianCalendar.setTime(date);
            result = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        } catch (Exception e) {
            throw new SystemException("Failed to parse XMLGregorianCalendar from string", e);
        }
        return result;
    }

    private boolean getBooleanFieldValue(String fieldName, List<WebInsQuoteDetail> detailList) {
        WebInsQuoteDetail field = getDetailByFieldName(fieldName, detailList);
        if (null == field) {
            return false;
        }

        String value = field.getFieldValue();
        if (WebInsQuoteDetail.TRUE.equalsIgnoreCase(value)) {
            return true;
        }
        if (WebInsQuoteDetail.FALSE.equalsIgnoreCase(value)) {
            return false;
        }

        return Boolean.valueOf(value);
    }

    private int getIntegerFieldValue(String fieldName, List<WebInsQuoteDetail> detailList) {
        WebInsQuoteDetail field = getDetailByFieldName(fieldName, detailList);
        if (null == field) {
            return 0;
        }
        return Integer.valueOf(field.getFieldValue());
    }

    private String getStringFieldValue(String fieldName, List<WebInsQuoteDetail> detailList) {
        WebInsQuoteDetail field = getDetailByFieldName(fieldName, detailList);
        if (null == field) {
            return null;
        }
        return field.getFieldValue();
    }

    private BigDecimal getDecimalFieldValue(String fieldName, List<WebInsQuoteDetail> detailList) throws SystemException {
        WebInsQuoteDetail field = getDetailByFieldName(fieldName, detailList);
        if (null == field) {
            return BigDecimal.ZERO;
        }

        DecimalFormat format = new DecimalFormat();
        format.setParseBigDecimal(true);
        BigDecimal result;
        try {
            result = (BigDecimal) format.parse(field.getFieldValue());

        } catch (ParseException e) {
            throw new SystemException("Failed to convert string to decimal", e);
        }
        return result;
    }

    private WebInsQuoteDetail getDetailByFieldName(String fieldName, List<WebInsQuoteDetail> detailList) {
        for (WebInsQuoteDetail detail : detailList) {
            if (detail.getFieldName().equalsIgnoreCase(fieldName)) {
                return detail;
            }
        }
        return null;
    }

    private XMLGregorianCalendar convertDateTimeToXmlGregorianDate(DateTime quoteDate) throws SystemException {
        if (null == quoteDate) {
            return null;
        }

        GregorianCalendar c = new GregorianCalendar();
        XMLGregorianCalendar date2 = null;
        c.setTime(quoteDate);
        try {
            date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
        } catch (DatatypeConfigurationException e) {
            throw new SystemException("Failed to parse date for query", e);
        }
        return date2;
    }

    private GlVehicle createGlVehicle(GlassesVehicle glassesVehicle) {
        GlVehicle response = new GlVehicle();
        response.setAcceptable(glassesVehicle.getAcceptability().toString());
        response.setCylinders(glassesVehicle.getCylinder());
        response.setSeriesDesc(glassesVehicle.getSeries());
        response.setEngineCap(glassesVehicle.getEngineCapacity());
        response.setEngineDesc(glassesVehicle.getEngineDescription());
        response.setModel(glassesVehicle.getFamily());
        response.setMake(glassesVehicle.getMake());
        response.setNvic(glassesVehicle.getNvic());
        response.setBodyType(glassesVehicle.getStyle());
        response.setTransmission(glassesVehicle.getTransmission());
        response.setValue(glassesVehicle.getValue());
        response.setVariant(glassesVehicle.getVariant());
        response.setVehYear(Integer.valueOf(glassesVehicle.getYear()));
        response.setCrPoints(glassesVehicle.getCrPoints());
        response.setTpPoints(glassesVehicle.getTpPoints());
        return response;
    }

    private InRfDet createInRfDet(UdlRecord record, String keyValue) {
        InRfDet response = new InRfDet();
        response.setrKey3(keyValue);
        response.setrClass(record.getCode());
        response.setrDescription(record.getDescription());
        response.setAcceptable(record.isAcceptable());
        return response;
    }

    private InRfDet createInRfDet(ExcessDetail record, String keyValue) throws SystemException {
        InRfDet response = new InRfDet();
        response.setrKey3(keyValue);
        response.setrClass(record.getCode());
        response.setrDescription(record.getDescription());
        response.setrValue(record.getPercentageAmount());
        response.setrUnit("%");
        response.setAcceptable(record.isDefaultValue());
        return response;
    }

    private InRfDet createInRfDet(OptionDetail record, String keyValue) throws SystemException {
        InRfDet response = new InRfDet();
        response.setrKey3(keyValue);
        response.setrClass(record.getCode());
        response.setrDescription(record.getDescription());
        response.setrValue(record.getDollarAmount());
        response.setrUnit("$");

        if (BigDecimal.ZERO.compareTo(record.getPercentageAmount()) != 0) {
            response.setrValue(record.getPercentageAmount());
            response.setrUnit("%");
        }
        response.setAcceptable(record.isAcceptable());
        return response;
    }

    private RefType createRefType(UdlRecord record) {
        RefType response = new RefType();
        response.setTypeCode(record.getCode());
        response.setTDescription(record.getDescription());
        response.setTText2("0");
        if (record.isAcceptable()) {
            response.setTText3("A");
        } else {
            // current data being returned is R or U (reject or unacceptable)
            response.setTText3("U");
        }
        return response;
    }

    private String getInitials(String nameString) {
        if (null == nameString || nameString.isEmpty()) {
            return null;
        }

        String[] splitStringArray = nameString.split(" ");
        StringBuilder sb = new StringBuilder();
        for (String name : splitStringArray) {
            sb.append(name.substring(0, 1));
        }
        return sb.toString().toUpperCase();
    }

    private String nullSafeStringToUpper(String input) {
        if(null == input || input.isEmpty()) {
            return input;
        }
        return input.toUpperCase();
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void logXml(Object objectToLog) {
        try {
            StringWriter writer = new StringWriter();
            JAXBContext contextA = JAXBContext.newInstance(com.ract.purelink.webservice.GetQuoteRequest.class);
            Marshaller m = contextA.createMarshaller();
            m.marshal(new JAXBElement(new QName("http://www.ract.com.au/RactPureLink", objectToLog.getClass().getName()), objectToLog.getClass(), objectToLog), writer);
            LogUtil.warn(this.getClass(), writer.toString());
        } catch (Exception e) {
            LogUtil.warn(getClass(), "Error trying to dump request xml");
            LogUtil.warn(getClass(), e);
        }
    }
}
