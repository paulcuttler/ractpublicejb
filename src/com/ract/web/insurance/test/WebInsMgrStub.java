package com.ract.web.insurance.test;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import com.ract.common.GenericException;
import com.ract.common.SystemDataException;
import com.ract.insurance.Policy;
import com.ract.util.DateTime;
import com.ract.web.agent.WebAgentStatus;
import com.ract.web.common.GnSetting;
import com.ract.web.common.WebPayable;
import com.ract.web.insurance.InRfDet;
import com.ract.web.insurance.InRiskSi;
import com.ract.web.insurance.InRiskSiLineItem;
import com.ract.web.insurance.ListPair;
import com.ract.web.insurance.Premium;
import com.ract.web.insurance.RefType;
import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsClientDetail;
import com.ract.web.insurance.WebInsGlMgrRemote;
import com.ract.web.insurance.WebInsMgrRemote;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;
@Stateless
@Remote({WebInsGlMgrRemote.class})
public class WebInsMgrStub implements WebInsMgrRemote {

/*	 
	@Override
	public Collection<Policy> getClientPolicies(Integer clientNumber,
			boolean loadRisks, boolean includeCancelled, boolean includeRenewal)
			throws GenericException
	{
		// TODO Auto-generated method stub
		return null;
	}
*/
	
	@Override
	public ArrayList<WebInsQuote> findQuotesByStatus(String status)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<WebInsQuote> findQuotesByStatus(String[] statusList)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<WebInsClient> getClientGroups(Integer quoteNo)
			throws Exception {
		ArrayList<WebInsClient> list = new ArrayList<WebInsClient>();
		WebInsClient clt = new WebInsClient();
		clt.setRactClientNo(new Integer(545132));
		clt.setGivenNames("Mr GJ Harrington;Mrs F Harrington");
		clt.setPostAddress("PO Box 158");
		clt.setPostPostcode("7000");
		clt.setPostStreet("Smith St");
		clt.setPostStreetChar("1/15");
		list.add(clt);
		
		clt = new WebInsClient();
		clt.setRactClientNo(new Integer(546121));
	    clt.setGivenNames("Mr GJ Harrington; Mr R Harrington");
	    clt.setPostAddress("PO Box 158");
	    clt.setPostStreet("");
	    clt.setPostSuburb("Hobart");
	    clt.setPostStsubid(new Integer(1362));
		list.add(clt);
		
		clt = new WebInsClient();
	    clt.setRactClientNo(new Integer(541661));
	    clt.setGivenNames("Mr GJ Harrington; Mrs F Harrington; Mr R Harrington");
	    clt.setPostStreetChar("45B");
	    clt.setPostStreet("Sun St");
	    clt.setPostSuburb("Hobart");
	    clt.setPostStsubid(new Integer(37621));
        list.add(clt);
		return list;
	}

	@Override
	public void setCoverType(Integer quoteNo, String coverType)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startCover(Integer quoteNo, String cardType, String bsb, String accountNumber, String cardExpiry, String accountName, String paymentDay, String paymentFrequency) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void useExistingGroup(Integer quoteNo, Integer ractclientNo)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	public Integer calculateNCD(WebInsClient clt) throws Exception {
		return new Integer(40);
	}

	public Boolean checkFiftyPlus(Integer quoteNo) throws Exception {
		return new Boolean(true);
	}

	public WebInsClient createInsuranceClient(Integer webQuoteNo) throws Exception {
		WebInsClient clt = new WebInsClient();
		clt.setWebClientNo(new Integer(176580));
		return clt;
	}

	@Override
	public ArrayList<WebInsQuoteDetail> getAllQuoteDetails(Integer quoteNo)
			throws Exception {
		ArrayList<WebInsQuoteDetail> list = new ArrayList<WebInsQuoteDetail>();
		list.add(new WebInsQuoteDetail(quoteNo,WebInsQuoteDetail.AGREED_VALUE,"21850"));
		list.add(new WebInsQuoteDetail(quoteNo,WebInsQuoteDetail.DRIVER_DOB,"15/07/1990"));
		list.add(new WebInsQuoteDetail(quoteNo,WebInsQuoteDetail.EXCESS,"500"));
		list.add(new WebInsQuoteDetail(quoteNo,WebInsQuoteDetail.FIFTY_PLUS,WebInsQuoteDetail.TRUE));
		list.add(new WebInsQuoteDetail(quoteNo,WebInsQuoteDetail.FIN_TYPE,WebInsQuoteDetail.TRUE));
		list.add(new WebInsQuoteDetail(quoteNo,WebInsQuoteDetail.SITUATION_ID,"16701"));
		list.add(new WebInsQuoteDetail(quoteNo,WebInsQuoteDetail.HIRE_CAR,WebInsQuoteDetail.TRUE));
		list.add(new WebInsQuoteDetail(quoteNo,WebInsQuoteDetail.NO_CLAIM_DISCOUNT,"40"));
		return list;
	}

	public Integer getBasicXs(String coverType, DateTime date) throws Exception {
        return new Integer(340);
	}

	
	public WebInsClient getWebInsClient(Integer webClientNo) throws Exception {
		WebInsClient clt = new WebInsClient();
		clt.setClientType(com.ract.web.client.WebClient.CLIENT_TYPE_INSURANCE);
		clt.setCurrentNCD(new Integer(60));
		clt.setBirthDate(new DateTime("16/07/1958"));
		clt.setDriver(false);
		clt.setDrivingFrequency(WebInsClient.DRIVE_3_4);
		clt.setEmailAddress("fred@ract.com.au");
		clt.setFaxNo("5555 5555");
		clt.setGender("M");
		clt.setGivenNames("Gordon James");
		clt.setCltGroup(false);
		clt.setHomePhone("03 0303 0303");
		clt.setMobilePhone("0444 555 555");
		clt.setNumberOfAccidents(new Integer(1));
		clt.setOwner(true);
		clt.setPostAddress("PO Box 158");
		clt.setPostPostcode("7000");
		clt.setPostStreet("Smith St");
		clt.setPostStreetChar("1/15");
		clt.setPostState("TAS");
		clt.setPostStsubid(new Integer(30993));
		clt.setPostSuburb("Hobart");
		clt.setPreferredAddress(true);
		clt.setRactClientNo(new Integer(249716));
		clt.setResiStreet("Sun St");
		clt.setResiStreetChar("1/15");
		clt.setResiStsubid(new Integer(7943));
		clt.setResiSuburb("Hobart");
		clt.setSurname("Harrington");
		clt.setTitle("Mr");
		clt.setWebClientNo(new Integer(1785));
		clt.setWebQuoteNo(new Integer(666666));
		return clt;
	}

	@Override
	public ArrayList<WebInsClientDetail> getClientDetails(Integer clientNo)
			throws Exception {
		// TODO Auto-generated method stub
		ArrayList<WebInsClientDetail> list = new ArrayList<WebInsClientDetail>();
		Integer yr = new Integer(2005);
		Integer mn = new Integer(3);
		list.add(new WebInsClientDetail(clientNo,WebInsClient.CLIENT_DETAIL_ACC_THEFT,mn,yr,"Stolen"));
		list.add(new WebInsClientDetail(clientNo,WebInsClient.CLIENT_DETAIL_ACC_THEFT,mn,yr,"Smashed car into power pole"));
		list.add(new WebInsClientDetail(clientNo,WebInsClient.CLIENT_DETAIL_REFUSED,mn,yr,"AAMI declined"));
		list.add(new WebInsClientDetail(clientNo,WebInsClient.CLIENT_DETAIL_SUSPENSION,mn,yr,"5 points for speeding"));
		list.add(new WebInsClientDetail(clientNo,WebInsClient.CLIENT_DETAIL_SUSPENSION,mn,yr,"DUI"));
		list.add(new WebInsClientDetail(clientNo,WebInsClient.CLIENT_DETAIL_SUSPENSION,mn,yr,"SUSP 2 Years"));
	
		return list;
	}

	@Override
	public ArrayList<WebInsClient> getClients(Integer webQuoteNo)
			throws Exception {
		ArrayList<WebInsClient> list = new ArrayList<WebInsClient>();
		WebInsClient clt = new WebInsClient();
		clt.setClientType(com.ract.web.client.WebClient.CLIENT_TYPE_INSURANCE);
		clt.setCurrentNCD(new Integer(60));
		clt.setBirthDate(new DateTime("16/07/1958"));
		clt.setDriver(false);
		clt.setDrivingFrequency(WebInsClient.DRIVE_3_4);
		clt.setEmailAddress("fred@ract.com.au");
		clt.setFaxNo("5555 5555");
		clt.setGender("M");
		clt.setGivenNames("Gordon James");
		clt.setCltGroup(false);
		clt.setHomePhone("03 0303 0303");
		clt.setMobilePhone("0444 555 555");
		clt.setNumberOfAccidents(new Integer(1));
		clt.setOwner(true);
		clt.setPostAddress("PO Box 158");
		clt.setPostPostcode("7000");
		clt.setPostStreet("Smith St");
		clt.setPostStreetChar("1/15");
		clt.setPostState("TAS");
		clt.setPostStsubid(new Integer(30993));
		clt.setPostSuburb("Hobart");
		clt.setPreferredAddress(true);
		clt.setRactClientNo(new Integer(249716));
		clt.setResiStreet("Sun St");
		clt.setResiStreetChar("1/15");
		clt.setResiStsubid(new Integer(7943));
		clt.setResiSuburb("Hobart");
		clt.setSurname("Harrington");
		clt.setTitle("Mr");
		clt.setWebClientNo(new Integer(1785));
		clt.setWebQuoteNo(new Integer(666666));
		list.add(clt);
		clt = new WebInsClient();
		clt.setClientType(com.ract.web.client.WebClient.CLIENT_TYPE_INSURANCE);
		clt.setCurrentNCD(new Integer(60));
		clt.setBirthDate(new DateTime("11/11/1962"));
		clt.setDriver(false);
		clt.setDrivingFrequency(WebInsClient.DRIVE_3_4);
		clt.setEmailAddress("fred@ract.com.au");
		clt.setFaxNo("5555 5555");
		clt.setGender("M");
		clt.setGivenNames("Harriet Melinda");
		clt.setCltGroup(false);
		clt.setHomePhone("03 0303 0303");
		clt.setMobilePhone("0444 555 555");
		clt.setNumberOfAccidents(new Integer(1));
		clt.setOwner(true);
		clt.setPostAddress("PO Box 158");
		clt.setPostPostcode("7000");
		clt.setPostStreet("Smith St");
		clt.setPostStreetChar("1/15");
		clt.setPostState("TAS");
		clt.setPostStsubid(new Integer(30993));
		clt.setPostSuburb("Hobart");
		clt.setPreferredAddress(true);
		clt.setRactClientNo(new Integer(249716));
		clt.setResiStreet("Sun St");
		clt.setResiStreetChar("1/15");
		clt.setResiStsubid(new Integer(7943));
		clt.setResiSuburb("Hobart");
		clt.setSurname("Harrington");
		clt.setTitle("Mrs");
		clt.setWebClientNo(new Integer(1785));
		clt.setWebQuoteNo(new Integer(666666));
		list.add(clt);
		return list;
	}

	
	public Integer getGlVehicleValue(Integer quoteNo) {
		// TODO Auto-generated method stub
		return new Integer(21750);
	}

	
	public Premium getPremium(Integer webQuoteNo, String paymentFrequency,
			String coverType) throws Exception {
		// TODO Auto-generated method stub
		
        Premium p = new Premium(new BigDecimal(645.36),new BigDecimal(53.78));
		return p;
	}

	
	public WebInsQuote getQuote(Integer quoteNo) {
		WebInsQuote qt = new WebInsQuote();
		qt.setCoverType("CAR");
		qt.setCreateDate(new DateTime());
		try
		{
		   qt.setQuoteDate(new DateTime("25/12/2008"));
		}
		catch(Exception ex){}
		qt.setQuoteStatus(WebInsQuote.NEW);
		qt.setQuoteType(WebInsQuote.TYPE_MOTOR);
		qt.setWebQuoteNo(new Integer(5555));
		return null;
	}

	
	public String getQuoteDetail(Integer quoteNo, String fieldName) {
		return "Have a nice day";
	}

	@Override
	public WebInsClient getRatingDriver(Integer quoteNo) throws Exception {
		WebInsClient clt = new WebInsClient();
		clt.setClientType(com.ract.web.client.WebClient.CLIENT_TYPE_INSURANCE);
		clt.setCurrentNCD(new Integer(60));
		clt.setBirthDate(new DateTime("11/11/1991"));
		clt.setDriver(false);
		clt.setDrivingFrequency(WebInsClient.DRIVE_3_4);
		clt.setEmailAddress("fred@ract.com.au");
		clt.setFaxNo("5555 5555");
		clt.setGender("M");
		clt.setGivenNames("James David");
		clt.setCltGroup(false);
		clt.setHomePhone("03 0303 0303");
		clt.setMobilePhone("0444 666 666");
		clt.setNumberOfAccidents(new Integer(1));
		clt.setOwner(true);
		clt.setPostAddress("PO Box 158");
		clt.setPostPostcode("7000");
		clt.setPostStreet("Smith St");
		clt.setPostStreetChar("1/15");
		clt.setPostState("TAS");
		clt.setPostStsubid(new Integer(30993));
		clt.setPostSuburb("Hobart");
		clt.setPreferredAddress(true);
		clt.setRactClientNo(new Integer(249716));
		clt.setResiStreet("Sun St");
		clt.setResiStreetChar("1/15");
		clt.setResiStsubid(new Integer(7943));
		clt.setResiSuburb("Hobart");
		clt.setSurname("Harrington");
		clt.setTitle("Mr");
		clt.setWebClientNo(new Integer(1785));
		clt.setWebQuoteNo(new Integer(666666));
		return clt;
	}

	@Override
	public String listClientDetails(Integer clientNo) {

		return "Mr Fred George Smith"
		     + "\n155 Sun St"
		     + "\nHobart TAS 2007"
		     + "\nisDriver = no";
	}

	
	public void printCoverDoc(Integer quoteNo) throws Exception {
	}


	public void removeAllClientDetails(Integer clientNo) throws Exception {
	}

	
	public void removeClient(Integer clientNo) throws Exception {
	}

	
	public void removeClientDetail(WebInsClientDetail det) throws Exception {
	}

	
	public void removeClientDetail(Integer clientNo, Integer detailSeq)
			throws Exception {
	

	}

	@Override
	public void removeQuoteDetail(Integer quoteNo, String fieldName)
			throws Exception {
	

	}

	@Override
	public void setClient(WebInsClient clt) throws Exception {
	

	}

	@Override
	public void setClientDetail(WebInsClientDetail det) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public WebInsClient setFromRACTClient(Integer quoteNo, String cardNo,
			DateTime birthDate) {
		WebInsClient clt = new WebInsClient();
		clt.setClientType(com.ract.web.client.WebClient.CLIENT_TYPE_INSURANCE);
		clt.setCurrentNCD(new Integer(60));
		try
		{
		clt.setBirthDate(new DateTime("11/11/1991"));
		}
		catch(Exception ex){}
		clt.setDriver(false);
		clt.setDrivingFrequency(WebInsClient.DRIVE_3_4);
		clt.setEmailAddress("fred@ract.com.au");
		clt.setFaxNo("5555 5555");
		clt.setGender("M");
		clt.setGivenNames("James David");
		clt.setCltGroup(false);
		clt.setHomePhone("03 0303 0303");
		clt.setMobilePhone("0444 666 666");
		clt.setNumberOfAccidents(new Integer(1));
		clt.setOwner(true);
		clt.setPostAddress("PO Box 158");
		clt.setPostPostcode("7000");
		clt.setPostStreet("Smith St");
		clt.setPostStreetChar("1/15");
		clt.setPostState("TAS");
		clt.setPostStsubid(new Integer(30993));
		clt.setPostSuburb("Hobart");
		clt.setPreferredAddress(true);
		clt.setRactClientNo(new Integer(249716));
		clt.setResiStreet("Sun St");
		clt.setResiStreetChar("1/15");
		clt.setResiStsubid(new Integer(7943));
		clt.setResiSuburb("Hobart");
		clt.setSurname("Harrington");
		clt.setTitle("Mr");
		clt.setWebClientNo(new Integer(1785));
		clt.setWebQuoteNo(new Integer(666666));
		return clt;
	}

	@Override
	public void setQuoteDate(Integer quoteNo, DateTime date) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void setQuoteDetail(Integer quoteNo, String fieldName,
			String fieldValue) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setQuoteStatus(Integer quoteNo, String status) {
		// TODO Auto-generated method stub

	}

	@Override
	public Integer startQuote(DateTime quoteDate, String quoteType)
			throws Exception {
		// TODO Auto-generated method stub
		return new Integer(12345);
	}

	@Override
	public Integer validateSecuredQuoteNo(String securedQuoteNo)
			throws Exception {
		// TODO Auto-generated method stub
		return new Integer(12345);
	}
	public ArrayList<RefType> getRefList(String refType, DateTime effDate)throws Exception
	{
		ArrayList<RefType> list = new ArrayList<RefType>();
		RefType type = null;
		if(refType.equals(WebInsQuoteDetail.REF_TYPE_USAGE))
		{
		type = new RefType("p",
				           "Private",
				           "",
				           "A");
		list.add(type);
		type = new RefType("CD",
		           "Courier/Delivery",
		           "",
		           "U");
        list.add(type);
		type = new RefType("TO",
		           "Tradesman (owner driver)",
		           "",
		           "A");
        list.add(type);
		type = new RefType("TE",
		           "Tradesman (employees driving the vehicle)",
		           "",
		           "R");
        list.add(type);
        type = new RefType("DE",
		           "Driver Education",
		           "",
		           "U");
        list.add(type);
		type = new RefType("F",
		           "Farmer",
		           "",
		           "A");
        list.add(type);
		type = new RefType("S",
		           "Sales person",
		           "",
		           "R");
        list.add(type);
		}
		else if(refType.equals(WebInsQuoteDetail.REF_TYPE_FINANCIERS))
		{
		       type = new RefType("AMP001",
			           "AMP",
			           "",
			           "");
	        list.add(type);
			type = new RefType("ANZ001",
			           "ANZ Bank",
			           "",
			           "");
	        list.add(type);
			type = new RefType("ESA001",
			           "Esanda Finance",
			           "",
			           "");
	        list.add(type);

		}
        
        return list;
	}
	public GnSetting getGnSetting(String tableName,String tCode )throws Exception
	{
		GnSetting gn = new GnSetting();
		gn.setDescription("Have a nice day");
		return gn;
	}

	@Override
	public void setStartCoverDate(Integer quoteNo, DateTime coverDate)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String allocateSalesBranch() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<WebInsQuote> findQuotesBySalesBranch(String salesBranch)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Policy> getClientPolicies(Integer clientNumber,
			boolean loadRisks, boolean includeCancelled, boolean includeRenewal)
			throws GenericException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<InRiskSiLineItem> getAllSiLineItems(Integer quoteNo,
			Integer seqNo) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<InRfDet> getLookupData(String product, String riskType,
			String fieldName, DateTime effDate) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InRiskSiLineItem getSiLineItem(Integer quoteNo, Integer seqNo,
			Integer lineNo) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InRiskSi getSpecifiedItem(Integer webQuoteNo, Integer siSeqNo)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<InRiskSi> getSpecifiedItems(Integer webQuoteNo)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeAllSiLineItems(InRiskSi si) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeSpecifiedItem(Integer webQuoteNo, Integer siSeqNo)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setAllSiLineItems(InRiskSi item) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setSiLineItem(InRiskSiLineItem lineItem) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setSpecifiedItem(InRiskSi item) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setSpecifiedItems(ArrayList<InRiskSi> itemList)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setQuoteDetail(WebInsQuoteDetail det) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getInsSetting(String tName, String tKey, DateTime effDate)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WebInsQuoteDetail getDetail(Integer quoteNo, String fieldName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeAllSpecifiedItems(Integer quoteNo) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
    public WebAgentStatus validateAgent(String agentCode, String agentPassword) throws Exception
    {
	    // TODO Auto-generated method stub
	    return null;
    }

	@Override
	public ArrayList<ListPair> getFixedExcesses(String prodType,
			String riskType, DateTime effDate) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<WebInsClient> checkClients(Integer quoteNo, String coverType) throws Exception {
		// TODO Auto-generated method stub
	    return null;
	}

	@Override
	public String getNotionalProdType(Integer clientNo, String fiftyPlus,
			String riskType, DateTime effDate) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String allocateSalesBranch(String agentCode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getMessageText(String category, String type,
			String subType, String prodType, String coverType, DateTime startDate)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<WebPayable> getConvertedTransactions(DateTime since)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<WebPayable> getUnreceiptedPayables(DateTime since)
			throws RemoteException, SystemDataException {
		// TODO Auto-generated method stub
		return null;
	}

    @Override
    public Float getMinVehicleValuePercentage(DateTime queryDate, int vehicleAge) throws RemoteException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Float getMaxVehicleValuePercentage(DateTime queryDate, int vehicleAge) throws RemoteException {
        // TODO Auto-generated method stub
        return null;
    }


}
