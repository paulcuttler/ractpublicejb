package com.ract.web.insurance;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import com.ract.common.SystemException;
import com.ract.motorwebclient.webservice.GetNvicByPlateRequest;
import com.ract.motorwebclient.webservice.GetNvicByPlateResponse;
import com.ract.motorwebclient.webservice.MotorWebClient;
import com.ract.motorwebclient.webservice.MotorWebClientGetNvicByPlateInputRactErrorFaultMessage;
import com.ract.motorwebclient.webservice.MotorWebClient_Service;
import com.ract.motorwebclient.webservice.State;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;

public class MotorWebAdapter {
    private static String motorWebClientEndpoint = "http://localhost/MotorWebClientWS/MotorWebClient.svc";
    private static MotorWebClient_Service service;

    static {
        try {
            motorWebClientEndpoint = FileUtil.getProperty("master", "motorwebclient.server.url");
            LogUtil.warn(MotorWebAdapter.class, "Loaded MotorWebClient endpoint from configuration: " + motorWebClientEndpoint);
        } catch (Exception e1) {
            LogUtil.warn(MotorWebAdapter.class, "*******************************************************************************************");
            LogUtil.warn(MotorWebAdapter.class, e1);
            LogUtil.warn(MotorWebAdapter.class, "Could not determine motorwebclient.server.url, defaulting to: " + motorWebClientEndpoint);
        }

        String wsdl_location = motorWebClientEndpoint + "?singleWsdl";
        LogUtil.warn(MotorWebAdapter.class, "Generated wsdl location based on endpoint: " + wsdl_location);
        
        try {
            service = new MotorWebClient_Service(new URL(wsdl_location), new QName("http://www.ract.com.au/MotorWebClient", "MotorWebClient"));
        } catch (MalformedURLException e) {
            LogUtil.fatal(MotorWebAdapter.class, e);
            LogUtil.warn(MotorWebAdapter.class, "Failed to create URL for the wsdl Location: '" + wsdl_location + "', retrying using default configuration");
            LogUtil.warn(MotorWebAdapter.class, e);
            service = new MotorWebClient_Service();
        }
    }

    public MotorWebAdapter() {
    }

    private MotorWebClient getPort()
    {
        MotorWebClient port = service.getBasicHttpBindingMotorWebClient();
        BindingProvider bindingProvider = (BindingProvider) port;
        bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, motorWebClientEndpoint);
        return port;
    }
    
    public String getNvicByRegistration(String plate, String state) throws Exception {
        if (null == plate || null == state) {
            return "";
        }

        GetNvicByPlateRequest request = new GetNvicByPlateRequest();
        request.setPlate(plate);
        request.setState(State.fromValue(state));

        GetNvicByPlateResponse response = null;

        try {
            response = getPort().getNvicByPlateInput(request);
        } catch (MotorWebClientGetNvicByPlateInputRactErrorFaultMessage e) {
            throw new SystemException("Error occured extracting nvic by registration", e);
        }

        if (null == response.getNvic()) {
            return "";
        }

        return response.getNvic().getValue();
    }
}
