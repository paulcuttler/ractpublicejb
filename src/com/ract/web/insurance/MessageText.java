package com.ract.web.insurance;

public class MessageText {
   private String heading;
   private String text;
   
   public MessageText(String heading, String text)
   {
	   this.heading = heading;
	   this.text = text;
   }
   public String getHeading() {
	   return heading;
   }
   public void setHeading(String heading) {
	   this.heading = heading;
   }
   public String getText() {
	   return text;
   }
   public void setText(String text) {
	   this.text = text;
   }
}
