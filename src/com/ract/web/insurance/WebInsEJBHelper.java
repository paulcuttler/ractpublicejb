package com.ract.web.insurance;

import javax.naming.Context;
import javax.naming.InitialContext;

import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;

/**
 * Initialised via jndi.properties
 * @author hollidayj
 *
 */
public class WebInsEJBHelper 
{

	public static WebInsMgrRemote getWebInsMgr()throws Exception
	{
		Context ctx = new InitialContext();
		WebInsMgrRemote	bean = (WebInsMgrRemote) ctx.lookup("WebInsMgr/remote");
    return bean;
	}
	
	public static WebInsGlMgrRemote getWebGlMgr()throws Exception
	{
		Context ctx = new InitialContext();
		WebInsGlMgrRemote bean = (WebInsGlMgrRemote) ctx.lookup("WebInsGlMgr/remote");
    return bean;
	}
	
	public static ClientMgr getClientMgr() throws Exception
	{		   
    ClientMgr mgr= ClientEJBHelper.getClientMgr();
    return mgr;
	}

	
}
