package com.ract.web.insurance;

public class Company 
{
	private String companyName;      
	private String address1;       
	private String address2;       
	private String companyPhone;   
	private String abn;            
	private String servicePhoneNo; 
	private String postalAddress;  
	private String phonePayNumber; 
	private String AFSNumber;      
	private String webPhoneNo;
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCompanyPhone() {
		return companyPhone;
	}
	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}
	public String getAbn() {
		return abn;
	}
	public void setAbn(String abn) {
		this.abn = abn;
	}
	public String getServicePhoneNo() {
		return servicePhoneNo;
	}
	public void setServicePhoneNo(String servicePhoneNo) {
		this.servicePhoneNo = servicePhoneNo;
	}
	public String getPostalAddress() {
		return postalAddress;
	}
	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}
	public String getPhonePayNumber() {
		return phonePayNumber;
	}
	public void setPhonePayNumber(String phonePayNumber) {
		this.phonePayNumber = phonePayNumber;
	}
	public String getAFSNumber() {
		return AFSNumber;
	}
	public void setAFSNumber(String number) {
		AFSNumber = number;
	}
	public String getWebPhoneNo() {
		return webPhoneNo;
	}
	public void setWebPhoneNo(String webPhoneNo) {
		this.webPhoneNo = webPhoneNo;
	}     

}
