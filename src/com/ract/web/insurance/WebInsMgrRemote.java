package com.ract.web.insurance;

import javax.ejb.Remote;

import com.ract.common.GenericException;
import com.ract.common.SystemDataException;
import com.ract.insurance.Policy;
import com.ract.util.DateTime;
import com.ract.web.common.*;
import com.ract.web.agent.*;

import java.rmi.RemoteException;
import java.util.*;

@Remote
public interface WebInsMgrRemote {

	public Integer startQuote(DateTime quoteDate,
                              String quoteType)throws Exception;

	public WebInsQuote getQuote(Integer quoteNo);

	public void setQuoteStatus(Integer quoteNo,String status);
	
	public void setQuoteDetail(Integer quoteNo,
                               String fieldName,
                               String fieldValue);

	public String getQuoteDetail(Integer quoteNo,
                               String fieldName);
	
	public WebInsClient setFromRACTClient(Integer quoteNo,
			                              String cardNo,
			                              DateTime birthDate);

	public Integer getGlVehicleValue(Integer quoteNo);

	public WebInsClient createInsuranceClient(Integer webQuoteNo)throws Exception;

	public WebInsClient getWebInsClient(Integer webClientNo)throws Exception;

	public void setClient(WebInsClient clt)throws Exception;

	public ArrayList<WebInsClient> getClients(Integer webQuoteNo)throws Exception;

	public void removeClientDetail(WebInsClientDetail det)throws Exception;

	public void removeClientDetail(Integer clientNo, Integer detailSeq)throws Exception;

    public void removeAllClientDetails(Integer clientNo)throws Exception;

	public void removeClient(Integer clientNo)throws Exception;

	public ArrayList<WebInsClientDetail> getClientDetails(Integer clientNo)throws Exception;

    public String listClientDetails(Integer clientNo);
    

	public void setClientDetail(WebInsClientDetail det)throws Exception;

	public ArrayList<WebInsQuoteDetail> getAllQuoteDetails(Integer quoteNo)throws Exception;


    public Integer validateSecuredQuoteNo(String securedQuoteNo)throws Exception;

	public void setQuoteDate(Integer quoteNo, DateTime qDate)throws Exception;


	public void removeQuoteDetail(Integer quoteNo,String fieldName)throws Exception;


    public Premium getPremium(Integer webQuoteNo,
            String paymentFrequency,
            String coverType)throws Exception;


    public WebInsClient getRatingDriver(Integer quoteNo)throws Exception;
   

    public Integer calculateNCD(WebInsClient clt)throws Exception;
    

    public Boolean checkFiftyPlus(Integer quoteNo)throws Exception;
    

    public void printCoverDoc(Integer quoteNo)throws Exception;


    public Integer getBasicXs(String coverType, DateTime cDate)throws Exception;

  	public Collection<Policy> getClientPolicies(Integer clientNumber, boolean loadRisks, boolean includeCancelled, boolean includeRenewal) throws GenericException;    

    public void startCover(Integer quoteNo, String cardType, String bsb, String accountNumber, String cardExpiry, String accountName, String paymentDay, String paymentFrequency) throws Exception;
    
	public void setCoverType(Integer quoteNo, String coverType)throws Exception;
//	public void setQuote(WebInsQuote quote)throws Exception;
	public void setStartCoverDate(Integer quoteNo, DateTime coverDate)throws Exception;

	public ArrayList<WebInsClient> getClientGroups(Integer quoteNo)throws Exception;

	public void useExistingGroup(Integer quoteNo, Integer ractclientNo)throws Exception;


	public ArrayList<WebInsQuote> findQuotesByStatus(String status)throws Exception;
	

	public ArrayList<WebInsQuote> findQuotesByStatus(String[] statusList)throws Exception;
	public ArrayList<RefType> getRefList(String refType, DateTime effDate)throws Exception;
	public GnSetting getGnSetting(String tableName,String tCode )throws Exception;
	public ArrayList<WebInsQuote> findQuotesBySalesBranch(String salesBranch)throws Exception;
	public String allocateSalesBranch()throws Exception;
	public String allocateSalesBranch(String agentCode)throws Exception;

	public ArrayList<InRiskSi> getSpecifiedItems(Integer webQuoteNo)throws Exception;
	public InRiskSiLineItem getSiLineItem(Integer quoteNo,
            Integer seqNo,
            Integer lineNo)throws Exception;
	public ArrayList<InRiskSiLineItem> getAllSiLineItems(Integer quoteNo,
            Integer seqNo)throws Exception;
	public InRiskSi getSpecifiedItem(Integer webQuoteNo,
            Integer siSeqNo)throws Exception;
	public void setSpecifiedItem(InRiskSi item)throws Exception;
	public void setAllSiLineItems(InRiskSi item)throws Exception;
	public void setSiLineItem(InRiskSiLineItem lineItem)throws Exception;
	public void setSpecifiedItems(ArrayList<InRiskSi> itemList)throws Exception;
	public void removeSpecifiedItem(Integer webQuoteNo,
            Integer siSeqNo)throws Exception;
	public void removeAllSiLineItems(InRiskSi si);
	public ArrayList<InRfDet> getLookupData(String product,
            String riskType,
            String fieldName,
            DateTime effDate)throws Exception;
	public void setQuoteDetail(WebInsQuoteDetail det)throws Exception;
	public String getInsSetting(String tName,
                                String tKey,
                                DateTime effDate)throws Exception;
	public WebInsQuoteDetail getDetail(Integer quoteNo,String fieldName);
	public void removeAllSpecifiedItems(Integer quoteNo)throws Exception;
	
	public WebAgentStatus validateAgent( String agentCode, 
			                             String agentPassword)	throws Exception;
	public ArrayList<ListPair> getFixedExcesses(String prodType,
			                          String riskType,
			                          DateTime effDate)throws Exception;

	 public List<WebInsClient> checkClients(Integer quoteNo, String coverType)throws Exception;
	 
	 public String getNotionalProdType(Integer clientNo,
                String fiftyPlus,
                String  riskType,
                DateTime effDate)throws Exception;
	 
	 public String getMessageText(String category, String type, String subType, String prodType, String coverType, DateTime startDate) throws Exception;
	 
	 public Collection<WebPayable> getConvertedTransactions(DateTime since) throws RemoteException;
	 
	 public Collection<WebPayable> getUnreceiptedPayables(DateTime since) throws RemoteException, SystemDataException;
	 
	 public Float getMinVehicleValuePercentage(DateTime queryDate, int vehicleAge) throws RemoteException;
	 
	 public Float getMaxVehicleValuePercentage(DateTime queryDate, int vehicleAge) throws RemoteException;
}
