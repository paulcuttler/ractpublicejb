package com.ract.web.insurance;

import com.ract.common.CommonConstants;
import com.ract.common.proxy.ProgressProxy;

public 	class 		InsProxy 
		extends 	WebInsuranceProxy 
		implements	ProgressProxy 
{
	public InsProxy()throws Exception
	{
		   super(CommonConstants.getProgressAppServerURL(),  
				 CommonConstants.getProgressAppserverUser(),
			     CommonConstants.getProgressAppserverPassword(),
			     null);
	}
}
