package com.ract.web.insurance;

import javax.persistence.*;
import java.io.*;
@Embeddable
public class WebInsQuoteDetailPK implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6727472385834667536L;
	public Integer webQuoteNo;
	public String  fieldName = "";
//	public String  stringKey = "";
	
	public WebInsQuoteDetailPK(){}
	
//	public WebInsQuoteDetailPK(Integer webQuoteNo, String fieldName, String stringKey) {
//		this.webQuoteNo = webQuoteNo;
//		this.fieldName = fieldName;
//		this.stringKey = stringKey;
//	}

	
	public WebInsQuoteDetailPK(Integer webQuoteNo, String fieldName) {
		this.webQuoteNo = webQuoteNo;
		this.fieldName = fieldName;
	}
	public Integer getWebQuoteNo() {
		return webQuoteNo;
	}
	public void setWebQuoteNo(Integer webQuoteNo) {
		this.webQuoteNo = webQuoteNo;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public boolean equals(Object obj)
	{
		WebInsQuoteDetailPK pk = (WebInsQuoteDetailPK)obj;
		return pk.webQuoteNo.equals(this.webQuoteNo)
		       && pk.fieldName.equals(this.fieldName);
	}
	public int hashCode()
	{
	    return this.fieldName.hashCode() + this.webQuoteNo
	            /*+ this.stringKey.hashCode() */ ;
	}
//	public String getStringKey()
//	{ 
//		return this.stringKey;
//	}
//    public void setStringKey(String key)
//    { 
//    	this.stringKey = key;
//    }
	

}
