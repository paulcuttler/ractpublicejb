package com.ract.web.payment;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Membership version of a WebPayment.
 * @author hollidayj
 *
 */

@Entity
@DiscriminatorValue("MEM")
public class WebMembershipPayment extends WebPayment
{
  public WebMembershipPayment()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4888787163080844258L;

	public WebMembershipPayment(String transactionReference, String cardHolder, String cardType, String cardNumber, String cvv, String expiryDate, String paymentType)
  {
  	super(transactionReference, TRANSACTION_TYPE_MEMBERSHIP, cardHolder, cardType, cardNumber, cvv, expiryDate, paymentType);
  }
	
	public WebMembershipPayment(String transactionReference, String bsb, String accountName, String accountNumber, String paymentType) {
		super(transactionReference, TRANSACTION_TYPE_MEMBERSHIP, bsb, accountName, accountNumber, paymentType);
	}
	
	public WebMembershipPayment(String transactionReference, String paymentType) {
		super(transactionReference, TRANSACTION_TYPE_MEMBERSHIP, paymentType);
	}
}
