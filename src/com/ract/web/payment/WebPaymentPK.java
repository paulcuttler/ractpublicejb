package com.ract.web.payment;

import java.io.Serializable;

import javax.persistence.Embeddable;

/**
 * The primary key for a WebPayment.
 * @author hollidayj
 */
@Embeddable
public class WebPaymentPK implements Serializable
{
	
	private static final long serialVersionUID = -675134515887025379L;

	public WebPaymentPK(String transactionType, String transactionReference)
	{
		this.setTransactionType(transactionType);
		this.setTransactionReference(transactionReference);
	}
	
	/*
	 * typically the transaction header id
	 */
	private String transactionReference;

	private String transactionType;
	
	public String getTransactionType()
	{
		return transactionType;
	}

	public void setTransactionType(String transactionType)
	{
		this.transactionType = transactionType;
	}

	public String getTransactionReference()
	{
		return transactionReference;
	}

	public void setTransactionReference(String transactionReference)
	{
		this.transactionReference = transactionReference;
	}	

	public WebPaymentPK()
	{
		//default
	}
	
  public boolean equals(Object obj)
  {
    if(obj instanceof WebPaymentPK)
    {
    	WebPaymentPK that = (WebPaymentPK)obj;
      return this.transactionType.equals(that.transactionType) && this.transactionReference.equals(that.transactionReference);
    }
    return false;
  }

  public int hashCode()
  {
    return this.transactionType.hashCode() + this.transactionReference.hashCode();
  }

  public String toString()
  {
    return this.transactionType + "/" + transactionReference;
  }
	
}
