package com.ract.web.payment;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.Parameter;
import org.jasypt.hibernate3.type.EncryptedStringType;

import com.ract.common.SourceSystem;
import com.ract.util.FileUtil;

/**
 * A web payment containing all payment details entered by a customer.
 * @author hollidayj
 */

@Entity
@DiscriminatorFormula("transactionType")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@TypeDef(
	    name="encryptedString", 
	    typeClass=EncryptedStringType.class, 
	    parameters = {
	    	@Parameter(name="encryptorRegisteredName", value="ractStringEncryptor")
	    }
	)
public abstract class WebPayment implements Serializable
{
	
	public static final String TRANSACTION_TYPE_MEMBERSHIP = SourceSystem.MEMBERSHIP.getAbbreviation();
	public static final String TRANSACTION_TYPE_INSURANCE = SourceSystem.INSURANCE.getAbbreviation();	
	
	public WebPayment()
	{
		
	}
	
	public WebPayment(String transactionReference, String transactionType, String cardHolder, String cardType, String cardNumber, String cvv, String expiryDate, String paymentType)
	{
		this.setWebPaymentPK(new WebPaymentPK(transactionType,transactionReference));
		this.setCardHolder(cardHolder);
		this.setCardType(cardType);
		this.setCardNumber(cardNumber);
		this.setCVV(cvv);
		this.setExpiryDate(expiryDate);
		this.setPaymentType(paymentType);
	}
	
	public WebPayment(String transactionReference, String transactionType, String bsb, String accountName, String accountNumber, String paymentType) 
	{
		this.setWebPaymentPK(new WebPaymentPK(transactionType,transactionReference));
		this.setBsb(bsb);
		this.setAccountName(accountName);
		this.setAccountNumber(accountNumber);
		this.setPaymentType(paymentType);
	}
	
	public WebPayment(String transactionReference, String transactionType, String paymentType) 
	{
		this.setWebPaymentPK(new WebPaymentPK(transactionType,transactionReference));
		this.setPaymentType(paymentType);
	}

	@Id
	private WebPaymentPK webPaymentPK;
	
	// Jasypt encrypted card number - encrypted as a string
	@Type(type="encryptedString")
	private String cardNumber;
	
	private String expiryDate;
	
	private String cardType;
	
	private String cardHolder;
	
	private String CVV;
	
	private String accountName;
	
	private String accountNumber;
	
	private String bsb;
	
	private String paymentType;
	
	public String getCardNumber()
	{
		return cardNumber;
	}

	public void setCardNumber(String cardNumber)
	{
		this.cardNumber = cardNumber;
	}

	public String getExpiryDate()
	{
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate)
	{
		this.expiryDate = expiryDate;
	}

	public String getCardType()
	{
		return cardType;
	}

	public void setCardType(String cardType)
	{
		this.cardType = cardType;
	}

	public String getCardHolder()
	{
		return cardHolder;
	}

	public void setCardHolder(String cardHolder)
	{
		this.cardHolder = cardHolder;
	}

	public String getCVV()
	{
		return CVV;
	}

	public void setCVV(String cvv)
	{
		CVV = cvv;
	}

	public WebPaymentPK getWebPaymentPK()
	{
		return webPaymentPK;
	}

	public void setWebPaymentPK(WebPaymentPK webPaymentPK)
	{
		this.webPaymentPK = webPaymentPK;
	}
	
	  /** 
	   * @param paymentType
	   */
	  public void setPaymentType(String paymentType) 
	  {
		  this.paymentType = paymentType;
	  }
	  
	  /**
	   * Payment type - the kind of payment details provided.
	   * @return
	   */
	  public String getPaymentType() 
	  {
		 return paymentType;
	  }
	  
	  /**
	   * @param bsb
	   */
	  public void setBsb(String bsb) 
	  {
		  this.bsb = bsb;
	  }
	  
	  /**
	   * BSB - the bank identifier
	   * @return
	   */
	  public String getBsb() 
	  {
		 return bsb;
	  }
	  
	  /**
	   * @param accountName
	   */
	  public void setAccountName(String accountName) 
	  {
		  this.accountName = accountName;
	  }
	  
	  /**
	   * Account Name 
	   * @return
	   */
	  public String getAccountName() 
	  {
		 return accountName;
	  }
	  
	  /**
	   * 
	   * @param accountNumber
	   */
	  public void setAccountNumber(String accountNumber) 
	  {
		  this.accountNumber = accountNumber;
	  }
	  
	  /**
	   * 
	   * @return
	   */
	  public String getAccountNumber() 
	  {
		 return accountNumber;
	  }

	public String toString()
	{
		StringBuffer desc = new StringBuffer();
		desc.append(webPaymentPK);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(cardHolder);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(cardType);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(cardNumber);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(CVV);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(expiryDate);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(paymentType);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(bsb);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(accountName);
		desc.append(FileUtil.SEPARATOR_COMMA);
		desc.append(accountNumber);
		return desc.toString();
	}	
	
	
}
