package com.ract.web.payment;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Insurance version of a WebPayment.
 * @author hollidayj
 */

@Entity
@DiscriminatorValue("INS")
public class WebInsurancePayment extends WebPayment
{
  /**
	 * 
	 */
	private static final long serialVersionUID = -8258504527207822873L;

	public WebInsurancePayment(String transactionReference)
  {
  	this.setWebPaymentPK(new WebPaymentPK(WebPayment.TRANSACTION_TYPE_INSURANCE,transactionReference));
  }
}
