package com.ract.web.agent;

import java.io.Serializable;

public 	class 		WebAgentStatus 
		implements 	Serializable
{
	
	/**
     * 
     */
    private static final long serialVersionUID = 7028199914719241670L;

	private String		agentStatusMessage;
	
	private Integer		agentId;
	
	private Integer		agentStatusCode;

	private String		agentName;
	
	/**
	 * @param agentStatusMessage
	 * @param agentId
	 * @param agentStatusCode
	 */
	public WebAgentStatus	(	String 	agentStatusMessage, 
								Integer agentId,
								Integer agentStatusCode
							) 
	{
		this.agentStatusMessage = agentStatusMessage;
		this.agentId = agentId;
		this.agentStatusCode = agentStatusCode;
	}

	public WebAgentStatus() 
	{
		
	}

	public String getAgentStatusMessage() 
	{
		return agentStatusMessage;
	}

	public void setAgentStatusMessage(String agentStatusMessage) 
	{
		this.agentStatusMessage = agentStatusMessage;
	}

	public Integer getAgentId() 
	{
		return agentId;
	}

	public void setAgentId(Integer agentId) 
	{
		this.agentId = agentId;
	}

	public Integer getAgentStatusCode() 
	{
		return agentStatusCode;
	}

	public void setAgentStatusCode(Integer agentStatusCode) 
	{
		this.agentStatusCode = agentStatusCode;
	}

	public String getAgentName()
    {
    	return agentName;
    }

	public void setAgentName(String agentName)
    {
    	this.agentName = agentName;
    }
	
	
}
