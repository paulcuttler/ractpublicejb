package com.ract.motorwebclient.webservice;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the com.ract.motorwebclient.webservice package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content. The Java representation of XML content can consist of schema derived interfaces and classes representing the binding of schema type definitions, element declarations and model groups. Factory methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetNvicByPlateInputRequest_QNAME = new QName("http://www.ract.com.au/MotorWebClient", "request");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _GetNvicByPlateResponse_QNAME = new QName("http://www.ract.com.au/MotorWebClient", "GetNvicByPlateResponse");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _GetNvicByPlateRequest_QNAME = new QName("http://www.ract.com.au/MotorWebClient", "GetNvicByPlateRequest");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _TestConnectivityRequest_QNAME = new QName("http://www.ract.com.au/MotorWebClient", "TestConnectivityRequest");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _TestConnectivityResponse_QNAME = new QName("http://www.ract.com.au/MotorWebClient", "TestConnectivityResponse");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _RactError_QNAME = new QName("http://www.ract.com.au/MotorWebClient", "RactError");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _State_QNAME = new QName("http://www.ract.com.au/MotorWebClient", "State");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _GetNvicByPlateInputResponseGetNvicByPlateInputResult_QNAME = new QName("http://www.ract.com.au/MotorWebClient", "GetNvicByPlateInputResult");
    private final static QName _GetNvicByPlateResponseNvic_QNAME = new QName("http://www.ract.com.au/MotorWebClient", "nvic");
    private final static QName _RactErrorAdditionalInformation_QNAME = new QName("http://www.ract.com.au/MotorWebClient", "additionalInformation");
    private final static QName _TestConnectivityInputResponseTestConnectivityInputResult_QNAME = new QName("http://www.ract.com.au/MotorWebClient", "TestConnectivityInputResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ract.motorwebclient.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetNvicByPlateInput }
     * 
     */
    public GetNvicByPlateInput createGetNvicByPlateInput() {
        return new GetNvicByPlateInput();
    }

    /**
     * Create an instance of {@link GetNvicByPlateInputResponse }
     * 
     */
    public GetNvicByPlateInputResponse createGetNvicByPlateInputResponse() {
        return new GetNvicByPlateInputResponse();
    }

    /**
     * Create an instance of {@link GetNvicByPlateRequest }
     * 
     */
    public GetNvicByPlateRequest createGetNvicByPlateRequest() {
        return new GetNvicByPlateRequest();
    }

    /**
     * Create an instance of {@link GetNvicByPlateResponse }
     * 
     */
    public GetNvicByPlateResponse createGetNvicByPlateResponse() {
        return new GetNvicByPlateResponse();
    }

    /**
     * Create an instance of {@link RactError }
     * 
     */
    public RactError createRactError() {
        return new RactError();
    }

    /**
     * Create an instance of {@link TestConnectivityResponse }
     * 
     */
    public TestConnectivityResponse createTestConnectivityResponse() {
        return new TestConnectivityResponse();
    }

    /**
     * Create an instance of {@link TestConnectivityInputResponse }
     * 
     */
    public TestConnectivityInputResponse createTestConnectivityInputResponse() {
        return new TestConnectivityInputResponse();
    }

    /**
     * Create an instance of {@link TestConnectivityRequest }
     * 
     */
    public TestConnectivityRequest createTestConnectivityRequest() {
        return new TestConnectivityRequest();
    }

    /**
     * Create an instance of {@link TestConnectivityInput }
     * 
     */
    public TestConnectivityInput createTestConnectivityInput() {
        return new TestConnectivityInput();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNvicByPlateRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/MotorWebClient", name = "request", scope = GetNvicByPlateInput.class)
    public JAXBElement<GetNvicByPlateRequest> createGetNvicByPlateInputRequest(GetNvicByPlateRequest value) {
        return new JAXBElement<GetNvicByPlateRequest>(_GetNvicByPlateInputRequest_QNAME, GetNvicByPlateRequest.class, GetNvicByPlateInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNvicByPlateResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/MotorWebClient", name = "GetNvicByPlateResponse")
    public JAXBElement<GetNvicByPlateResponse> createGetNvicByPlateResponse(GetNvicByPlateResponse value) {
        return new JAXBElement<GetNvicByPlateResponse>(_GetNvicByPlateResponse_QNAME, GetNvicByPlateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNvicByPlateRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/MotorWebClient", name = "GetNvicByPlateRequest")
    public JAXBElement<GetNvicByPlateRequest> createGetNvicByPlateRequest(GetNvicByPlateRequest value) {
        return new JAXBElement<GetNvicByPlateRequest>(_GetNvicByPlateRequest_QNAME, GetNvicByPlateRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestConnectivityRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/MotorWebClient", name = "TestConnectivityRequest")
    public JAXBElement<TestConnectivityRequest> createTestConnectivityRequest(TestConnectivityRequest value) {
        return new JAXBElement<TestConnectivityRequest>(_TestConnectivityRequest_QNAME, TestConnectivityRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestConnectivityResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/MotorWebClient", name = "TestConnectivityResponse")
    public JAXBElement<TestConnectivityResponse> createTestConnectivityResponse(TestConnectivityResponse value) {
        return new JAXBElement<TestConnectivityResponse>(_TestConnectivityResponse_QNAME, TestConnectivityResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RactError }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/MotorWebClient", name = "RactError")
    public JAXBElement<RactError> createRactError(RactError value) {
        return new JAXBElement<RactError>(_RactError_QNAME, RactError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link State }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/MotorWebClient", name = "State")
    public JAXBElement<State> createState(State value) {
        return new JAXBElement<State>(_State_QNAME, State.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNvicByPlateResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/MotorWebClient", name = "GetNvicByPlateInputResult", scope = GetNvicByPlateInputResponse.class)
    public JAXBElement<GetNvicByPlateResponse> createGetNvicByPlateInputResponseGetNvicByPlateInputResult(GetNvicByPlateResponse value) {
        return new JAXBElement<GetNvicByPlateResponse>(_GetNvicByPlateInputResponseGetNvicByPlateInputResult_QNAME, GetNvicByPlateResponse.class, GetNvicByPlateInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/MotorWebClient", name = "nvic", scope = GetNvicByPlateResponse.class)
    public JAXBElement<String> createGetNvicByPlateResponseNvic(String value) {
        return new JAXBElement<String>(_GetNvicByPlateResponseNvic_QNAME, String.class, GetNvicByPlateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/MotorWebClient", name = "additionalInformation", scope = RactError.class)
    public JAXBElement<String> createRactErrorAdditionalInformation(String value) {
        return new JAXBElement<String>(_RactErrorAdditionalInformation_QNAME, String.class, RactError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestConnectivityResponse }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/MotorWebClient", name = "TestConnectivityInputResult", scope = TestConnectivityInputResponse.class)
    public JAXBElement<TestConnectivityResponse> createTestConnectivityInputResponseTestConnectivityInputResult(TestConnectivityResponse value) {
        return new JAXBElement<TestConnectivityResponse>(_TestConnectivityInputResponseTestConnectivityInputResult_QNAME, TestConnectivityResponse.class, TestConnectivityInputResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestConnectivityRequest }{@code >}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ract.com.au/MotorWebClient", name = "request", scope = TestConnectivityInput.class)
    public JAXBElement<TestConnectivityRequest> createTestConnectivityInputRequest(TestConnectivityRequest value) {
        return new JAXBElement<TestConnectivityRequest>(_GetNvicByPlateInputRequest_QNAME, TestConnectivityRequest.class, TestConnectivityInput.class, value);
    }

}
