package com.ract.motorwebclient.webservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.1.3-hudson-390- Generated source version: 2.0
 * 
 */
@WebService(name = "MotorWebClient", targetNamespace = "http://www.ract.com.au/MotorWebClient")
public interface MotorWebClient {

    /**
     * 
     * @param request
     * @return returns com.ract.motorwebclient.webservice.GetNvicByPlateResponse
     * @throws MotorWebClientGetNvicByPlateInputRactErrorFaultMessage
     */
    @WebMethod(operationName = "GetNvicByPlateInput", action = "http://www.ract.com.au/MotorWebClient/MotorWebClient/GetNvicByPlateInput")
    @WebResult(name = "GetNvicByPlateInputResult", targetNamespace = "http://www.ract.com.au/MotorWebClient")
    @RequestWrapper(localName = "GetNvicByPlateInput", targetNamespace = "http://www.ract.com.au/MotorWebClient", className = "com.ract.motorwebclient.webservice.GetNvicByPlateInput")
    @ResponseWrapper(localName = "GetNvicByPlateInputResponse", targetNamespace = "http://www.ract.com.au/MotorWebClient", className = "com.ract.motorwebclient.webservice.GetNvicByPlateInputResponse")
    public GetNvicByPlateResponse getNvicByPlateInput(@WebParam(name = "request", targetNamespace = "http://www.ract.com.au/MotorWebClient") GetNvicByPlateRequest request) throws MotorWebClientGetNvicByPlateInputRactErrorFaultMessage;

    /**
     * 
     * @param request
     * @return returns com.ract.motorwebclient.webservice.TestConnectivityResponse
     * @throws MotorWebClientTestConnectivityInputRactErrorFaultMessage
     */
    @WebMethod(operationName = "TestConnectivityInput", action = "http://www.ract.com.au/MotorWebClient/MotorWebClient/TestConnectivityInput")
    @WebResult(name = "TestConnectivityInputResult", targetNamespace = "http://www.ract.com.au/MotorWebClient")
    @RequestWrapper(localName = "TestConnectivityInput", targetNamespace = "http://www.ract.com.au/MotorWebClient", className = "com.ract.motorwebclient.webservice.TestConnectivityInput")
    @ResponseWrapper(localName = "TestConnectivityInputResponse", targetNamespace = "http://www.ract.com.au/MotorWebClient", className = "com.ract.motorwebclient.webservice.TestConnectivityInputResponse")
    public TestConnectivityResponse testConnectivityInput(@WebParam(name = "request", targetNamespace = "http://www.ract.com.au/MotorWebClient") TestConnectivityRequest request) throws MotorWebClientTestConnectivityInputRactErrorFaultMessage;

}
