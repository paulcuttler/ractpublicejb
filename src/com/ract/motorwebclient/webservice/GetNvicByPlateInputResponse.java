package com.ract.motorwebclient.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetNvicByPlateInputResult" type="{http://www.ract.com.au/MotorWebClient}GetNvicByPlateResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getNvicByPlateInputResult" })
@XmlRootElement(name = "GetNvicByPlateInputResponse")
public class GetNvicByPlateInputResponse {

    @XmlElementRef(name = "GetNvicByPlateInputResult", namespace = "http://www.ract.com.au/MotorWebClient", type = JAXBElement.class)
    protected JAXBElement<GetNvicByPlateResponse> getNvicByPlateInputResult;

    /**
     * Gets the value of the getNvicByPlateInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link GetNvicByPlateResponse }{@code >}
     * 
     */
    public JAXBElement<GetNvicByPlateResponse> getGetNvicByPlateInputResult() {
        return getNvicByPlateInputResult;
    }

    /**
     * Sets the value of the getNvicByPlateInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link GetNvicByPlateResponse }{@code >}
     * 
     */
    public void setGetNvicByPlateInputResult(JAXBElement<GetNvicByPlateResponse> value) {
        this.getNvicByPlateInputResult = ((JAXBElement<GetNvicByPlateResponse>) value);
    }

}
