package com.ract.motorwebclient.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TestConnectivityInputResult" type="{http://www.ract.com.au/MotorWebClient}TestConnectivityResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "testConnectivityInputResult" })
@XmlRootElement(name = "TestConnectivityInputResponse")
public class TestConnectivityInputResponse {

    @XmlElementRef(name = "TestConnectivityInputResult", namespace = "http://www.ract.com.au/MotorWebClient", type = JAXBElement.class)
    protected JAXBElement<TestConnectivityResponse> testConnectivityInputResult;

    /**
     * Gets the value of the testConnectivityInputResult property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link TestConnectivityResponse }{@code >}
     * 
     */
    public JAXBElement<TestConnectivityResponse> getTestConnectivityInputResult() {
        return testConnectivityInputResult;
    }

    /**
     * Sets the value of the testConnectivityInputResult property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link TestConnectivityResponse }{@code >}
     * 
     */
    public void setTestConnectivityInputResult(JAXBElement<TestConnectivityResponse> value) {
        this.testConnectivityInputResult = ((JAXBElement<TestConnectivityResponse>) value);
    }

}
