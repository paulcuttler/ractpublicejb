package com.ract.motorwebclient.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TestConnectivityRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TestConnectivityRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="logResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TestConnectivityRequest", propOrder = { "logResult" })
public class TestConnectivityRequest {

    protected boolean logResult;

    /**
     * Gets the value of the logResult property.
     * 
     */
    public boolean isLogResult() {
        return logResult;
    }

    /**
     * Sets the value of the logResult property.
     * 
     */
    public void setLogResult(boolean value) {
        this.logResult = value;
    }

}
