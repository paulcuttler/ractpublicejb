package com.insight4.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCardNumberFromPersonIdResult" type="{http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement}GetCardNumberFromPersonIdResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getCardNumberFromPersonIdResult" })
@XmlRootElement(name = "GetCardNumberFromPersonIdResponse")
public class GetCardNumberFromPersonIdResponse {

	@XmlElementRef(name = "GetCardNumberFromPersonIdResult", namespace = "http://tempuri.org/", type = JAXBElement.class)
	protected JAXBElement<GetCardNumberFromPersonIdResult> getCardNumberFromPersonIdResult;

	/**
	 * Gets the value of the getCardNumberFromPersonIdResult property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}
	 *         {@link GetCardNumberFromPersonIdResult }{@code >}
	 * 
	 */
	public JAXBElement<GetCardNumberFromPersonIdResult> getGetCardNumberFromPersonIdResult() {
		return getCardNumberFromPersonIdResult;
	}

	/**
	 * Sets the value of the getCardNumberFromPersonIdResult property.
	 * 
	 * @param value
	 *          allowed object is {@link JAXBElement }{@code <}
	 *          {@link GetCardNumberFromPersonIdResult }{@code >}
	 * 
	 */
	public void setGetCardNumberFromPersonIdResult(
			JAXBElement<GetCardNumberFromPersonIdResult> value) {
		this.getCardNumberFromPersonIdResult = ((JAXBElement<GetCardNumberFromPersonIdResult>) value);
	}

}
