package com.insight4.ws;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the com.insight4.ws package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	private final static QName _RemoveCardRequestResultErrorMessage_QNAME = new QName(
			"http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement",
			"ErrorMessage");
	private final static QName _AnyURI_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
	private final static QName _DateTime_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
	private final static QName _Char_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "char");
	private final static QName _QName_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "QName");
	private final static QName _UnsignedShort_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
	private final static QName _Float_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "float");
	private final static QName _Long_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "long");
	private final static QName _Short_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "short");
	private final static QName _Base64Binary_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
	private final static QName _Byte_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "byte");
	private final static QName _Boolean_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
	private final static QName _GetPersonIdFromCardNumberResult_QNAME = new QName(
			"http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement",
			"GetPersonIdFromCardNumberResult");
	private final static QName _GetCardNumberFromPersonIdResult_QNAME = new QName(
			"http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement",
			"GetCardNumberFromPersonIdResult");
	private final static QName _UnsignedByte_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
	private final static QName _AnyType_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
	private final static QName _UnsignedInt_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
	private final static QName _Int_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "int");
	private final static QName _RemoveCardRequestResult_QNAME = new QName(
			"http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement",
			"RemoveCardRequestResult");
	private final static QName _Decimal_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
	private final static QName _Double_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "double");
	private final static QName _Guid_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "guid");
	private final static QName _Duration_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "duration");
	private final static QName _String_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "string");
	private final static QName _AddCardRequestResult_QNAME = new QName(
			"http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement",
			"AddCardRequestResult");
	private final static QName _UnsignedLong_QNAME = new QName(
			"http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
	private final static QName _GetCardNumberFromPersonIdResultCardNumber_QNAME = new QName(
			"http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement",
			"CardNumber");
	private final static QName _GetCardNumberFromPersonIdResponseGetCardNumberFromPersonIdResult_QNAME = new QName(
			"http://tempuri.org/", "GetCardNumberFromPersonIdResult");
	private final static QName _GetPersonIdFromCardNumberCardNumber_QNAME = new QName(
			"http://tempuri.org/", "cardNumber");
	private final static QName _GenerateCardNumberResponseGenerateCardNumberResult_QNAME = new QName(
			"http://tempuri.org/", "GenerateCardNumberResult");
	private final static QName _GetPersonIdFromCardNumberResponseGetPersonIdFromCardNumberResult_QNAME = new QName(
			"http://tempuri.org/", "GetPersonIdFromCardNumberResult");
	private final static QName _AddCardRequestFromPersonFormResponseAddCardRequestFromPersonFormResult_QNAME = new QName(
			"http://tempuri.org/", "AddCardRequestFromPersonFormResult");
	private final static QName _AddCardRequestFromPersonFormRequestingUser_QNAME = new QName(
			"http://tempuri.org/", "RequestingUser");
	private final static QName _AddCardRequestFromPersonFormPersonId_QNAME = new QName(
			"http://tempuri.org/", "PersonId");
	private final static QName _AddCardRequestFromPersonFormTimeStamp_QNAME = new QName(
			"http://tempuri.org/", "TimeStamp");
	private final static QName _AddCardRequestFromPersonFormNewCardRequest_QNAME = new QName(
			"http://tempuri.org/", "NewCardRequest");
	private final static QName _GenerateCardNumberRactNumber_QNAME = new QName(
			"http://tempuri.org/", "ractNumber");
	private final static QName _GenerateCardNumberIssuanceNumber_QNAME = new QName(
			"http://tempuri.org/", "issuanceNumber");
	private final static QName _GenerateCardNumberMembershipNumber_QNAME = new QName(
			"http://tempuri.org/", "membershipNumber");
	private final static QName _GenerateCardNumberProductidentifier_QNAME = new QName(
			"http://tempuri.org/", "productidentifier");
	private final static QName _RemoveCardRequestRoadsideId_QNAME = new QName(
			"http://tempuri.org/", "RoadsideId");
	private final static QName _AddCardRequestProductIdentifier_QNAME = new QName(
			"http://tempuri.org/", "ProductIdentifier");
	private final static QName _AddCardRequestReasonCode_QNAME = new QName(
			"http://tempuri.org/", "ReasonCode");
	private final static QName _AddCardRequestCarRegistration_QNAME = new QName(
			"http://tempuri.org/", "CarRegistration");
	private final static QName _AddCardRequestResponseAddCardRequestResult_QNAME = new QName(
			"http://tempuri.org/", "AddCardRequestResult");
	private final static QName _RemoveCardRequestResponseRemoveCardRequestResult_QNAME = new QName(
			"http://tempuri.org/", "RemoveCardRequestResult");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: com.insight4.ws
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link RemoveCardRequestResult }
	 * 
	 */
	public RemoveCardRequestResult createRemoveCardRequestResult() {
		return new RemoveCardRequestResult();
	}

	/**
	 * Create an instance of {@link GetCardNumberFromPersonIdResult }
	 * 
	 */
	public GetCardNumberFromPersonIdResult createGetCardNumberFromPersonIdResult() {
		return new GetCardNumberFromPersonIdResult();
	}

	/**
	 * Create an instance of {@link GetCardNumberFromPersonIdResponse }
	 * 
	 */
	public GetCardNumberFromPersonIdResponse createGetCardNumberFromPersonIdResponse() {
		return new GetCardNumberFromPersonIdResponse();
	}

	/**
	 * Create an instance of {@link GetCardNumberFromPersonId }
	 * 
	 */
	public GetCardNumberFromPersonId createGetCardNumberFromPersonId() {
		return new GetCardNumberFromPersonId();
	}

	/**
	 * Create an instance of {@link DoWork }
	 * 
	 */
	public DoWork createDoWork() {
		return new DoWork();
	}

	/**
	 * Create an instance of {@link GetPersonIdFromCardNumber }
	 * 
	 */
	public GetPersonIdFromCardNumber createGetPersonIdFromCardNumber() {
		return new GetPersonIdFromCardNumber();
	}

	/**
	 * Create an instance of {@link GetPersonIdFromCardNumberResponse }
	 * 
	 */
	public GetPersonIdFromCardNumberResponse createGetPersonIdFromCardNumberResponse() {
		return new GetPersonIdFromCardNumberResponse();
	}

	/**
	 * Create an instance of {@link GenerateCardNumberResponse }
	 * 
	 */
	public GenerateCardNumberResponse createGenerateCardNumberResponse() {
		return new GenerateCardNumberResponse();
	}

	/**
	 * Create an instance of {@link AddCardRequestFromPersonFormResponse }
	 * 
	 */
	public AddCardRequestFromPersonFormResponse createAddCardRequestFromPersonFormResponse() {
		return new AddCardRequestFromPersonFormResponse();
	}

	/**
	 * Create an instance of {@link GetPersonIdFromCardNumberResult }
	 * 
	 */
	public GetPersonIdFromCardNumberResult createGetPersonIdFromCardNumberResult() {
		return new GetPersonIdFromCardNumberResult();
	}

	/**
	 * Create an instance of {@link AddCardRequestFromPersonForm }
	 * 
	 */
	public AddCardRequestFromPersonForm createAddCardRequestFromPersonForm() {
		return new AddCardRequestFromPersonForm();
	}

	/**
	 * Create an instance of {@link GenerateCardNumber }
	 * 
	 */
	public GenerateCardNumber createGenerateCardNumber() {
		return new GenerateCardNumber();
	}

	/**
	 * Create an instance of {@link DoWorkResponse }
	 * 
	 */
	public DoWorkResponse createDoWorkResponse() {
		return new DoWorkResponse();
	}

	/**
	 * Create an instance of {@link AddCardRequestResult }
	 * 
	 */
	public AddCardRequestResult createAddCardRequestResult() {
		return new AddCardRequestResult();
	}

	/**
	 * Create an instance of {@link RemoveCardRequest }
	 * 
	 */
	public RemoveCardRequest createRemoveCardRequest() {
		return new RemoveCardRequest();
	}

	/**
	 * Create an instance of {@link AddCardRequestResponse }
	 * 
	 */
	public AddCardRequestResponse createAddCardRequestResponse() {
		return new AddCardRequestResponse();
	}

	/**
	 * Create an instance of {@link AddCardRequest }
	 * 
	 */
	public AddCardRequest createAddCardRequest() {
		return new AddCardRequest();
	}

	/**
	 * Create an instance of {@link RemoveCardRequestResponse }
	 * 
	 */
	public RemoveCardRequestResponse createRemoveCardRequestResponse() {
		return new RemoveCardRequestResponse();
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement", name = "ErrorMessage", scope = RemoveCardRequestResult.class)
	public JAXBElement<String> createRemoveCardRequestResultErrorMessage(
			String value) {
		return new JAXBElement<String>(_RemoveCardRequestResultErrorMessage_QNAME,
				String.class, RemoveCardRequestResult.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
	public JAXBElement<String> createAnyURI(String value) {
		return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}
	 * {@link XMLGregorianCalendar }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
	public JAXBElement<XMLGregorianCalendar> createDateTime(
			XMLGregorianCalendar value) {
		return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME,
				XMLGregorianCalendar.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
	public JAXBElement<Integer> createChar(Integer value) {
		return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
	public JAXBElement<QName> createQName(QName value) {
		return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
	public JAXBElement<Integer> createUnsignedShort(Integer value) {
		return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null,
				value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
	public JAXBElement<Float> createFloat(Float value) {
		return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
	public JAXBElement<Long> createLong(Long value) {
		return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
	public JAXBElement<Short> createShort(Short value) {
		return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
	public JAXBElement<byte[]> createBase64Binary(byte[] value) {
		return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null,
				((byte[]) value));
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
	public JAXBElement<Byte> createByte(Byte value) {
		return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
	public JAXBElement<Boolean> createBoolean(Boolean value) {
		return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}
	 * {@link GetPersonIdFromCardNumberResult }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement", name = "GetPersonIdFromCardNumberResult")
	public JAXBElement<GetPersonIdFromCardNumberResult> createGetPersonIdFromCardNumberResult(
			GetPersonIdFromCardNumberResult value) {
		return new JAXBElement<GetPersonIdFromCardNumberResult>(
				_GetPersonIdFromCardNumberResult_QNAME,
				GetPersonIdFromCardNumberResult.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}
	 * {@link GetCardNumberFromPersonIdResult }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement", name = "GetCardNumberFromPersonIdResult")
	public JAXBElement<GetCardNumberFromPersonIdResult> createGetCardNumberFromPersonIdResult(
			GetCardNumberFromPersonIdResult value) {
		return new JAXBElement<GetCardNumberFromPersonIdResult>(
				_GetCardNumberFromPersonIdResult_QNAME,
				GetCardNumberFromPersonIdResult.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
	public JAXBElement<Short> createUnsignedByte(Short value) {
		return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
	public JAXBElement<Object> createAnyType(Object value) {
		return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
	public JAXBElement<Long> createUnsignedInt(Long value) {
		return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
	public JAXBElement<Integer> createInt(Integer value) {
		return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}
	 * {@link RemoveCardRequestResult }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement", name = "RemoveCardRequestResult")
	public JAXBElement<RemoveCardRequestResult> createRemoveCardRequestResult(
			RemoveCardRequestResult value) {
		return new JAXBElement<RemoveCardRequestResult>(
				_RemoveCardRequestResult_QNAME, RemoveCardRequestResult.class, null,
				value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }
	 * {@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
	public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
		return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null,
				value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
	public JAXBElement<Double> createDouble(Double value) {
		return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
	public JAXBElement<String> createGuid(String value) {
		return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
	public JAXBElement<Duration> createDuration(Duration value) {
		return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null,
				value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
	public JAXBElement<String> createString(String value) {
		return new JAXBElement<String>(_String_QNAME, String.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}
	 * {@link AddCardRequestResult }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement", name = "AddCardRequestResult")
	public JAXBElement<AddCardRequestResult> createAddCardRequestResult(
			AddCardRequestResult value) {
		return new JAXBElement<AddCardRequestResult>(_AddCardRequestResult_QNAME,
				AddCardRequestResult.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }
	 * {@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
	public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
		return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class,
				null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement", name = "CardNumber", scope = GetCardNumberFromPersonIdResult.class)
	public JAXBElement<String> createGetCardNumberFromPersonIdResultCardNumber(
			String value) {
		return new JAXBElement<String>(
				_GetCardNumberFromPersonIdResultCardNumber_QNAME, String.class,
				GetCardNumberFromPersonIdResult.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement", name = "ErrorMessage", scope = GetCardNumberFromPersonIdResult.class)
	public JAXBElement<String> createGetCardNumberFromPersonIdResultErrorMessage(
			String value) {
		return new JAXBElement<String>(_RemoveCardRequestResultErrorMessage_QNAME,
				String.class, GetCardNumberFromPersonIdResult.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}
	 * {@link GetCardNumberFromPersonIdResult }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "GetCardNumberFromPersonIdResult", scope = GetCardNumberFromPersonIdResponse.class)
	public JAXBElement<GetCardNumberFromPersonIdResult> createGetCardNumberFromPersonIdResponseGetCardNumberFromPersonIdResult(
			GetCardNumberFromPersonIdResult value) {
		return new JAXBElement<GetCardNumberFromPersonIdResult>(
				_GetCardNumberFromPersonIdResponseGetCardNumberFromPersonIdResult_QNAME,
				GetCardNumberFromPersonIdResult.class,
				GetCardNumberFromPersonIdResponse.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "cardNumber", scope = GetPersonIdFromCardNumber.class)
	public JAXBElement<String> createGetPersonIdFromCardNumberCardNumber(
			String value) {
		return new JAXBElement<String>(_GetPersonIdFromCardNumberCardNumber_QNAME,
				String.class, GetPersonIdFromCardNumber.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "GenerateCardNumberResult", scope = GenerateCardNumberResponse.class)
	public JAXBElement<String> createGenerateCardNumberResponseGenerateCardNumberResult(
			String value) {
		return new JAXBElement<String>(
				_GenerateCardNumberResponseGenerateCardNumberResult_QNAME,
				String.class, GenerateCardNumberResponse.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}
	 * {@link GetPersonIdFromCardNumberResult }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "GetPersonIdFromCardNumberResult", scope = GetPersonIdFromCardNumberResponse.class)
	public JAXBElement<GetPersonIdFromCardNumberResult> createGetPersonIdFromCardNumberResponseGetPersonIdFromCardNumberResult(
			GetPersonIdFromCardNumberResult value) {
		return new JAXBElement<GetPersonIdFromCardNumberResult>(
				_GetPersonIdFromCardNumberResponseGetPersonIdFromCardNumberResult_QNAME,
				GetPersonIdFromCardNumberResult.class,
				GetPersonIdFromCardNumberResponse.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}
	 * {@link AddCardRequestResult }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "AddCardRequestFromPersonFormResult", scope = AddCardRequestFromPersonFormResponse.class)
	public JAXBElement<AddCardRequestResult> createAddCardRequestFromPersonFormResponseAddCardRequestFromPersonFormResult(
			AddCardRequestResult value) {
		return new JAXBElement<AddCardRequestResult>(
				_AddCardRequestFromPersonFormResponseAddCardRequestFromPersonFormResult_QNAME,
				AddCardRequestResult.class, AddCardRequestFromPersonFormResponse.class,
				value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement", name = "ErrorMessage", scope = GetPersonIdFromCardNumberResult.class)
	public JAXBElement<String> createGetPersonIdFromCardNumberResultErrorMessage(
			String value) {
		return new JAXBElement<String>(_RemoveCardRequestResultErrorMessage_QNAME,
				String.class, GetPersonIdFromCardNumberResult.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "RequestingUser", scope = AddCardRequestFromPersonForm.class)
	public JAXBElement<String> createAddCardRequestFromPersonFormRequestingUser(
			String value) {
		return new JAXBElement<String>(
				_AddCardRequestFromPersonFormRequestingUser_QNAME, String.class,
				AddCardRequestFromPersonForm.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "PersonId", scope = AddCardRequestFromPersonForm.class)
	public JAXBElement<String> createAddCardRequestFromPersonFormPersonId(
			String value) {
		return new JAXBElement<String>(_AddCardRequestFromPersonFormPersonId_QNAME,
				String.class, AddCardRequestFromPersonForm.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "TimeStamp", scope = AddCardRequestFromPersonForm.class)
	public JAXBElement<String> createAddCardRequestFromPersonFormTimeStamp(
			String value) {
		return new JAXBElement<String>(
				_AddCardRequestFromPersonFormTimeStamp_QNAME, String.class,
				AddCardRequestFromPersonForm.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "NewCardRequest", scope = AddCardRequestFromPersonForm.class)
	public JAXBElement<String> createAddCardRequestFromPersonFormNewCardRequest(
			String value) {
		return new JAXBElement<String>(
				_AddCardRequestFromPersonFormNewCardRequest_QNAME, String.class,
				AddCardRequestFromPersonForm.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "ractNumber", scope = GenerateCardNumber.class)
	public JAXBElement<String> createGenerateCardNumberRactNumber(String value) {
		return new JAXBElement<String>(_GenerateCardNumberRactNumber_QNAME,
				String.class, GenerateCardNumber.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "issuanceNumber", scope = GenerateCardNumber.class)
	public JAXBElement<String> createGenerateCardNumberIssuanceNumber(String value) {
		return new JAXBElement<String>(_GenerateCardNumberIssuanceNumber_QNAME,
				String.class, GenerateCardNumber.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "membershipNumber", scope = GenerateCardNumber.class)
	public JAXBElement<String> createGenerateCardNumberMembershipNumber(
			String value) {
		return new JAXBElement<String>(_GenerateCardNumberMembershipNumber_QNAME,
				String.class, GenerateCardNumber.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "productidentifier", scope = GenerateCardNumber.class)
	public JAXBElement<String> createGenerateCardNumberProductidentifier(
			String value) {
		return new JAXBElement<String>(_GenerateCardNumberProductidentifier_QNAME,
				String.class, GenerateCardNumber.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement", name = "CardNumber", scope = AddCardRequestResult.class)
	public JAXBElement<String> createAddCardRequestResultCardNumber(String value) {
		return new JAXBElement<String>(
				_GetCardNumberFromPersonIdResultCardNumber_QNAME, String.class,
				AddCardRequestResult.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement", name = "ErrorMessage", scope = AddCardRequestResult.class)
	public JAXBElement<String> createAddCardRequestResultErrorMessage(String value) {
		return new JAXBElement<String>(_RemoveCardRequestResultErrorMessage_QNAME,
				String.class, AddCardRequestResult.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "RoadsideId", scope = RemoveCardRequest.class)
	public JAXBElement<String> createRemoveCardRequestRoadsideId(String value) {
		return new JAXBElement<String>(_RemoveCardRequestRoadsideId_QNAME,
				String.class, RemoveCardRequest.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "PersonId", scope = AddCardRequest.class)
	public JAXBElement<String> createAddCardRequestPersonId(String value) {
		return new JAXBElement<String>(_AddCardRequestFromPersonFormPersonId_QNAME,
				String.class, AddCardRequest.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "ProductIdentifier", scope = AddCardRequest.class)
	public JAXBElement<String> createAddCardRequestProductIdentifier(String value) {
		return new JAXBElement<String>(_AddCardRequestProductIdentifier_QNAME,
				String.class, AddCardRequest.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "ReasonCode", scope = AddCardRequest.class)
	public JAXBElement<String> createAddCardRequestReasonCode(String value) {
		return new JAXBElement<String>(_AddCardRequestReasonCode_QNAME,
				String.class, AddCardRequest.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "CarRegistration", scope = AddCardRequest.class)
	public JAXBElement<String> createAddCardRequestCarRegistration(String value) {
		return new JAXBElement<String>(_AddCardRequestCarRegistration_QNAME,
				String.class, AddCardRequest.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}
	 * {@link AddCardRequestResult }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "AddCardRequestResult", scope = AddCardRequestResponse.class)
	public JAXBElement<AddCardRequestResult> createAddCardRequestResponseAddCardRequestResult(
			AddCardRequestResult value) {
		return new JAXBElement<AddCardRequestResult>(
				_AddCardRequestResponseAddCardRequestResult_QNAME,
				AddCardRequestResult.class, AddCardRequestResponse.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}
	 * {@link RemoveCardRequestResult }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://tempuri.org/", name = "RemoveCardRequestResult", scope = RemoveCardRequestResponse.class)
	public JAXBElement<RemoveCardRequestResult> createRemoveCardRequestResponseRemoveCardRequestResult(
			RemoveCardRequestResult value) {
		return new JAXBElement<RemoveCardRequestResult>(
				_RemoveCardRequestResponseRemoveCardRequestResult_QNAME,
				RemoveCardRequestResult.class, RemoveCardRequestResponse.class, value);
	}

}
