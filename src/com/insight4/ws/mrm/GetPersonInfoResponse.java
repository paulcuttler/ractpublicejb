package com.insight4.ws.mrm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetPersonInfoResult" type="{http://ecr.ract.com/getClient}Person" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getPersonInfoResult" })
@XmlRootElement(name = "GetPersonInfoResponse")
public class GetPersonInfoResponse {

	@XmlElement(name = "GetPersonInfoResult")
	protected Person getPersonInfoResult;

	/**
	 * Gets the value of the getPersonInfoResult property.
	 * 
	 * @return possible object is {@link Person }
	 * 
	 */
	public Person getGetPersonInfoResult() {
		return getPersonInfoResult;
	}

	/**
	 * Sets the value of the getPersonInfoResult property.
	 * 
	 * @param value
	 *          allowed object is {@link Person }
	 * 
	 */
	public void setGetPersonInfoResult(Person value) {
		this.getPersonInfoResult = value;
	}

}
