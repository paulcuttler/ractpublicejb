package com.insight4.ws.mrm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for Person complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="Person">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MobilePhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WorkPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HomePhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HomeAddress" type="{http://ecr.ract.com/getClient}Address" minOccurs="0"/>
 *         &lt;element name="PostalAddress" type="{http://ecr.ract.com/getClient}Address" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Person", propOrder = { "birthDate", "emailAddress", "gender",
		"firstName", "middleName", "lastName", "title", "mobilePhone", "workPhone",
		"homePhone", "homeAddress", "postalAddress" })
public class Person {

	@XmlElement(name = "BirthDate")
	protected String birthDate;
	@XmlElement(name = "EmailAddress")
	protected String emailAddress;
	@XmlElement(name = "Gender")
	protected String gender;
	@XmlElement(name = "FirstName")
	protected String firstName;
	@XmlElement(name = "MiddleName")
	protected String middleName;
	@XmlElement(name = "LastName")
	protected String lastName;
	@XmlElement(name = "Title")
	protected String title;
	@XmlElement(name = "MobilePhone")
	protected String mobilePhone;
	@XmlElement(name = "WorkPhone")
	protected String workPhone;
	@XmlElement(name = "HomePhone")
	protected String homePhone;
	@XmlElement(name = "HomeAddress")
	protected Address homeAddress;
	@XmlElement(name = "PostalAddress")
	protected Address postalAddress;

	/**
	 * Gets the value of the birthDate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBirthDate() {
		return birthDate;
	}

	/**
	 * Sets the value of the birthDate property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setBirthDate(String value) {
		this.birthDate = value;
	}

	/**
	 * Gets the value of the emailAddress property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * Sets the value of the emailAddress property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setEmailAddress(String value) {
		this.emailAddress = value;
	}

	/**
	 * Gets the value of the gender property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * Sets the value of the gender property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setGender(String value) {
		this.gender = value;
	}

	/**
	 * Gets the value of the firstName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the value of the firstName property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setFirstName(String value) {
		this.firstName = value;
	}

	/**
	 * Gets the value of the middleName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * Sets the value of the middleName property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setMiddleName(String value) {
		this.middleName = value;
	}

	/**
	 * Gets the value of the lastName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the value of the lastName property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setLastName(String value) {
		this.lastName = value;
	}

	/**
	 * Gets the value of the title property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the value of the title property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setTitle(String value) {
		this.title = value;
	}

	/**
	 * Gets the value of the mobilePhone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}

	/**
	 * Sets the value of the mobilePhone property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setMobilePhone(String value) {
		this.mobilePhone = value;
	}

	/**
	 * Gets the value of the workPhone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWorkPhone() {
		return workPhone;
	}

	/**
	 * Sets the value of the workPhone property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setWorkPhone(String value) {
		this.workPhone = value;
	}

	/**
	 * Gets the value of the homePhone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getHomePhone() {
		return homePhone;
	}

	/**
	 * Sets the value of the homePhone property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setHomePhone(String value) {
		this.homePhone = value;
	}

	/**
	 * Gets the value of the homeAddress property.
	 * 
	 * @return possible object is {@link Address }
	 * 
	 */
	public Address getHomeAddress() {
		return homeAddress;
	}

	/**
	 * Sets the value of the homeAddress property.
	 * 
	 * @param value
	 *          allowed object is {@link Address }
	 * 
	 */
	public void setHomeAddress(Address value) {
		this.homeAddress = value;
	}

	/**
	 * Gets the value of the postalAddress property.
	 * 
	 * @return possible object is {@link Address }
	 * 
	 */
	public Address getPostalAddress() {
		return postalAddress;
	}

	/**
	 * Sets the value of the postalAddress property.
	 * 
	 * @param value
	 *          allowed object is {@link Address }
	 * 
	 */
	public void setPostalAddress(Address value) {
		this.postalAddress = value;
	}

}
