package com.insight4.ws.mrm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for Address complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="Address">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PropertyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ausbar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Gnaf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Latitude" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Longitude" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DPID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StreetNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StreetName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Postcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Address", propOrder = { "propertyName", "ausbar", "gnaf",
		"latitude", "longitude", "dpid", "streetNumber", "streetName", "city",
		"state", "postcode", "country" })
public class Address {

	@XmlElement(name = "PropertyName")
	protected String propertyName;
	@XmlElement(name = "Ausbar")
	protected String ausbar;
	@XmlElement(name = "Gnaf")
	protected String gnaf;
	@XmlElement(name = "Latitude")
	protected String latitude;
	@XmlElement(name = "Longitude")
	protected String longitude;
	@XmlElement(name = "DPID")
	protected String dpid;
	@XmlElement(name = "StreetNumber")
	protected String streetNumber;
	@XmlElement(name = "StreetName")
	protected String streetName;
	@XmlElement(name = "City")
	protected String city;
	@XmlElement(name = "State")
	protected String state;
	@XmlElement(name = "Postcode")
	protected String postcode;
	@XmlElement(name = "Country")
	protected String country;

	/**
	 * Gets the value of the propertyName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPropertyName() {
		return propertyName;
	}

	/**
	 * Sets the value of the propertyName property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPropertyName(String value) {
		this.propertyName = value;
	}

	/**
	 * Gets the value of the ausbar property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAusbar() {
		return ausbar;
	}

	/**
	 * Sets the value of the ausbar property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setAusbar(String value) {
		this.ausbar = value;
	}

	/**
	 * Gets the value of the gnaf property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGnaf() {
		return gnaf;
	}

	/**
	 * Sets the value of the gnaf property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setGnaf(String value) {
		this.gnaf = value;
	}

	/**
	 * Gets the value of the latitude property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * Sets the value of the latitude property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setLatitude(String value) {
		this.latitude = value;
	}

	/**
	 * Gets the value of the longitude property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * Sets the value of the longitude property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setLongitude(String value) {
		this.longitude = value;
	}

	/**
	 * Gets the value of the dpid property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDPID() {
		return dpid;
	}

	/**
	 * Sets the value of the dpid property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setDPID(String value) {
		this.dpid = value;
	}

	/**
	 * Gets the value of the streetNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStreetNumber() {
		return streetNumber;
	}

	/**
	 * Sets the value of the streetNumber property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setStreetNumber(String value) {
		this.streetNumber = value;
	}

	/**
	 * Gets the value of the streetName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * Sets the value of the streetName property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setStreetName(String value) {
		this.streetName = value;
	}

	/**
	 * Gets the value of the city property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the value of the city property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCity(String value) {
		this.city = value;
	}

	/**
	 * Gets the value of the state property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the value of the state property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setState(String value) {
		this.state = value;
	}

	/**
	 * Gets the value of the postcode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPostcode() {
		return postcode;
	}

	/**
	 * Sets the value of the postcode property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPostcode(String value) {
		this.postcode = value;
	}

	/**
	 * Gets the value of the country property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the value of the country property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCountry(String value) {
		this.country = value;
	}

}
