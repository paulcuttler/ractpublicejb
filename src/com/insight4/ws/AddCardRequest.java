package com.insight4.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PersonId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimeAddressee" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="TierCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="StaffMember" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CarRegistration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProductIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NewCardRequest" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ReasonCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "personId", "primeAddressee", "tierCode",
		"staffMember", "carRegistration", "productIdentifier", "newCardRequest",
		"reasonCode" })
@XmlRootElement(name = "AddCardRequest")
public class AddCardRequest {

	@XmlElementRef(name = "PersonId", namespace = "http://tempuri.org/", type = JAXBElement.class)
	protected JAXBElement<String> personId;
	@XmlElement(name = "PrimeAddressee")
	protected Boolean primeAddressee;
	@XmlElement(name = "TierCode")
	protected Integer tierCode;
	@XmlElement(name = "StaffMember")
	protected Boolean staffMember;
	@XmlElementRef(name = "CarRegistration", namespace = "http://tempuri.org/", type = JAXBElement.class)
	protected JAXBElement<String> carRegistration;
	@XmlElementRef(name = "ProductIdentifier", namespace = "http://tempuri.org/", type = JAXBElement.class)
	protected JAXBElement<String> productIdentifier;
	@XmlElement(name = "NewCardRequest")
	protected Boolean newCardRequest;
	@XmlElementRef(name = "ReasonCode", namespace = "http://tempuri.org/", type = JAXBElement.class)
	protected JAXBElement<String> reasonCode;

	/**
	 * Gets the value of the personId property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}{@link String }
	 *         {@code >}
	 * 
	 */
	public JAXBElement<String> getPersonId() {
		return personId;
	}

	/**
	 * Sets the value of the personId property.
	 * 
	 * @param value
	 *          allowed object is {@link JAXBElement }{@code <}{@link String }
	 *          {@code >}
	 * 
	 */
	public void setPersonId(JAXBElement<String> value) {
		this.personId = ((JAXBElement<String>) value);
	}

	/**
	 * Gets the value of the primeAddressee property.
	 * 
	 * @return possible object is {@link Boolean }
	 * 
	 */
	public Boolean isPrimeAddressee() {
		return primeAddressee;
	}

	/**
	 * Sets the value of the primeAddressee property.
	 * 
	 * @param value
	 *          allowed object is {@link Boolean }
	 * 
	 */
	public void setPrimeAddressee(Boolean value) {
		this.primeAddressee = value;
	}

	/**
	 * Gets the value of the tierCode property.
	 * 
	 * @return possible object is {@link Integer }
	 * 
	 */
	public Integer getTierCode() {
		return tierCode;
	}

	/**
	 * Sets the value of the tierCode property.
	 * 
	 * @param value
	 *          allowed object is {@link Integer }
	 * 
	 */
	public void setTierCode(Integer value) {
		this.tierCode = value;
	}

	/**
	 * Gets the value of the staffMember property.
	 * 
	 * @return possible object is {@link Boolean }
	 * 
	 */
	public Boolean isStaffMember() {
		return staffMember;
	}

	/**
	 * Sets the value of the staffMember property.
	 * 
	 * @param value
	 *          allowed object is {@link Boolean }
	 * 
	 */
	public void setStaffMember(Boolean value) {
		this.staffMember = value;
	}

	/**
	 * Gets the value of the carRegistration property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}{@link String }
	 *         {@code >}
	 * 
	 */
	public JAXBElement<String> getCarRegistration() {
		return carRegistration;
	}

	/**
	 * Sets the value of the carRegistration property.
	 * 
	 * @param value
	 *          allowed object is {@link JAXBElement }{@code <}{@link String }
	 *          {@code >}
	 * 
	 */
	public void setCarRegistration(JAXBElement<String> value) {
		this.carRegistration = ((JAXBElement<String>) value);
	}

	/**
	 * Gets the value of the productIdentifier property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}{@link String }
	 *         {@code >}
	 * 
	 */
	public JAXBElement<String> getProductIdentifier() {
		return productIdentifier;
	}

	/**
	 * Sets the value of the productIdentifier property.
	 * 
	 * @param value
	 *          allowed object is {@link JAXBElement }{@code <}{@link String }
	 *          {@code >}
	 * 
	 */
	public void setProductIdentifier(JAXBElement<String> value) {
		this.productIdentifier = ((JAXBElement<String>) value);
	}

	/**
	 * Gets the value of the newCardRequest property.
	 * 
	 * @return possible object is {@link Boolean }
	 * 
	 */
	public Boolean isNewCardRequest() {
		return newCardRequest;
	}

	/**
	 * Sets the value of the newCardRequest property.
	 * 
	 * @param value
	 *          allowed object is {@link Boolean }
	 * 
	 */
	public void setNewCardRequest(Boolean value) {
		this.newCardRequest = value;
	}

	/**
	 * Gets the value of the reasonCode property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}{@link String }
	 *         {@code >}
	 * 
	 */
	public JAXBElement<String> getReasonCode() {
		return reasonCode;
	}

	/**
	 * Sets the value of the reasonCode property.
	 * 
	 * @param value
	 *          allowed object is {@link JAXBElement }{@code <}{@link String }
	 *          {@code >}
	 * 
	 */
	public void setReasonCode(JAXBElement<String> value) {
		this.reasonCode = ((JAXBElement<String>) value);
	}

}
