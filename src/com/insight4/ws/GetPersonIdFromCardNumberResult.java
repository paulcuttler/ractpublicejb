package com.insight4.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GetPersonIdFromCardNumberResult complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GetPersonIdFromCardNumberResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ErrorCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ErrorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PersonId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPersonIdFromCardNumberResult", namespace = "http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement", propOrder = {
		"errorCode", "errorMessage", "personId" })
public class GetPersonIdFromCardNumberResult {

	@XmlElement(name = "ErrorCode")
	protected Integer errorCode;
	@XmlElementRef(name = "ErrorMessage", namespace = "http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement", type = JAXBElement.class)
	protected JAXBElement<String> errorMessage;
	@XmlElement(name = "PersonId")
	protected Integer personId;

	/**
	 * Gets the value of the errorCode property.
	 * 
	 * @return possible object is {@link Integer }
	 * 
	 */
	public Integer getErrorCode() {
		return errorCode;
	}

	/**
	 * Sets the value of the errorCode property.
	 * 
	 * @param value
	 *          allowed object is {@link Integer }
	 * 
	 */
	public void setErrorCode(Integer value) {
		this.errorCode = value;
	}

	/**
	 * Gets the value of the errorMessage property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}{@link String }
	 *         {@code >}
	 * 
	 */
	public JAXBElement<String> getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Sets the value of the errorMessage property.
	 * 
	 * @param value
	 *          allowed object is {@link JAXBElement }{@code <}{@link String }
	 *          {@code >}
	 * 
	 */
	public void setErrorMessage(JAXBElement<String> value) {
		this.errorMessage = ((JAXBElement<String>) value);
	}

	/**
	 * Gets the value of the personId property.
	 * 
	 * @return possible object is {@link Integer }
	 * 
	 */
	public Integer getPersonId() {
		return personId;
	}

	/**
	 * Sets the value of the personId property.
	 * 
	 * @param value
	 *          allowed object is {@link Integer }
	 * 
	 */
	public void setPersonId(Integer value) {
		this.personId = value;
	}

}
