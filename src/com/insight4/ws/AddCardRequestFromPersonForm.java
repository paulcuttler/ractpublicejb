package com.insight4.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PersonId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NewCardRequest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RequestingUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TimeStamp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "personId", "newCardRequest",
		"requestingUser", "timeStamp" })
@XmlRootElement(name = "AddCardRequestFromPersonForm")
public class AddCardRequestFromPersonForm {

	@XmlElementRef(name = "PersonId", namespace = "http://tempuri.org/", type = JAXBElement.class)
	protected JAXBElement<String> personId;
	@XmlElementRef(name = "NewCardRequest", namespace = "http://tempuri.org/", type = JAXBElement.class)
	protected JAXBElement<String> newCardRequest;
	@XmlElementRef(name = "RequestingUser", namespace = "http://tempuri.org/", type = JAXBElement.class)
	protected JAXBElement<String> requestingUser;
	@XmlElementRef(name = "TimeStamp", namespace = "http://tempuri.org/", type = JAXBElement.class)
	protected JAXBElement<String> timeStamp;

	/**
	 * Gets the value of the personId property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}{@link String }
	 *         {@code >}
	 * 
	 */
	public JAXBElement<String> getPersonId() {
		return personId;
	}

	/**
	 * Sets the value of the personId property.
	 * 
	 * @param value
	 *          allowed object is {@link JAXBElement }{@code <}{@link String }
	 *          {@code >}
	 * 
	 */
	public void setPersonId(JAXBElement<String> value) {
		this.personId = ((JAXBElement<String>) value);
	}

	/**
	 * Gets the value of the newCardRequest property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}{@link String }
	 *         {@code >}
	 * 
	 */
	public JAXBElement<String> getNewCardRequest() {
		return newCardRequest;
	}

	/**
	 * Sets the value of the newCardRequest property.
	 * 
	 * @param value
	 *          allowed object is {@link JAXBElement }{@code <}{@link String }
	 *          {@code >}
	 * 
	 */
	public void setNewCardRequest(JAXBElement<String> value) {
		this.newCardRequest = ((JAXBElement<String>) value);
	}

	/**
	 * Gets the value of the requestingUser property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}{@link String }
	 *         {@code >}
	 * 
	 */
	public JAXBElement<String> getRequestingUser() {
		return requestingUser;
	}

	/**
	 * Sets the value of the requestingUser property.
	 * 
	 * @param value
	 *          allowed object is {@link JAXBElement }{@code <}{@link String }
	 *          {@code >}
	 * 
	 */
	public void setRequestingUser(JAXBElement<String> value) {
		this.requestingUser = ((JAXBElement<String>) value);
	}

	/**
	 * Gets the value of the timeStamp property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}{@link String }
	 *         {@code >}
	 * 
	 */
	public JAXBElement<String> getTimeStamp() {
		return timeStamp;
	}

	/**
	 * Sets the value of the timeStamp property.
	 * 
	 * @param value
	 *          allowed object is {@link JAXBElement }{@code <}{@link String }
	 *          {@code >}
	 * 
	 */
	public void setTimeStamp(JAXBElement<String> value) {
		this.timeStamp = ((JAXBElement<String>) value);
	}

}
