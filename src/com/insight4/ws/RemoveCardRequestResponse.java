package com.insight4.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RemoveCardRequestResult" type="{http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement}RemoveCardRequestResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "removeCardRequestResult" })
@XmlRootElement(name = "RemoveCardRequestResponse")
public class RemoveCardRequestResponse {

	@XmlElementRef(name = "RemoveCardRequestResult", namespace = "http://tempuri.org/", type = JAXBElement.class)
	protected JAXBElement<RemoveCardRequestResult> removeCardRequestResult;

	/**
	 * Gets the value of the removeCardRequestResult property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}
	 *         {@link RemoveCardRequestResult }{@code >}
	 * 
	 */
	public JAXBElement<RemoveCardRequestResult> getRemoveCardRequestResult() {
		return removeCardRequestResult;
	}

	/**
	 * Sets the value of the removeCardRequestResult property.
	 * 
	 * @param value
	 *          allowed object is {@link JAXBElement }{@code <}
	 *          {@link RemoveCardRequestResult }{@code >}
	 * 
	 */
	public void setRemoveCardRequestResult(
			JAXBElement<RemoveCardRequestResult> value) {
		this.removeCardRequestResult = ((JAXBElement<RemoveCardRequestResult>) value);
	}

}
