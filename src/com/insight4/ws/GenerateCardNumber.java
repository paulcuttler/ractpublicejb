package com.insight4.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ractNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="membershipNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="issuanceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="productidentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "ractNumber", "membershipNumber",
		"issuanceNumber", "productidentifier" })
@XmlRootElement(name = "GenerateCardNumber")
public class GenerateCardNumber {

	@XmlElementRef(name = "ractNumber", namespace = "http://tempuri.org/", type = JAXBElement.class)
	protected JAXBElement<String> ractNumber;
	@XmlElementRef(name = "membershipNumber", namespace = "http://tempuri.org/", type = JAXBElement.class)
	protected JAXBElement<String> membershipNumber;
	@XmlElementRef(name = "issuanceNumber", namespace = "http://tempuri.org/", type = JAXBElement.class)
	protected JAXBElement<String> issuanceNumber;
	@XmlElementRef(name = "productidentifier", namespace = "http://tempuri.org/", type = JAXBElement.class)
	protected JAXBElement<String> productidentifier;

	/**
	 * Gets the value of the ractNumber property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}{@link String }
	 *         {@code >}
	 * 
	 */
	public JAXBElement<String> getRactNumber() {
		return ractNumber;
	}

	/**
	 * Sets the value of the ractNumber property.
	 * 
	 * @param value
	 *          allowed object is {@link JAXBElement }{@code <}{@link String }
	 *          {@code >}
	 * 
	 */
	public void setRactNumber(JAXBElement<String> value) {
		this.ractNumber = ((JAXBElement<String>) value);
	}

	/**
	 * Gets the value of the membershipNumber property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}{@link String }
	 *         {@code >}
	 * 
	 */
	public JAXBElement<String> getMembershipNumber() {
		return membershipNumber;
	}

	/**
	 * Sets the value of the membershipNumber property.
	 * 
	 * @param value
	 *          allowed object is {@link JAXBElement }{@code <}{@link String }
	 *          {@code >}
	 * 
	 */
	public void setMembershipNumber(JAXBElement<String> value) {
		this.membershipNumber = ((JAXBElement<String>) value);
	}

	/**
	 * Gets the value of the issuanceNumber property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}{@link String }
	 *         {@code >}
	 * 
	 */
	public JAXBElement<String> getIssuanceNumber() {
		return issuanceNumber;
	}

	/**
	 * Sets the value of the issuanceNumber property.
	 * 
	 * @param value
	 *          allowed object is {@link JAXBElement }{@code <}{@link String }
	 *          {@code >}
	 * 
	 */
	public void setIssuanceNumber(JAXBElement<String> value) {
		this.issuanceNumber = ((JAXBElement<String>) value);
	}

	/**
	 * Gets the value of the productidentifier property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}{@link String }
	 *         {@code >}
	 * 
	 */
	public JAXBElement<String> getProductidentifier() {
		return productidentifier;
	}

	/**
	 * Sets the value of the productidentifier property.
	 * 
	 * @param value
	 *          allowed object is {@link JAXBElement }{@code <}{@link String }
	 *          {@code >}
	 * 
	 */
	public void setProductidentifier(JAXBElement<String> value) {
		this.productidentifier = ((JAXBElement<String>) value);
	}

}
