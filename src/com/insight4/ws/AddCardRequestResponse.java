package com.insight4.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddCardRequestResult" type="{http://schemas.datacontract.org/2004/07/Ract.Services.CardManagement}AddCardRequestResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "addCardRequestResult" })
@XmlRootElement(name = "AddCardRequestResponse")
public class AddCardRequestResponse {

	@XmlElementRef(name = "AddCardRequestResult", namespace = "http://tempuri.org/", type = JAXBElement.class)
	protected JAXBElement<AddCardRequestResult> addCardRequestResult;

	/**
	 * Gets the value of the addCardRequestResult property.
	 * 
	 * @return possible object is {@link JAXBElement }{@code <}
	 *         {@link AddCardRequestResult }{@code >}
	 * 
	 */
	public JAXBElement<AddCardRequestResult> getAddCardRequestResult() {
		return addCardRequestResult;
	}

	/**
	 * Sets the value of the addCardRequestResult property.
	 * 
	 * @param value
	 *          allowed object is {@link JAXBElement }{@code <}
	 *          {@link AddCardRequestResult }{@code >}
	 * 
	 */
	public void setAddCardRequestResult(JAXBElement<AddCardRequestResult> value) {
		this.addCardRequestResult = ((JAXBElement<AddCardRequestResult>) value);
	}

}
