package com.qas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for QAConfigType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="QAConfigType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IniFile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IniSection" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QAConfigType", propOrder = { "iniFile", "iniSection" })
public class QAConfigType {

	@XmlElement(name = "IniFile")
	protected String iniFile;
	@XmlElement(name = "IniSection")
	protected String iniSection;

	/**
	 * Gets the value of the iniFile property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIniFile() {
		return iniFile;
	}

	/**
	 * Sets the value of the iniFile property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setIniFile(String value) {
		this.iniFile = value;
	}

	/**
	 * Gets the value of the iniSection property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIniSection() {
		return iniSection;
	}

	/**
	 * Sets the value of the iniSection property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setIniSection(String value) {
		this.iniSection = value;
	}

}
