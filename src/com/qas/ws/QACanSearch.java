package com.qas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Country" type="{http://www.qas.com/web-2010-01}DataIDType"/>
 *         &lt;element name="Engine" type="{http://www.qas.com/web-2010-01}EngineType"/>
 *         &lt;element name="Layout" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QAConfig" type="{http://www.qas.com/web-2010-01}QAConfigType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Language" type="{http://www.qas.com/web-2010-01}LanguageIDType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "country", "engine", "layout", "qaConfig" })
@XmlRootElement(name = "QACanSearch")
public class QACanSearch {

	@XmlElement(name = "Country", required = true)
	protected String country;
	@XmlElement(name = "Engine", required = true)
	protected EngineType engine;
	@XmlElement(name = "Layout")
	protected String layout;
	@XmlElement(name = "QAConfig")
	protected QAConfigType qaConfig;
	@XmlAttribute(name = "Language")
	protected String language;

	/**
	 * Gets the value of the country property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the value of the country property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCountry(String value) {
		this.country = value;
	}

	/**
	 * Gets the value of the engine property.
	 * 
	 * @return possible object is {@link EngineType }
	 * 
	 */
	public EngineType getEngine() {
		return engine;
	}

	/**
	 * Sets the value of the engine property.
	 * 
	 * @param value
	 *          allowed object is {@link EngineType }
	 * 
	 */
	public void setEngine(EngineType value) {
		this.engine = value;
	}

	/**
	 * Gets the value of the layout property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLayout() {
		return layout;
	}

	/**
	 * Sets the value of the layout property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setLayout(String value) {
		this.layout = value;
	}

	/**
	 * Gets the value of the qaConfig property.
	 * 
	 * @return possible object is {@link QAConfigType }
	 * 
	 */
	public QAConfigType getQAConfig() {
		return qaConfig;
	}

	/**
	 * Sets the value of the qaConfig property.
	 * 
	 * @param value
	 *          allowed object is {@link QAConfigType }
	 * 
	 */
	public void setQAConfig(QAConfigType value) {
		this.qaConfig = value;
	}

	/**
	 * Gets the value of the language property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets the value of the language property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setLanguage(String value) {
		this.language = value;
	}

}
