package com.qas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Layout" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Moniker" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="QAConfig" type="{http://www.qas.com/web-2010-01}QAConfigType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Language" type="{http://www.qas.com/web-2010-01}LanguageIDType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "layout", "moniker", "qaConfig" })
@XmlRootElement(name = "QAGetAddress")
public class QAGetAddress {

	@XmlElement(name = "Layout", required = true)
	protected String layout;
	@XmlElement(name = "Moniker", required = true)
	protected String moniker;
	@XmlElement(name = "QAConfig")
	protected QAConfigType qaConfig;
	@XmlAttribute(name = "Language")
	protected String language;

	/**
	 * Gets the value of the layout property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLayout() {
		return layout;
	}

	/**
	 * Sets the value of the layout property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setLayout(String value) {
		this.layout = value;
	}

	/**
	 * Gets the value of the moniker property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMoniker() {
		return moniker;
	}

	/**
	 * Sets the value of the moniker property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setMoniker(String value) {
		this.moniker = value;
	}

	/**
	 * Gets the value of the qaConfig property.
	 * 
	 * @return possible object is {@link QAConfigType }
	 * 
	 */
	public QAConfigType getQAConfig() {
		return qaConfig;
	}

	/**
	 * Sets the value of the qaConfig property.
	 * 
	 * @param value
	 *          allowed object is {@link QAConfigType }
	 * 
	 */
	public void setQAConfig(QAConfigType value) {
		this.qaConfig = value;
	}

	/**
	 * Gets the value of the language property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets the value of the language property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setLanguage(String value) {
		this.language = value;
	}

}
