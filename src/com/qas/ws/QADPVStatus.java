package com.qas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QADPVLockDetails" type="{http://www.qas.com/web-2010-01}QADPVLockDetailsType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="DPVReturnedStatus" type="{http://www.qas.com/web-2010-01}DPVStatusType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "qadpvLockDetails" })
@XmlRootElement(name = "QADPVStatus")
public class QADPVStatus {

	@XmlElement(name = "QADPVLockDetails")
	protected QADPVLockDetailsType qadpvLockDetails;
	@XmlAttribute(name = "DPVReturnedStatus")
	protected DPVStatusType dpvReturnedStatus;

	/**
	 * Gets the value of the qadpvLockDetails property.
	 * 
	 * @return possible object is {@link QADPVLockDetailsType }
	 * 
	 */
	public QADPVLockDetailsType getQADPVLockDetails() {
		return qadpvLockDetails;
	}

	/**
	 * Sets the value of the qadpvLockDetails property.
	 * 
	 * @param value
	 *          allowed object is {@link QADPVLockDetailsType }
	 * 
	 */
	public void setQADPVLockDetails(QADPVLockDetailsType value) {
		this.qadpvLockDetails = value;
	}

	/**
	 * Gets the value of the dpvReturnedStatus property.
	 * 
	 * @return possible object is {@link DPVStatusType }
	 * 
	 */
	public DPVStatusType getDPVReturnedStatus() {
		return dpvReturnedStatus;
	}

	/**
	 * Sets the value of the dpvReturnedStatus property.
	 * 
	 * @param value
	 *          allowed object is {@link DPVStatusType }
	 * 
	 */
	public void setDPVReturnedStatus(DPVStatusType value) {
		this.dpvReturnedStatus = value;
	}

}
