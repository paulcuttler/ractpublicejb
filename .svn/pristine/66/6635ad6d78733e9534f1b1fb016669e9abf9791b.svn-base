package com.ract.purelink.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Java class for AddUpdateFindPartyRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddUpdateFindPartyRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="correspondanceAddress" type="{http://www.ract.com.au/RactPureLink}Address"/>
 *         &lt;element name="residentialAddress" type="{http://www.ract.com.au/RactPureLink}Address" minOccurs="0"/>
 *         &lt;element name="contact" type="{http://www.ract.com.au/RactPureLink}ContactList"/>
 *         &lt;element name="salutation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="forename" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="initials" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="surname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dateOfBirth" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="gender" type="{http://www.ract.com.au/RactPureLink}Gender"/>
 *         &lt;element name="ractId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddUpdateFindPartyRequest", propOrder = { "correspondanceAddress", "residentialAddress", "contact", "salutation", "title", "forename", "initials", "surname", "dateOfBirth", "gender", "ractId" })
public class AddUpdateFindPartyRequest {

    @XmlElement(required = true, nillable = true)
    protected Address correspondanceAddress;
    @XmlElementRef(name = "residentialAddress", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<Address> residentialAddress;
    @XmlElement(required = true, nillable = true)
    protected ContactList contact;
    @XmlElement(required = true, nillable = true)
    protected String salutation;
    @XmlElement(required = true, nillable = true)
    protected String title;
    @XmlElement(required = true, nillable = true)
    protected String forename;
    @XmlElement(required = true, nillable = true)
    protected String initials;
    @XmlElement(required = true, nillable = true)
    protected String surname;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateOfBirth;
    @XmlElement(required = true)
    protected Gender gender;
    @XmlElementRef(name = "ractId", namespace = "http://www.ract.com.au/RactPureLink", type = JAXBElement.class)
    protected JAXBElement<String> ractId;

    /**
     * Gets the value of the correspondanceAddress property.
     * 
     * @return possible object is {@link Address }
     * 
     */
    public Address getCorrespondanceAddress() {
        return correspondanceAddress;
    }

    /**
     * Sets the value of the correspondanceAddress property.
     * 
     * @param value
     *            allowed object is {@link Address }
     * 
     */
    public void setCorrespondanceAddress(Address value) {
        this.correspondanceAddress = value;
    }

    /**
     * Gets the value of the residentialAddress property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link Address }{@code >}
     * 
     */
    public JAXBElement<Address> getResidentialAddress() {
        return residentialAddress;
    }

    /**
     * Sets the value of the residentialAddress property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link Address }{@code >}
     * 
     */
    public void setResidentialAddress(JAXBElement<Address> value) {
        this.residentialAddress = ((JAXBElement<Address>) value);
    }

    /**
     * Gets the value of the contact property.
     * 
     * @return possible object is {@link ContactList }
     * 
     */
    public ContactList getContact() {
        return contact;
    }

    /**
     * Sets the value of the contact property.
     * 
     * @param value
     *            allowed object is {@link ContactList }
     * 
     */
    public void setContact(ContactList value) {
        this.contact = value;
    }

    /**
     * Gets the value of the salutation property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getSalutation() {
        return salutation;
    }

    /**
     * Sets the value of the salutation property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setSalutation(String value) {
        this.salutation = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the forename property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getForename() {
        return forename;
    }

    /**
     * Sets the value of the forename property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setForename(String value) {
        this.forename = value;
    }

    /**
     * Gets the value of the initials property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getInitials() {
        return initials;
    }

    /**
     * Sets the value of the initials property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setInitials(String value) {
        this.initials = value;
    }

    /**
     * Gets the value of the surname property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets the value of the surname property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setSurname(String value) {
        this.surname = value;
    }

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *            allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setDateOfBirth(XMLGregorianCalendar value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return possible object is {@link Gender }
     * 
     */
    public Gender getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *            allowed object is {@link Gender }
     * 
     */
    public void setGender(Gender value) {
        this.gender = value;
    }

    /**
     * Gets the value of the ractId property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public JAXBElement<String> getRactId() {
        return ractId;
    }

    /**
     * Sets the value of the ractId property.
     * 
     * @param value
     *            allowed object is {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     */
    public void setRactId(JAXBElement<String> value) {
        this.ractId = ((JAXBElement<String>) value);
    }

}
